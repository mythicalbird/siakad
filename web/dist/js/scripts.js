$.AdminLTESidebarTweak = {};

$.AdminLTESidebarTweak.options = {
    EnableRemember: true,
    NoTransitionAfterReload: false
    //Removes the transition after page reload.
};

$(function () {
    "use strict";

    $("body").on("collapsed.pushMenu", function(){
        if($.AdminLTESidebarTweak.options.EnableRemember){
            var toggleState = 'opened';
            if($("body").hasClass('sidebar-collapse')){
                toggleState = 'closed';
            }
            document.cookie = "toggleState="+toggleState;
        } 
    });

    if($.AdminLTESidebarTweak.options.EnableRemember){
        var re = new RegExp('toggleState' + "=([^;]+)");
        var value = re.exec(document.cookie);
        var toggleState = (value != null) ? unescape(value[1]) : null;
        if(toggleState == 'closed'){
            if($.AdminLTESidebarTweak.options.NoTransitionAfterReload){
                $("body").addClass('sidebar-collapse hold-transition').delay(100).queue(function(){
                    $(this).removeClass('hold-transition'); 
                });
            }else{
                $("body").addClass('sidebar-collapse');
            }
        }
    } 
});  
function Comma(Num) { //function to add commas to textboxes
        Num += '';
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        x = Num.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1))
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        return x1 + x2;
}

$("form").validator();
$('.js-datepicker').datepicker({ format: 'dd-mm-yyyy' })
    .on('changeDate', function(e) {
        // `e` here contains the extra attributes
        console.log(e);
    });

function openWindow(url) {
 window.open(url, "window_baru", "toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=auto,height=auto,left =304,top = 150.5");
}

$(function() {
	$('.drpick').daterangepicker({
		"autoApply": true,
		"locale": {
				"format": "DD-MM-YYYY",
				"separator": " s/d ",
				"applyLabel": "Simpan",
				"cancelLabel": "Batel",
				"fromLabel": "Mulai",
				"toLabel": "Sampai",
				"customRangeLabel": "Custom",
				"weekLabel": "W",
				"daysOfWeek": [
						"Mg",
						"Se",
						"Sl",
						"Rb",
						"Km",
						"Jm",
						"Sa"
				],
				"monthNames": [
						"Januari",
						"Februari",
						"Maret",
						"April",
						"Mei",
						"Juni",
						"Juli",
						"Agustus",
						"September",
						"Oktober",
						"November",
						"Desember"
				],
				"firstDay": 1
		},
		"showCustomRangeLabel": false,
		// "startDate": "03/02/2018",
		// "endDate": "03/08/2018",
		"drops": "up"
	}, function(start, end, label) {
	console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
	});
	$('.select2').select2();
});

$(".edit").on('click', function(e){
			e.preventDefault();
			var uri = $(this).data('url');
			$("#edit-form").load(uri);
			if (uri != 'undefined' && uri != '' && uri != '#') {
				$('#myModal').modal('show');
			}
	});
