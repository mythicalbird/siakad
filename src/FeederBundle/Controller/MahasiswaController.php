<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MahasiswaController extends Controller
{
    protected $appService;
    protected $feeder;
    protected $encoder;

    public function __construct(AppService $appService, FeederService $feeder, UserPasswordEncoderInterface $encoder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
      $this->encoder = $encoder;
    }

    /**
     * @Route("/feeder/mahasiswa_pt/ajax_import", name="feeder_mahasiswa_ajax_import")
     * @Method({"POST"})
     */
    public function importAction(Request $request) 
    { 
        $response = new JsonResponse();
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( $request->get('kode_prodi') );
        $tahun = $request->get('tahun');
        $dataFeeder = $this->feeder->ws( 'GetRecordset', array(
          'table'   => "mahasiswa_pt",
          'filter'  => "kode_prodi='".$prodi->getKodeProdi()."' AND mulai_smt ilike '".$tahun."%'",
          'limit'   => 1,
          'offset'  => $request->get('offset')
        ) );
        if( isset($dataFeeder['result'][0]) ) {
          $data = $dataFeeder['result'][0];

          $dataFeederPd = $this->feeder->ws( 'GetListMahasiswa', array(
            'filter'  => "id_reg_pd='".$data['id_reg_pd']."'"
          ) );
          if ( isset($dataFeederPd['result'][0]) ) {

            $data = array_merge($dataFeederPd['result'][0], $data);

            $dataFeederMhs = $this->feeder->ws( 'GetRecord', array(
              'table'   => "mahasiswa",
              'filter'  => "id_pd='".trim($data['id_pd'])."'"
            ) );

            if( count($dataFeederMhs['result']) > 0 ) {
              $data = array_merge($dataFeederMhs['result'], $data);
              $this->insertOrUpdateAction(trim($data['nipd']), $prodi, $data);
            }

          }
          $response->setData(array(
            'success'   => 1,
            'message'   => 'Mahasiswa: ' . $data['nm_pd'] . ' berhasil diimport'
          ));
        }
        sleep(1);
        return $response;
    }


    private function insertOrUpdateAction($nim, $prodi, $data = array()) 
    {
      $em = $this->getDoctrine()->getManager();
      foreach ($data as $key => $value) {
        $data[$key] = trim($value);
      }
      $now = new\DateTime();
      $mulai_smt = $data['mulai_smt'];
      $tahun = substr($mulai_smt, 0, 4);
      $kodeSemester = ( substr($mulai_smt, -1) == 1 ) ? 1 : 2;
      $em = $this->getDoctrine()->getManager();
      $ta = $this->appService->getTa($mulai_smt, true);
      $periode = $em->getRepository('AppBundle:PmbPeriode')
        ->findOneBy(array(
          'tahun' => $tahun,
          'prodi' => $prodi
        ));
      if ( !$periode ) {
        $periode = new \AppBundle\Entity\PmbPeriode();
        $periode->setTahun($tahun);
        $periode->setStatus(0);
        $periode->setProdi($prodi);
        $em->persist($periode);
        $em->flush();
      }

      $current_ta = $this->appService->getTahunAkademik();
      $ganjil = ( $kodeSemester == 1 ) ? true : false;
      $semester = $kodeSemester;

      for ($i=0; $i < ($current_ta->getTahun()-$tahun); $i++) { 
        $semester += 2;
      }

      /**
       * Periode = obj
       * Semester = int
       * semesterMasuk = obj
       * kelas = obj (optional)
       * pa = obj
       */

      $user = $this->getDoctrine()->getRepository('AppBundle:User')
        ->findOneByUsername($nim);
      if ( !$user ) {
        $user = new \AppBundle\Entity\User();
        $user->setUsername($nim);
        $encoded = $this->encoder->encodePassword($user, $nim);
        $user->setPassword($encoded);

        $user->setNama($data['nm_pd']);
        $user->setGelarDepan(null);
        $user->setGelarBelakang(null);
        $user->setTptLahir($data['tmpt_lahir']);
        if ( !empty($data['tgl_lahir']) ) {
          $user->setTglLahir( new\DateTime($data['tgl_lahir']) );
        }
        $user->setKtp(null);
        $user->setNoKk(null);
        $user->setNpwp(null);
        $user->setJk($data['jk']);
        $user->setAgama( $this->appService->getMasterTermObject('agama', $data['id_agama']) );        
        $user->setWargaNegara(null);
        $user->setStatusSipil(null);
        $user->setTelp(null);
        $user->setHp(null);
        $user->setEmail(null);
        $user->setAlamat(null);
        $user->setRt(null);
        $user->setRw(null);
        $user->setDusun(null);
        $user->setDesa(null);
        $user->setPos(null);
      }

      $user->setProdi($prodi);
      $user->setHakAkses( $this->appService->getMasterTermObject('hak_akses', 4) );
      if ( !empty($data['tgl_masuk_sp']) ) {
        $user->setTglMasuk( new\DateTime($data['tgl_masuk_sp']) );
      }
      if ( !empty($data['tgl_keluar']) ) {
        $user->setTglKeluar( new\DateTime($data['tgl_keluar']) );
      }
      $user->setUpdatedAt($now);
      $em->persist($user);
      $em->flush();

      if ( null !== $user->getDataMahasiswa() ) {
        $mhs = $user->getDataMahasiswa();
      } else {
        $mhs = new \AppBundle\Entity\Mahasiswa();
        // $mhs->setSemesterMasuk( $data['semesterMasuk'] );
        // $mhs->setKelas( $data['kelas'] );
        // $mhs->setPa( $data['pa'] );
        // $mhs->setIpk( $data['ipk'] );
        // $mhs->setSks( $data['sks'] );

        // if ( null !== $mhsArgs['jalurMasuk'] ) {
        //   $mhs->setJalurMasuk( $mhsArgs['jalurMasuk'] );
        // }

        // $mhs->setStatusMasuk( $mhsArgs['statusMasuk'] );
        // $mhs->setNoSeriIjazah( $mhsArgs['noSeriIjazah'] );

        // if ( null !== $mhsArgs['tglIjazah'] ) {
        //   $mhs->setTglIjazah( $mhsArgs['tglIjazah'] );
        // }

        // $mhs->setNoSkYudisium( $mhsArgs['noSkYudisium'] );

        // if ( null !== $mhsArgs['tglSkYudisium'] ) {
        //   $mhs->setTglSkYudisium( $mhsArgs['tglSkYudisium'] );
        // }

        // $mhs->setNilaiUsm( $mhsArgs['nilaiUsm'] );
        $mhs->setKodeSekolah( '' );
        $mhs->setNamaSekolah( '' );
        $mhs->setNis( $data['nisn'] );
        $mhs->setNilaiUn( '' );
        $mhs->setAsalKodePt( '' );
        $mhs->setAsalNamaPt( $data['nm_pt_asal'] );
        $mhs->setAsalJenjang( '' );
        $mhs->setAsalProdi( $data['nm_prodi_asal'] );
        $mhs->setAsalNim( $data['nisn'] );
        $mhs->setAsalSksDiakui( '' );
        // $mhs->setRiwayatPendidikan( $mhsArgs['riwayatPendidikan'] );
        // $msh->setBerkas( $mhsArgs['berkas'] );

        // $ayah =  array(
        //   'nama'          => $data['nm_ayah'],
        //   'nik'           => '',
        //   'agama'         => null,
        //   'pendidikan'    => $data['fk__jenjang_pendidikan_ayah'],
        //   'status'        => '',
        //   'alamat'        => '',
        //   'provinsi'      => null,
        //   'kota'          => null,
        //   'pos'           => '',
        //   'telp'          => '',
        //   'hp'            => '',
        //   'email'         => '',
        // );
        // if ( !empty($data['id_pekerjaan_ayah']) ) {
        //   $pekerjaanAyah = $this->appService->getMasterTermObject('pekerjaan', trim($data['id_pekerjaan_ayah']));
        //   $ayah['pekerjaan'] = $pekerjaanAyah;
        // }
        // if ( !empty($data['id_penghasilan_ayah']) ) {
        //   $penghasilanAyah = $this->appService->getMasterTermObject('penghasilan', trim($data['id_penghasilan_ayah']));
        //   $ayah['penghasilan'] = $penghasilanAyah;
        // }
        // $ibu =  array(
        //   'nama'          => $data['nm_ibu_kandung'],
        //   'nik'           => '',
        //   'agama'         => null,
        //   'pendidikan'    => $data['fk__jenjang_pendidikan_ayah'],
        //   'status'        => '',
        //   'alamat'        => '',
        //   'provinsi'      => null,
        //   'kota'          => null,
        //   'pos'           => '',
        //   'telp'          => '',
        //   'hp'            => '',
        //   'email'         => '',
        // );
        // if ( !empty($data['id_pekerjaan_ibu']) ) {
        //   $pekerjaanIbu = $this->appService->getMasterTermObject('pekerjaan', trim($data['id_pekerjaan_ibu']));
        //   $ibu['pekerjaan'] = $pekerjaanIbu;
        // }
        // if ( !empty($data['id_penghasilan_ibu']) ) {
        //   $penghasilanIbu = $this->appService->getMasterTermObject('penghasilan', trim($data['id_penghasilan_ibu']));
        //   $ibu['penghasilan'] = $penghasilanIbu;
        // }
        // $orangtua = array(
        //     'ayah'  => $ayah,
        //     'ibu'   => $ibu,
        // );
        // $mhs->setOrangtua( $orangtua );
      }
      // set dosen pembimbing
      $dataFeederPa = $this->feeder->ws( 'GetRecord', array(
        'table'   => "dosen_pembimbing",
        'filter'  => "id_reg_pd='".$data['id_reg_pd']."'"
      ) );
      if ( isset($dataFeederPa['result']) && count($dataFeederPa['result']) > 0 ) {
        $data_pa = $dataFeederPa['result'];
        foreach ($data_pa as $key => $value) {
          $data_pa[$key] = trim($value);
        }
        $pa = $this->getDoctrine()->getRepository('AppBundle:Dosen')
          ->findOneByUuid( $data_pa['id_sdm'] );
        if ( $pa ) {
          $mhs->setPa($pa);
        }
      }

      // $dataFeederPa = $this->feeder->ws( 'GetRecord', array(
      //   'table'   => "dosen_pembimbing",
      //   'filter'  => "id_reg_pd='".$data['id_reg_pd']."'"
      // ) );
      // if ( isset($dataFeederPa['result']) && count($dataFeederPa['result']) > 0 ) {
      //   $data_pa = $dataFeederPa['result'];
      //   foreach ($data_pa as $key => $value) {
      //     $data_pa[$key] = trim($value);
      //   }
      //   $dataFeederDosen = $this->feeder->ws( 'GetRecord', array(
      //     'table'   => "dosen",
      //     'filter'  => "id_sdm='".$data_pa['id_sdm']."'"
      //   ) );
      //   if ( isset($dataFeederDosen['result']) && count($dataFeederDosen['result']) > 0 ) {
      //     $data_dosen = $dataFeederDosen['result'];
      //     foreach ($data_dosen as $key => $value) {
      //       $data_dosen[$key] = trim($value);
      //     }
      //     $paUser = $this->getDoctrine()->getRepository('AppBundle:User')
      //       ->findOneByUsername( $data_dosen['nidn'] );
      //     if ( $paUser ) {
      //       if ( null !== $paUser->getDataDosen() ) {
      //         $mhs->setPa($paUser->getDataDosen());
      //       }
      //     }
      //   }
      // }
      
      $mhs->setMaba( 0 );
      $mhs->setTrashed( 0 );
      $mhs->setJenisDaftar( $this->appService->getMasterTermObject('jenis_pendaftaran', $data['id_jns_daftar']) );
      $mhs->setJenisKeluar( $this->appService->getMasterTermObject('jenis_keluar', $data['id_jns_keluar']) );
      $mhs->setStatus( $this->appService->getMasterTermObjectByName('status_mahasiswa', $data['nm_stat_mhs']) );
      $mhs->setAngkatan( $tahun );
      $mhs->setPeriode( $periode );
      $mhs->setUuid( $data['id_pd'] );
      $mhs->setRegPd( $data['id_reg_pd'] );
      $mhs->setRaw($data);
      $mhs->setSemester( $semester );
      $em->persist($mhs);
      $user->setDataMahasiswa($mhs);
      $em->persist($user);
      $em->flush();

      return;

    }

    /**
     * @Route("/feeder/mahasiswa/tester", name="tester_mahasiswa_get_list")
     */
    public function feederGetListMahasiswaSandbox()
    {
        $results = $this->getFeedData(63201, 2014);
        echo "<pre>";
        print_r($results);
        echo "</pre>";
        exit;
    }


    /**
     * @Route("/feeder/mahasiswa/tester2", name="tester_mahasiswa_get_list2")
     */
    public function feederGetListMahasiswaSandbox2()
    {
        $ret = array();
        $res = array();
        $results = $this->getFeedData(63201, 2014);

        foreach ($results as $data) {
          $nim = trim($data['nipd']);

          $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
            ->findOneByKodeProdi( 63201 );

          if ( ! $prodi ) {
            $ret['success'] = 0;
            $ret['message'] = '...';
          } else {
            $this->insertOrUpdateAction($nim, $prodi, $data);
            $ret['success'] = 1;
            $ret['message'] = 'Mahasiswa: ' . $data['nm_pd'] . ' berhasil diimport';
          }

          $res = $ret;
        }

        echo "<pre>";
        print_r($res);
        echo "</pre>";
        exit;
    }

}
