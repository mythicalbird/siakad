<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class NilaiKuliahKrsController extends Controller
{
    protected $appService;
    protected $feeder;
    protected $encoder;

    public function __construct(AppService $appService, FeederService $feeder, UserPasswordEncoderInterface $encoder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
      $this->encoder = $encoder;
    }

    /**
     * @Route("/feeder/nilai_krs/ajax_feed", name="feeder_nilai_krs_ajax_feed")
     * @Method({"POST"})
     */
    public function feedAction(Request $request) 
    {
        $response = new JsonResponse();
        $results = array();
        if ( !empty( $request->get('kode_prodi') ) ) {
          $results = $this->getFeedData( $request->get('kode_prodi') );
        }
        $response->setData($results);
        return $response;
    }

    private function getFeedData($kode_prodi, $results = array()) 
    {
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi($kode_prodi);

        $dataMakulKrs = $this->getDoctrine()->getRepository('AppBundle:Makul')
          ->findAll();
        foreach ($dataMakulKrs as $makul) {

          $dataNilaiFeeder = $this->feeder->ws( 'GetRecordset', array(
            'table'   => "nilai",
            'filter'  => "kode_mk='".$makul->getKode()."'",
            'limit'   => 1
          ) );

          if ( count($dataNilaiFeeder['result']) > 0 ) {
            
            $data = $dataNilaiFeeder['result'];
            
            for ( $i = 0; $i < count($data); $i++ ) {

              $results[] = $data[$i];

            }

          }

          // sleep(0.5);

        }

        return $results;
    }


    /**
     * @Route("/feeder/nilai_krs/ajax_import", name="feeder_nilai_krs_ajax_import")
     * @Method({"POST"})
     */
    public function importAction(Request $request) 
    { 
        // $data = $request->get('data');
        // $this->getFeedData( $request->get('kode_prodi') , $data );
        $response = new JsonResponse();
        $data = $request->get('data');
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( $request->get('kode_prodi') );
        $this->insertOrUpdateAction($prodi, $data);
        $response->setData(array(
          'success'     => 1,
          'message'     => 'Nilai Perkuliahan ' . $data['nipd'] . ' berhasil diimport'
        ));
        sleep(1);
        return $response;
    }



    private function insertOrUpdateAction($prodi, $data = array()) 
    {
      $em = $this->getDoctrine()->getManager();
      foreach ($data as $key => $value) {
        if (!is_array($value)) {
          $data[$key] = trim($value);
        }
      }
      
      $user = $em->getRepository('AppBundle:User')
        ->findOneBy(array(
          'username'  => $data['nipd'],
          'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4)
        ));

      if ( !$user ) {
        return;
      }

      if ( null === $user->getDataMahasiswa() ) {
        return;
      }

      $makulData = $em->getRepository('AppBundle:Makul')
        ->findOneByKode($data['kode_mk']);

      if ( !$makulData ) {
        return;
      }

      $ta = $this->appService->getTa($data['id_smt'], true);

      $kurikulum = $em->getRepository('AppBundle:Kurikulum')
        ->findOneBy(array(
          'prodi'   => $prodi,
          'kode'    => $data['id_smt']
        ));

      if ( !$kurikulum ) {
        return;
      }

      $makul = $em->getRepository('AppBundle:KurikulumMakul')
        ->findOneBy(array(
          'prodi'     => $prodi,
          'makul'     => $makulData,
          'kurikulum' => $kurikulum
        ));

      if ( !$makul ) {
        return;
      }

      $mhs = $user->getDataMahasiswa();

      $krs = $em->getRepository('AppBundle:Krs')
        ->findOneBy( array(
            'ta'        => $ta,
            'mahasiswa' => $mhs,
            'makul'     => $makul
        ) );

      if ( !$krs ) {
        $krs = new \AppBundle\Entity\Krs();
        $krs->setTa( $ta );
        $krs->setMahasiswa( $mhs );
        $krs->setSemester(null);
        $krs->setMakul( $makul );
        $krs->setStatus( '' );
        $krs->setNilaiHuruf( $data['nilai_huruf'] );
        $krs->setNilaiAngka( $data['nilai_angka'] );
        $krs->setNilaiAkhir( $data['nilai_indeks'] );
        $em->persist($krs);
        $em->flush();
      }
      return;
    }

    /**
     * @Route("/feeder/nilai_krs/tester", name="tester_nilai_krs_get_list")
     */
    public function feederGetListMakulSandbox()
    {
        $results = $this->getFeedData(63101);
        echo "<pre>";
        print_r($results);
        echo "</pre>";
        exit;
    }

    /**
     * @Route("/feeder/nilai_krs/tester2", name="tester_nilai_krs_get_list2")
     */
    public function feederGetListMakulSandbox2()
    {
        $ret = array();
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( 63201 );
        $results = $this->getFeedData(63201);
        foreach ( $results as $data) {
          $this->insertOrUpdateAction($prodi, $data);
          $ret['success'] = 1;
          $ret['message'] = 'Nilai Perkuliahan berhasil diimport';
        }
        echo "<pre>";
        print_r($ret);
        echo "</pre>";
        exit;
    }
}
