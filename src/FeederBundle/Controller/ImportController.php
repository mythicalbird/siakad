<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ImportController extends Controller
{
    protected $appService;
    protected $feeder;

    public function __construct(AppService $appService, FeederService $feeder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
    }

    /**
     * @Route("/feeder/get_count/recordset", name="feeder_get_count_recordset")
     * @Method({"POST"})
     */
    public function indexAction(Request $request) 
    {
				$count = 0;
				$kode_prodi = $request->get('kode_prodi');
        $table = $request->get('table');
        $tahun = $request->get('tahun');
				$count = $this->getRecordDataAction($kode_prodi, $table, $tahun);
        return new Response($count);
    }

    private function getRecordDataAction($kode_prodi, $table, $tahun)
    {
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( $kode_prodi );
        $params = array( 'table' => $table );
        if( $table == 'mahasiswa_pt' ) {
          $params['filter'] = "kode_prodi='".$prodi->getKodeProdi()."' AND mulai_smt ilike '".$tahun."%'";
        } elseif ( $table == 'kurikulum' ) {
          $params['filter'] = "id_sms='".$prodi->getUuid()."'";
        } elseif ( $table == 'mata_kuliah' ) {
          $params['filter'] = "kode_prodi='".$prodi->getKodeProdi()."'";
        }
        $dataFeederCount = $this->feeder->ws( 'GetCountRecordset', $params);
        if ( !isset($dataFeederCount['result']) ) {
          return 0;
        }
        return $dataFeederCount['result'];
    }

}
