<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DosenController extends Controller
{
    protected $appService;
    protected $feeder;
    protected $encoder;

    public function __construct(AppService $appService, FeederService $feeder, UserPasswordEncoderInterface $encoder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
      $this->encoder = $encoder;
    }

    /**
     * @Route("/feeder/dosen/ajax_import", name="feeder_dosen_ajax_import")
     * @Method({"POST"})
     */
    public function importAction(Request $request) 
    { 
        $response = new JsonResponse();
				///{"table":"dosen", "limit":"1","offset":"82"}
        $dataFeeder = $this->feeder->ws( 'GetRecordset', array(
					'table'		=> "dosen",
					'limit'		=> 1,
					'offset'	=> $request->get('offset')
				) );
				if( isset($dataFeeder['result'][0]) ) {
					$data = $dataFeeder['result'][0];
					$nidn = trim($data['nidn']);
					$this->insertOrUpdateAction($nidn, $data);
					$response->setData( array(
						'success'   => 1,
						'message'   => 'Dosen: ' . $data['nm_sdm'] . ' (' . $nidn . ')' . ' berhasil diimport'
					) );
				}
				sleep(1);
        return $response;
    }

    private function insertOrUpdateAction($nidn = '', $data = []) 
    {
      $em = $this->getDoctrine()->getManager();
      foreach ($data as $key => $value) {
        $data[$key] = trim($value);
      }
      $now = new\DateTime();
      $wilayah = $em->getRepository('AppBundle:Wilayah')
        ->findOneByKode( $data['id_wil'] );
      if ( ! $wilayah ) {
        $wilayah = $em->getRepository('AppBundle:Wilayah')
          ->findOneByKode( '999999' );
      }
      if ( empty($nidn) ) {
        $no_nidn = true;
        $user = true;
        while ( $user ) {
          $nidn = random_int(5, 10);
          $user = $em->getRepository('AppBundle:User')
            ->findOneByUsername($nidn);
        }
        $nidn = 'D' . $nidn;
      } else {
        $no_nidn = false;
      }
      $user = $em->getRepository('AppBundle:User')
        ->findOneByUsername($nidn);
      if ( !$user ) {
        $user = new \AppBundle\Entity\User();
        $user->setUsername($nidn);
        $encoded = $this->encoder->encodePassword($user, $nidn);
        $user->setPassword($encoded);
        $user->setGelarDepan('');
        $user->setGelarBelakang('');
        $user->setTptLahir($data['tmpt_lahir']);        
        $tgl_lahir = trim($data['tgl_lahir']);
        if ( !empty($tgl_lahir) ) {
          $tgl_lahir = new\DateTime($tgl_lahir);
          $user->setTglLahir( $tgl_lahir );
        }
        $user->setKtp($data['nik']);
        $user->setNoKk('');
        $user->setNpwp($data['npwp']);
        $user->setJk($data['jk']);
        $user->setAgama( $this->appService->getMasterTermObject('agama', $data['id_agama']) );
        $user->setWargaNegara( $data['kewarganegaraan'] );
        $user->setStatusSipil($data['stat_kawin']);
        $user->setTelp($data['no_tel_rmh']);
        $user->setHp($data['no_hp']);
        $user->setEmail($data['email']);
        $user->setProdi( $this->getUser()->getProdi() );
        $user->setAlamat($data['jln']);
        $user->setRt($data['rt']);
        $user->setRw($data['rw']);
        $user->setDusun($data['nm_dsn']);
        $user->setDesa($data['ds_kel']);
        $user->setWilayah($wilayah);
        $user->setPos($data['kode_pos']);
        $user->setHakAkses( $this->appService->getMasterTermObject('hak_akses', 3) );
        $user->setTglMasuk(null);
        $user->setTglKeluar(null);
        $user->setDataDosen(null);
      }
      $user->setNama($data['nm_sdm']);
      $user->setUpdatedAt($now);
      $em->persist($user);
      $em->flush();

      if ( null !== $user->getDataDosen() ) {
        $dosen = $user->getDataDosen();
      } else {
        $dosen = new \AppBundle\Entity\Dosen();
        $dosen->setNip($data['nip']);
        $dosen->setTglMulaiKerja(null);
        $dosen->setSemesterMulaiKerja(null);
        $dosen->setPendidikanTertinggi(null);
        $dosen->setInstitusiInduk(null);
        $dosen->setStatus($data['fk__stat_aktif']);
        $dosen->setStatusKerja($this->appService->getMasterTermObject('p.id_ikatan_kerja', $data['fk__stat_aktif']));
        $dosen->setJabatanAkademik(null);
        $dosen->setJabatanFungsional(null);
        $dosen->setJabatanStruktural(null);
        $dosen->setPangkatGolongan( $this->appService->getMasterTermObject('pangkat_golongan', $data['id_pangkat_gol']) );
        // $dosen->setPendidikan(null);
        $dosen->setAktif( ( $data['id_stat_aktif'] == 1 ) ? 1 : 0 );
        $dosen->setNamaIbuKandung($data['nm_ibu_kandung']);
      }
      if ( !$no_nidn ) {
        $dosen->setNidn($nidn);
      }
      $dosen->setRegPtk(null);
      $dosen->setUuid($data['id_sdm']);
      $dosen->setRaw($data);
      $em->persist($dosen);
      $em->flush();
      $user->setDataDosen($dosen);
      $em->persist($user);
      $em->flush();
    }

    /**
     * @Route("/feeder/dosen/tester", name="tester_dosen_get_list")
     */
    public function feederGetListDosenSandbox()
    {
        $results = $this->getRecordDataAction(63201, 'dosen');
        echo "<pre>";
        print_r($results);
        echo "</pre>";
        exit;
    }

    /**
     * @Route("/feeder/dosen/tester2", name="tester_dosen_get_list2")
     */
    public function feederGetListMakulSandbox2()
    {
        $count = $this->getRecordDataAction(63201, 'dosen');

        $ret = array();

        for ($i=0; $i < $count; $i++) { 

          $dataFeeder = $this->feeder->ws( 'GetRecordset', array(
            'table'   => "dosen",
            'limit'   => 1,
            'offset'  => $i
          ) );
          if( isset($dataFeeder['result'][0]) ) {
            
            $data = $dataFeeder['result'][0];
            $nidn = trim($data['nidn']);
            $this->insertOrUpdateAction($nidn, $data);

            $ret[] = array(
              'success'   => 1,
              'message'   => 'Dosen: ' . $data['nm_sdm'] . ' (' . $nidn . ')' . ' berhasil diimport'
            );

          }
        }

        echo "<pre>";
        print_r($ret);
        echo "</pre>";
        exit;
    }

    private function getRecordDataAction($kode_prodi, $table, $tahun = 0)
    {
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( $kode_prodi );
        $params = array( 'table' => $table );
        if( $table == 'mahasiswa_pt' ) {
          $params['filter'] = "kode_prodi='".$prodi->getKodeProdi()."' AND mulai_smt ilike '".$tahun."%'";
        } elseif ( $table == 'kurikulum' ) {
          $params['filter'] = "id_sms='".$prodi->getUuid()."'";
        } elseif ( $table == 'mata_kuliah' ) {
          $params['filter'] = "kode_prodi='".$prodi->getKodeProdi()."'";
        }
        $dataFeederCount = $this->feeder->ws( 'GetCountRecordset', $params);
        if ( !isset($dataFeederCount['result']) ) {
          return 0;
        }
        return $dataFeederCount['result'];
    }

}
