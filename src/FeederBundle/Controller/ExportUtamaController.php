<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;

class ExportUtamaController extends Controller
{
    protected $appService;
    protected $feeder;

    public function __construct(AppService $appService, FeederService $feeder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
    }

    /**
     * @Route("/feeder/export/utama/get_data", name="feeder_export_utama_get_data")
     * @Method({"POST"})
     */
    public function exportGetDataAjaxAction(Request $request) 
    {

      $results = array();
      $response = new JsonResponse();

      $table = $request->get('table');
      $kode_prodi = $request->get('kode_prodi');
      $tahun = $request->get('tahun');
      
      $results = $this->exportGetDataAction($table, $kode_prodi, $tahun);

      $response->setData($results);

      return $response;

    }

    /**
     * @Route("/feeder/export/utama/{table}", name="feeder_export_utama")
     * @Method({"POST"})
     */
    public function exportAjaxAction(Request $request, $table) 
    {
      $ret = array();
      $response = new JsonResponse();

      $ret['success'] = 0;
      $ret['message'] = 'gagal';

      $data = $request->get('data');


      if ( $table == 'mahasiswa' ) {
        
        $ret = $this->exportMahasiswaAction($data);

      } elseif ( $table == 'dosen' ) {
        
        $ret = $this->exportDosenAction($data);

      }

      $response->setData($ret);
      sleep(1);
      return $response;

    }

    private function exportGetDataAction($table, $kode_prodi, $tahun = 0) 
    {

      $results = array();

      $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
        ->findOneByKodeProdi( $kode_prodi );

      if ( ! $prodi ) {
        return $response;
      }

      if ( $table == 'mahasiswa' ) {

        $hakAkses = $this->appService->getMasterTermObject( 'hak_akses', 4 );
        $dataMahasiswaUser = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findBy(array(
            'prodi'     => $prodi,
            'hakAkses'  => $hakAkses
          ));

        foreach ($dataMahasiswaUser as $data) {

          if ( null !== $data->getDataMahasiswa() ) {

              $mahasiswa = $data->getDataMahasiswa();

              if ( null !== $mahasiswa->getAngkatan() && $mahasiswa->getAngkatan() == $tahun && $mahasiswa->getMaba() == 0 ) {

                  $orangtua = $this->appService->getOrangtuaMahasiswa( $data->getUsername() );
                  $ayah = $orangtua['ayah'];
                  $ibu = $orangtua['ibu'];

                  $data_mhs = array(
                    'id_pd'             => ( null !== $mahasiswa->getUuid() ) ? $mahasiswa->getUuid() : "",
                    'nm_pd'             => $data->getNama(),
                    'jk'                => strtoupper($data->getJk()),
                    'jln'               => $data->getAlamat(),
                    'rt'                => $data->getRt(),
                    'rw'                => $data->getRw(),
                    'nm_dsn'            => $data->getDusun(),
                    'ds_kel'            => $data->getDesa(),
                    'kode_pos'          => $data->getPos(),
                    'nisn'              => $mahasiswa->getNis(),
                    'nik'               => $data->getKtp(),
                    'tmpt_lahir'        => $data->getTptLahir(),
                    'tgl_lahir'         => ( null !== $data->getTglLahir() ) ? $data->getTglLahir()->format('Y-m-d') : null,
                    'nm_ayah'           => $ayah['nama'],
                    'tgl_lahir_ayah'    => null,
                    'nik_ayah'          => $ayah['nik'],
                    'id_jenjang_pendidikan_ayah'=> $ayah['fk_id_pendidikan'],
                    'id_pekerjaan_ayah' => $ayah['fk_id_pekerjaan'],
                    'id_penghasilan_ayah' => $ayah['fk_id_penghasilan'],
                    'nm_ibu_kandung'    => $ibu['nama'],
                    'tgl_lahir_ibu'     => null,
                    'nik_ibu'           => $ibu['nik'],
                    'id_jenjang_pendidikan_ibu' => $ibu['fk_id_pendidikan'],
                    'id_pekerjaan_ibu'  => $ibu['fk_id_pekerjaan'],
                    'id_penghasilan_ibu'=> $ibu['fk_id_penghasilan'],
                    'id_kk'             => ( '' != $data->getNoKk() ) ? $data->getNoKk() : 0,
                    'no_tel_rmh'        => $data->getTelp(),
                    'no_hp'             => $data->getHp(),
                    'email'             => $data->getEmail(),
                    'a_terima_kps'      => '',
                    'no_kps'            => '',
                    'npwp'              => $data->getNpwp(),
                    'id_wil'            => ( null !== $data->getWilayah() ) ? trim($data->getWilayah()->getKode()) : null,
                    // 'id_jns_tinggal'    => '',
                    'id_agama'          => ( null !== $data->getAgama() ) ? $data->getAgama()->getKode() : null,
                    'kewarganegaraan'   => 'ID',
                  );

                  $data_mhs_pt = array(
                    'id_reg_pd'           => ( null !== $mahasiswa->getRegPd() ) ? $mahasiswa->getRegPd() : '',
                    'id_pd'               => $mahasiswa->getUuid(),
                    'id_sp'               => $this->appService->getSetting('id_sp', true),
                    'id_sms'              => $prodi->getUuid(),
                    'id_prodi'            => $prodi->getKodeProdi(),
                    'nipd'                => $data->getUsername(),
                    'tgl_masuk_sp'        => ( null !== $data->getTglMasuk() ) ? $data->getTglMasuk()->format('Y-m-d') : null,
                    'tgl_keluar'          => ( null !== $data->getTglKeluar() ) ? $data->getTglKeluar()->format('Y-m-d') : null,
                    // 'skhun'               => '',
                    // 'no_peserta_ujian'    => '',
                    'no_seri_ijazah'      => '',
                    'a_pernah_paud'       => 0,
                    'a_pernah_tk'         => 0,
                    'tgl_create'          => date('Y-m-d'),
                    'mulai_smt'           => ( null !== $mahasiswa->getSemesterMasuk() ) ? $mahasiswa->getSemesterMasuk()->getKode() : null,
                    'sks_diakui'          => '',
                    'jalur_skripsi'       => '',
                    'judul_skripsi'       => '',
                    'bln_awal_bimbingan'  => '',
                    'bln_akhir_bimbingan' => '',
                    'sk_yudisium'         => $mahasiswa->getNoSkYudisium(),
                    'tgl_sk_yudisium'     => ( null !== $mahasiswa->getTglSkYudisium() ) ? $mahasiswa->getTglSkYudisium()->format('Y-m-d') : null,
                    'ipk'                 => $mahasiswa->getIpk(),
                    // 'sert_prof'           => '',
                    'a_pindah_mhs_asing'  => '',
                    'id_pt_asal'          => '',
                    'id_prodi_asal'       => '',
                    'id_jns_daftar'       => ( null !== $mahasiswa->getJenisDaftar() ) ? $mahasiswa->getJenisDaftar()->getKode() : null,
                    'id_jns_keluar'       => ( null !== $mahasiswa->getJenisKeluar() ) ? $mahasiswa->getJenisKeluar()->getKode() : null,
                    'id_jalur_masuk'      => ( null !== $mahasiswa->getJalurMasuk() ) ? $mahasiswa->getJalurMasuk()->getKode() : null,
                    'id_pembiayaan'       => ( null !== $mahasiswa->getPembiayaan() ) ? $mahasiswa->getPembiayaan()->getKode() : null,
                  );

                  $results[] = array(
                    'mhs'     => $data_mhs,
                    'mhs_pt'  => $data_mhs_pt
                  );

              }

          }

        }

      } 
      elseif ( $table == 'dosen' ) {

        $hakAkses = $this->appService->getMasterTermObject( 'hak_akses', 3 );
        $dataDosenUser = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findBy(array(
            'prodi'     => $prodi,
            'hakAkses'  => $hakAkses
          ));

        foreach ($dataDosenUser as $data) {

          if ( null !== $data->getDataDosen() ) {

              $dosen = $data->getDataDosen();
              $raw = $dosen->getRaw();

              $results[] = array(
                'id_sms'              => $prodi->getUuid(),
                'id_sdm'              => ( null !== $dosen->getUuid() ) ? $dosen->getUuid() : "",
                'nm_sdm'              => $data->getNama(),
                'jk'                  => $data->getJk(),
                'tmpt_lahir'          => ( !empty($data->getTptLahir()) ) ? $data->getTptLahir() : 'tidak diisi',
                'tgl_lahir'           => ( null !== $data->getTglLahir() ) ? $data->getTglLahir()->format('Y-m-d') : null,
                'nm_ibu_kandung'      => $dosen->getNamaIbuKandung(),
                'stat_kawin'          => $data->getStatusSipil(),
                'nik'                 => $data->getKtp(),
                'nip'                 => $dosen->getNip(),
                'niy_nigk'            => trim($raw['niy_nigk']),
                'nuptk'               => trim($raw['nuptk']),
                'nidn'                => $data->getUsername(),
                'nsdmi'               => '',
                'jln'                 => $data->getAlamat(),
                'rt'                  => $data->getRt(),
                'rw'                  => $data->getRw(),
                'nm_dsn'              => ( !empty($data->getDusun()) ) ? $data->getDusun() : 'tidak diisi',
                'ds_kel'              => ( !empty($data->getDesa()) ) ? $data->getDesa() : 'tidak diisi',
                'kode_pos'            => $data->getPos(),
                'nik'                 => $data->getKtp(),
                'no_tel_rmh'          => $data->getTelp(),
                'no_hp'               => $data->getHp(),
                'email'               => $data->getEmail(),
                'tmt_pns'             => trim($raw['tmt_pns']),
                'nm_suami_istri'      => trim($raw['nm_suami_istri']),
                'nip_suami_istri'     => trim($raw['nip_suami_istri']),
                'sk_cpns'             => trim($raw['sk_cpns']),
                'tgl_sk_cpns'         => trim($raw['tgl_sk_cpns']),
                'sk_angkat'           => trim($raw['sk_angkat']),
                'tmt_sk_angkat'       => trim($raw['tmt_sk_angkat']),
                'npwp'                => $data->getNpwp(),
                'nm_wp'               => $data->getNama(),
                'stat_data'           => trim($raw['stat_data']),
                'a_lisensi_kepsek'    => trim($raw['a_lisensi_kepsek']),
                'a_braille'           => trim($raw['a_braille']),
                'a_bhs_isyarat'       => ( '' != $data->getNoKk() ) ? $data->getNoKk() : 0,
                'jml_sekolah_binaan'  => trim($raw['jml_sekolah_binaan']),
                'a_diklat_awas'       => trim($raw['a_diklat_awas']),
                'akta_ijin_ajar'      => trim($raw['akta_ijin_ajar']),
                'nira'                => trim($raw['nira']),
                'kewarganegaraan'     => 'ID',
                'id_jns_sdm'          => 0,
                'id_wil'              => ( null !== $data->getWilayah() ) ? trim($data->getWilayah()->getKode()) : 999999,
                'id_stat_aktif'       => ( $dosen->getAktif() === 1 ) ? 1 : 0,
                'id_blob'             => '',
                'id_agama'            => ( null !== $data->getAgama() ) ? $data->getAgama()->getKode() : 99,
                'id_keahlian_lab'     => trim($raw['id_keahlian_lab']),
                'id_pekerjaan_suami_istri'  => trim($raw['id_pekerjaan_suami_istri']),
                'id_sumber_gaji'      => trim($raw['id_sumber_gaji']),
                'id_lemb_angkat'      => trim($raw['id_lemb_angkat']),
                'id_pangkat_gol'      => ( null !== $dosen->getPangkatGolongan() ) ? $dosen->getPangkatGolongan()->getKode() : null,
                'mampu_handle_kk'     => trim($raw['mampu_handle_kk']),
                'id_bid_pengawas'     => trim($raw['id_bid_pengawas']),
                
              );

          }

        }
        
      }

      return $results;

    }

    private function exportMahasiswaAction($data = array()) 
    {

      $ret = array();
      $result = array();

      $data_mhs = $data['mhs'];
      $data_mhs_pt = $data['mhs_pt'];

      $em = $this->getDoctrine()->getManager();
      $user = $em->getRepository('AppBundle:User')
        ->findOneByUsername($data_mhs_pt['nipd']);
      $mahasiswa = $user->getDataMahasiswa();

      /**
       * Record mahasiswa aja
       */
      $record_mhs = array(
        'nm_pd'             => $data_mhs['nm_pd'],
        'jk'                => $data_mhs['jk'],
        'jln'               => $data_mhs['jln'],
        'rt'                => $data_mhs['rt'],
        'rw'                => $data_mhs['rw'],
        'nm_dsn'            => $data_mhs['nm_dsn'],
        'ds_kel'            => $data_mhs['ds_kel'],
        'kode_pos'          => $data_mhs['kode_pos'],
        'nisn'              => $data_mhs['nisn'],
        'nik'               => $data_mhs['nik'],
        'tmpt_lahir'        => $data_mhs['tmpt_lahir'],
        'tgl_lahir'         => $data_mhs['tgl_lahir'],
        'nm_ayah'           => $data_mhs['nm_ayah'],
        'tgl_lahir_ayah'    => $data_mhs['tgl_lahir_ayah'],
        'nik_ayah'          => $data_mhs['nik_ayah'],
        'id_jenjang_pendidikan_ayah'=> $data_mhs['id_jenjang_pendidikan_ayah'],
        'id_pekerjaan_ayah' => $data_mhs['id_pekerjaan_ayah'],
        'id_penghasilan_ayah'       => $data_mhs['id_penghasilan_ayah'],
        'id_kebutuhan_khusus_ayah'  => 0,
        'nm_ibu_kandung'    => $data_mhs['nm_ibu_kandung'],
        'tgl_lahir_ibu'     => $data_mhs['tgl_lahir_ibu'],
        'nik_ibu'           => $data_mhs['nik_ibu'],
        'id_jenjang_pendidikan_ibu' => $data_mhs['id_jenjang_pendidikan_ibu'],
        'id_pekerjaan_ibu'  => $data_mhs['id_pekerjaan_ibu'],
        'id_penghasilan_ibu'=> $data_mhs['id_penghasilan_ibu'],
        'id_kebutuhan_khusus_ibu'  => 0,
        'id_kk'             => $data_mhs['id_kk'],
        'no_tel_rmh'        => '0819209381321', //$data_mhs['no_tel_rmh'],
        'no_hp'             => '0819209381321', //$data_mhs['no_hp'],
        'email'             => $data_mhs['email'],
        'a_terima_kps'      => 0, //$data_mhs['a_terima_kps'],
        'no_kps'            => '', //$data_mhs['no_kps'],
        'npwp'              => $data_mhs['npwp'],
        'id_wil'            => $data_mhs['id_wil'],
        // 'id_jns_tinggal'    => $data_mhs['id_jns_tinggal'],
        'id_agama'          => $data_mhs['id_agama'],
        'kewarganegaraan'   => $data_mhs['kewarganegaraan'],
      );

      if ( !empty($data_mhs['id_pd']) ) {

        $record = array(
          'key'     => array('id_pd' => $data_mhs['id_pd']),
          'data'    => $record_mhs
        );

        $feederMhs = $this->feeder->ws( 'GetRecord', array(
          'table'   => 'mahasiswa',
          'filter'  => "id_pd='".$data_mhs['id_pd']."'",
        ) );

        if ( count($feederMhs['result']) > 0 ) {
          
          //mode update
          $feed = $this->feeder->ws( 'UpdateRecord', array(
            'table' => 'mahasiswa',
            'data'  => json_encode($record)
          ));

          $message = 'Data mahasiswa: ' . $data_mhs['nm_pd'];
          if ( trim($feed['result']['error_desc']) != '' ) {
            $message .= ' [error: ' . $feed['result']['error_desc'] . ']';
          } else {
            $message .= ' berhasil dikirim...';
          }

          $ret['success'] = 1;
          $ret['message'] = $message;

          $result[] = $feed;

        } 


      }

      else {

        // mode insert
        $feed = $this->feeder->ws( 'InsertRecord', array(
          'table' => 'mahasiswa',
          'data'  => json_encode($record_mhs)
        ));

        if ( isset($feed['result']['id_pd']) ) {
          $id_pd = $feed['result']['id_pd'];
          $mahasiswa->setUuid( $id_pd );
          $data_mhs_pt['id_pd'] = $id_pd;
        }

        $message = 'Data mahasiswa: ' . $data_mhs['nm_pd'];
        if ( trim($feed['result']['error_desc']) != '' ) {
          $message .= ' [error: ' . $feed['result']['error_desc'] . ']';
        } else {
          $message .= ' berhasil dikirim...';
        }

        $ret['success'] = 1;
        $ret['message'] = $message;

        $result[] = $feed;

      }



      /**
       * Record mahasiswa_pt
       */
      $record_mhs_pt = array(
        'id_pd'               => $data_mhs_pt['id_pd'],
        'id_sp'               => $data_mhs_pt['id_sp'],
        'id_sms'              => $data_mhs_pt['id_sms'],
        'nipd'                => $data_mhs_pt['nipd'],
        'tgl_masuk_sp'        => $data_mhs_pt['tgl_masuk_sp'],
        'tgl_keluar'          => $data_mhs_pt['tgl_keluar'],
        // 'skhun'               => $data_mhs_pt['skhun'],
        // 'no_peserta_ujian'    => $data_mhs_pt['no_peserta_ujian'],
        // 'no_seri_ijazah'      => $data_mhs_pt['no_seri_ijazah'],
        'a_pernah_paud'       => $data_mhs_pt['a_pernah_paud'],
        'a_pernah_tk'         => $data_mhs_pt['a_pernah_tk'],
        'tgl_create'          => $data_mhs_pt['tgl_create'],
        'mulai_smt'           => $data_mhs_pt['mulai_smt'],
        // 'sks_diakui'          => $data_mhs_pt['sks_diakui'],
        // 'jalur_skripsi'       => $data_mhs_pt['jalur_skripsi'],
        // 'judul_skripsi'       => $data_mhs_pt['judul_skripsi'],
        'sk_yudisium'         => $data_mhs_pt['sk_yudisium'],
        'tgl_sk_yudisium'     => $data_mhs_pt['tgl_sk_yudisium'],
        'ipk'                 => $data_mhs_pt['ipk'],
        // 'a_pindah_mhs_asing'  => $data_mhs_pt['a_pindah_mhs_asing'],
        'id_pt_asal'          => $data_mhs_pt['id_pt_asal'],
        'id_prodi_asal'       => $data_mhs_pt['id_prodi_asal'],
        'id_jns_daftar'       => $data_mhs_pt['id_jns_daftar'],
        'id_jns_keluar'       => $data_mhs_pt['id_jns_keluar'],
        'id_jalur_masuk'      => $data_mhs_pt['id_jalur_masuk'],
        'id_pembiayaan'       => $data_mhs_pt['id_pembiayaan'],
      );

      if ( !empty($data_mhs_pt['id_reg_pd']) ) {

        $record = array(
          'key'     => array('id_reg_pd' => $data_mhs_pt['id_reg_pd']),
          'data'    => $record_mhs_pt
        );

        $feederMhsPt = $this->feeder->ws( 'GetRecord', array(
          'table'   => 'mahasiswa_pt',
          'filter'  => "id_reg_pd='".$data_mhs_pt['id_reg_pd']."'",
        ) );

        if ( count($feederMhsPt['result']) > 0 ) {
          
          //mode update
          $feed = $this->feeder->ws( 'UpdateRecord', array(
            'table' => 'mahasiswa_pt',
            'data'  => json_encode($record)
          ));

          $message = 'Data mahasiswa: ' . $data_mhs['nm_pd'];
          if ( trim($feed['result']['error_desc']) != '' ) {
            $message .= ' [error: ' . $feed['result']['error_desc'] . ']';
          } else {
            $message .= ' berhasil dikirim...';
          }

          $ret['success'] = 1;
          $ret['message'] = $message;

          $result[] = $feed;

        } 


      }

      else {

        // mode insert
        $feed = $this->feeder->ws( 'InsertRecord', array(
          'table' => 'mahasiswa_pt',
          'data'  => json_encode($record_mhs_pt)
        ));

        if ( isset($feed['result']['id_reg_pd']) ) {
          $id_reg_pd = $feed['result']['id_reg_pd'];
          $mahasiswa->setRegPd( $id_reg_pd );
        }

        $message = 'Data mahasiswa: ' . $data_mhs['nm_pd'];
        if ( trim($feed['result']['error_desc']) != '' ) {
          $message .= ' [error: ' . $feed['result']['error_desc'] . ']';
        } else {
          $message .= ' berhasil dikirim...';
        }

        $ret['success'] = 1;
        $ret['message'] = $message;

        $result[] = $feed;

      }

      $ret['result'] = $result;

      $em->persist($mahasiswa);
      $em->flush();

      return $ret;

    }

    private function exportDosenAction($data = array()) 
    {

      $ret = array();
      $result = array();

      $record_data = array(
        'nm_sdm'              => $data['nm_sdm'],
        'jk'                  => $data['jk'],
        'tmpt_lahir'          => $data['tmpt_lahir'],
        'tgl_lahir'           => $data['tgl_lahir'],
        'nm_ibu_kandung'      => 'tidak diisi', //$data['nm_ibu_kandung'],
        'stat_kawin'          => $data['stat_kawin'],
        'nik'                 => $data['nik'],
        'nip'                 => $data['nip'],
        'niy_nigk'            => $data['niy_nigk'],
        'nuptk'               => $data['nuptk'],
        'nidn'                => $data['nidn'],
        'nsdmi'               => $data['nsdmi'],
        'jln'                 => $data['jln'],
        'rt'                  => $data['rt'],
        'rw'                  => $data['rw'],
        'nm_dsn'              => $data['nm_dsn'],
        'ds_kel'              => $data['ds_kel'],
        'kode_pos'            => $data['kode_pos'],
        'nik'                 => $data['nik'],
        'no_tel_rmh'          => $data['no_tel_rmh'],
        'no_hp'               => $data['no_hp'],
        'email'               => $data['email'],
        'tmt_pns'             => $data['tmt_pns'],
        'nm_suami_istri'      => $data['nm_suami_istri'],
        'nip_suami_istri'     => $data['nip_suami_istri'],
        'sk_cpns'             => $data['sk_cpns'],
        'tgl_sk_cpns'         => $data['tgl_sk_cpns'],
        'sk_angkat'           => $data['sk_angkat'],
        'tmt_sk_angkat'       => $data['tmt_sk_angkat'],
        'npwp'                => $data['npwp'],
        'nm_wp'               => $data['nm_wp'],
        'stat_data'           => $data['stat_data'],
        'a_lisensi_kepsek'    => $data['a_lisensi_kepsek'],
        'a_braille'           => $data['a_braille'],
        'a_bhs_isyarat'       => $data['a_bhs_isyarat'],
        'jml_sekolah_binaan'  => $data['jml_sekolah_binaan'],
        'a_diklat_awas'       => $data['a_diklat_awas'],
        'akta_ijin_ajar'      => $data['akta_ijin_ajar'],
        'nira'                => $data['nira'],
        'kewarganegaraan'     => 'ID',
        'id_jns_sdm'          => $data['id_jns_sdm'],
        'id_wil'              => $data['id_wil'],
        'id_stat_aktif'       => $data['id_stat_aktif'],
        'id_blob'             => $data['id_blob'],
        'id_agama'            => $data['id_agama'],
        'id_keahlian_lab'     => $data['id_keahlian_lab'],
        'id_pekerjaan_suami_istri'  => $data['id_pekerjaan_suami_istri'],
        'id_sumber_gaji'      => $data['id_sumber_gaji'],
        'id_lemb_angkat'      => $data['id_lemb_angkat'],
        'id_pangkat_gol'      => $data['id_pangkat_gol'],
        'mampu_handle_kk'     => $data['mampu_handle_kk'],
        'id_bid_pengawas'     => $data['id_bid_pengawas'],
      );

      if ( !empty($data['id_sdm']) ) {

        $record = array(
          'key'     => array('id_sdm' => $data['id_sdm']),
          'data'    => $record_data
        );

        $feederDosen = $this->feeder->ws( 'GetRecord', array(
          'table'   => 'dosen',
          'filter'  => "id_sdm='".$data['id_sdm']."'",
        ) );

        if ( count($feederDosen['result']) > 0 ) {
          
          //mode update
          $feed = $this->feeder->ws( 'UpdateRecord', array(
            'table' => 'dosen',
            'data'  => json_encode($record)
          ));

          $message = 'Data dosen: ' . $data['nm_sdm'];
          if ( trim($feed['result']['error_desc']) != '' ) {
            $message .= ' [error: ' . $feed['result']['error_desc'] . ']';
          } else {
            $message .= ' berhasil dikirim...';
          }

          $ret['success'] = 1;
          $ret['message'] = $message;

          $result[] = $feed;

        } 


      }

      $ret['result'] = $result;

      // $em->persist($dosen);
      // $em->flush();

      return $ret;

    }

    /**
     * @Route("/feeder/export/utama/tester2", name="feeder_export_utama_tester2")
     */
    public function exportTester2Action(Request $request) 
    {

      $result = array();
      $dataAll = $this->exportGetDataAction('dosen', 63101, 2018);

      // echo "<pre>";
      // print_r($dataAll);
      // echo "</pre>";exit;

      foreach ($dataAll as $data) {
        $result[] = $this->exportDosenAction($data);
      }

      echo "<pre>";
      print_r($result);
      echo "</pre>";exit;

    }

    /**
     * @Route("/feeder/export/utama/tester", name="feeder_export_utama_tester")
     * @Method({"POST"})
     */
    public function exportTesterAction(Request $request) 
    {

      $results = array();
      $response = new JsonResponse();

      $table = $request->get('table');
      $kode_prodi = $request->get('kode_prodi');
      $tahun = $request->get('tahun');
      $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
        ->findOneByKodeProdi( $kode_prodi );

      if ( ! $prodi ) {
        return $response;
      }

      

      if ( null !== $data->getDataMahasiswa() ) {

          $mahasiswa = $data->getDataMahasiswa();

          if ( null !== $mahasiswa->getAngkatan() && $mahasiswa->getAngkatan() == $tahun ) {

              $data_mhs_pt = array(
                'id_reg_pd'           => '',
                'id_sp'               => '',
                'nipd'                => '',
                'tgl_masuk_sp'        => '',
                'tgl_keluar'          => '',
                'ket'                 => '',
                'skhun'               => '',
                'no_peserta_ujian'    => '',
                'no_seri_ijazah'      => '',
                'a_pernah_paud'       => '',
                'a_pernah_tk'         => '',
                'tgl_create'          => '',
                'mulai_smt'           => '',
                'sks_diakui'          => '',
                'jalur_skripsi'       => '',
                'judul_skripsi'       => '',
                'bln_awal_bimbingan'  => '',
                'bln_akhir_bimbingan' => '',
                'sk_yudisium'         => '',
                'tgl_sk_yudisium'     => '',
                'ipk'                 => '',
                'sert_prof'           => '',
                'a_pindah_mhs_asing'  => '',
                'id_pt_asal'          => '',
                'id_prodi_asal'       => '',
                'id_jns_daftar'       => '',
                'id_jns_keluar'       => '',
                'id_jalur_masuk'      => '',
                'id_pembiayaan'       => '',
              );

              $data_mhs = array(
                'id_pd'             => ( null !== $mahasiswa->getUuid() ) ? $mahasiswa->getUuid() : "",
                'id_sms'            => $prodi->getUuid(),
                'nm_pd'             => $data->getNama(),
                'jk'                => $data->getJk(),
                'jln'               => $data->getAlamat(),
                'rt'                => $data->getRt(),
                'rw'                => $data->getRw(),
                'nm_dsn'            => $data->getDusun(),
                'ds_kel'            => $data->getDesa(),
                'kode_pos'          => $data->getPos(),
                'nisn'              => '',
                'nik'               => $data->getKtp(),
                'tmpt_lahir'        => $data->getTptLahir(),
                'tgl_lahir'         => ( null !== $data->getTglLahir() ) ? $data->getTglLahir()->format('Y-m-d') : null,
                'nm_ayah'           => '',
                'tgl_lahir_ayah'    => '',
                'nik_ayah'          => '',
                'id_jenjang_pendidikan_ayah'=> '',
                'id_pekerjaan_ayah' => '',
                'id_penghasilan_ayah'       => '',
                'id_kebutuhan_khusus_ayah'  => '',
                'nm_ibu_kandung'    => '',
                'tgl_lahir_ibu'     => '',
                'nik_ibu'           => '',
                'id_jenjang_pendidikan_ibu' => '',
                'id_pekerjaan_ibu'  => '',
                'id_penghasilan_ibu'=> '',
                'id_kebutuhan_khusus_ibu'   => '',
                'nm_wali'           => '',
                'tgl_lahir_wali'    => '',
                'id_jenjang_pendidikan_wali'=> '',
                'id_pekerjaan_wali' => '',
                'id_penghasilan_wali'       => '',
                'id_kk'             => ( '' != $data->getNoKk() ) ? $data->getNoKk() : 0,
                'no_tel_rmh'        => $data->getTelp(),
                'no_hp'             => $data->getHp(),
                'email'             => $data->getEmail(),
                'a_terima_kps'      => '',
                'no_kps'            => '',
                'npwp'              => $data->getNpwp(),
                'id_wil'            => '',
                'id_jns_tinggal'    => '',
                'id_agama'          => ( null !== $data->getAgama() ) ? $data->getAgama()->getKode() : null,
                'id_alat_transport' => '',
                'kewarganegaraan'   => 'ID',
              );

              $results[] = array(
                'mhs'     => $data_mhs,
                'mhs_pt'  => $data_mhs_pt
              );

          }

      }

      $response->setData($results);

      return $response;

    }

}
