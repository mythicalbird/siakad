<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\DosenPendidikan;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use AppBundle\Form\Type\BerkasType;
use AppBundle\Form\Type\PmbOrangTuaType;

class DosenPenelitianFormType extends AbstractType
{
    protected $em;
  
    public function __construct(EntityManager $em) {
      $this->em = $em;
    }
  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $jenjangPendidikanMaster = $this->em->getRepository('AppBundle:Master')
          ->findByType('jenjang_pendidikan');
        $jenjang_choices = array();
        foreach ($jenjangPendidikanMaster as $choice) {
            $jenjang_choices[$choice->getNama()] = $choice->getNama();
        }
        $builder
          ->add('jenjang', ChoiceType::class, array(
              'label' => 'Jenjang Studi',
              'required'  => false,
              'choices' => $jenjang_choices
          ))
          ->add('bidangKeilmuan', null, array(
              'label' => 'Bidang Keilmuan',
              'required'  => false,
          ))
          ->add('gelar', null, array(
              'label' => 'Gelar Akademik',
              'required'  => false,
          ))
          ->add('perguruanTinggi', null, array(
              'label' => 'Perguruan Tinggi',
              'required'  => false,
          ))
          ->add('tglIjazah', null, array(
              'label' => 'Tanggal Ijazah',
              'required'  => false,
          ))
          ->add('tahun', null, array(
              'label' => 'Tahun',
              'required'  => false,
          ))
          ->add('sksLulus', null, array(
              'label' => 'SKS Lulus',
              'required'  => false,
          ))
          ->add('ipkAkhir', null, array(
              'label' => 'IPK Akhir',
              'required'  => false,
          ))
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class' => 'btn btn-primary'
              ),
          ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DosenPendidikan::class
        ]);
    }
}