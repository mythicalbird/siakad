<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Pegawai;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PegawaiFormType extends AbstractType
{
    protected $em;
  
    public function __construct(EntityManager $em) {
      $this->em = $em;
    }
  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kode', null, array(
                'label' => 'Kode Pegawai'
            ))
            ->add('nip', null, array(
              'label'         => 'NIP',
              'attr'          => array(
                'placeholder'   => '(jika ada)'
              )
            ))
            ->add('pangkat')
            ->add('statusPerkawinan', null, array(
                'label' => 'Status Perkawinan'
            ))
            ->add('jumlahAnak', null, array(
                'label' => 'Jumlah Anak'
            ))
            ->add('mulaiBekerja', null, array(
                'label' => 'Mulai Bekerja',
                'widget'=> 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                'format' => 'dd-mm-yyyy',
                // adds a class that can be selected in JavaScript
                'attr' => ['class' => 'js-datepicker'],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pegawai::class
        ]);
    }
}