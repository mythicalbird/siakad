<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\Master;
use AppBundle\Entity\Pmb;
use AppBundle\Entity\Mahasiswa;
use AppBundle\Entity\MahasiswaOrangtua;
use AppBundle\Entity\PmbPeriode;
use AppBundle\Entity\Upload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class OptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('imageFile', VichImageType::class, [
              'label' => false,
              'required' => false,
  //             'allow_delete' => true,
  //             'download_label' => '...',
  //             'download_uri' => true,
  //             'image_uri' => true,
  //             'imagine_pattern' => '...',
          ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Upload::class,
        ]);
    }
}