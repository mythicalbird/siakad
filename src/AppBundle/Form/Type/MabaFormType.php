<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\Master;
use AppBundle\Entity\Pmb;
use AppBundle\Entity\User;
use AppBundle\Entity\MahasiswaOrangtua;
use AppBundle\Entity\PmbPeriode;
use AppBundle\Entity\Kelas;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Mahasiswa;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use AppBundle\Form\Type\BerkasType;
use AppBundle\Form\Type\PmbOrangTuaType;
use AppBundle\Service\AppService;

class MabaFormType extends AbstractType
{
    protected $em;
    protected $appService;
  
    public function __construct(EntityManager $em, AppService $appService) {
      $this->em = $em;
      $this->appService = $appService;
    }
  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $asalNamaPtChoices = array();
        $asalJenjangChoices = array();
        $asalProdiChoices = array();
        $asalNamaPtObjects = $this->em->getRepository('AppBundle:Master')
          ->findByType('asal_perguruan_tinggi');
        if ( $asalNamaPtObjects ) {
          foreach ($asalNamaPtObjects as $asalNamaPt) {
            $asalNamaPtChoices[$asalNamaPt->getNama()] = $asalNamaPt->getNama();
          }
        }
        $asalJenjangObjects = $this->em->getRepository('AppBundle:Master')
          ->findByType('jenjang_pendidikan');
        if ( $asalJenjangObjects ) {
          foreach ($asalJenjangObjects as $asalJenjang) {
            $asalJenjangChoices[$asalJenjang->getNama()] = $asalJenjang->getNama();
          }
        }
        $asalProdiObjects = $this->em->getRepository('AppBundle:Master')
          ->findByType('asal_prodi');
        if ( $asalProdiObjects ) {
          foreach ($asalProdiObjects as $asalProdi) {
            $asalProdiChoices[$asalProdi->getNama()] = $asalProdi->getNama();
          }
        }
        $option = $this->appService->getGlobalOption();
        $ta = $option['ta'];
        $builder
          ->add('status', EntityType::class, array(
              'required'  => false,
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'status_mahasiswa');
              },
              'choice_label' => 'nama',
              'placeholder' => '-- Pilih --',
          ))
          ->add('angkatan', null, array(
              'required'    => false,
              'label'       => 'Tahun Masuk'
          ))
          // ->add('semesterMasuk', EntityType::class, array(
          //     'required'      => false,
          //     'class' => 'AppBundle:Semester',
          //     'query_builder' => function (EntityRepository $er) {
          //         return $er->createQueryBuilder('s');
          //     },
          //     'choice_label'  => 'nama',
          //     'placeholder'   => '-- Pilih --',
          // ))
          ->add('semester', ChoiceType::class, array(
              'label'   => 'Semester Sekarang',
              'choices'  => $this->appService->getSemesterChoices(),
              'placeholder'   => '-- Pilih --',
          ))
          ->add('kelas', EntityType::class, array(
              'required'      => false,
              'class'         => 'AppBundle:Kelas',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('k');
              },
              'choice_label'  => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          // ->add('pmb', PmbFormType::class) 

          ->add('riwayatPendidikan', CollectionType::class, array(
              'mapped'  => false
          ))

          ->add('kodeSekolah', null, array(
              'required'      => false,
              'label'         => 'Kode Sekolah',
          ))
          ->add('namaSekolah', null, array(
              'required'        => false,
              'label'          => 'Nama Sekolah',
          ))
          ->add('nis', null, array(
              'required'      => false,
              'label' => 'NIS',
          ))
          ->add('nilaiUn', null, array(
              'required'      => false,
              'label' => 'Nilai UN',
          ))
          ->add('asalKodePt', null, array(
              'required'      => false,
              'label' => 'Kode Perguruan Tinggi',
          ))
          ->add('asalNamaPt', ChoiceType::class, array(
              'required'      => false,
              'label'         => 'Nama Perguruan Tinggi',
              'choices'       => $asalNamaPtChoices,
              'placeholder' => '-- Pilih --',
          ))
          ->add('asalJenjang', ChoiceType::class, array(
              'required'      => false,
              'label' => 'Jenjang',
              'choices'       => $asalJenjangChoices,
              'placeholder' => '-- Pilih --',
          ))
          ->add('asalProdi', ChoiceType::class, array(
              'required'      => false,
              'label'         => 'Program Studi',
              'choices'       => $asalProdiChoices,
              'placeholder'   => '-- Pilih --',
          ))
          ->add('asalNim', null, array(
              'required'      => false,
              'label' => 'NIM Asal',
          ))
          ->add('asalSksDiakui', null, array(
              'required'      => false,
              'label' => 'SKS Diakui',
          )) 

          ->add(
              $builder->create('orangtua', FormType::class, array('by_reference' => true))
                  ->add('ayah', PmbOrangTuaType::class)
                  ->add('ibu', PmbOrangTuaType::class)
          )
        ;
        $masterBerkas = $this->em->createQueryBuilder()
                ->select('m')
                ->from('AppBundle:Master', 'm')
                ->where('m.type= :type')
                ->setParameter('type', 'berkas_pmb')
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $berkasField = $builder->create('berkas', FormType::class, array('by_reference' => true
              ));
        foreach( $masterBerkas as $berkas ) {
          $berkasField
              ->add($berkas['kode'], BerkasType::class);
        }
        $builder
          ->add($berkasField) 
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mahasiswa::class,
        ]);
    }
}