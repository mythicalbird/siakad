<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Dosen;
use AppBundle\Entity\Kelas;
use AppBundle\Entity\DosenPengampu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class DosenPengampuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('kelas', null, array(
              'label' => false,
              'attr'  => array(
                'readonly'  => 'readonly'
              )
          ))
          ->add('dosen', EntityType::class, array(
              'label' => false,
              'class' => 'AppBundle:Dosen',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('d');
              },
              'choice_label' => function(Dosen $entity = null) {
                  return ( null !== $entity->getUser() ) ? $entity->getUser()->getNama() : $entity->getId();
              },
              'placeholder' => '-- PILIH --',
              'required'  =>false
          ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DosenPengampu::class,
        ]);
    }
}