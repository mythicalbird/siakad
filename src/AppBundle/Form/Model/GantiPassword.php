<?php

namespace AppBundle\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

class GantiPassword
{
    /**
     * @SecurityAssert\UserPassword(
     *     message = "Password yang anda masukkan salah"
     * )
     */
     protected $oldPassword;

    /**
     * @Assert\Length(
     *     min = 6,
     *     minMessage = "Kata sandi baru minimal 6 karakter"
     * )
     */
     protected $newPassword;
}