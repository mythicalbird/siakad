<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * BatasSks
 *
 * @ORM\Table(name="pembayaran_biaya_kuliah")
 * @ORM\Entity
 */
class PembayaranBiayaKuliah
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="kode_tagihan", type="string", length=150, nullable=true)
     */
    private $kode;

    /**
     * @var string
     *
     * @ORM\Column(name="nama_tagihan", type="string", length=255)
     */
    private $nama;

    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi;

    /**
     * @ORM\Column(name="jumlah_gel_1", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $gelombang1;

    /**
     * @ORM\Column(name="jumlah_gel_2", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $gelombang2;


    /**
     * @ORM\Column(name="jumlah_gel_3", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $gelombang3;

    /**
     * @var string
     *
     * @ORM\Column(name="semester", type="string", length=100, nullable=true)
     */
    private $semester;

    /**
     * @ORM\Column(name="batas_pembayaran", type="date", nullable=true)
     */
    private $batasBayar;

    /**
     * @var string
     *
     * @ORM\Column(name="syarat_krs", type="string", length=100, nullable=true)
     */
    private $syaratKrs;

    /**
     * @ORM\OneToMany(targetEntity="PembayaranTransaksi", mappedBy="biayaKuliah")
     */
    private $transaksi;

    public function __construct() {
        $this->transaksi = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return BiayaKuliahRincian
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return BiayaKuliahRincian
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set gelombang1
     *
     * @param string $gelombang1
     *
     * @return BiayaKuliahRincian
     */
    public function setGelombang1($gelombang1)
    {
        $this->gelombang1 = $gelombang1;

        return $this;
    }

    /**
     * Get gelombang1
     *
     * @return string
     */
    public function getGelombang1()
    {
        return $this->gelombang1;
    }

    /**
     * Set gelombang2
     *
     * @param string $gelombang2
     *
     * @return BiayaKuliahRincian
     */
    public function setGelombang2($gelombang2)
    {
        $this->gelombang2 = $gelombang2;

        return $this;
    }

    /**
     * Get gelombang2
     *
     * @return string
     */
    public function getGelombang2()
    {
        return $this->gelombang2;
    }

    /**
     * Set gelombang3
     *
     * @param string $gelombang3
     *
     * @return BiayaKuliahRincian
     */
    public function setGelombang3($gelombang3)
    {
        $this->gelombang3 = $gelombang3;

        return $this;
    }

    /**
     * Get gelombang3
     *
     * @return string
     */
    public function getGelombang3()
    {
        return $this->gelombang3;
    }

    /**
     * Set semester
     *
     * @param string $semester
     *
     * @return BiayaKuliahRincian
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return string
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set batasBayar
     *
     * @param \DateTime $batasBayar
     *
     * @return BiayaKuliahRincian
     */
    public function setBatasBayar($batasBayar)
    {
        $this->batasBayar = $batasBayar;

        return $this;
    }

    /**
     * Get batasBayar
     *
     * @return \DateTime
     */
    public function getBatasBayar()
    {
        return $this->batasBayar;
    }

    /**
     * Set syaratKrs
     *
     * @param string $syaratKrs
     *
     * @return BiayaKuliahRincian
     */
    public function setSyaratKrs($syaratKrs)
    {
        $this->syaratKrs = $syaratKrs;

        return $this;
    }

    /**
     * Get syaratKrs
     *
     * @return string
     */
    public function getSyaratKrs()
    {
        return $this->syaratKrs;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return BiayaKuliahRincian
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }

    /**
     * Add transaksi
     *
     * @param \AppBundle\Entity\PembayaranTransaksi $transaksi
     *
     * @return PembayaranBiayaKuliah
     */
    public function addTransaksi(\AppBundle\Entity\PembayaranTransaksi $transaksi)
    {
        $this->transaksi[] = $transaksi;

        return $this;
    }

    /**
     * Remove transaksi
     *
     * @param \AppBundle\Entity\PembayaranTransaksi $transaksi
     */
    public function removeTransaksi(\AppBundle\Entity\PembayaranTransaksi $transaksi)
    {
        $this->transaksi->removeElement($transaksi);
    }

    /**
     * Get transaksi
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransaksi()
    {
        return $this->transaksi;
    }
}
