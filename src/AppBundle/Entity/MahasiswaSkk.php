<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MahasiswaSkk
 *
 * @ORM\Table(name="mahasiswa_skk")
 * @ORM\Entity
 */
class MahasiswaSkk
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Mahasiswa")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $mahasiswa;

    /**
     * @var string
     *
     * @ORM\Column(name="kegiatan", type="string", length=255)
     */
    private $kegiatan;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_skk", referencedColumnName="id")
     */
    private $kategori;

    /**
     * @var string
     *
     * @ORM\Column(name="uraian", type="text", nullable=true)
     */
    private $uraian;

    /**
     * @var string
     *
     * @ORM\Column(name="semester", type="string", nullable=true)
     */
    private $semester;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tanggal", type="date", nullable=true)
     */
    private $tanggal;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kegiatan
     *
     * @param string $kegiatan
     *
     * @return MahasiswaSkk
     */
    public function setKegiatan($kegiatan)
    {
        $this->kegiatan = $kegiatan;

        return $this;
    }

    /**
     * Get kegiatan
     *
     * @return string
     */
    public function getKegiatan()
    {
        return $this->kegiatan;
    }

    /**
     * Set uraian
     *
     * @param string $uraian
     *
     * @return MahasiswaSkk
     */
    public function setUraian($uraian)
    {
        $this->uraian = $uraian;

        return $this;
    }

    /**
     * Get uraian
     *
     * @return string
     */
    public function getUraian()
    {
        return $this->uraian;
    }

    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return MahasiswaSkk
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }

    /**
     * Set mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return MahasiswaSkk
     */
    public function setMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa = null)
    {
        $this->mahasiswa = $mahasiswa;

        return $this;
    }

    /**
     * Get mahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }

    /**
     * Set kategori
     *
     * @param \AppBundle\Entity\Master $kategori
     *
     * @return MahasiswaSkk
     */
    public function setKategori(\AppBundle\Entity\Master $kategori = null)
    {
        $this->kategori = $kategori;

        return $this;
    }

    /**
     * Get kategori
     *
     * @return \AppBundle\Entity\Master
     */
    public function getKategori()
    {
        return $this->kategori;
    }

    /**
     * Set semester
     *
     * @param string $semester
     *
     * @return MahasiswaSkk
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return string
     */
    public function getSemester()
    {
        return $this->semester;
    }
}
