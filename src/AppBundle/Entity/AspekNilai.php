<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="aspek_nilai")
 * @ORM\Entity
 */
class AspekNilai
{
    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_th_akademik", referencedColumnName="id")
     */
    private $tahun;

    /**
     * @var integer
     *
     * @ORM\Column(name="aspek_nilai", type="string", length=255)
     */
    private $nama;
  
    /**
     * @ORM\Column(name="persen", type="integer")
     */
    private $persen;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;




    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return AspekNilai
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set persen
     *
     * @param integer $persen
     *
     * @return AspekNilai
     */
    public function setPersen($persen)
    {
        $this->persen = $persen;

        return $this;
    }

    /**
     * Get persen
     *
     * @return integer
     */
    public function getPersen()
    {
        return $this->persen;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tahun
     *
     * @param \AppBundle\Entity\TahunAkademik $tahun
     *
     * @return AspekNilai
     */
    public function setTahun(\AppBundle\Entity\TahunAkademik $tahun = null)
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get tahun
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getTahun()
    {
        return $this->tahun;
    }
}
