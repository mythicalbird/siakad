<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kunjungan
 *
 * @ORM\Table(name="perpus_kunjungan")
 * @ORM\Entity
 */
class PerpusKunjungan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="barcode", type="string", length=255)
     */
    private $barcode;

    /**
     * @var string
     *
     * @ORM\Column(name="no_identitas", type="string", length=255, nullable=true)
     */
    private $noIdentitas;

    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255)
     */
    private $nama;

    /**
     * @var string
     *
     * @ORM\Column(name="jk", type="string", length=5)
     */
    private $jk;

    /**
     * @var string
     *
     * @ORM\Column(name="jenis", type="string", length=50)
     */
    private $jenis;

    /**
     * @var string
     *
     * @ORM\Column(name="alamat", type="text")
     */
    private $alamat;

    /**
     * @var string
     *
     * @ORM\Column(name="telp", type="string", length=20, nullable=true)
     */
    private $telp;

    /**
     * @var string
     *
     * @ORM\Column(name="tujuan", type="text")
     */
    private $tujuan;


    /**
     * @ORM\Column(name="tanggal", type="datetime", nullable=true)
     */
    private $tanggal;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set barcode
     *
     * @param string $barcode
     *
     * @return Kunjungan
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode
     *
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set noIdentitas
     *
     * @param string $noIdentitas
     *
     * @return Kunjungan
     */
    public function setNoIdentitas($noIdentitas)
    {
        $this->noIdentitas = $noIdentitas;

        return $this;
    }

    /**
     * Get noIdentitas
     *
     * @return string
     */
    public function getNoIdentitas()
    {
        return $this->noIdentitas;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return Kunjungan
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set jk
     *
     * @param string $jk
     *
     * @return Kunjungan
     */
    public function setJk($jk)
    {
        $this->jk = $jk;

        return $this;
    }

    /**
     * Get jk
     *
     * @return string
     */
    public function getJk()
    {
        return $this->jk;
    }

    /**
     * Set jenis
     *
     * @param string $jenis
     *
     * @return Kunjungan
     */
    public function setJenis($jenis)
    {
        $this->jenis = $jenis;

        return $this;
    }

    /**
     * Get jenis
     *
     * @return string
     */
    public function getJenis()
    {
        return $this->jenis;
    }

    /**
     * Set alamat
     *
     * @param string $alamat
     *
     * @return Kunjungan
     */
    public function setAlamat($alamat)
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Get alamat
     *
     * @return string
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * Set telp
     *
     * @param string $telp
     *
     * @return Kunjungan
     */
    public function setTelp($telp)
    {
        $this->telp = $telp;

        return $this;
    }

    /**
     * Get telp
     *
     * @return string
     */
    public function getTelp()
    {
        return $this->telp;
    }

    /**
     * Set tujuan
     *
     * @param string $tujuan
     *
     * @return Kunjungan
     */
    public function setTujuan($tujuan)
    {
        $this->tujuan = $tujuan;

        return $this;
    }

    /**
     * Get tujuan
     *
     * @return string
     */
    public function getTujuan()
    {
        return $this->tujuan;
    }

    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return Kunjungan
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }
}
