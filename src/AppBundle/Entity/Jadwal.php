<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jadwal
 *
 * @ORM\Table(name="jadwal")
 * @ORM\Entity
 */
class Jadwal
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_ta", referencedColumnName="id")
     */
    private $ta; // dari makul

    /**
     * @ORM\ManyToOne(targetEntity="Kelas")
     * @ORM\JoinColumn(name="id_kelas", referencedColumnName="id")
     */
    private $kelas;  //dari makul

    /**
     * @ORM\Column(name="semester", type="integer")
     */
    private $semester; // dari makul

    /**
     * @ORM\ManyToOne(targetEntity="KurikulumMakul")
     * @ORM\JoinColumn(name="id_kurikulum_makul", referencedColumnName="id")
     */
    private $makul;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_hari", referencedColumnName="id")
     */
    private $hari;

    /**
     * @ORM\ManyToOne(targetEntity="JamKuliah")
     * @ORM\JoinColumn(name="id_jam_kuliah", referencedColumnName="id")
     */
    private $jam;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set semester
     *
     * @param integer $semester
     *
     * @return Jadwal
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set ta
     *
     * @param \AppBundle\Entity\TahunAkademik $ta
     *
     * @return Jadwal
     */
    public function setTa(\AppBundle\Entity\TahunAkademik $ta = null)
    {
        $this->ta = $ta;

        return $this;
    }

    /**
     * Get ta
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getTa()
    {
        return $this->ta;
    }

    /**
     * Set kelas
     *
     * @param \AppBundle\Entity\Kelas $kelas
     *
     * @return Jadwal
     */
    public function setKelas(\AppBundle\Entity\Kelas $kelas = null)
    {
        $this->kelas = $kelas;

        return $this;
    }

    /**
     * Get kelas
     *
     * @return \AppBundle\Entity\Kelas
     */
    public function getKelas()
    {
        return $this->kelas;
    }

    /**
     * Set makul
     *
     * @param \AppBundle\Entity\KurikulumMakul $makul
     *
     * @return Jadwal
     */
    public function setMakul(\AppBundle\Entity\KurikulumMakul $makul = null)
    {
        $this->makul = $makul;

        return $this;
    }

    /**
     * Get makul
     *
     * @return \AppBundle\Entity\KurikulumMakul
     */
    public function getMakul()
    {
        return $this->makul;
    }

    /**
     * Set hari
     *
     * @param \AppBundle\Entity\Master $hari
     *
     * @return Jadwal
     */
    public function setHari(\AppBundle\Entity\Master $hari = null)
    {
        $this->hari = $hari;

        return $this;
    }

    /**
     * Get hari
     *
     * @return \AppBundle\Entity\Master
     */
    public function getHari()
    {
        return $this->hari;
    }

    /**
     * Set jam
     *
     * @param \AppBundle\Entity\JamKuliah $jam
     *
     * @return Jadwal
     */
    public function setJam(\AppBundle\Entity\JamKuliah $jam = null)
    {
        $this->jam = $jam;

        return $this;
    }

    /**
     * Get jam
     *
     * @return \AppBundle\Entity\JamKuliah
     */
    public function getJam()
    {
        return $this->jam;
    }
}
