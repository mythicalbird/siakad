<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Makul
 *
 * @ORM\Table(name="kurikulum_makul")
 * @ORM\Entity
 */
class KurikulumMakul
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi; // gak perlu / udah ada di kurikulum
	
    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_ta", referencedColumnName="id")
     */
    private $ta;  // gak perlu / udah ada di kurikulum
  
    /**
     * @ORM\ManyToOne(targetEntity="Kurikulum", inversedBy="makuls")
     * @ORM\JoinColumn(name="id_kurikulum", referencedColumnName="id")
     */
    private $kurikulum;

    /**
     * @ORM\ManyToOne(targetEntity="Makul")
     * @ORM\JoinColumn(name="id_makul", referencedColumnName="id")
     */
    private $makul;

    /**
     * @ORM\Column(name="semester", type="integer")
     */
    private $semester = 1;

    /**
     * @ORM\ManyToOne(targetEntity="Kelas", inversedBy="makulKurikulum")
     * @ORM\JoinColumn(name="id_kelas", referencedColumnName="id")
     */
    private $kelas;

    /**
     * @ORM\Column(name="uuid", type="guid", nullable=true)
     */
    private $uuid; // id_mk

    /**
     * @ORM\Column(name="status", type="string", length=100)
     */
    private $status = 'publish';

    /**
     * @ORM\OneToMany(targetEntity="DosenPengampu", mappedBy="makul")
     */
    private $dosenPengampu;

    /**
     * @ORM\OneToMany(targetEntity="Krs", mappedBy="makul")
     */
    private $krs;

    public function __construct() {
        $this->krs = new ArrayCollection();
        $this->dosenPengampu = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set semester
     *
     * @param integer $semester
     *
     * @return KurikulumMakul
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return KurikulumMakul
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return KurikulumMakul
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return KurikulumMakul
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }

    /**
     * Set kurikulum
     *
     * @param \AppBundle\Entity\Kurikulum $kurikulum
     *
     * @return KurikulumMakul
     */
    public function setKurikulum(\AppBundle\Entity\Kurikulum $kurikulum = null)
    {
        $this->kurikulum = $kurikulum;

        return $this;
    }

    /**
     * Get kurikulum
     *
     * @return \AppBundle\Entity\Kurikulum
     */
    public function getKurikulum()
    {
        return $this->kurikulum;
    }

    /**
     * Set makul
     *
     * @param \AppBundle\Entity\Makul $makul
     *
     * @return KurikulumMakul
     */
    public function setMakul(\AppBundle\Entity\Makul $makul = null)
    {
        $this->makul = $makul;

        return $this;
    }

    /**
     * Get makul
     *
     * @return \AppBundle\Entity\Makul
     */
    public function getMakul()
    {
        return $this->makul;
    }

    /**
     * Set kelas
     *
     * @param \AppBundle\Entity\Kelas $kelas
     *
     * @return KurikulumMakul
     */
    public function setKelas(\AppBundle\Entity\Kelas $kelas = null)
    {
        $this->kelas = $kelas;

        return $this;
    }

    /**
     * Get kelas
     *
     * @return \AppBundle\Entity\Kelas
     */
    public function getKelas()
    {
        return $this->kelas;
    }

    /**
     * Add dosenPengampu
     *
     * @param \AppBundle\Entity\DosenPengampu $dosenPengampu
     *
     * @return KurikulumMakul
     */
    public function addDosenPengampu(\AppBundle\Entity\DosenPengampu $dosenPengampu)
    {
        $this->dosenPengampu[] = $dosenPengampu;

        return $this;
    }

    /**
     * Remove dosenPengampu
     *
     * @param \AppBundle\Entity\DosenPengampu $dosenPengampu
     */
    public function removeDosenPengampu(\AppBundle\Entity\DosenPengampu $dosenPengampu)
    {
        $this->dosenPengampu->removeElement($dosenPengampu);
    }

    /**
     * Get dosenPengampu
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDosenPengampu()
    {
        return $this->dosenPengampu;
    }

    /**
     * Add kr
     *
     * @param \AppBundle\Entity\Krs $kr
     *
     * @return KurikulumMakul
     */
    public function addKr(\AppBundle\Entity\Krs $kr)
    {
        $this->krs[] = $kr;

        return $this;
    }

    /**
     * Remove kr
     *
     * @param \AppBundle\Entity\Krs $kr
     */
    public function removeKr(\AppBundle\Entity\Krs $kr)
    {
        $this->krs->removeElement($kr);
    }

    /**
     * Get krs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKrs()
    {
        return $this->krs;
    }

    /**
     * Set ta
     *
     * @param \AppBundle\Entity\TahunAkademik $ta
     *
     * @return KurikulumMakul
     */
    public function setTa(\AppBundle\Entity\TahunAkademik $ta = null)
    {
        $this->ta = $ta;

        return $this;
    }

    /**
     * Get ta
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getTa()
    {
        return $this->ta;
    }
}
