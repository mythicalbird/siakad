<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MahasiswaCuti
 *
 * @ORM\Table(name="mahasiswa_cuti")
 * @ORM\Entity
 */
class MahasiswaCuti
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Mahasiswa")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $mahasiswa;

    /**
     * @var string
     *
     * @ORM\Column(name="tgl_mulai", type="string", length=20, nullable=true)
     */
    private $tglMulai;

    /**
     * @var string
     *
     * @ORM\Column(name="tgl_akhir", type="string", length=20, nullable=true)
     */
    private $tglAkhir;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20, nullable=true)
     */
    private $status;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tglMulai
     *
     * @param string $tglMulai
     *
     * @return MahasiswaCuti
     */
    public function setTglMulai($tglMulai)
    {
        $this->tglMulai = $tglMulai;

        return $this;
    }

    /**
     * Get tglMulai
     *
     * @return string
     */
    public function getTglMulai()
    {
        return $this->tglMulai;
    }

    /**
     * Set tglAkhir
     *
     * @param string $tglAkhir
     *
     * @return MahasiswaCuti
     */
    public function setTglAkhir($tglAkhir)
    {
        $this->tglAkhir = $tglAkhir;

        return $this;
    }

    /**
     * Get tglAkhir
     *
     * @return string
     */
    public function getTglAkhir()
    {
        return $this->tglAkhir;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return MahasiswaCuti
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return MahasiswaCuti
     */
    public function setMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa = null)
    {
        $this->mahasiswa = $mahasiswa;

        return $this;
    }

    /**
     * Get mahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }
}
