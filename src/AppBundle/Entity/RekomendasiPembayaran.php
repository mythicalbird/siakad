<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RekomendasiPembayaran
 *
 * @ORM\Table(name="rekomendasi_pembayaran")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RekomendasiPembayaranRepository")
 */
class RekomendasiPembayaran
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nim", type="string", length=255)
     */
    private $nim;

    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255)
     */
    private $nama;

    /**
     * @var string
     *
     * @ORM\Column(name="prodi", type="string", length=255)
     */
    private $prodi;

    /**
     * @var string
     *
     * @ORM\Column(name="diskon", type="string", length=255)
     */
    private $diskon;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nim
     *
     * @param string $nim
     *
     * @return RekomendasiPembayaran
     */
    public function setNim($nim)
    {
        $this->nim = $nim;

        return $this;
    }

    /**
     * Get nim
     *
     * @return string
     */
    public function getNim()
    {
        return $this->nim;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return RekomendasiPembayaran
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set prodi
     *
     * @param string $prodi
     *
     * @return RekomendasiPembayaran
     */
    public function setProdi($prodi)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return string
     */
    public function getProdi()
    {
        return $this->prodi;
    }

    /**
     * Set diskon
     *
     * @param string $diskon
     *
     * @return RekomendasiPembayaran
     */
    public function setDiskon($diskon)
    {
        $this->diskon = $diskon;

        return $this;
    }

    /**
     * Get diskon
     *
     * @return string
     */
    public function getDiskon()
    {
        return $this->diskon;
    }
}

