<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DosenPengampu
 *
 * @ORM\Table(name="dosen_pengampu")
 * @ORM\Entity
 */
class DosenPengampu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="KurikulumMakul", inversedBy="dosenPengampu")
     * @ORM\JoinColumn(name="id_makul", referencedColumnName="id")
     */
    private $makul;

    /**
     * @ORM\ManyToOne(targetEntity="Dosen", inversedBy="makulDiampu")
     * @ORM\JoinColumn(name="id_dosen", referencedColumnName="id")
     */
    private $dosen; 

    /**
     * @ORM\Column(name="uuid", type="guid", nullable=true)
     */
    private $uuid; // id_ajar

    /**
     * @ORM\Column(name="status", type="string", length=100)
     */
    private $status = 'publish';

    /**
     * @ORM\Column(name="raw", type="array", nullable=true)
     */
    private $raw;





    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return DosenPengampu
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return DosenPengampu
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set raw
     *
     * @param array $raw
     *
     * @return DosenPengampu
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return array
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Set makul
     *
     * @param \AppBundle\Entity\KurikulumMakul $makul
     *
     * @return DosenPengampu
     */
    public function setMakul(\AppBundle\Entity\KurikulumMakul $makul = null)
    {
        $this->makul = $makul;

        return $this;
    }

    /**
     * Get makul
     *
     * @return \AppBundle\Entity\KurikulumMakul
     */
    public function getMakul()
    {
        return $this->makul;
    }

    /**
     * Set dosen
     *
     * @param \AppBundle\Entity\Dosen $dosen
     *
     * @return DosenPengampu
     */
    public function setDosen(\AppBundle\Entity\Dosen $dosen = null)
    {
        $this->dosen = $dosen;

        return $this;
    }

    /**
     * Get dosen
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getDosen()
    {
        return $this->dosen;
    }
}
