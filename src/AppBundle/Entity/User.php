<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Users
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255, nullable=true)
     */
    private $nama;

    /**
     * @var string
     *
     * @ORM\Column(name="gelar_depan", type="string", length=100, nullable=true)
     */
    private $gelarDepan;

    /**
     * @var string
     *
     * @ORM\Column(name="gelar_belakang", type="string", length=100, nullable=true)
     */
    private $gelarBelakang;

    /**
     * @var string
     *
     * @ORM\Column(name="tpt_lahir", type="string", length=150, nullable=true)
     */
    private $tptLahir;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_lahir", type="date", nullable=true)
     */
    private $tglLahir;

    /**
     * @var string
     *
     * @ORM\Column(name="ktp", type="string", length=150, nullable=true)
     */
    private $ktp;

    /**
     * @var string
     *
     * @ORM\Column(name="no_kk", type="string", length=255, nullable=true)
     */
    private $noKk; 

    /**
     * @var string
     *
     * @ORM\Column(name="npwp", type="string", length=255, nullable=true)
     */
    private $npwp;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=10, nullable=true)
     */
    private $jk;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_agama", referencedColumnName="id")
     */
    private $agama;

    /**
     * @var string
     *
     * @ORM\Column(name="warga_negara", type="string", length=255, nullable=true)
     */
    private $wargaNegara;

    /**
     * @var string
     *
     * @ORM\Column(name="status_sipil", type="string", length=255, nullable=true)
     */
    private $statusSipil;

    /**
     * @ORM\Column(name="status", type="string", length=100)
     */
    private $status = 'active';

    /**
     * @var string
     *
     * @ORM\Column(name="telpon", type="string", length=255, nullable=true)
     */
    private $telp;

    /**
     * @var string
     *
     * @ORM\Column(name="hp", type="string", length=255, nullable=true)
     */
    private $hp;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi; 

    /**
     * @var string
     *
     * @ORM\Column(name="alamat", type="text", nullable=true)
     */
    private $alamat;
  
    /**
     * @ORM\Column(name="rt", type="string", length=10, nullable=true)
     */
    private $rt;
  
    /**
     * @ORM\Column(name="rw", type="string", length=10, nullable=true)
     */
    private $rw;

    /**
     * @ORM\Column(name="dusun", type="string", length=255, nullable=true)
     */
    private $dusun;
  
    /**
     * @ORM\Column(name="desa", type="string", length=255, nullable=true)
     */
    private $desa;
  
    /**
     * @ORM\Column(name="kecamatan", type="string", length=255, nullable=true)
     */
    private $kecamatan;

    /**
     * @ORM\ManyToOne(targetEntity="Wilayah")
     * @ORM\JoinColumn(name="id_wilayah", referencedColumnName="id")
     */
    private $wilayah;

    /**
     * @ORM\Column(name="kode_pos", type="string", length=5, nullable=true)
     */
    private $pos;
  
    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_role", referencedColumnName="id")
     */
    private $hakAkses;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(name="tgl_masuk", type="date", nullable=true)
     */
    private $tglMasuk;

    /**
     * @ORM\Column(name="tgl_keluar", type="date", nullable=true)
     */
    private $tglKeluar;

    /**
     * @ORM\OneToOne(targetEntity="Mahasiswa", inversedBy="user")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $dataMahasiswa;

    /**
     * @ORM\OneToOne(targetEntity="Dosen", inversedBy="user")
     * @ORM\JoinColumn(name="id_dosen", referencedColumnName="id")
     */
    private $dataDosen;

    /**
     * @ORM\OneToOne(targetEntity="Pegawai", inversedBy="user")
     * @ORM\JoinColumn(name="id_pegawai", referencedColumnName="id")
     */
    private $dataPegawai;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="upload_image", fileNameProperty="imageName", size="imageSize")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Aktifitas", mappedBy="user")
     */
    private $aktifitas;

    public function __construct() {
        $this->aktifitas = new ArrayCollection();
    }


    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
        
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
        
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }
    
    /**
     * @param integer $imageSize
     *
     * @return Product
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;
        
        return $this;
    }

    /**
     * @return integer|null
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }



    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        $akses = ( null !== $this->getHakAkses() ) ? $this->getHakAkses()->getNama() : 'USER';
        $akses = 'ROLE_' . strtoupper(str_replace(' ', '_', $akses));
        $roles = array($akses);
        if ( $akses != 'ROLE_USER' ) {
            $roles[] = 'ROLE_USER';
        }
        return $roles;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }





    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return User
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set gelarDepan
     *
     * @param string $gelarDepan
     *
     * @return User
     */
    public function setGelarDepan($gelarDepan)
    {
        $this->gelarDepan = $gelarDepan;

        return $this;
    }

    /**
     * Get gelarDepan
     *
     * @return string
     */
    public function getGelarDepan()
    {
        return $this->gelarDepan;
    }

    /**
     * Set gelarBelakang
     *
     * @param string $gelarBelakang
     *
     * @return User
     */
    public function setGelarBelakang($gelarBelakang)
    {
        $this->gelarBelakang = $gelarBelakang;

        return $this;
    }

    /**
     * Get gelarBelakang
     *
     * @return string
     */
    public function getGelarBelakang()
    {
        return $this->gelarBelakang;
    }

    /**
     * Set tptLahir
     *
     * @param string $tptLahir
     *
     * @return User
     */
    public function setTptLahir($tptLahir)
    {
        $this->tptLahir = $tptLahir;

        return $this;
    }

    /**
     * Get tptLahir
     *
     * @return string
     */
    public function getTptLahir()
    {
        return $this->tptLahir;
    }

    /**
     * Set tglLahir
     *
     * @param \DateTime $tglLahir
     *
     * @return User
     */
    public function setTglLahir($tglLahir)
    {
        $this->tglLahir = $tglLahir;

        return $this;
    }

    /**
     * Get tglLahir
     *
     * @return \DateTime
     */
    public function getTglLahir()
    {
        return $this->tglLahir;
    }

    /**
     * Set ktp
     *
     * @param string $ktp
     *
     * @return User
     */
    public function setKtp($ktp)
    {
        $this->ktp = $ktp;

        return $this;
    }

    /**
     * Get ktp
     *
     * @return string
     */
    public function getKtp()
    {
        return $this->ktp;
    }

    /**
     * Set noKk
     *
     * @param string $noKk
     *
     * @return User
     */
    public function setNoKk($noKk)
    {
        $this->noKk = $noKk;

        return $this;
    }

    /**
     * Get noKk
     *
     * @return string
     */
    public function getNoKk()
    {
        return $this->noKk;
    }

    /**
     * Set npwp
     *
     * @param string $npwp
     *
     * @return User
     */
    public function setNpwp($npwp)
    {
        $this->npwp = $npwp;

        return $this;
    }

    /**
     * Get npwp
     *
     * @return string
     */
    public function getNpwp()
    {
        return $this->npwp;
    }

    /**
     * Set jk
     *
     * @param string $jk
     *
     * @return User
     */
    public function setJk($jk)
    {
        $this->jk = $jk;

        return $this;
    }

    /**
     * Get jk
     *
     * @return string
     */
    public function getJk()
    {
        return $this->jk;
    }

    /**
     * Set wargaNegara
     *
     * @param string $wargaNegara
     *
     * @return User
     */
    public function setWargaNegara($wargaNegara)
    {
        $this->wargaNegara = $wargaNegara;

        return $this;
    }

    /**
     * Get wargaNegara
     *
     * @return string
     */
    public function getWargaNegara()
    {
        return $this->wargaNegara;
    }

    /**
     * Set statusSipil
     *
     * @param string $statusSipil
     *
     * @return User
     */
    public function setStatusSipil($statusSipil)
    {
        $this->statusSipil = $statusSipil;

        return $this;
    }

    /**
     * Get statusSipil
     *
     * @return string
     */
    public function getStatusSipil()
    {
        return $this->statusSipil;
    }

    /**
     * Set telp
     *
     * @param string $telp
     *
     * @return User
     */
    public function setTelp($telp)
    {
        $this->telp = $telp;

        return $this;
    }

    /**
     * Get telp
     *
     * @return string
     */
    public function getTelp()
    {
        return $this->telp;
    }

    /**
     * Set hp
     *
     * @param string $hp
     *
     * @return User
     */
    public function setHp($hp)
    {
        $this->hp = $hp;

        return $this;
    }

    /**
     * Get hp
     *
     * @return string
     */
    public function getHp()
    {
        return $this->hp;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set alamat
     *
     * @param string $alamat
     *
     * @return User
     */
    public function setAlamat($alamat)
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Get alamat
     *
     * @return string
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * Set rt
     *
     * @param string $rt
     *
     * @return User
     */
    public function setRt($rt)
    {
        $this->rt = $rt;

        return $this;
    }

    /**
     * Get rt
     *
     * @return string
     */
    public function getRt()
    {
        return $this->rt;
    }

    /**
     * Set rw
     *
     * @param string $rw
     *
     * @return User
     */
    public function setRw($rw)
    {
        $this->rw = $rw;

        return $this;
    }

    /**
     * Get rw
     *
     * @return string
     */
    public function getRw()
    {
        return $this->rw;
    }

    /**
     * Set dusun
     *
     * @param string $dusun
     *
     * @return User
     */
    public function setDusun($dusun)
    {
        $this->dusun = $dusun;

        return $this;
    }

    /**
     * Get dusun
     *
     * @return string
     */
    public function getDusun()
    {
        return $this->dusun;
    }

    /**
     * Set desa
     *
     * @param string $desa
     *
     * @return User
     */
    public function setDesa($desa)
    {
        $this->desa = $desa;

        return $this;
    }

    /**
     * Get desa
     *
     * @return string
     */
    public function getDesa()
    {
        return $this->desa;
    }

    /**
     * Set kecamatan
     *
     * @param string $kecamatan
     *
     * @return User
     */
    public function setKecamatan($kecamatan)
    {
        $this->kecamatan = $kecamatan;

        return $this;
    }

    /**
     * Get kecamatan
     *
     * @return string
     */
    public function getKecamatan()
    {
        return $this->kecamatan;
    }

    /**
     * Set pos
     *
     * @param string $pos
     *
     * @return User
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return string
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set tglMasuk
     *
     * @param \DateTime $tglMasuk
     *
     * @return User
     */
    public function setTglMasuk($tglMasuk)
    {
        $this->tglMasuk = $tglMasuk;

        return $this;
    }

    /**
     * Get tglMasuk
     *
     * @return \DateTime
     */
    public function getTglMasuk()
    {
        return $this->tglMasuk;
    }

    /**
     * Set tglKeluar
     *
     * @param \DateTime $tglKeluar
     *
     * @return User
     */
    public function setTglKeluar($tglKeluar)
    {
        $this->tglKeluar = $tglKeluar;

        return $this;
    }

    /**
     * Get tglKeluar
     *
     * @return \DateTime
     */
    public function getTglKeluar()
    {
        return $this->tglKeluar;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set agama
     *
     * @param \AppBundle\Entity\Master $agama
     *
     * @return User
     */
    public function setAgama(\AppBundle\Entity\Master $agama = null)
    {
        $this->agama = $agama;

        return $this;
    }

    /**
     * Get agama
     *
     * @return \AppBundle\Entity\Master
     */
    public function getAgama()
    {
        return $this->agama;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return User
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }

    /**
     * Set wilayah
     *
     * @param \AppBundle\Entity\Wilayah $wilayah
     *
     * @return User
     */
    public function setWilayah(\AppBundle\Entity\Wilayah $wilayah = null)
    {
        $this->wilayah = $wilayah;

        return $this;
    }

    /**
     * Get wilayah
     *
     * @return \AppBundle\Entity\Wilayah
     */
    public function getWilayah()
    {
        return $this->wilayah;
    }

    /**
     * Set hakAkses
     *
     * @param \AppBundle\Entity\Master $hakAkses
     *
     * @return User
     */
    public function setHakAkses(\AppBundle\Entity\Master $hakAkses = null)
    {
        $this->hakAkses = $hakAkses;

        return $this;
    }

    /**
     * Get hakAkses
     *
     * @return \AppBundle\Entity\Master
     */
    public function getHakAkses()
    {
        return $this->hakAkses;
    }

    /**
     * Set dataMahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $dataMahasiswa
     *
     * @return User
     */
    public function setDataMahasiswa(\AppBundle\Entity\Mahasiswa $dataMahasiswa = null)
    {
        $this->dataMahasiswa = $dataMahasiswa;

        return $this;
    }

    /**
     * Get dataMahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getDataMahasiswa()
    {
        return $this->dataMahasiswa;
    }

    /**
     * Set dataDosen
     *
     * @param \AppBundle\Entity\Dosen $dataDosen
     *
     * @return User
     */
    public function setDataDosen(\AppBundle\Entity\Dosen $dataDosen = null)
    {
        $this->dataDosen = $dataDosen;

        return $this;
    }

    /**
     * Get dataDosen
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getDataDosen()
    {
        return $this->dataDosen;
    }

    /**
     * Set dataPegawai
     *
     * @param \AppBundle\Entity\Pegawai $dataPegawai
     *
     * @return User
     */
    public function setDataPegawai(\AppBundle\Entity\Pegawai $dataPegawai = null)
    {
        $this->dataPegawai = $dataPegawai;

        return $this;
    }

    /**
     * Get dataPegawai
     *
     * @return \AppBundle\Entity\Pegawai
     */
    public function getDataPegawai()
    {
        return $this->dataPegawai;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add aktifita
     *
     * @param \AppBundle\Entity\Aktifitas $aktifita
     *
     * @return User
     */
    public function addAktifita(\AppBundle\Entity\Aktifitas $aktifita)
    {
        $this->aktifitas[] = $aktifita;

        return $this;
    }

    /**
     * Remove aktifita
     *
     * @param \AppBundle\Entity\Aktifitas $aktifita
     */
    public function removeAktifita(\AppBundle\Entity\Aktifitas $aktifita)
    {
        $this->aktifitas->removeElement($aktifita);
    }

    /**
     * Get aktifitas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAktifitas()
    {
        return $this->aktifitas;
    }
}
