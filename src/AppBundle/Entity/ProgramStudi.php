<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jurusan
 *
 * @ORM\Table(name="prodi")
 * @ORM\Entity
 */
class ProgramStudi
{
    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="integer")
     */
    private $kode;

    /**
     * @var string
     *
     * @ORM\Column(name="kode_prodi", type="string", length=50, nullable=true)
     */
    private $kodeProdi; // dikti

    /**
     * @var string
     *
     * @ORM\Column(name="nama_prodi", type="string", length=100, nullable=false)
     */
    private $namaProdi;

    /**
     * @ORM\Column(name="jenjang", type="string", length=10, nullable=false)
     */
    private $jenjang;

    /**
     * @var string
     *
     * @ORM\Column(name="gelar_akademik", type="string", length=255, nullable=true)
     */
    private $gelarAkademik;

    /**
     * @var string
     *
     * @ORM\Column(name="sks_lulus", type="string", length=255, nullable=true)
     */
    private $sksLulus;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var date
     *
     * @ORM\Column(name="tgl_berdiri", type="date", nullable=true)
     */
    private $tglBerdiri;

    /**
     * @var string
     *
     * @ORM\Column(name="frekuensi_kurikulum", type="string", length=255, nullable=true)
     */
    private $frekuensiKurikulum;
  
    /**
     * @var string
     *
     * @ORM\Column(name="pelaksanaan_kurikulum", type="string", length=255, nullable=true)
     */
    private $pelaksanaanKurikulum;

    /**
     * @var string
     *
     * @ORM\Column(name="nama_sesi", type="string", length=255, nullable=true)
     */
    private $namaSesi;

    /**
     * @var string
     *
     * @ORM\Column(name="jumlah_sesi", type="integer", length=3, nullable=true)
     */
    private $jumlahSesi;

    /**
     * @var string
     *
     * @ORM\Column(name="batas_sesi", type="string", length=255, nullable=true)
     */
    private $batasSesi;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ProdiPejabat")
     * @ORM\JoinColumn(name="id_pejabat", referencedColumnName="id")
     */
    private $pejabat; 


    /**
     * @ORM\Column(name="nama_ketua", type="string", length=100, nullable=true)
     */
    private $ketua; 
 
    /**
     * @ORM\Column(name="hp_ketua", type="string", length=100, nullable=true)
     */
    private $hpKetua;

    /**
     * @ORM\Column(name="nama_operator", type="string", length=100, nullable=true)
     */
    private $operator; 

    /**
     * @ORM\Column(name="hp_operator", type="string", length=100, nullable=true)
     */
    private $hpOperator;

    /**
     * @var string
     *
     * @ORM\Column(name="nomor_sk_dikti", type="string", length=255, nullable=true)
     */
    private $nomorSkDikti;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_sk_dikti", type="string", length=255, nullable=true)
     */
    private $tglSkDikti;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_berakhir_sk_dikti", type="string", length=255, nullable=true)
     */
    private $tglBerakhirSkDikti;

    /**
     * @var string
     *
     * @ORM\Column(name="nomor_sk_ban", type="string", length=255, nullable=true)
     */
    private $nomorSkBan;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_sk_ban", type="string", length=255, nullable=true)
     */
    private $tglSkBan;

    /**
     * @ORM\Column(name="tgl_berakhir_sk_ban", type="string", length=255, nullable=true)
     */
    private $tglBerakhirSkBan;

    /**
     * @ORM\Column(name="akreditasi", type="string", length=255, nullable=true)
     */
    private $akreditasi;

    /**
     * @ORM\Column(name="alamat", type="text", length=65535, nullable=true)
     */
    private $alamat;

    /**
     * @ORM\Column(name="rt", type="string", length=10, nullable=true)
     */
    private $rt;

    /**
     * @ORM\Column(name="rw", type="string", length=10, nullable=true)
     */
    private $rw;

    /**
     * @ORM\Column(name="dusun", type="string", length=255, nullable=true)
     */
    private $dusun;

    /**
     * @ORM\Column(name="desa", type="string", length=255, nullable=true)
     */
    private $desa;

    /**
     * @ORM\Column(name="kode_pos", type="string", length=50, nullable=true)
     */
    private $pos;

    /**
     * @ORM\Column(name="telpon", type="string", length=255, nullable=true)
     */
    private $telp;

    /**
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;
    
    /**
     * @ORM\Column(name="raw", type="array", nullable=true)
     */
    private $raw;

    /**
     * @ORM\Column(name="uuid", type="guid", nullable=true)
     */
    private $uuid;



    /**
     * Set kode
     *
     * @param integer $kode
     *
     * @return ProgramStudi
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return integer
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set kodeProdi
     *
     * @param string $kodeProdi
     *
     * @return ProgramStudi
     */
    public function setKodeProdi($kodeProdi)
    {
        $this->kodeProdi = $kodeProdi;

        return $this;
    }

    /**
     * Get kodeProdi
     *
     * @return string
     */
    public function getKodeProdi()
    {
        return $this->kodeProdi;
    }

    /**
     * Set namaProdi
     *
     * @param string $namaProdi
     *
     * @return ProgramStudi
     */
    public function setNamaProdi($namaProdi)
    {
        $this->namaProdi = $namaProdi;

        return $this;
    }

    /**
     * Get namaProdi
     *
     * @return string
     */
    public function getNamaProdi()
    {
        return $this->namaProdi;
    }

    /**
     * Set jenjang
     *
     * @param string $jenjang
     *
     * @return ProgramStudi
     */
    public function setJenjang($jenjang)
    {
        $this->jenjang = $jenjang;

        return $this;
    }

    /**
     * Get jenjang
     *
     * @return string
     */
    public function getJenjang()
    {
        return $this->jenjang;
    }

    /**
     * Set gelarAkademik
     *
     * @param string $gelarAkademik
     *
     * @return ProgramStudi
     */
    public function setGelarAkademik($gelarAkademik)
    {
        $this->gelarAkademik = $gelarAkademik;

        return $this;
    }

    /**
     * Get gelarAkademik
     *
     * @return string
     */
    public function getGelarAkademik()
    {
        return $this->gelarAkademik;
    }

    /**
     * Set sksLulus
     *
     * @param string $sksLulus
     *
     * @return ProgramStudi
     */
    public function setSksLulus($sksLulus)
    {
        $this->sksLulus = $sksLulus;

        return $this;
    }

    /**
     * Get sksLulus
     *
     * @return string
     */
    public function getSksLulus()
    {
        return $this->sksLulus;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ProgramStudi
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set tglBerdiri
     *
     * @param \DateTime $tglBerdiri
     *
     * @return ProgramStudi
     */
    public function setTglBerdiri($tglBerdiri)
    {
        $this->tglBerdiri = $tglBerdiri;

        return $this;
    }

    /**
     * Get tglBerdiri
     *
     * @return \DateTime
     */
    public function getTglBerdiri()
    {
        return $this->tglBerdiri;
    }

    /**
     * Set frekuensiKurikulum
     *
     * @param string $frekuensiKurikulum
     *
     * @return ProgramStudi
     */
    public function setFrekuensiKurikulum($frekuensiKurikulum)
    {
        $this->frekuensiKurikulum = $frekuensiKurikulum;

        return $this;
    }

    /**
     * Get frekuensiKurikulum
     *
     * @return string
     */
    public function getFrekuensiKurikulum()
    {
        return $this->frekuensiKurikulum;
    }

    /**
     * Set pelaksanaanKurikulum
     *
     * @param string $pelaksanaanKurikulum
     *
     * @return ProgramStudi
     */
    public function setPelaksanaanKurikulum($pelaksanaanKurikulum)
    {
        $this->pelaksanaanKurikulum = $pelaksanaanKurikulum;

        return $this;
    }

    /**
     * Get pelaksanaanKurikulum
     *
     * @return string
     */
    public function getPelaksanaanKurikulum()
    {
        return $this->pelaksanaanKurikulum;
    }

    /**
     * Set namaSesi
     *
     * @param string $namaSesi
     *
     * @return ProgramStudi
     */
    public function setNamaSesi($namaSesi)
    {
        $this->namaSesi = $namaSesi;

        return $this;
    }

    /**
     * Get namaSesi
     *
     * @return string
     */
    public function getNamaSesi()
    {
        return $this->namaSesi;
    }

    /**
     * Set jumlahSesi
     *
     * @param integer $jumlahSesi
     *
     * @return ProgramStudi
     */
    public function setJumlahSesi($jumlahSesi)
    {
        $this->jumlahSesi = $jumlahSesi;

        return $this;
    }

    /**
     * Get jumlahSesi
     *
     * @return integer
     */
    public function getJumlahSesi()
    {
        return $this->jumlahSesi;
    }

    /**
     * Set batasSesi
     *
     * @param string $batasSesi
     *
     * @return ProgramStudi
     */
    public function setBatasSesi($batasSesi)
    {
        $this->batasSesi = $batasSesi;

        return $this;
    }

    /**
     * Get batasSesi
     *
     * @return string
     */
    public function getBatasSesi()
    {
        return $this->batasSesi;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ketua
     *
     * @param string $ketua
     *
     * @return ProgramStudi
     */
    public function setKetua($ketua)
    {
        $this->ketua = $ketua;

        return $this;
    }

    /**
     * Get ketua
     *
     * @return string
     */
    public function getKetua()
    {
        return $this->ketua;
    }

    /**
     * Set hpKetua
     *
     * @param string $hpKetua
     *
     * @return ProgramStudi
     */
    public function setHpKetua($hpKetua)
    {
        $this->hpKetua = $hpKetua;

        return $this;
    }

    /**
     * Get hpKetua
     *
     * @return string
     */
    public function getHpKetua()
    {
        return $this->hpKetua;
    }

    /**
     * Set operator
     *
     * @param string $operator
     *
     * @return ProgramStudi
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * Get operator
     *
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * Set hpOperator
     *
     * @param string $hpOperator
     *
     * @return ProgramStudi
     */
    public function setHpOperator($hpOperator)
    {
        $this->hpOperator = $hpOperator;

        return $this;
    }

    /**
     * Get hpOperator
     *
     * @return string
     */
    public function getHpOperator()
    {
        return $this->hpOperator;
    }

    /**
     * Set nomorSkDikti
     *
     * @param string $nomorSkDikti
     *
     * @return ProgramStudi
     */
    public function setNomorSkDikti($nomorSkDikti)
    {
        $this->nomorSkDikti = $nomorSkDikti;

        return $this;
    }

    /**
     * Get nomorSkDikti
     *
     * @return string
     */
    public function getNomorSkDikti()
    {
        return $this->nomorSkDikti;
    }

    /**
     * Set tglSkDikti
     *
     * @param string $tglSkDikti
     *
     * @return ProgramStudi
     */
    public function setTglSkDikti($tglSkDikti)
    {
        $this->tglSkDikti = $tglSkDikti;

        return $this;
    }

    /**
     * Get tglSkDikti
     *
     * @return string
     */
    public function getTglSkDikti()
    {
        return $this->tglSkDikti;
    }

    /**
     * Set tglBerakhirSkDikti
     *
     * @param string $tglBerakhirSkDikti
     *
     * @return ProgramStudi
     */
    public function setTglBerakhirSkDikti($tglBerakhirSkDikti)
    {
        $this->tglBerakhirSkDikti = $tglBerakhirSkDikti;

        return $this;
    }

    /**
     * Get tglBerakhirSkDikti
     *
     * @return string
     */
    public function getTglBerakhirSkDikti()
    {
        return $this->tglBerakhirSkDikti;
    }

    /**
     * Set nomorSkBan
     *
     * @param string $nomorSkBan
     *
     * @return ProgramStudi
     */
    public function setNomorSkBan($nomorSkBan)
    {
        $this->nomorSkBan = $nomorSkBan;

        return $this;
    }

    /**
     * Get nomorSkBan
     *
     * @return string
     */
    public function getNomorSkBan()
    {
        return $this->nomorSkBan;
    }

    /**
     * Set tglSkBan
     *
     * @param string $tglSkBan
     *
     * @return ProgramStudi
     */
    public function setTglSkBan($tglSkBan)
    {
        $this->tglSkBan = $tglSkBan;

        return $this;
    }

    /**
     * Get tglSkBan
     *
     * @return string
     */
    public function getTglSkBan()
    {
        return $this->tglSkBan;
    }

    /**
     * Set tglBerakhirSkBan
     *
     * @param string $tglBerakhirSkBan
     *
     * @return ProgramStudi
     */
    public function setTglBerakhirSkBan($tglBerakhirSkBan)
    {
        $this->tglBerakhirSkBan = $tglBerakhirSkBan;

        return $this;
    }

    /**
     * Get tglBerakhirSkBan
     *
     * @return string
     */
    public function getTglBerakhirSkBan()
    {
        return $this->tglBerakhirSkBan;
    }

    /**
     * Set akreditasi
     *
     * @param string $akreditasi
     *
     * @return ProgramStudi
     */
    public function setAkreditasi($akreditasi)
    {
        $this->akreditasi = $akreditasi;

        return $this;
    }

    /**
     * Get akreditasi
     *
     * @return string
     */
    public function getAkreditasi()
    {
        return $this->akreditasi;
    }

    /**
     * Set alamat
     *
     * @param string $alamat
     *
     * @return ProgramStudi
     */
    public function setAlamat($alamat)
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Get alamat
     *
     * @return string
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * Set rt
     *
     * @param string $rt
     *
     * @return ProgramStudi
     */
    public function setRt($rt)
    {
        $this->rt = $rt;

        return $this;
    }

    /**
     * Get rt
     *
     * @return string
     */
    public function getRt()
    {
        return $this->rt;
    }

    /**
     * Set rw
     *
     * @param string $rw
     *
     * @return ProgramStudi
     */
    public function setRw($rw)
    {
        $this->rw = $rw;

        return $this;
    }

    /**
     * Get rw
     *
     * @return string
     */
    public function getRw()
    {
        return $this->rw;
    }

    /**
     * Set dusun
     *
     * @param string $dusun
     *
     * @return ProgramStudi
     */
    public function setDusun($dusun)
    {
        $this->dusun = $dusun;

        return $this;
    }

    /**
     * Get dusun
     *
     * @return string
     */
    public function getDusun()
    {
        return $this->dusun;
    }

    /**
     * Set desa
     *
     * @param string $desa
     *
     * @return ProgramStudi
     */
    public function setDesa($desa)
    {
        $this->desa = $desa;

        return $this;
    }

    /**
     * Get desa
     *
     * @return string
     */
    public function getDesa()
    {
        return $this->desa;
    }

    /**
     * Set pos
     *
     * @param string $pos
     *
     * @return ProgramStudi
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return string
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set telp
     *
     * @param string $telp
     *
     * @return ProgramStudi
     */
    public function setTelp($telp)
    {
        $this->telp = $telp;

        return $this;
    }

    /**
     * Get telp
     *
     * @return string
     */
    public function getTelp()
    {
        return $this->telp;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return ProgramStudi
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ProgramStudi
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return ProgramStudi
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set raw
     *
     * @param array $raw
     *
     * @return ProgramStudi
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return array
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return ProgramStudi
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set pejabat
     *
     * @param \AppBundle\Entity\ProdiPejabat $pejabat
     *
     * @return ProgramStudi
     */
    public function setPejabat(\AppBundle\Entity\ProdiPejabat $pejabat = null)
    {
        $this->pejabat = $pejabat;

        return $this;
    }

    /**
     * Get pejabat
     *
     * @return \AppBundle\Entity\ProdiPejabat
     */
    public function getPejabat()
    {
        return $this->pejabat;
    }
}
