<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inventaris
 *
 * @ORM\Table(name="inventaris")
 * @ORM\Entity
 */
class Inventaris
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="string", length=150)
     */
    private $kodeBarang;

    /**
     * @var string
     *
     * @ORM\Column(name="nama_barang", type="string", length=255)
     */
    private $namaBarang;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="merk", type="string", length=255, nullable=true)
     */
    private $merk;

    /**
     * @var string
     *
     * @ORM\Column(name="harga_beli", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $hargaBeli;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_beli", type="date", nullable=true)
     */
    private $tglBeli;

    /**
     * @var string
     *
     * @ORM\Column(name="jadwal_rawat", type="date", nullable=true)
     */
    private $jadwalRawat;

    /**
     * @var string
     *
     * @ORM\Column(name="asal", type="string", length=255, nullable=true)
     */
    private $asal;

    /**
     * @var string
     *
     * @ORM\Column(name="kepemilikan", type="string", length=255, nullable=true)
     */
    private $kepemilikan;

    /**
     * @var string
     *
     * @ORM\Column(name="umur_pakai", type="string", length=100, nullable=true)
     */
    private $umurPakai;

    /**
     * @ORM\ManyToOne(targetEntity="Lokasi", inversedBy="barang")
     * @ORM\JoinColumn(name="id_lokasi", referencedColumnName="id")
     */
    private $lokasi;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kodeBarang
     *
     * @param string $kodeBarang
     *
     * @return Inventaris
     */
    public function setKodeBarang($kodeBarang)
    {
        $this->kodeBarang = $kodeBarang;

        return $this;
    }

    /**
     * Get kodeBarang
     *
     * @return string
     */
    public function getKodeBarang()
    {
        return $this->kodeBarang;
    }

    /**
     * Set namaBarang
     *
     * @param string $namaBarang
     *
     * @return Inventaris
     */
    public function setNamaBarang($namaBarang)
    {
        $this->namaBarang = $namaBarang;

        return $this;
    }

    /**
     * Get namaBarang
     *
     * @return string
     */
    public function getNamaBarang()
    {
        return $this->namaBarang;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Inventaris
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set merk
     *
     * @param string $merk
     *
     * @return Inventaris
     */
    public function setMerk($merk)
    {
        $this->merk = $merk;

        return $this;
    }

    /**
     * Get merk
     *
     * @return string
     */
    public function getMerk()
    {
        return $this->merk;
    }

    /**
     * Set hargaBeli
     *
     * @param string $hargaBeli
     *
     * @return Inventaris
     */
    public function setHargaBeli($hargaBeli)
    {
        $this->hargaBeli = $hargaBeli;

        return $this;
    }

    /**
     * Get hargaBeli
     *
     * @return string
     */
    public function getHargaBeli()
    {
        return $this->hargaBeli;
    }

    /**
     * Set tglBeli
     *
     * @param \DateTime $tglBeli
     *
     * @return Inventaris
     */
    public function setTglBeli($tglBeli)
    {
        $this->tglBeli = $tglBeli;

        return $this;
    }

    /**
     * Get tglBeli
     *
     * @return \DateTime
     */
    public function getTglBeli()
    {
        return $this->tglBeli;
    }

    /**
     * Set jadwalRawat
     *
     * @param \DateTime $jadwalRawat
     *
     * @return Inventaris
     */
    public function setJadwalRawat($jadwalRawat)
    {
        $this->jadwalRawat = $jadwalRawat;

        return $this;
    }

    /**
     * Get jadwalRawat
     *
     * @return \DateTime
     */
    public function getJadwalRawat()
    {
        return $this->jadwalRawat;
    }

    /**
     * Set asal
     *
     * @param string $asal
     *
     * @return Inventaris
     */
    public function setAsal($asal)
    {
        $this->asal = $asal;

        return $this;
    }

    /**
     * Get asal
     *
     * @return string
     */
    public function getAsal()
    {
        return $this->asal;
    }

    /**
     * Set kepemilikan
     *
     * @param string $kepemilikan
     *
     * @return Inventaris
     */
    public function setKepemilikan($kepemilikan)
    {
        $this->kepemilikan = $kepemilikan;

        return $this;
    }

    /**
     * Get kepemilikan
     *
     * @return string
     */
    public function getKepemilikan()
    {
        return $this->kepemilikan;
    }

    /**
     * Set umurPakai
     *
     * @param string $umurPakai
     *
     * @return Inventaris
     */
    public function setUmurPakai($umurPakai)
    {
        $this->umurPakai = $umurPakai;

        return $this;
    }

    /**
     * Get umurPakai
     *
     * @return string
     */
    public function getUmurPakai()
    {
        return $this->umurPakai;
    }

    /**
     * Set lokasi
     *
     * @param \AppBundle\Entity\Lokasi $lokasi
     *
     * @return Inventaris
     */
    public function setLokasi(\AppBundle\Entity\Lokasi $lokasi = null)
    {
        $this->lokasi = $lokasi;

        return $this;
    }

    /**
     * Get lokasi
     *
     * @return \AppBundle\Entity\Lokasi
     */
    public function getLokasi()
    {
        return $this->lokasi;
    }
}
