<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TugasAkhir
 *
 * @ORM\Table(name="tugas_akhir")
 * @ORM\Entity
 */
class TugasAkhir
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="judul", type="string", length=255)
     */
    private $judul;

    /**
     * @var string
     *
     * @ORM\Column(name="diterima", type="boolean", nullable=true)
     */
    private $diterima;

    /**
     * @ORM\ManyToOne(targetEntity="Mahasiswa")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $mahasiswa;


    /**
     * @ORM\Column(name="tgl_pengajuan", type="datetime", nullable=true)
     */
    private $tglPengajuan;    

    /**
     * @ORM\Column(name="tgl_mulai", type="date", nullable=true)
     */
    private $tglMulai;  

    /**
     * @ORM\Column(name="tgl_berakhir", type="date", nullable=true)
     */
    private $tglBerakhir;  

    /**
     * @ORM\Column(name="tgl_sidang", type="date", nullable=true)
     */
    private $tglSidang;  

    /**
     * @ORM\ManyToOne(targetEntity="Dosen")
     * @ORM\JoinColumn(name="id_dosen_penguji_1", referencedColumnName="id")
     */
    private $penguji1;

    /**
     * @ORM\ManyToOne(targetEntity="Dosen")
     * @ORM\JoinColumn(name="id_dosen_penguji_2", referencedColumnName="id")
     */
    private $penguji2;

    /**
     * @ORM\ManyToOne(targetEntity="Dosen")
     * @ORM\JoinColumn(name="id_dosen_pembimbing_1", referencedColumnName="id")
     */
    private $pembimbing1;

    /**
     * @ORM\ManyToOne(targetEntity="Dosen")
     * @ORM\JoinColumn(name="id_dosen_pembimbing_2", referencedColumnName="id")
     */
    private $pembimbing2;

    /**
     * @ORM\Column(name="status", type="string")
     */
    private $status = 'publish';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set judul
     *
     * @param string $judul
     *
     * @return TugasAkhir
     */
    public function setJudul($judul)
    {
        $this->judul = $judul;

        return $this;
    }

    /**
     * Get judul
     *
     * @return string
     */
    public function getJudul()
    {
        return $this->judul;
    }

    /**
     * Set diterima
     *
     * @param boolean $diterima
     *
     * @return TugasAkhir
     */
    public function setDiterima($diterima)
    {
        $this->diterima = $diterima;

        return $this;
    }

    /**
     * Get diterima
     *
     * @return boolean
     */
    public function getDiterima()
    {
        return $this->diterima;
    }

    /**
     * Set tglPengajuan
     *
     * @param \DateTime $tglPengajuan
     *
     * @return TugasAkhir
     */
    public function setTglPengajuan($tglPengajuan)
    {
        $this->tglPengajuan = $tglPengajuan;

        return $this;
    }

    /**
     * Get tglPengajuan
     *
     * @return \DateTime
     */
    public function getTglPengajuan()
    {
        return $this->tglPengajuan;
    }

    /**
     * Set tglMulai
     *
     * @param \DateTime $tglMulai
     *
     * @return TugasAkhir
     */
    public function setTglMulai($tglMulai)
    {
        $this->tglMulai = $tglMulai;

        return $this;
    }

    /**
     * Get tglMulai
     *
     * @return \DateTime
     */
    public function getTglMulai()
    {
        return $this->tglMulai;
    }

    /**
     * Set tglBerakhir
     *
     * @param \DateTime $tglBerakhir
     *
     * @return TugasAkhir
     */
    public function setTglBerakhir($tglBerakhir)
    {
        $this->tglBerakhir = $tglBerakhir;

        return $this;
    }

    /**
     * Get tglBerakhir
     *
     * @return \DateTime
     */
    public function getTglBerakhir()
    {
        return $this->tglBerakhir;
    }

    /**
     * Set tglSidang
     *
     * @param \DateTime $tglSidang
     *
     * @return TugasAkhir
     */
    public function setTglSidang($tglSidang)
    {
        $this->tglSidang = $tglSidang;

        return $this;
    }

    /**
     * Get tglSidang
     *
     * @return \DateTime
     */
    public function getTglSidang()
    {
        return $this->tglSidang;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TugasAkhir
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return TugasAkhir
     */
    public function setMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa = null)
    {
        $this->mahasiswa = $mahasiswa;

        return $this;
    }

    /**
     * Get mahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }

    /**
     * Set penguji1
     *
     * @param \AppBundle\Entity\Dosen $penguji1
     *
     * @return TugasAkhir
     */
    public function setPenguji1(\AppBundle\Entity\Dosen $penguji1 = null)
    {
        $this->penguji1 = $penguji1;

        return $this;
    }

    /**
     * Get penguji1
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getPenguji1()
    {
        return $this->penguji1;
    }

    /**
     * Set penguji2
     *
     * @param \AppBundle\Entity\Dosen $penguji2
     *
     * @return TugasAkhir
     */
    public function setPenguji2(\AppBundle\Entity\Dosen $penguji2 = null)
    {
        $this->penguji2 = $penguji2;

        return $this;
    }

    /**
     * Get penguji2
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getPenguji2()
    {
        return $this->penguji2;
    }

    /**
     * Set pembimbing1
     *
     * @param \AppBundle\Entity\Dosen $pembimbing1
     *
     * @return TugasAkhir
     */
    public function setPembimbing1(\AppBundle\Entity\Dosen $pembimbing1 = null)
    {
        $this->pembimbing1 = $pembimbing1;

        return $this;
    }

    /**
     * Get pembimbing1
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getPembimbing1()
    {
        return $this->pembimbing1;
    }

    /**
     * Set pembimbing2
     *
     * @param \AppBundle\Entity\Dosen $pembimbing2
     *
     * @return TugasAkhir
     */
    public function setPembimbing2(\AppBundle\Entity\Dosen $pembimbing2 = null)
    {
        $this->pembimbing2 = $pembimbing2;

        return $this;
    }

    /**
     * Get pembimbing2
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getPembimbing2()
    {
        return $this->pembimbing2;
    }
}
