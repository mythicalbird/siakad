<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PmbPeriode
 *
 * @ORM\Table(name="pmb_periode")
 * @ORM\Entity
 */
class PmbPeriode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="tahun", type="integer", length=4, nullable=true)
     */
    private $tahun;
  
    /**
     * @var int
     *
     * @ORM\Column(name="kode", type="string", length=150, nullable=true)
     */
    private $kode;

    /**
     * @var int
     *
     * @ORM\Column(name="kapasitas", type="integer", nullable=true)
     */
    private $kapasitas;

    /**
     * @var string
     *
     * @ORM\Column(name="biaya_pendaftaran", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $biayaPendaftaran;

    /**
     * @var int
     *
     * @ORM\Column(name="nilai_minimal", type="integer", nullable=true)
     */
    private $nilaiMinimal;
  
    /**
     * @ORM\Column(name="tgl_pendaftaran", type="string", length=255, nullable=true)
     */
    private $tglPendaftaran;
  
    /**
     * @ORM\Column(name="tgl_usm", type="string", length=255, nullable=true)
     */
    private $tglUsm;
  
    /**
     * @ORM\Column(name="tgl_her", type="string", length=255, nullable=true)
     */
    private $tglHer;

    /**
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status;
  
    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi;

    /**
     * @ORM\OneToMany(targetEntity="Mahasiswa", mappedBy="periode")
     */
    private $mahasiswa;

    public function __construct()
    {
        $this->mahasiswa = new \Doctrine\Common\Collections\ArrayCollection();
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tahun
     *
     * @param integer $tahun
     *
     * @return PmbPeriode
     */
    public function setTahun($tahun)
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get tahun
     *
     * @return integer
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return PmbPeriode
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set kapasitas
     *
     * @param integer $kapasitas
     *
     * @return PmbPeriode
     */
    public function setKapasitas($kapasitas)
    {
        $this->kapasitas = $kapasitas;

        return $this;
    }

    /**
     * Get kapasitas
     *
     * @return integer
     */
    public function getKapasitas()
    {
        return $this->kapasitas;
    }

    /**
     * Set biayaPendaftaran
     *
     * @param string $biayaPendaftaran
     *
     * @return PmbPeriode
     */
    public function setBiayaPendaftaran($biayaPendaftaran)
    {
        $this->biayaPendaftaran = $biayaPendaftaran;

        return $this;
    }

    /**
     * Get biayaPendaftaran
     *
     * @return string
     */
    public function getBiayaPendaftaran()
    {
        return $this->biayaPendaftaran;
    }

    /**
     * Set nilaiMinimal
     *
     * @param integer $nilaiMinimal
     *
     * @return PmbPeriode
     */
    public function setNilaiMinimal($nilaiMinimal)
    {
        $this->nilaiMinimal = $nilaiMinimal;

        return $this;
    }

    /**
     * Get nilaiMinimal
     *
     * @return integer
     */
    public function getNilaiMinimal()
    {
        return $this->nilaiMinimal;
    }

    /**
     * Set tglPendaftaran
     *
     * @param string $tglPendaftaran
     *
     * @return PmbPeriode
     */
    public function setTglPendaftaran($tglPendaftaran)
    {
        $this->tglPendaftaran = $tglPendaftaran;

        return $this;
    }

    /**
     * Get tglPendaftaran
     *
     * @return string
     */
    public function getTglPendaftaran()
    {
        return $this->tglPendaftaran;
    }

    /**
     * Set tglUsm
     *
     * @param string $tglUsm
     *
     * @return PmbPeriode
     */
    public function setTglUsm($tglUsm)
    {
        $this->tglUsm = $tglUsm;

        return $this;
    }

    /**
     * Get tglUsm
     *
     * @return string
     */
    public function getTglUsm()
    {
        return $this->tglUsm;
    }

    /**
     * Set tglHer
     *
     * @param string $tglHer
     *
     * @return PmbPeriode
     */
    public function setTglHer($tglHer)
    {
        $this->tglHer = $tglHer;

        return $this;
    }

    /**
     * Get tglHer
     *
     * @return string
     */
    public function getTglHer()
    {
        return $this->tglHer;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return PmbPeriode
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }

    /**
     * Add mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return PmbPeriode
     */
    public function addMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa)
    {
        $this->mahasiswa[] = $mahasiswa;

        return $this;
    }

    /**
     * Remove mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     */
    public function removeMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa)
    {
        $this->mahasiswa->removeElement($mahasiswa);
    }

    /**
     * Get mahasiswa
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return PmbPeriode
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
