<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Mahasiswa
 *
 * @ORM\Table(name="mahasiswa")
 * @ORM\Entity
 */
class Mahasiswa
{ 

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_jenis_daftar", referencedColumnName="id")
     */
    private $jenisDaftar;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_jenis_keluar", referencedColumnName="id")
     */
    private $jenisKeluar;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_status", referencedColumnName="id")
     */
    private $status;

    /**
     * @ORM\Column(name="orangtua", type="array", nullable=true)
     */
    private $orangtua;
  
    /**
     * @ORM\Column(name="semester", type="integer", nullable=true)
     */
    private $semester;

    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_ta", referencedColumnName="id")
     */
    private $semesterMasuk;
  
    /**
     * @ORM\ManyToOne(targetEntity="Kelas")
     * @ORM\JoinColumn(name="id_kelas", referencedColumnName="id")
     */
    private $kelas;

    /**
     * @ORM\ManyToOne(targetEntity="Dosen")
     * @ORM\JoinColumn(name="id_dosen_pa", referencedColumnName="id")
     */
    private $pa;

    /**
     * @ORM\Column(name="ipk", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $ipk;

    /**
     * @ORM\Column(name="sks", type="integer", nullable=true)
     */
    private $sks;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_jalur_masuk", referencedColumnName="id")
     */
    private $jalurMasuk;

    /**
     * @var string
     *
     * @ORM\Column(name="angkatan", type="string", length=255, nullable=true)
     */
    private $angkatan; // tahun

    /**
     * @ORM\ManyToOne(targetEntity="PmbPeriode", inversedBy="mahasiswa")
     * @ORM\JoinColumn(name="id_periode", referencedColumnName="id")
     */
    private $periode;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="dataMahasiswa")
     */
    private $user;

    /**
     * @ORM\Column(name="uuid", type="guid", nullable=true)
     */
    private $uuid; // id_pd

    /**
     * @ORM\Column(name="id_reg_pd", type="guid", nullable=true)
     */
    private $regPd; // id_reg_pd

    /**
     * @ORM\Column(name="no_seri_ijazah", type="string", length=255, nullable=true)
     */
    private $noSeriIjazah;

    /**
     * @ORM\Column(name="tgl_ijazah", type="date", nullable=true)
     */
    private $tglIjazah;

    /**
     * @ORM\Column(name="no_sk_yudisium", type="string", length=255, nullable=true)
     */
    private $noSkYudisium;

    /**
     * @ORM\Column(name="tgl_sk_yudisium", type="date", nullable=true)
     */
    private $tglSkYudisium; 

    /**
     * @ORM\ManyToOne(targetEntity="SatuanPendidikan")
     * @ORM\JoinColumn(name="id_asal_sekolah", referencedColumnName="id")
     */
    private $asalSekolah;

    /**
     * @ORM\ManyToOne(targetEntity="SatuanPendidikan")
     * @ORM\JoinColumn(name="id_asal_pt", referencedColumnName="id")
     */
    private $asalPt;

    /**
     * @ORM\Column(name="kode_sekolah", type="string", length=255, nullable=true)
     */
    private $kodeSekolah;
  
    /**
     * @ORM\Column(name="nama_sekolah", type="string", length=255, nullable=true)
     */
    private $namaSekolah;
  
    /**
     * @ORM\Column(name="nis", type="string", length=255, nullable=true)
     */
    private $nis;
  
    /**
     * @ORM\Column(name="nilai_un", type="string", length=255, nullable=true)
     */
    private $nilaiUn;
  
    /**
     * @ORM\Column(name="asal_kode_pt", type="string", length=255, nullable=true)
     */
    private $asalKodePt;
  
    /**
     * @ORM\Column(name="asal_nama_pt", type="string", length=255, nullable=true)
     */
    private $asalNamaPt;
  
    /**
     * @ORM\Column(name="asal_jenjang", type="string", length=255, nullable=true)
     */
    private $asalJenjang;
  
    /**
     * @ORM\Column(name="asal_prodi", type="string", length=255, nullable=true)
     */
    private $asalProdi;
  
    /**
     * @ORM\Column(name="asal_nim", type="string", length=255, nullable=true)
     */
    private $asalNim;
  
    /**
     * @ORM\Column(name="asal_sks_diakui", type="string", length=255, nullable=true)
     */
    private $asalSksDiakui;

    /**
     * @ORM\Column(name="maba", type="boolean")
     */
    private $maba;

    /**
     * @var integer 
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="MahasiswaPendidikan", mappedBy="mahasiswa")
     */
    private $riwayatPendidikan;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_pembiayaan", referencedColumnName="id")
     */
    private $pembiayaan;

    /**
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    private $data;

    /**
     * @ORM\Column(name="raw", type="array", nullable=true)
     */
    private $raw;

    /**
     * @ORM\OneToMany(targetEntity="Berkas", mappedBy="mahasiswa")
     */
    private $berkas;

    /**
     * @ORM\Column(name="trashed", type="boolean")
     */
    private $trashed = 0;

    public function __construct() {
        $this->berkas = new ArrayCollection();
        $this->riwayatPendidikan = new ArrayCollection();
    }

    public function getSemesterSkrg()
    {
				$semester = $this->semester;
				if( null !== $this->angkatan ) {
					$bulan = date('m');
					$tahun = date('Y');
					$semester = ($tahun-$this->angkatan);
					if( $bulan > 6 ) {
						$semester = $semester+1;
					}
				}
        return $semester;
    }

    public function getProdi() {
        if ( null !== $this->getUser() ) {
            return $this->getUser()->getProdi();
        }
        return null;
    }

    /**
     * Set orangtua
     *
     * @param array $orangtua
     *
     * @return Mahasiswa
     */
    public function setOrangtua($orangtua)
    {
        $this->orangtua = $orangtua;

        return $this;
    }

    /**
     * Get orangtua
     *
     * @return array
     */
    public function getOrangtua()
    {
        return $this->orangtua;
    }

    /**
     * Set semester
     *
     * @param integer $semester
     *
     * @return Mahasiswa
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set ipk
     *
     * @param string $ipk
     *
     * @return Mahasiswa
     */
    public function setIpk($ipk)
    {
        $this->ipk = $ipk;

        return $this;
    }

    /**
     * Get ipk
     *
     * @return string
     */
    public function getIpk()
    {
        return $this->ipk;
    }

    /**
     * Set sks
     *
     * @param integer $sks
     *
     * @return Mahasiswa
     */
    public function setSks($sks)
    {
        $this->sks = $sks;

        return $this;
    }

    /**
     * Get sks
     *
     * @return integer
     */
    public function getSks()
    {
        return $this->sks;
    }

    /**
     * Set angkatan
     *
     * @param string $angkatan
     *
     * @return Mahasiswa
     */
    public function setAngkatan($angkatan)
    {
        $this->angkatan = $angkatan;

        return $this;
    }

    /**
     * Get angkatan
     *
     * @return string
     */
    public function getAngkatan()
    {
        return $this->angkatan;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return Mahasiswa
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set regPd
     *
     * @param guid $regPd
     *
     * @return Mahasiswa
     */
    public function setRegPd($regPd)
    {
        $this->regPd = $regPd;

        return $this;
    }

    /**
     * Get regPd
     *
     * @return guid
     */
    public function getRegPd()
    {
        return $this->regPd;
    }

    /**
     * Set noSeriIjazah
     *
     * @param string $noSeriIjazah
     *
     * @return Mahasiswa
     */
    public function setNoSeriIjazah($noSeriIjazah)
    {
        $this->noSeriIjazah = $noSeriIjazah;

        return $this;
    }

    /**
     * Get noSeriIjazah
     *
     * @return string
     */
    public function getNoSeriIjazah()
    {
        return $this->noSeriIjazah;
    }

    /**
     * Set tglIjazah
     *
     * @param \DateTime $tglIjazah
     *
     * @return Mahasiswa
     */
    public function setTglIjazah($tglIjazah)
    {
        $this->tglIjazah = $tglIjazah;

        return $this;
    }

    /**
     * Get tglIjazah
     *
     * @return \DateTime
     */
    public function getTglIjazah()
    {
        return $this->tglIjazah;
    }

    /**
     * Set noSkYudisium
     *
     * @param string $noSkYudisium
     *
     * @return Mahasiswa
     */
    public function setNoSkYudisium($noSkYudisium)
    {
        $this->noSkYudisium = $noSkYudisium;

        return $this;
    }

    /**
     * Get noSkYudisium
     *
     * @return string
     */
    public function getNoSkYudisium()
    {
        return $this->noSkYudisium;
    }

    /**
     * Set tglSkYudisium
     *
     * @param \DateTime $tglSkYudisium
     *
     * @return Mahasiswa
     */
    public function setTglSkYudisium($tglSkYudisium)
    {
        $this->tglSkYudisium = $tglSkYudisium;

        return $this;
    }

    /**
     * Get tglSkYudisium
     *
     * @return \DateTime
     */
    public function getTglSkYudisium()
    {
        return $this->tglSkYudisium;
    }

    /**
     * Set kodeSekolah
     *
     * @param string $kodeSekolah
     *
     * @return Mahasiswa
     */
    public function setKodeSekolah($kodeSekolah)
    {
        $this->kodeSekolah = $kodeSekolah;

        return $this;
    }

    /**
     * Get kodeSekolah
     *
     * @return string
     */
    public function getKodeSekolah()
    {
        return $this->kodeSekolah;
    }

    /**
     * Set namaSekolah
     *
     * @param string $namaSekolah
     *
     * @return Mahasiswa
     */
    public function setNamaSekolah($namaSekolah)
    {
        $this->namaSekolah = $namaSekolah;

        return $this;
    }

    /**
     * Get namaSekolah
     *
     * @return string
     */
    public function getNamaSekolah()
    {
        return $this->namaSekolah;
    }

    /**
     * Set nis
     *
     * @param string $nis
     *
     * @return Mahasiswa
     */
    public function setNis($nis)
    {
        $this->nis = $nis;

        return $this;
    }

    /**
     * Get nis
     *
     * @return string
     */
    public function getNis()
    {
        return $this->nis;
    }

    /**
     * Set nilaiUn
     *
     * @param string $nilaiUn
     *
     * @return Mahasiswa
     */
    public function setNilaiUn($nilaiUn)
    {
        $this->nilaiUn = $nilaiUn;

        return $this;
    }

    /**
     * Get nilaiUn
     *
     * @return string
     */
    public function getNilaiUn()
    {
        return $this->nilaiUn;
    }

    /**
     * Set asalKodePt
     *
     * @param string $asalKodePt
     *
     * @return Mahasiswa
     */
    public function setAsalKodePt($asalKodePt)
    {
        $this->asalKodePt = $asalKodePt;

        return $this;
    }

    /**
     * Get asalKodePt
     *
     * @return string
     */
    public function getAsalKodePt()
    {
        return $this->asalKodePt;
    }

    /**
     * Set asalNamaPt
     *
     * @param string $asalNamaPt
     *
     * @return Mahasiswa
     */
    public function setAsalNamaPt($asalNamaPt)
    {
        $this->asalNamaPt = $asalNamaPt;

        return $this;
    }

    /**
     * Get asalNamaPt
     *
     * @return string
     */
    public function getAsalNamaPt()
    {
        return $this->asalNamaPt;
    }

    /**
     * Set asalJenjang
     *
     * @param string $asalJenjang
     *
     * @return Mahasiswa
     */
    public function setAsalJenjang($asalJenjang)
    {
        $this->asalJenjang = $asalJenjang;

        return $this;
    }

    /**
     * Get asalJenjang
     *
     * @return string
     */
    public function getAsalJenjang()
    {
        return $this->asalJenjang;
    }

    /**
     * Set asalProdi
     *
     * @param string $asalProdi
     *
     * @return Mahasiswa
     */
    public function setAsalProdi($asalProdi)
    {
        $this->asalProdi = $asalProdi;

        return $this;
    }

    /**
     * Get asalProdi
     *
     * @return string
     */
    public function getAsalProdi()
    {
        return $this->asalProdi;
    }

    /**
     * Set asalNim
     *
     * @param string $asalNim
     *
     * @return Mahasiswa
     */
    public function setAsalNim($asalNim)
    {
        $this->asalNim = $asalNim;

        return $this;
    }

    /**
     * Get asalNim
     *
     * @return string
     */
    public function getAsalNim()
    {
        return $this->asalNim;
    }

    /**
     * Set asalSksDiakui
     *
     * @param string $asalSksDiakui
     *
     * @return Mahasiswa
     */
    public function setAsalSksDiakui($asalSksDiakui)
    {
        $this->asalSksDiakui = $asalSksDiakui;

        return $this;
    }

    /**
     * Get asalSksDiakui
     *
     * @return string
     */
    public function getAsalSksDiakui()
    {
        return $this->asalSksDiakui;
    }

    /**
     * Set maba
     *
     * @param boolean $maba
     *
     * @return Mahasiswa
     */
    public function setMaba($maba)
    {
        $this->maba = $maba;

        return $this;
    }

    /**
     * Get maba
     *
     * @return boolean
     */
    public function getMaba()
    {
        return $this->maba;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return Mahasiswa
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set raw
     *
     * @param array $raw
     *
     * @return Mahasiswa
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return array
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Set jenisDaftar
     *
     * @param \AppBundle\Entity\Master $jenisDaftar
     *
     * @return Mahasiswa
     */
    public function setJenisDaftar(\AppBundle\Entity\Master $jenisDaftar = null)
    {
        $this->jenisDaftar = $jenisDaftar;

        return $this;
    }

    /**
     * Get jenisDaftar
     *
     * @return \AppBundle\Entity\Master
     */
    public function getJenisDaftar()
    {
        return $this->jenisDaftar;
    }

    /**
     * Set jenisKeluar
     *
     * @param \AppBundle\Entity\Master $jenisKeluar
     *
     * @return Mahasiswa
     */
    public function setJenisKeluar(\AppBundle\Entity\Master $jenisKeluar = null)
    {
        $this->jenisKeluar = $jenisKeluar;

        return $this;
    }

    /**
     * Get jenisKeluar
     *
     * @return \AppBundle\Entity\Master
     */
    public function getJenisKeluar()
    {
        return $this->jenisKeluar;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\Master $status
     *
     * @return Mahasiswa
     */
    public function setStatus(\AppBundle\Entity\Master $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\Master
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set semesterMasuk
     *
     * @param \AppBundle\Entity\TahunAkademik $semesterMasuk
     *
     * @return Mahasiswa
     */
    public function setSemesterMasuk(\AppBundle\Entity\TahunAkademik $semesterMasuk = null)
    {
        $this->semesterMasuk = $semesterMasuk;

        return $this;
    }

    /**
     * Get semesterMasuk
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getSemesterMasuk()
    {
        return $this->semesterMasuk;
    }

    /**
     * Set kelas
     *
     * @param \AppBundle\Entity\Kelas $kelas
     *
     * @return Mahasiswa
     */
    public function setKelas(\AppBundle\Entity\Kelas $kelas = null)
    {
        $this->kelas = $kelas;

        return $this;
    }

    /**
     * Get kelas
     *
     * @return \AppBundle\Entity\Kelas
     */
    public function getKelas()
    {
        return $this->kelas;
    }

    /**
     * Set pa
     *
     * @param \AppBundle\Entity\Dosen $pa
     *
     * @return Mahasiswa
     */
    public function setPa(\AppBundle\Entity\Dosen $pa = null)
    {
        $this->pa = $pa;

        return $this;
    }

    /**
     * Get pa
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getPa()
    {
        return $this->pa;
    }

    /**
     * Set jalurMasuk
     *
     * @param \AppBundle\Entity\Master $jalurMasuk
     *
     * @return Mahasiswa
     */
    public function setJalurMasuk(\AppBundle\Entity\Master $jalurMasuk = null)
    {
        $this->jalurMasuk = $jalurMasuk;

        return $this;
    }

    /**
     * Get jalurMasuk
     *
     * @return \AppBundle\Entity\Master
     */
    public function getJalurMasuk()
    {
        return $this->jalurMasuk;
    }

    /**
     * Set periode
     *
     * @param \AppBundle\Entity\PmbPeriode $periode
     *
     * @return Mahasiswa
     */
    public function setPeriode(\AppBundle\Entity\PmbPeriode $periode = null)
    {
        $this->periode = $periode;

        return $this;
    }

    /**
     * Get periode
     *
     * @return \AppBundle\Entity\PmbPeriode
     */
    public function getPeriode()
    {
        return $this->periode;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Mahasiswa
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set asalSekolah
     *
     * @param \AppBundle\Entity\SatuanPendidikan $asalSekolah
     *
     * @return Mahasiswa
     */
    public function setAsalSekolah(\AppBundle\Entity\SatuanPendidikan $asalSekolah = null)
    {
        $this->asalSekolah = $asalSekolah;

        return $this;
    }

    /**
     * Get asalSekolah
     *
     * @return \AppBundle\Entity\SatuanPendidikan
     */
    public function getAsalSekolah()
    {
        return $this->asalSekolah;
    }

    /**
     * Set asalPt
     *
     * @param \AppBundle\Entity\SatuanPendidikan $asalPt
     *
     * @return Mahasiswa
     */
    public function setAsalPt(\AppBundle\Entity\SatuanPendidikan $asalPt = null)
    {
        $this->asalPt = $asalPt;

        return $this;
    }

    /**
     * Get asalPt
     *
     * @return \AppBundle\Entity\SatuanPendidikan
     */
    public function getAsalPt()
    {
        return $this->asalPt;
    }

    /**
     * Add riwayatPendidikan
     *
     * @param \AppBundle\Entity\MahasiswaPendidikan $riwayatPendidikan
     *
     * @return Mahasiswa
     */
    public function addRiwayatPendidikan(\AppBundle\Entity\MahasiswaPendidikan $riwayatPendidikan)
    {
        $this->riwayatPendidikan[] = $riwayatPendidikan;

        return $this;
    }

    /**
     * Remove riwayatPendidikan
     *
     * @param \AppBundle\Entity\MahasiswaPendidikan $riwayatPendidikan
     */
    public function removeRiwayatPendidikan(\AppBundle\Entity\MahasiswaPendidikan $riwayatPendidikan)
    {
        $this->riwayatPendidikan->removeElement($riwayatPendidikan);
    }

    /**
     * Get riwayatPendidikan
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRiwayatPendidikan()
    {
        return $this->riwayatPendidikan;
    }

    /**
     * Set pembiayaan
     *
     * @param \AppBundle\Entity\Master $pembiayaan
     *
     * @return Mahasiswa
     */
    public function setPembiayaan(\AppBundle\Entity\Master $pembiayaan = null)
    {
        $this->pembiayaan = $pembiayaan;

        return $this;
    }

    /**
     * Get pembiayaan
     *
     * @return \AppBundle\Entity\Master
     */
    public function getPembiayaan()
    {
        return $this->pembiayaan;
    }

    /**
     * Add kr
     *
     * @param \AppBundle\Entity\Krs $kr
     *
     * @return Mahasiswa
     */
    public function addKr(\AppBundle\Entity\Krs $kr)
    {
        $this->krs[] = $kr;

        return $this;
    }

    /**
     * Remove kr
     *
     * @param \AppBundle\Entity\Krs $kr
     */
    public function removeKr(\AppBundle\Entity\Krs $kr)
    {
        $this->krs->removeElement($kr);
    }

    /**
     * Get krs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKrs()
    {
        return $this->krs;
    }

    /**
     * Add berka
     *
     * @param \AppBundle\Entity\Berkas $berka
     *
     * @return Mahasiswa
     */
    public function addBerka(\AppBundle\Entity\Berkas $berka)
    {
        $this->berkas[] = $berka;

        return $this;
    }

    /**
     * Remove berka
     *
     * @param \AppBundle\Entity\Berkas $berka
     */
    public function removeBerka(\AppBundle\Entity\Berkas $berka)
    {
        $this->berkas->removeElement($berka);
    }

    /**
     * Get berkas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBerkas()
    {
        return $this->berkas;
    }

    /**
     * Set trashed
     *
     * @param boolean $trashed
     *
     * @return Mahasiswa
     */
    public function setTrashed($trashed)
    {
        $this->trashed = $trashed;

        return $this;
    }

    /**
     * Get trashed
     *
     * @return boolean
     */
    public function getTrashed()
    {
        return $this->trashed;
    }
}
