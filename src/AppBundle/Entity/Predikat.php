<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Predikat
 *
 * @ORM\Table(name="predikat")
 * @ORM\Entity
 */
class Predikat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="predikat", type="string", length=255)
     */
    private $predikat;

    /**
     * @var string
     *
     * @ORM\Column(name="bobot_min", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $bobotMin;

    /**
     * @var string
     *
     * @ORM\Column(name="bobot_max", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $bobotMax;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set predikat
     *
     * @param string $predikat
     *
     * @return Predikat
     */
    public function setPredikat($predikat)
    {
        $this->predikat = $predikat;

        return $this;
    }

    /**
     * Get predikat
     *
     * @return string
     */
    public function getPredikat()
    {
        return $this->predikat;
    }

    /**
     * Set bobotMin
     *
     * @param string $bobotMin
     *
     * @return Predikat
     */
    public function setBobotMin($bobotMin)
    {
        $this->bobotMin = $bobotMin;

        return $this;
    }

    /**
     * Get bobotMin
     *
     * @return string
     */
    public function getBobotMin()
    {
        return $this->bobotMin;
    }

    /**
     * Set bobotMax
     *
     * @param string $bobotMax
     *
     * @return Predikat
     */
    public function setBobotMax($bobotMax)
    {
        $this->bobotMax = $bobotMax;

        return $this;
    }

    /**
     * Get bobotMax
     *
     * @return string
     */
    public function getBobotMax()
    {
        return $this->bobotMax;
    }
}

