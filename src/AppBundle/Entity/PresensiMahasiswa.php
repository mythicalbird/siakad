<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dosen
 *
 * @ORM\Table(name="presensi_mahasiswa")
 * @ORM\Entity
 */
class PresensiMahasiswa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Mahasiswa")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $mahasiswa;

    /**
     * @ORM\ManyToOne(targetEntity="KurikulumMakul")
     * @ORM\JoinColumn(name="id_kurikulum_makul", referencedColumnName="id")
     */
    private $makul;

    /**
     * @ORM\ManyToOne(targetEntity="JamKuliah")
     * @ORM\JoinColumn(name="id_jam", referencedColumnName="id")
     */
    private $jam;

    /**
     * @var date
     *
     * @ORM\Column(name="tanggal", type="date", length=255)
     */
    private $tanggal;

    /**
     * @ORM\Column(name="keterangan", type="string", length=10)
     */
    private $ket;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return PresensiMahasiswa
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }

    /**
     * Set ket
     *
     * @param string $ket
     *
     * @return PresensiMahasiswa
     */
    public function setKet($ket)
    {
        $this->ket = $ket;

        return $this;
    }

    /**
     * Get ket
     *
     * @return string
     */
    public function getKet()
    {
        return $this->ket;
    }

    /**
     * Set mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return PresensiMahasiswa
     */
    public function setMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa = null)
    {
        $this->mahasiswa = $mahasiswa;

        return $this;
    }

    /**
     * Get mahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }

    /**
     * Set makul
     *
     * @param \AppBundle\Entity\KurikulumMakul $makul
     *
     * @return PresensiMahasiswa
     */
    public function setMakul(\AppBundle\Entity\KurikulumMakul $makul = null)
    {
        $this->makul = $makul;

        return $this;
    }

    /**
     * Get makul
     *
     * @return \AppBundle\Entity\KurikulumMakul
     */
    public function getMakul()
    {
        return $this->makul;
    }

    /**
     * Set jam
     *
     * @param \AppBundle\Entity\JamKuliah $jam
     *
     * @return PresensiMahasiswa
     */
    public function setJam(\AppBundle\Entity\JamKuliah $jam = null)
    {
        $this->jam = $jam;

        return $this;
    }

    /**
     * Get jam
     *
     * @return \AppBundle\Entity\JamKuliah
     */
    public function getJam()
    {
        return $this->jam;
    }
}
