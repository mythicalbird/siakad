<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MahasiswaPendidikan
 *
 * @ORM\Table(name="mahasiswa_pendidikan")
 * @ORM\Entity
 */
class MahasiswaPendidikan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Mahasiswa", inversedBy="riwayatPendidikan")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $mahasiswa;

    /**
     * @ORM\Column(name="jenjang", type="string", length=100, nullable=true)
     */
    private $jenjang;

    /**
     * @var string
     *
     * @ORM\Column(name="bidang_ilmu", type="string", length=100, nullable=true)
     */
    private $bidangKeilmuan;

    /**
     * @var string
     *
     * @ORM\Column(name="gelarAkademik", type="string", length=100, nullable=true)
     */
    private $gelarAkademik;

    /**
     * @ORM\Column(name="perguruan_tinggi", type="string", length=100, nullable=true)
     */
    private $pt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tglIjazah", type="string", nullable=true)
     */
    private $tglIjazah;

    /**
     * @var string
     *
     * @ORM\Column(name="tahun", type="string", length=5, nullable=true)
     */
    private $tahun;

    /**
     * @var int
     *
     * @ORM\Column(name="sksLulus", type="integer", nullable=true)
     */
    private $sksLulus;

    /**
     * @var string
     *
     * @ORM\Column(name="ipk", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $ipk;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jenjang
     *
     * @param string $jenjang
     *
     * @return MahasiswaPendidikan
     */
    public function setJenjang($jenjang)
    {
        $this->jenjang = $jenjang;

        return $this;
    }

    /**
     * Get jenjang
     *
     * @return string
     */
    public function getJenjang()
    {
        return $this->jenjang;
    }

    /**
     * Set bidangKeilmuan
     *
     * @param string $bidangKeilmuan
     *
     * @return MahasiswaPendidikan
     */
    public function setBidangKeilmuan($bidangKeilmuan)
    {
        $this->bidangKeilmuan = $bidangKeilmuan;

        return $this;
    }

    /**
     * Get bidangKeilmuan
     *
     * @return string
     */
    public function getBidangKeilmuan()
    {
        return $this->bidangKeilmuan;
    }

    /**
     * Set gelarAkademik
     *
     * @param string $gelarAkademik
     *
     * @return MahasiswaPendidikan
     */
    public function setGelarAkademik($gelarAkademik)
    {
        $this->gelarAkademik = $gelarAkademik;

        return $this;
    }

    /**
     * Get gelarAkademik
     *
     * @return string
     */
    public function getGelarAkademik()
    {
        return $this->gelarAkademik;
    }

    /**
     * Set pt
     *
     * @param string $pt
     *
     * @return MahasiswaPendidikan
     */
    public function setPt($pt)
    {
        $this->pt = $pt;

        return $this;
    }

    /**
     * Get pt
     *
     * @return string
     */
    public function getPt()
    {
        return $this->pt;
    }

    /**
     * Set tglIjazah
     *
     * @param string $tglIjazah
     *
     * @return MahasiswaPendidikan
     */
    public function setTglIjazah($tglIjazah)
    {
        $this->tglIjazah = $tglIjazah;

        return $this;
    }

    /**
     * Get tglIjazah
     *
     * @return string
     */
    public function getTglIjazah()
    {
        return $this->tglIjazah;
    }

    /**
     * Set tahun
     *
     * @param string $tahun
     *
     * @return MahasiswaPendidikan
     */
    public function setTahun($tahun)
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get tahun
     *
     * @return string
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Set sksLulus
     *
     * @param integer $sksLulus
     *
     * @return MahasiswaPendidikan
     */
    public function setSksLulus($sksLulus)
    {
        $this->sksLulus = $sksLulus;

        return $this;
    }

    /**
     * Get sksLulus
     *
     * @return integer
     */
    public function getSksLulus()
    {
        return $this->sksLulus;
    }

    /**
     * Set ipk
     *
     * @param string $ipk
     *
     * @return MahasiswaPendidikan
     */
    public function setIpk($ipk)
    {
        $this->ipk = $ipk;

        return $this;
    }

    /**
     * Get ipk
     *
     * @return string
     */
    public function getIpk()
    {
        return $this->ipk;
    }

    /**
     * Set mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return MahasiswaPendidikan
     */
    public function setMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa = null)
    {
        $this->mahasiswa = $mahasiswa;

        return $this;
    }

    /**
     * Get mahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }
}
