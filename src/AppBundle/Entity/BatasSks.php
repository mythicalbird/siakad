<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BatasSks
 *
 * @ORM\Table(name="batas_sks")
 * @ORM\Entity
 */
class BatasSks
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ipk_min", type="decimal", precision=10, scale=2)
     */
    private $ipkMin;

    /**
     * @var string
     *
     * @ORM\Column(name="ipk_max", type="decimal", precision=10, scale=2)
     */
    private $ipkMax;

    /**
     * @var int
     *
     * @ORM\Column(name="jumlah_sks", type="integer", nullable=true)
     */
    private $jumlahSks;

    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ipkMin
     *
     * @param string $ipkMin
     *
     * @return BatasSks
     */
    public function setIpkMin($ipkMin)
    {
        $this->ipkMin = $ipkMin;

        return $this;
    }

    /**
     * Get ipkMin
     *
     * @return string
     */
    public function getIpkMin()
    {
        return $this->ipkMin;
    }

    /**
     * Set ipkMax
     *
     * @param string $ipkMax
     *
     * @return BatasSks
     */
    public function setIpkMax($ipkMax)
    {
        $this->ipkMax = $ipkMax;

        return $this;
    }

    /**
     * Get ipkMax
     *
     * @return string
     */
    public function getIpkMax()
    {
        return $this->ipkMax;
    }

    /**
     * Set jumlahSks
     *
     * @param integer $jumlahSks
     *
     * @return BatasSks
     */
    public function setJumlahSks($jumlahSks)
    {
        $this->jumlahSks = $jumlahSks;

        return $this;
    }

    /**
     * Get jumlahSks
     *
     * @return int
     */
    public function getJumlahSks()
    {
        return $this->jumlahSks;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return BatasSks
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }
}
