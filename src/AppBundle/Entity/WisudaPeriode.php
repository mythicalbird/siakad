<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * WisudaPeriode
 *
 * @ORM\Table(name="wisuda_periode")
 * @ORM\Entity
 */
class WisudaPeriode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
  
    /**
     * @ORM\Column(name="tahun", type="string", length=5)
     */
    private $tahun;

    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255)
     */
    private $nama;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_mulai", type="date")
     */
    private $tglMulai;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_selesai", type="date")
     */
    private $tglSelesai;
  
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_wisuda", type="date")
     */
    private $tglWisuda;

    /**
     * @ORM\OneToMany(targetEntity="Wisudawan", mappedBy="periode")
     */
    private $wisudawan;

    public function __construct() {
        $this->wisudawan = new ArrayCollection();
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tahun
     *
     * @param string $tahun
     *
     * @return WisudaPeriode
     */
    public function setTahun($tahun)
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get tahun
     *
     * @return string
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return WisudaPeriode
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set tglMulai
     *
     * @param \DateTime $tglMulai
     *
     * @return WisudaPeriode
     */
    public function setTglMulai($tglMulai)
    {
        $this->tglMulai = $tglMulai;

        return $this;
    }

    /**
     * Get tglMulai
     *
     * @return \DateTime
     */
    public function getTglMulai()
    {
        return $this->tglMulai;
    }

    /**
     * Set tglSelesai
     *
     * @param \DateTime $tglSelesai
     *
     * @return WisudaPeriode
     */
    public function setTglSelesai($tglSelesai)
    {
        $this->tglSelesai = $tglSelesai;

        return $this;
    }

    /**
     * Get tglSelesai
     *
     * @return \DateTime
     */
    public function getTglSelesai()
    {
        return $this->tglSelesai;
    }

    /**
     * Set tglWisuda
     *
     * @param \DateTime $tglWisuda
     *
     * @return WisudaPeriode
     */
    public function setTglWisuda($tglWisuda)
    {
        $this->tglWisuda = $tglWisuda;

        return $this;
    }

    /**
     * Get tglWisuda
     *
     * @return \DateTime
     */
    public function getTglWisuda()
    {
        return $this->tglWisuda;
    }

    /**
     * Add wisudawan
     *
     * @param \AppBundle\Entity\Wisudawan $wisudawan
     *
     * @return WisudaPeriode
     */
    public function addWisudawan(\AppBundle\Entity\Wisudawan $wisudawan)
    {
        $this->wisudawan[] = $wisudawan;

        return $this;
    }

    /**
     * Remove wisudawan
     *
     * @param \AppBundle\Entity\Wisudawan $wisudawan
     */
    public function removeWisudawan(\AppBundle\Entity\Wisudawan $wisudawan)
    {
        $this->wisudawan->removeElement($wisudawan);
    }

    /**
     * Get wisudawan
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWisudawan()
    {
        return $this->wisudawan;
    }
}
