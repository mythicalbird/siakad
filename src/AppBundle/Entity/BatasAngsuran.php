<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BatasAngsuran
 *
 * @ORM\Table(name="batas_angsuran")
 * @ORM\Entity
 */
class BatasAngsuran
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="semester", type="string", length=255, nullable=true)
     */
    private $semester;

    /**
     * @var string
     *
     * @ORM\Column(name="angsuran_ke", type="string", length=255, nullable=true)
     */
    private $angsuran;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tanggal", type="date", nullable=true)
     */
    private $tanggal;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set semester
     *
     * @param string $semester
     *
     * @return BatasAngsuran
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return string
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set angsuran
     *
     * @param string $angsuran
     *
     * @return BatasAngsuran
     */
    public function setAngsuran($angsuran)
    {
        $this->angsuran = $angsuran;

        return $this;
    }

    /**
     * Get angsuran
     *
     * @return string
     */
    public function getAngsuran()
    {
        return $this->angsuran;
    }

    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return BatasAngsuran
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }
}
