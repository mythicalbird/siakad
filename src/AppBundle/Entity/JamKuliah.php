<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JamKuliah
 *
 * @ORM\Table(name="jam_kuliah")
 * @ORM\Entity
 */
class JamKuliah
{
    /**
     * @var string
     *
     * @ORM\Column(name="jam", type="string", length=20)
     */
    private $jam;

    /**
     * @var time
     *
     * @ORM\Column(name="dari", type="time")
     */
    private $dari;

    /**
     * @var time
     *
     * @ORM\Column(name="sampai", type="time")
     */
    private $sampai;
  
    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Set jam
     *
     * @param string $jam
     *
     * @return JamKuliah
     */
    public function setJam($jam)
    {
        $this->jam = $jam;

        return $this;
    }

    /**
     * Get jam
     *
     * @return string
     */
    public function getJam()
    {
        return $this->jam;
    }

    /**
     * Set dari
     *
     * @param \DateTime $dari
     *
     * @return JamKuliah
     */
    public function setDari($dari)
    {
        $this->dari = $dari;

        return $this;
    }

    /**
     * Get dari
     *
     * @return \DateTime
     */
    public function getDari()
    {
        return $this->dari;
    }

    /**
     * Set sampai
     *
     * @param \DateTime $sampai
     *
     * @return JamKuliah
     */
    public function setSampai($sampai)
    {
        $this->sampai = $sampai;

        return $this;
    }

    /**
     * Get sampai
     *
     * @return \DateTime
     */
    public function getSampai()
    {
        return $this->sampai;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return JamKuliah
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }
}
