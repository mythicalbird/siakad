<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="bobot_nilai")
 * @ORM\Entity
 */
class BobotNilai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="predikat", type="string", length=100, nullable=false)
     */
    private $predikat;

    /**
     * @var integer
     *
     * @ORM\Column(name="bobot", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $bobot;
  
    /**
     * @ORM\Column(name="nilai_min", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $min;

    /**
     * @ORM\Column(name="nilai_max", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $max;
  
    /**
     * @ORM\Column(name="lulus", type="boolean")
     */
    private $lulus;

    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi;

    /**
     * @ORM\Column(name="global", type="boolean")
     */
    private $global;
  
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;




    /**
     * Set predikat
     *
     * @param string $predikat
     *
     * @return BobotNilai
     */
    public function setPredikat($predikat)
    {
        $this->predikat = $predikat;

        return $this;
    }

    /**
     * Get predikat
     *
     * @return string
     */
    public function getPredikat()
    {
        return $this->predikat;
    }

    /**
     * Set bobot
     *
     * @param string $bobot
     *
     * @return BobotNilai
     */
    public function setBobot($bobot)
    {
        $this->bobot = $bobot;

        return $this;
    }

    /**
     * Get bobot
     *
     * @return string
     */
    public function getBobot()
    {
        return $this->bobot;
    }

    /**
     * Set min
     *
     * @param string $min
     *
     * @return BobotNilai
     */
    public function setMin($min)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Get min
     *
     * @return string
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set max
     *
     * @param string $max
     *
     * @return BobotNilai
     */
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Get max
     *
     * @return string
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Set lulus
     *
     * @param boolean $lulus
     *
     * @return BobotNilai
     */
    public function setLulus($lulus)
    {
        $this->lulus = $lulus;

        return $this;
    }

    /**
     * Get lulus
     *
     * @return boolean
     */
    public function getLulus()
    {
        return $this->lulus;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return BobotNilai
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }

    /**
     * Set global
     *
     * @param boolean $global
     *
     * @return BobotNilai
     */
    public function setGlobal($global)
    {
        $this->global = $global;

        return $this;
    }

    /**
     * Get global
     *
     * @return boolean
     */
    public function getGlobal()
    {
        return $this->global;
    }
}
