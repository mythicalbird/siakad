<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dosen
 *
 * @ORM\Table(name="pegawai")
 * @ORM\Entity
 */
class Pegawai
{
    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="string", length=100, nullable=true)
     */
    private $kode;

    /**
     * @var string
     *
     * @ORM\Column(name="nip", type="string", length=100, nullable=true)
     */
    private $nip;

    /**
     * @var string
     *
     * @ORM\Column(name="status_perkawinan", type="string", length=150, nullable=true)
     */
    private $statusPerkawinan;

    /**
     * @var string
     *
     * @ORM\Column(name="jumlah_anak", type="integer", nullable=true)
     */
    private $jumlahAnak;

    /**
     * @var string
     *
     * @ORM\Column(name="pangkat", type="string", length=255, nullable=true)
     */
    private $pangkat;

    /**
     * @var date
     *
     * @ORM\Column(name="mulai_bekerja", type="date", length=255, nullable=true)
     */
    private $mulaiBekerja;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="dataPegawai")
     */
    private $user;

    /**
     * @ORM\Column(name="uuid", type="guid", nullable=true)
     */
    private $uuid;



    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return Pegawai
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set nip
     *
     * @param string $nip
     *
     * @return Pegawai
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set statusPerkawinan
     *
     * @param string $statusPerkawinan
     *
     * @return Pegawai
     */
    public function setStatusPerkawinan($statusPerkawinan)
    {
        $this->statusPerkawinan = $statusPerkawinan;

        return $this;
    }

    /**
     * Get statusPerkawinan
     *
     * @return string
     */
    public function getStatusPerkawinan()
    {
        return $this->statusPerkawinan;
    }

    /**
     * Set jumlahAnak
     *
     * @param integer $jumlahAnak
     *
     * @return Pegawai
     */
    public function setJumlahAnak($jumlahAnak)
    {
        $this->jumlahAnak = $jumlahAnak;

        return $this;
    }

    /**
     * Get jumlahAnak
     *
     * @return integer
     */
    public function getJumlahAnak()
    {
        return $this->jumlahAnak;
    }

    /**
     * Set pangkat
     *
     * @param string $pangkat
     *
     * @return Pegawai
     */
    public function setPangkat($pangkat)
    {
        $this->pangkat = $pangkat;

        return $this;
    }

    /**
     * Get pangkat
     *
     * @return string
     */
    public function getPangkat()
    {
        return $this->pangkat;
    }

    /**
     * Set mulaiBekerja
     *
     * @param \DateTime $mulaiBekerja
     *
     * @return Pegawai
     */
    public function setMulaiBekerja($mulaiBekerja)
    {
        $this->mulaiBekerja = $mulaiBekerja;

        return $this;
    }

    /**
     * Get mulaiBekerja
     *
     * @return \DateTime
     */
    public function getMulaiBekerja()
    {
        return $this->mulaiBekerja;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return Pegawai
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Pegawai
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
