<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SatuanPendidikan
 *
 * @ORM\Table(name="satuan_pendidikan")
 * @ORM\Entity
 */
class SatuanPendidikan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="jenis", type="string", length=100)
     */
    private $jenis; // sekolah or perguruan_tinggi

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_bp", referencedColumnName="id")
     */
    private $bp;

    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="string", length=100)
     */
    private $kode; //npsn

    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255)
     */
    private $nama;

    /**
     * @var string
     *
     * @ORM\Column(name="singkatan", type="string", length=255)
     */
    private $singkatan;

    /**
     * @var string
     *
     * @ORM\Column(name="alamat", type="text", nullable=true)
     */
    private $alamat;

    /**
     * @ORM\ManyToOne(targetEntity="Wilayah")
     * @ORM\JoinColumn(name="id_wilayah", referencedColumnName="id")
     */
    private $wilayah;

    /**
     * @var string
     *
     * @ORM\Column(name="pos", type="string", length=50, nullable=true)
     */
    private $pos;

    /**
     * @var string
     *
     * @ORM\Column(name="telp", type="string", length=100, nullable=true)
     */
    private $telp;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=100, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=255, nullable=true)
     */
    private $uuid;

    /**
     * @ORM\Column(name="raw", type="array", nullable=true)
     */
    private $raw;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jenis
     *
     * @param string $jenis
     *
     * @return SatuanPendidikan
     */
    public function setJenis($jenis)
    {
        $this->jenis = $jenis;

        return $this;
    }

    /**
     * Get jenis
     *
     * @return string
     */
    public function getJenis()
    {
        return $this->jenis;
    }

    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return SatuanPendidikan
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set pos
     *
     * @param string $pos
     *
     * @return SatuanPendidikan
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return string
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set telp
     *
     * @param string $telp
     *
     * @return SatuanPendidikan
     */
    public function setTelp($telp)
    {
        $this->telp = $telp;

        return $this;
    }

    /**
     * Get telp
     *
     * @return string
     */
    public function getTelp()
    {
        return $this->telp;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return SatuanPendidikan
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return SatuanPendidikan
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return SatuanPendidikan
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set wilayah
     *
     * @param \AppBundle\Entity\Wilayah $wilayah
     *
     * @return SatuanPendidikan
     */
    public function setWilayah(\AppBundle\Entity\Wilayah $wilayah = null)
    {
        $this->wilayah = $wilayah;

        return $this;
    }

    /**
     * Get wilayah
     *
     * @return \AppBundle\Entity\Wilayah
     */
    public function getWilayah()
    {
        return $this->wilayah;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return SatuanPendidikan
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set bp
     *
     * @param \AppBundle\Entity\Master $bp
     *
     * @return SatuanPendidikan
     */
    public function setBp(\AppBundle\Entity\Master $bp = null)
    {
        $this->bp = $bp;

        return $this;
    }

    /**
     * Get bp
     *
     * @return \AppBundle\Entity\Master
     */
    public function getBp()
    {
        return $this->bp;
    }

    /**
     * Set alamat
     *
     * @param string $alamat
     *
     * @return SatuanPendidikan
     */
    public function setAlamat($alamat)
    {
        $this->alamat = $alamat;

        return $this;
    }

    /**
     * Get alamat
     *
     * @return string
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return SatuanPendidikan
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set raw
     *
     * @param array $raw
     *
     * @return SatuanPendidikan
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return array
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Set singkatan
     *
     * @param string $singkatan
     *
     * @return SatuanPendidikan
     */
    public function setSingkatan($singkatan)
    {
        $this->singkatan = $singkatan;

        return $this;
    }

    /**
     * Get singkatan
     *
     * @return string
     */
    public function getSingkatan()
    {
        return $this->singkatan;
    }
}
