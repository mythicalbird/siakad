<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pendidikan
 *
 * @ORM\Table(name="dosen_pendidikan")
 * @ORM\Entity
 */
class DosenPendidikan
{
    /**
     * @ORM\ManyToOne(targetEntity="Dosen", inversedBy="pendidikan")
     * @ORM\JoinColumn(name="id_dosen", referencedColumnName="id")
     */
    private $dosen;

    /**
     * @var string
     *
     * @ORM\Column(name="jenjang", type="string", length=255, nullable=true)
     */
    private $jenjang;

    /**
     * @var string
     *
     * @ORM\Column(name="bidang_keilmuan", type="string", length=255, nullable=true)
     */
    private $bidangKeilmuan;

    /**
     * @var string
     *
     * @ORM\Column(name="gelar", type="string", length=20, nullable=true)
     */
    private $gelar;

    /**
     * @var string
     *
     * @ORM\Column(name="perguruan_tinggi", type="string", length=255, nullable=true)
     */
    private $perguruanTinggi;

    /**
     * @var string
     *
     * @ORM\Column(name="tgl_ijazah", type="date", nullable=true)
     */
    private $tglIjazah;

    /**
     * @var string
     *
     * @ORM\Column(name="tahun", type="string", length=20)
     */
    private $tahun;

    /**
     * @var string
     *
     * @ORM\Column(name="sks_lulus", type="string", length=20)
     */
    private $sksLulus;

    /**
     * @var string
     *
     * @ORM\Column(name="ipk_akhir", type="string", length=20)
     */
    private $ipkAkhir;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;




    /**
     * Set jenjang
     *
     * @param string $jenjang
     *
     * @return DosenPendidikan
     */
    public function setJenjang($jenjang)
    {
        $this->jenjang = $jenjang;

        return $this;
    }

    /**
     * Get jenjang
     *
     * @return string
     */
    public function getJenjang()
    {
        return $this->jenjang;
    }

    /**
     * Set bidangKeilmuan
     *
     * @param string $bidangKeilmuan
     *
     * @return DosenPendidikan
     */
    public function setBidangKeilmuan($bidangKeilmuan)
    {
        $this->bidangKeilmuan = $bidangKeilmuan;

        return $this;
    }

    /**
     * Get bidangKeilmuan
     *
     * @return string
     */
    public function getBidangKeilmuan()
    {
        return $this->bidangKeilmuan;
    }

    /**
     * Set gelar
     *
     * @param string $gelar
     *
     * @return DosenPendidikan
     */
    public function setGelar($gelar)
    {
        $this->gelar = $gelar;

        return $this;
    }

    /**
     * Get gelar
     *
     * @return string
     */
    public function getGelar()
    {
        return $this->gelar;
    }

    /**
     * Set perguruanTinggi
     *
     * @param string $perguruanTinggi
     *
     * @return DosenPendidikan
     */
    public function setPerguruanTinggi($perguruanTinggi)
    {
        $this->perguruanTinggi = $perguruanTinggi;

        return $this;
    }

    /**
     * Get perguruanTinggi
     *
     * @return string
     */
    public function getPerguruanTinggi()
    {
        return $this->perguruanTinggi;
    }

    /**
     * Set tglIjazah
     *
     * @param \DateTime $tglIjazah
     *
     * @return DosenPendidikan
     */
    public function setTglIjazah($tglIjazah)
    {
        $this->tglIjazah = $tglIjazah;

        return $this;
    }

    /**
     * Get tglIjazah
     *
     * @return \DateTime
     */
    public function getTglIjazah()
    {
        return $this->tglIjazah;
    }

    /**
     * Set tahun
     *
     * @param string $tahun
     *
     * @return DosenPendidikan
     */
    public function setTahun($tahun)
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get tahun
     *
     * @return string
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Set sksLulus
     *
     * @param string $sksLulus
     *
     * @return DosenPendidikan
     */
    public function setSksLulus($sksLulus)
    {
        $this->sksLulus = $sksLulus;

        return $this;
    }

    /**
     * Get sksLulus
     *
     * @return string
     */
    public function getSksLulus()
    {
        return $this->sksLulus;
    }

    /**
     * Set ipkAkhir
     *
     * @param string $ipkAkhir
     *
     * @return DosenPendidikan
     */
    public function setIpkAkhir($ipkAkhir)
    {
        $this->ipkAkhir = $ipkAkhir;

        return $this;
    }

    /**
     * Get ipkAkhir
     *
     * @return string
     */
    public function getIpkAkhir()
    {
        return $this->ipkAkhir;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dosen
     *
     * @param \AppBundle\Entity\Dosen $dosen
     *
     * @return DosenPendidikan
     */
    public function setDosen(\AppBundle\Entity\Dosen $dosen = null)
    {
        $this->dosen = $dosen;

        return $this;
    }

    /**
     * Get dosen
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getDosen()
    {
        return $this->dosen;
    }
}
