<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Gedung
 *
 * @ORM\Table(name="lokasi")
 * @ORM\Entity
 */
class Lokasi
{
    /**
     * @var string
     *
     * @ORM\Column(name="nama_lokasi", type="string", length=40, nullable=false)
     */
    private $namaLokasi;

    /**
     * @var string
     *
     * @ORM\Column(name="keterangan", type="text", length=65535, nullable=true)
     */
    private $keterangan;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Inventaris", mappedBy="lokasi")
     */
    private $barang;

    public function __construct() {
        $this->barang = new ArrayCollection();
    }


    /**
     * Set namaLokasi
     *
     * @param string $namaLokasi
     *
     * @return Lokasi
     */
    public function setNamaLokasi($namaLokasi)
    {
        $this->namaLokasi = $namaLokasi;

        return $this;
    }

    /**
     * Get namaLokasi
     *
     * @return string
     */
    public function getNamaLokasi()
    {
        return $this->namaLokasi;
    }

    /**
     * Set keterangan
     *
     * @param string $keterangan
     *
     * @return Lokasi
     */
    public function setKeterangan($keterangan)
    {
        $this->keterangan = $keterangan;

        return $this;
    }

    /**
     * Get keterangan
     *
     * @return string
     */
    public function getKeterangan()
    {
        return $this->keterangan;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add inventari
     *
     * @param \AppBundle\Entity\Inventaris $inventari
     *
     * @return Lokasi
     */
    public function addInventari(\AppBundle\Entity\Inventaris $inventari)
    {
        $this->inventaris[] = $inventari;

        return $this;
    }

    /**
     * Remove inventari
     *
     * @param \AppBundle\Entity\Inventaris $inventari
     */
    public function removeInventari(\AppBundle\Entity\Inventaris $inventari)
    {
        $this->inventaris->removeElement($inventari);
    }

    /**
     * Get inventaris
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventaris()
    {
        return $this->inventaris;
    }

    /**
     * Add barang
     *
     * @param \AppBundle\Entity\Inventaris $barang
     *
     * @return Lokasi
     */
    public function addBarang(\AppBundle\Entity\Inventaris $barang)
    {
        $this->barang[] = $barang;

        return $this;
    }

    /**
     * Remove barang
     *
     * @param \AppBundle\Entity\Inventaris $barang
     */
    public function removeBarang(\AppBundle\Entity\Inventaris $barang)
    {
        $this->barang->removeElement($barang);
    }

    /**
     * Get barang
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBarang()
    {
        return $this->barang;
    }
}
