<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wisuda
 *
 * @ORM\Table(name="wisuda")
 * @ORM\Entity
 */
class Wisudawan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="WisudaPeriode", inversedBy="wisudawan")
     * @ORM\JoinColumn(name="id_periode", referencedColumnName="id")
     */
    private $periode;

    /**
     * @ORM\ManyToOne(targetEntity="Mahasiswa")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $mahasiswa;

    /**
     * @var bool
     *
     * @ORM\Column(name="toga", type="boolean", nullable=true)
     */
    private $toga;

    /**
     * @var string
     *
     * @ORM\Column(name="ukuran_toga", type="string", length=100, nullable=true)
     */
    private $ukuranToga;

    /**
     * @var bool
     *
     * @ORM\Column(name="topi", type="boolean", nullable=true)
     */
    private $topi;

    /**
     * @var string
     *
     * @ORM\Column(name="ukuran_topi", type="string", length=100, nullable=true)
     */
    private $ukuranTopi;

    /**
     * @var bool
     *
     * @ORM\Column(name="kalung", type="boolean", nullable=true)
     */
    private $kalung;

    /**
     * @var bool
     *
     * @ORM\Column(name="undangan", type="boolean", nullable=true)
     */
    private $undangan;

    /**
     * @var bool
     *
     * @ORM\Column(name="photo", type="boolean", nullable=true)
     */
    private $photo;

    /**
     * @var bool
     *
     * @ORM\Column(name="ijazah", type="boolean", nullable=true)
     */
    private $ijazah;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set toga
     *
     * @param boolean $toga
     *
     * @return Wisudawan
     */
    public function setToga($toga)
    {
        $this->toga = $toga;

        return $this;
    }

    /**
     * Get toga
     *
     * @return boolean
     */
    public function getToga()
    {
        return $this->toga;
    }

    /**
     * Set ukuranToga
     *
     * @param string $ukuranToga
     *
     * @return Wisudawan
     */
    public function setUkuranToga($ukuranToga)
    {
        $this->ukuranToga = $ukuranToga;

        return $this;
    }

    /**
     * Get ukuranToga
     *
     * @return string
     */
    public function getUkuranToga()
    {
        return $this->ukuranToga;
    }

    /**
     * Set topi
     *
     * @param boolean $topi
     *
     * @return Wisudawan
     */
    public function setTopi($topi)
    {
        $this->topi = $topi;

        return $this;
    }

    /**
     * Get topi
     *
     * @return boolean
     */
    public function getTopi()
    {
        return $this->topi;
    }

    /**
     * Set ukuranTopi
     *
     * @param string $ukuranTopi
     *
     * @return Wisudawan
     */
    public function setUkuranTopi($ukuranTopi)
    {
        $this->ukuranTopi = $ukuranTopi;

        return $this;
    }

    /**
     * Get ukuranTopi
     *
     * @return string
     */
    public function getUkuranTopi()
    {
        return $this->ukuranTopi;
    }

    /**
     * Set kalung
     *
     * @param boolean $kalung
     *
     * @return Wisudawan
     */
    public function setKalung($kalung)
    {
        $this->kalung = $kalung;

        return $this;
    }

    /**
     * Get kalung
     *
     * @return boolean
     */
    public function getKalung()
    {
        return $this->kalung;
    }

    /**
     * Set undangan
     *
     * @param boolean $undangan
     *
     * @return Wisudawan
     */
    public function setUndangan($undangan)
    {
        $this->undangan = $undangan;

        return $this;
    }

    /**
     * Get undangan
     *
     * @return boolean
     */
    public function getUndangan()
    {
        return $this->undangan;
    }

    /**
     * Set photo
     *
     * @param boolean $photo
     *
     * @return Wisudawan
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return boolean
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set ijazah
     *
     * @param boolean $ijazah
     *
     * @return Wisudawan
     */
    public function setIjazah($ijazah)
    {
        $this->ijazah = $ijazah;

        return $this;
    }

    /**
     * Get ijazah
     *
     * @return boolean
     */
    public function getIjazah()
    {
        return $this->ijazah;
    }

    /**
     * Set periode
     *
     * @param \AppBundle\Entity\WisudaPeriode $periode
     *
     * @return Wisudawan
     */
    public function setPeriode(\AppBundle\Entity\WisudaPeriode $periode = null)
    {
        $this->periode = $periode;

        return $this;
    }

    /**
     * Get periode
     *
     * @return \AppBundle\Entity\WisudaPeriode
     */
    public function getPeriode()
    {
        return $this->periode;
    }

    /**
     * Set mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return Wisudawan
     */
    public function setMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa = null)
    {
        $this->mahasiswa = $mahasiswa;

        return $this;
    }

    /**
     * Get mahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }
}
