<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Makul
 *
 * @ORM\Table(name="makul")
 * @ORM\Entity
 */
class Makul
{
    /**
     * @var string
     *
     * @ORM\Column(name="nama_makul", type="string", length=100, nullable=false)
     */
    private $nama;

    /**
     * @var string
     *
     * @ORM\Column(name="nama_makul_eng", type="string", length=100, nullable=true)
     */
    private $namaEng;

    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="string", length=8)
     */
    private $kode;

    /**
     * @var integer
     *
     * @ORM\Column(name="sks_teori", type="integer", nullable=true)
     */
    private $sksTeori;

    /**
     * @var integer
     *
     * @ORM\Column(name="sks_praktek", type="integer", nullable=true)
     */
    private $sksPraktek;

    /**
     * @var integer
     *
     * @ORM\Column(name="sks_lapangan", type="integer", nullable=true)
     */
    private $sksLapangan;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_kelompok", referencedColumnName="id", nullable=true)
     */
    private $kelompok;
  
    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_jenis_makul", referencedColumnName="id", nullable=true)
     */
    private $jenis;

    /**
     * @var string
     *
     * @ORM\Column(name="silabus", type="boolean", nullable=true)
     */
    private $silabus;

    /**
     * @var string
     *
     * @ORM\Column(name="satuan_acara", type="boolean", nullable=true)
     */
    private $satuanAcara;

    /**
     * @var string
     *
     * @ORM\Column(name="bahan_ajar", type="boolean", nullable=true)
     */
    private $bahanAjar;

    /**
     * @var string
     *
     * @ORM\Column(name="diktat", type="boolean", nullable=true)
     */
    private $diktat;

    /**
     * @var string
     *
     * @ORM\Column(name="nilai_minimal", type="string", length=50, nullable=true)
     */
    private $nilaiMinimal;

    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi; // baru ditambahkan dari export feeder

    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_ta", referencedColumnName="id")
     */
    private $ta; // sebagai semester

    /**
     * @ORM\Column(name="status", type="string", length=100)
     */
    private $status = 'publish';

    /**
     * @ORM\Column(name="raw", type="array", nullable=true)
     */
    private $raw;

    /**
     * @ORM\Column(name="uuid", type="guid", nullable=true)
     */
    private $uuid;


    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return Makul
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set namaEng
     *
     * @param string $namaEng
     *
     * @return Makul
     */
    public function setNamaEng($namaEng)
    {
        $this->namaEng = $namaEng;

        return $this;
    }

    /**
     * Get namaEng
     *
     * @return string
     */
    public function getNamaEng()
    {
        return $this->namaEng;
    }

    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return Makul
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set sksTeori
     *
     * @param integer $sksTeori
     *
     * @return Makul
     */
    public function setSksTeori($sksTeori)
    {
        $this->sksTeori = $sksTeori;

        return $this;
    }

    /**
     * Get sksTeori
     *
     * @return integer
     */
    public function getSksTeori()
    {
        return $this->sksTeori;
    }

    /**
     * Set sksPraktek
     *
     * @param integer $sksPraktek
     *
     * @return Makul
     */
    public function setSksPraktek($sksPraktek)
    {
        $this->sksPraktek = $sksPraktek;

        return $this;
    }

    /**
     * Get sksPraktek
     *
     * @return integer
     */
    public function getSksPraktek()
    {
        return $this->sksPraktek;
    }

    /**
     * Set sksLapangan
     *
     * @param integer $sksLapangan
     *
     * @return Makul
     */
    public function setSksLapangan($sksLapangan)
    {
        $this->sksLapangan = $sksLapangan;

        return $this;
    }

    /**
     * Get sksLapangan
     *
     * @return integer
     */
    public function getSksLapangan()
    {
        return $this->sksLapangan;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set silabus
     *
     * @param boolean $silabus
     *
     * @return Makul
     */
    public function setSilabus($silabus)
    {
        $this->silabus = $silabus;

        return $this;
    }

    /**
     * Get silabus
     *
     * @return boolean
     */
    public function getSilabus()
    {
        return $this->silabus;
    }

    /**
     * Set satuanAcara
     *
     * @param boolean $satuanAcara
     *
     * @return Makul
     */
    public function setSatuanAcara($satuanAcara)
    {
        $this->satuanAcara = $satuanAcara;

        return $this;
    }

    /**
     * Get satuanAcara
     *
     * @return boolean
     */
    public function getSatuanAcara()
    {
        return $this->satuanAcara;
    }

    /**
     * Set bahanAjar
     *
     * @param boolean $bahanAjar
     *
     * @return Makul
     */
    public function setBahanAjar($bahanAjar)
    {
        $this->bahanAjar = $bahanAjar;

        return $this;
    }

    /**
     * Get bahanAjar
     *
     * @return boolean
     */
    public function getBahanAjar()
    {
        return $this->bahanAjar;
    }

    /**
     * Set diktat
     *
     * @param boolean $diktat
     *
     * @return Makul
     */
    public function setDiktat($diktat)
    {
        $this->diktat = $diktat;

        return $this;
    }

    /**
     * Get diktat
     *
     * @return boolean
     */
    public function getDiktat()
    {
        return $this->diktat;
    }

    /**
     * Set nilaiMinimal
     *
     * @param string $nilaiMinimal
     *
     * @return Makul
     */
    public function setNilaiMinimal($nilaiMinimal)
    {
        $this->nilaiMinimal = $nilaiMinimal;

        return $this;
    }

    /**
     * Get nilaiMinimal
     *
     * @return string
     */
    public function getNilaiMinimal()
    {
        return $this->nilaiMinimal;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Makul
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set raw
     *
     * @param array $raw
     *
     * @return Makul
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return array
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return Makul
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set kelompok
     *
     * @param \AppBundle\Entity\Master $kelompok
     *
     * @return Makul
     */
    public function setKelompok(\AppBundle\Entity\Master $kelompok = null)
    {
        $this->kelompok = $kelompok;

        return $this;
    }

    /**
     * Get kelompok
     *
     * @return \AppBundle\Entity\Master
     */
    public function getKelompok()
    {
        return $this->kelompok;
    }

    /**
     * Set jenis
     *
     * @param \AppBundle\Entity\Master $jenis
     *
     * @return Makul
     */
    public function setJenis(\AppBundle\Entity\Master $jenis = null)
    {
        $this->jenis = $jenis;

        return $this;
    }

    /**
     * Get jenis
     *
     * @return \AppBundle\Entity\Master
     */
    public function getJenis()
    {
        return $this->jenis;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return Makul
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }

    /**
     * Set ta
     *
     * @param \AppBundle\Entity\TahunAkademik $ta
     *
     * @return Makul
     */
    public function setTa(\AppBundle\Entity\TahunAkademik $ta = null)
    {
        $this->ta = $ta;

        return $this;
    }

    /**
     * Get ta
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getTa()
    {
        return $this->ta;
    }
}
