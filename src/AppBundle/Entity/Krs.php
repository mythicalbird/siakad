<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Krs
 *
 * @ORM\Table(name="krs")
 * @ORM\Entity
 */
class Krs
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Mahasiswa")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $mahasiswa; 

    /**
     * @ORM\ManyToOne(targetEntity="KurikulumMakul", inversedBy="krs")
     * @ORM\JoinColumn(name="id_makul", referencedColumnName="id")
     */
    private $makul;

    /**
     * @ORM\Column(name="semester", type="integer", length=1)
     */
    private $semester; // set otomatis dari makul semester

    /**
     * @ORM\Column(name="status", type="string", length=100, nullable=true)
     */
    private $status; // aktif, diterima, proses, trash

    /**
     * @ORM\ManyToOne(targetEntity="NilaiPerkuliahan", inversedBy="nilai")
     * @ORM\JoinColumn(name="id_nilai_perkuliahan", referencedColumnName="id")
     */
    private $nilaiAspek; // array

    /**
     * @var string
     *
     * @ORM\Column(name="nilai_huruf", type="string", length=1, nullable=true)
     */
    private $nilaiHuruf;

    /**
     * @var string
     *
     * @ORM\Column(name="nilai_angka", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $nilaiAngka;

    /**
     * @ORM\Column(name="nilai_akhir", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $nilaiAkhir;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Krs
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set nilaiHuruf
     *
     * @param string $nilaiHuruf
     *
     * @return Krs
     */
    public function setNilaiHuruf($nilaiHuruf)
    {
        $this->nilaiHuruf = $nilaiHuruf;

        return $this;
    }

    /**
     * Get nilaiHuruf
     *
     * @return string
     */
    public function getNilaiHuruf()
    {
        return $this->nilaiHuruf;
    }

    /**
     * Set nilaiAngka
     *
     * @param string $nilaiAngka
     *
     * @return Krs
     */
    public function setNilaiAngka($nilaiAngka)
    {
        $this->nilaiAngka = $nilaiAngka;

        return $this;
    }

    /**
     * Get nilaiAngka
     *
     * @return string
     */
    public function getNilaiAngka()
    {
        return $this->nilaiAngka;
    }

    /**
     * Set nilaiAkhir
     *
     * @param string $nilaiAkhir
     *
     * @return Krs
     */
    public function setNilaiAkhir($nilaiAkhir)
    {
        $this->nilaiAkhir = $nilaiAkhir;

        return $this;
    }

    /**
     * Get nilaiAkhir
     *
     * @return string
     */
    public function getNilaiAkhir()
    {
        return $this->nilaiAkhir;
    }

    /**
     * Set mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return Krs
     */
    public function setMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa = null)
    {
        $this->mahasiswa = $mahasiswa;

        return $this;
    }

    /**
     * Get mahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }

    /**
     * Set makul
     *
     * @param \AppBundle\Entity\KurikulumMakul $makul
     *
     * @return Krs
     */
    public function setMakul(\AppBundle\Entity\KurikulumMakul $makul = null)
    {
        $this->makul = $makul;

        return $this;
    }

    /**
     * Get makul
     *
     * @return \AppBundle\Entity\KurikulumMakul
     */
    public function getMakul()
    {
        return $this->makul;
    }

    /**
     * Set nilaiAspek
     *
     * @param \AppBundle\Entity\NilaiPerkuliahan $nilaiAspek
     *
     * @return Krs
     */
    public function setNilaiAspek(\AppBundle\Entity\NilaiPerkuliahan $nilaiAspek = null)
    {
        $this->nilaiAspek = $nilaiAspek;

        return $this;
    }

    /**
     * Get nilaiAspek
     *
     * @return \AppBundle\Entity\NilaiPerkuliahan
     */
    public function getNilaiAspek()
    {
        return $this->nilaiAspek;
    }

    /**
     * Set semester
     *
     * @param integer $semester
     *
     * @return Krs
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }
}
