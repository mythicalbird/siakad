<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dosen
 *
 * @ORM\Table(name="presensi_dosen")
 * @ORM\Entity
 */
class PresensiDosen
{
    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_ta", referencedColumnName="id")
     */
    private $ta;

    /**
     * @ORM\ManyToOne(targetEntity="Dosen")
     * @ORM\JoinColumn(name="id_dosen", referencedColumnName="id")
     */
    private $dosen;

    /**
     * @ORM\ManyToOne(targetEntity="JamKuliah")
     * @ORM\JoinColumn(name="id_jam", referencedColumnName="id")
     */
    private $jam;

    /**
     * @var date
     *
     * @ORM\Column(name="tanggal", type="date", length=255)
     */
    private $tanggal;

    /**
     * @ORM\Column(name="keterangan", type="string", length=10)
     */
    private $ket;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return PresensiDosen
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }

    /**
     * Set ket
     *
     * @param string $ket
     *
     * @return PresensiDosen
     */
    public function setKet($ket)
    {
        $this->ket = $ket;

        return $this;
    }

    /**
     * Get ket
     *
     * @return string
     */
    public function getKet()
    {
        return $this->ket;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ta
     *
     * @param \AppBundle\Entity\TahunAkademik $ta
     *
     * @return PresensiDosen
     */
    public function setTa(\AppBundle\Entity\TahunAkademik $ta = null)
    {
        $this->ta = $ta;

        return $this;
    }

    /**
     * Get ta
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getTa()
    {
        return $this->ta;
    }

    /**
     * Set dosen
     *
     * @param \AppBundle\Entity\Dosen $dosen
     *
     * @return PresensiDosen
     */
    public function setDosen(\AppBundle\Entity\Dosen $dosen = null)
    {
        $this->dosen = $dosen;

        return $this;
    }

    /**
     * Get dosen
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getDosen()
    {
        return $this->dosen;
    }

    /**
     * Set jam
     *
     * @param \AppBundle\Entity\JamKuliah $jam
     *
     * @return PresensiDosen
     */
    public function setJam(\AppBundle\Entity\JamKuliah $jam = null)
    {
        $this->jam = $jam;

        return $this;
    }

    /**
     * Get jam
     *
     * @return \AppBundle\Entity\JamKuliah
     */
    public function getJam()
    {
        return $this->jam;
    }
}
