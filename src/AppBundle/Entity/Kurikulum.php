<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Kurikulum
 *
 * @ORM\Table(name="kurikulum")
 * @ORM\Entity
 */
class Kurikulum
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255)
     */
    private $nama;

    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="string", length=255, nullable=true)
     */
    private $kode;

    /**
     * @var int
     *
     * @ORM\Column(name="sks_wajib", type="integer", nullable=true)
     */
    private $sksWajib;

    /**
     * @var int
     *
     * @ORM\Column(name="sks_pilihan", type="integer", nullable=true)
     */
    private $sksPilihan;

    /**
     * @var int
     *
     * @ORM\Column(name="jml_sks_lulus", type="integer", nullable=true)
     */
    private $jumlahSksLulus;

    /**
     * @var int
     *
     * @ORM\Column(name="jml_sks_wajib", type="integer", nullable=true)
     */
    private $jumlahSksWajib;

    /**
     * @var int
     *
     * @ORM\Column(name="jml_sks_pilihan", type="integer", nullable=true)
     */
    private $jumlahSksPilihan;

    /**
     * @var int
     *
     * @ORM\Column(name="jml_sem_normal", type="integer", nullable=true)
     */
    private $jumlahSemesterNormal;

    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi; 

    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_tahun_akademik", referencedColumnName="id")
     */
    private $ta;

    /**
     * @ORM\Column(name="raw", type="array", nullable=true)
     */
    private $raw;

    /**
     * @ORM\Column(name="uuid", type="guid", nullable=true)
     */
    private $uuid;

    /**
     * @ORM\Column(name="status", type="string", length=100)
     */
    private $status = 'publish';

    /**
     * @ORM\OneToMany(targetEntity="KurikulumMakul", mappedBy="kurikulum")
     */
    private $makuls;
	
    public function __construct() {
        $this->makuls = new ArrayCollection();
    }
	

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return Kurikulum
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return Kurikulum
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set sksWajib
     *
     * @param integer $sksWajib
     *
     * @return Kurikulum
     */
    public function setSksWajib($sksWajib)
    {
        $this->sksWajib = $sksWajib;

        return $this;
    }

    /**
     * Get sksWajib
     *
     * @return integer
     */
    public function getSksWajib()
    {
        return $this->sksWajib;
    }

    /**
     * Set sksPilihan
     *
     * @param integer $sksPilihan
     *
     * @return Kurikulum
     */
    public function setSksPilihan($sksPilihan)
    {
        $this->sksPilihan = $sksPilihan;

        return $this;
    }

    /**
     * Get sksPilihan
     *
     * @return integer
     */
    public function getSksPilihan()
    {
        return $this->sksPilihan;
    }

    /**
     * Set jumlahSksLulus
     *
     * @param integer $jumlahSksLulus
     *
     * @return Kurikulum
     */
    public function setJumlahSksLulus($jumlahSksLulus)
    {
        $this->jumlahSksLulus = $jumlahSksLulus;

        return $this;
    }

    /**
     * Get jumlahSksLulus
     *
     * @return integer
     */
    public function getJumlahSksLulus()
    {
        return $this->jumlahSksLulus;
    }

    /**
     * Set jumlahSksWajib
     *
     * @param integer $jumlahSksWajib
     *
     * @return Kurikulum
     */
    public function setJumlahSksWajib($jumlahSksWajib)
    {
        $this->jumlahSksWajib = $jumlahSksWajib;

        return $this;
    }

    /**
     * Get jumlahSksWajib
     *
     * @return integer
     */
    public function getJumlahSksWajib()
    {
        return $this->jumlahSksWajib;
    }

    /**
     * Set jumlahSemesterNormal
     *
     * @param integer $jumlahSemesterNormal
     *
     * @return Kurikulum
     */
    public function setJumlahSemesterNormal($jumlahSemesterNormal)
    {
        $this->jumlahSemesterNormal = $jumlahSemesterNormal;

        return $this;
    }

    /**
     * Get jumlahSemesterNormal
     *
     * @return integer
     */
    public function getJumlahSemesterNormal()
    {
        return $this->jumlahSemesterNormal;
    }

    /**
     * Set jumlahSksPilihan
     *
     * @param integer $jumlahSksPilihan
     *
     * @return Kurikulum
     */
    public function setJumlahSksPilihan($jumlahSksPilihan)
    {
        $this->jumlahSksPilihan = $jumlahSksPilihan;

        return $this;
    }

    /**
     * Get jumlahSksPilihan
     *
     * @return integer
     */
    public function getJumlahSksPilihan()
    {
        return $this->jumlahSksPilihan;
    }

    /**
     * Set raw
     *
     * @param array $raw
     *
     * @return Kurikulum
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return array
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return Kurikulum
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Kurikulum
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return Kurikulum
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }

    /**
     * Set ta
     *
     * @param \AppBundle\Entity\TahunAkademik $ta
     *
     * @return Kurikulum
     */
    public function setTa(\AppBundle\Entity\TahunAkademik $ta = null)
    {
        $this->ta = $ta;

        return $this;
    }

    /**
     * Get ta
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getTa()
    {
        return $this->ta;
    }

    /**
     * Add makul
     *
     * @param \AppBundle\Entity\KurikulumMakul $makul
     *
     * @return Kurikulum
     */
    public function addMakul(\AppBundle\Entity\KurikulumMakul $makul)
    {
        $this->makuls[] = $makul;

        return $this;
    }

    /**
     * Remove makul
     *
     * @param \AppBundle\Entity\KurikulumMakul $makul
     */
    public function removeMakul(\AppBundle\Entity\KurikulumMakul $makul)
    {
        $this->makuls->removeElement($makul);
    }

    /**
     * Get makuls
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMakuls()
    {
        return $this->makuls;
    }
}
