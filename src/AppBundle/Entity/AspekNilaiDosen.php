<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="aspek_nilai_dosen")
 * @ORM\Entity
 */
class AspekNilaiDosen
{
    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_th_akademik", referencedColumnName="id")
     */
    private $tahun;

    /**
     * @ORM\ManyToOne(targetEntity="Makul")
     * @ORM\JoinColumn(name="id_makul", referencedColumnName="id")
     */
    private $makul;

    /**
     * @var integer
     *
     * @ORM\Column(name="aspek_nilai", type="string", length=255)
     */
    private $nama;
  
    /**
     * @ORM\Column(name="persen", type="integer")
     */
    private $persen;

    /**
     * @ORM\Column(name="ket", type="text", nullable=true)
     */
    private $ket;

    /**
     * @ORM\Column(name="default", type="boolean")
     */
    private $default;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return AspekNilai
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set persen
     *
     * @param integer $persen
     *
     * @return AspekNilai
     */
    public function setPersen($persen)
    {
        $this->persen = $persen;

        return $this;
    }

    /**
     * Get persen
     *
     * @return integer
     */
    public function getPersen()
    {
        return $this->persen;
    }

    /**
     * Set ket
     *
     * @param string $ket
     *
     * @return AspekNilai
     */
    public function setKet($ket)
    {
        $this->ket = $ket;

        return $this;
    }

    /**
     * Get ket
     *
     * @return string
     */
    public function getKet()
    {
        return $this->ket;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tahun
     *
     * @param \AppBundle\Entity\TahunAkademik $tahun
     *
     * @return AspekNilai
     */
    public function setTahun(\AppBundle\Entity\TahunAkademik $tahun = null)
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get tahun
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Set default
     *
     * @param boolean $default
     *
     * @return AspekNilai
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return boolean
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set makul
     *
     * @param \AppBundle\Entity\Makul $makul
     *
     * @return AspekNilai
     */
    public function setMakul(\AppBundle\Entity\Makul $makul = null)
    {
        $this->makul = $makul;

        return $this;
    }

    /**
     * Get makul
     *
     * @return \AppBundle\Entity\Makul
     */
    public function getMakul()
    {
        return $this->makul;
    }
}
