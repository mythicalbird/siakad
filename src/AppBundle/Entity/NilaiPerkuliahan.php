<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dosen
 *
 * @ORM\Table(name="nilai_perkuliahan")
 * @ORM\Entity
 */
class NilaiPerkuliahan
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Krs", inversedBy="nilai")
     * @ORM\JoinColumn(name="id_krs", referencedColumnName="id")
     */
    private $krs;

    /**
     * @ORM\ManyToOne(targetEntity="AspekNilai")
     * @ORM\JoinColumn(name="id_aspek_nilai", referencedColumnName="id")
     */
    private $aspekNilai;

    /**
     * @ORM\ManyToOne(targetEntity="AspekNilaiDosen")
     * @ORM\JoinColumn(name="id_aspek_nilai_dosen", referencedColumnName="id")
     */
    private $aspekNilaiDosen;

    /**
     * @var string
     *
     * @ORM\Column(name="nilai", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $nilai;

    /**
     * @ORM\ManyToOne(targetEntity="BobotNilai")
     * @ORM\JoinColumn(name="id_bobot_nilai", referencedColumnName="id")
     */
    private $bobot;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nilai
     *
     * @param string $nilai
     *
     * @return NilaiPerkuliahan
     */
    public function setNilai($nilai)
    {
        $this->nilai = $nilai;

        return $this;
    }

    /**
     * Get nilai
     *
     * @return string
     */
    public function getNilai()
    {
        return $this->nilai;
    }

    /**
     * Set krs
     *
     * @param \AppBundle\Entity\Krs $krs
     *
     * @return NilaiPerkuliahan
     */
    public function setKrs(\AppBundle\Entity\Krs $krs = null)
    {
        $this->krs = $krs;

        return $this;
    }

    /**
     * Get krs
     *
     * @return \AppBundle\Entity\Krs
     */
    public function getKrs()
    {
        return $this->krs;
    }

    /**
     * Set aspekNilai
     *
     * @param \AppBundle\Entity\AspekNilai $aspekNilai
     *
     * @return NilaiPerkuliahan
     */
    public function setAspekNilai(\AppBundle\Entity\AspekNilai $aspekNilai = null)
    {
        $this->aspekNilai = $aspekNilai;

        return $this;
    }

    /**
     * Get aspekNilai
     *
     * @return \AppBundle\Entity\AspekNilai
     */
    public function getAspekNilai()
    {
        return $this->aspekNilai;
    }

    /**
     * Set aspekNilaiDosen
     *
     * @param \AppBundle\Entity\AspekNilaiDosen $aspekNilaiDosen
     *
     * @return NilaiPerkuliahan
     */
    public function setAspekNilaiDosen(\AppBundle\Entity\AspekNilaiDosen $aspekNilaiDosen = null)
    {
        $this->aspekNilaiDosen = $aspekNilaiDosen;

        return $this;
    }

    /**
     * Get aspekNilaiDosen
     *
     * @return \AppBundle\Entity\AspekNilaiDosen
     */
    public function getAspekNilaiDosen()
    {
        return $this->aspekNilaiDosen;
    }

    /**
     * Set bobot
     *
     * @param \AppBundle\Entity\BobotNilai $bobot
     *
     * @return NilaiPerkuliahan
     */
    public function setBobot(\AppBundle\Entity\BobotNilai $bobot = null)
    {
        $this->bobot = $bobot;

        return $this;
    }

    /**
     * Get bobot
     *
     * @return \AppBundle\Entity\BobotNilai
     */
    public function getBobot()
    {
        return $this->bobot;
    }
}
