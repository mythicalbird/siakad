<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Kelas
 *
 * @ORM\Table(name="kelas")
 * @ORM\Entity
 */
class Kelas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255)
     */
    private $nama;

    /**
     * @var string
     *
     * @ORM\Column(name="ruangan", type="string", length=255, nullable=true)
     */
    private $ruangan;

    /**
     * @var int
     *
     * @ORM\Column(name="kapasitas", type="integer", nullable=true)
     */
    private $kapasitas;

    /**
     * @ORM\ManyToOne(targetEntity="ProgramStudi")
     * @ORM\JoinColumn(name="id_prodi", referencedColumnName="id")
     */
    private $prodi;

    /**
     * @ORM\Column(name="uuid", type="guid", nullable=true)
     */
    private $uuid;

    /**
     * @ORM\Column(name="status", type="string", length=100)
     */
    private $status = 'publish';

    /**
     * @ORM\OneToMany(targetEntity="KurikulumMakul", mappedBy="kelas")
     */
    private $makulKurikulum;

    public function __construct() {
        $this->makulKurikulum = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return Kelas
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set ruangan
     *
     * @param string $ruangan
     *
     * @return Kelas
     */
    public function setRuangan($ruangan)
    {
        $this->ruangan = $ruangan;

        return $this;
    }

    /**
     * Get ruangan
     *
     * @return string
     */
    public function getRuangan()
    {
        return $this->ruangan;
    }

    /**
     * Set kapasitas
     *
     * @param integer $kapasitas
     *
     * @return Kelas
     */
    public function setKapasitas($kapasitas)
    {
        $this->kapasitas = $kapasitas;

        return $this;
    }

    /**
     * Get kapasitas
     *
     * @return integer
     */
    public function getKapasitas()
    {
        return $this->kapasitas;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return Kelas
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Kelas
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set prodi
     *
     * @param \AppBundle\Entity\ProgramStudi $prodi
     *
     * @return Kelas
     */
    public function setProdi(\AppBundle\Entity\ProgramStudi $prodi = null)
    {
        $this->prodi = $prodi;

        return $this;
    }

    /**
     * Get prodi
     *
     * @return \AppBundle\Entity\ProgramStudi
     */
    public function getProdi()
    {
        return $this->prodi;
    }

    /**
     * Add makulKurikulum
     *
     * @param \AppBundle\Entity\KurikulumMakul $makulKurikulum
     *
     * @return Kelas
     */
    public function addMakulKurikulum(\AppBundle\Entity\KurikulumMakul $makulKurikulum)
    {
        $this->makulKurikulum[] = $makulKurikulum;

        return $this;
    }

    /**
     * Remove makulKurikulum
     *
     * @param \AppBundle\Entity\KurikulumMakul $makulKurikulum
     */
    public function removeMakulKurikulum(\AppBundle\Entity\KurikulumMakul $makulKurikulum)
    {
        $this->makulKurikulum->removeElement($makulKurikulum);
    }

    /**
     * Get makulKurikulum
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMakulKurikulum()
    {
        return $this->makulKurikulum;
    }
}
