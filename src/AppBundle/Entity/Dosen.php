<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Dosen
 *
 * @ORM\Table(name="dosen")
 * @ORM\Entity
 */
class Dosen
{
    /**
     * @var string
     *
     * @ORM\Column(name="nip", type="string", length=100, nullable=true)
     */
    private $nip;

    /**
     * @var string
     *
     * @ORM\Column(name="nidn", type="string", length=100, nullable=true)
     */
    private $nidn;

    /**
     * @var string
     *
     * @ORM\Column(name="tgl_mulai_kerja", type="date", nullable=true)
     */
    private $tglMulaiKerja;
  
    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_th_akademik", referencedColumnName="id")
     */
    private $semesterMulaiKerja;
  
    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_pendidikan", referencedColumnName="id")
     */
    private $pendidikanTertinggi;
  
    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_institusi_induk", referencedColumnName="id")
     */
    private $institusiInduk;
  
    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=150, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(name="aktif", type="boolean", nullable=true)
     */
    private $aktif;

    /**
     * @var string
     *
     * @ORM\Column(name="nama_ibu_kandung", type="string", length=150, nullable=true)
     */
    private $namaIbuKandung;
  
    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_status_ikatan_kerja", referencedColumnName="id")
     */
    private $statusKerja;
  
    /**
     * @ORM\Column(name="jabatan_akademik", type="string", length=255, nullable=true)
     */
    private $jabatanAkademik;
  
    /**
     * @ORM\Column(name="jabatan_fungsional", type="string", length=255, nullable=true)
     */
    private $jabatanFungsional;
  
    /**
     * @ORM\Column(name="jabatan_struktural", type="string", length=255, nullable=true)
     */
    private $jabatanStruktural;
  
    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_pangkat_golongan", referencedColumnName="id")
     */
    private $pangkatGolongan;

    /**
     * @ORM\OneToMany(targetEntity="DosenPendidikan", mappedBy="dosen")
     */
    private $pendidikan;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="dataDosen")
     */
    private $user;

    /**
     * @ORM\Column(name="uuid", type="guid", nullable=true)
     */
    private $uuid; // id_sdm

    /**
     * @ORM\Column(name="id_reg_ptk", type="guid", nullable=true)
     */
    private $regPtk; // id_reg_ptk

    /**
     * @ORM\Column(name="raw", type="array", nullable=true)
     */
    private $raw;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="DosenPelatihan", mappedBy="dosen")
     */
    private $pelatihan;

    /**
     * @ORM\OneToMany(targetEntity="DosenPengampu", mappedBy="dosen")
     */
    private $makulDiampu;

    public function __construct() {
        $this->makulDiampu = new ArrayCollection();
        $this->pendidikan = new ArrayCollection();
        $this->pelatihan = new ArrayCollection();
    }





    /**
     * Set nip
     *
     * @param string $nip
     *
     * @return Dosen
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set tglMulaiKerja
     *
     * @param \DateTime $tglMulaiKerja
     *
     * @return Dosen
     */
    public function setTglMulaiKerja($tglMulaiKerja)
    {
        $this->tglMulaiKerja = $tglMulaiKerja;

        return $this;
    }

    /**
     * Get tglMulaiKerja
     *
     * @return \DateTime
     */
    public function getTglMulaiKerja()
    {
        return $this->tglMulaiKerja;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Dosen
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set aktif
     *
     * @param boolean $aktif
     *
     * @return Dosen
     */
    public function setAktif($aktif)
    {
        $this->aktif = $aktif;

        return $this;
    }

    /**
     * Get aktif
     *
     * @return boolean
     */
    public function getAktif()
    {
        return $this->aktif;
    }

    /**
     * Set namaIbuKandung
     *
     * @param string $namaIbuKandung
     *
     * @return Dosen
     */
    public function setNamaIbuKandung($namaIbuKandung)
    {
        $this->namaIbuKandung = $namaIbuKandung;

        return $this;
    }

    /**
     * Get namaIbuKandung
     *
     * @return string
     */
    public function getNamaIbuKandung()
    {
        return $this->namaIbuKandung;
    }

    /**
     * Set jabatanAkademik
     *
     * @param string $jabatanAkademik
     *
     * @return Dosen
     */
    public function setJabatanAkademik($jabatanAkademik)
    {
        $this->jabatanAkademik = $jabatanAkademik;

        return $this;
    }

    /**
     * Get jabatanAkademik
     *
     * @return string
     */
    public function getJabatanAkademik()
    {
        return $this->jabatanAkademik;
    }

    /**
     * Set jabatanFungsional
     *
     * @param string $jabatanFungsional
     *
     * @return Dosen
     */
    public function setJabatanFungsional($jabatanFungsional)
    {
        $this->jabatanFungsional = $jabatanFungsional;

        return $this;
    }

    /**
     * Get jabatanFungsional
     *
     * @return string
     */
    public function getJabatanFungsional()
    {
        return $this->jabatanFungsional;
    }

    /**
     * Set jabatanStruktural
     *
     * @param string $jabatanStruktural
     *
     * @return Dosen
     */
    public function setJabatanStruktural($jabatanStruktural)
    {
        $this->jabatanStruktural = $jabatanStruktural;

        return $this;
    }

    /**
     * Get jabatanStruktural
     *
     * @return string
     */
    public function getJabatanStruktural()
    {
        return $this->jabatanStruktural;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return Dosen
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set regPtk
     *
     * @param guid $regPtk
     *
     * @return Dosen
     */
    public function setRegPtk($regPtk)
    {
        $this->regPtk = $regPtk;

        return $this;
    }

    /**
     * Get regPtk
     *
     * @return guid
     */
    public function getRegPtk()
    {
        return $this->regPtk;
    }

    /**
     * Set raw
     *
     * @param array $raw
     *
     * @return Dosen
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return array
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set semesterMulaiKerja
     *
     * @param \AppBundle\Entity\TahunAkademik $semesterMulaiKerja
     *
     * @return Dosen
     */
    public function setSemesterMulaiKerja(\AppBundle\Entity\TahunAkademik $semesterMulaiKerja = null)
    {
        $this->semesterMulaiKerja = $semesterMulaiKerja;

        return $this;
    }

    /**
     * Get semesterMulaiKerja
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getSemesterMulaiKerja()
    {
        return $this->semesterMulaiKerja;
    }

    /**
     * Set pendidikanTertinggi
     *
     * @param \AppBundle\Entity\Master $pendidikanTertinggi
     *
     * @return Dosen
     */
    public function setPendidikanTertinggi(\AppBundle\Entity\Master $pendidikanTertinggi = null)
    {
        $this->pendidikanTertinggi = $pendidikanTertinggi;

        return $this;
    }

    /**
     * Get pendidikanTertinggi
     *
     * @return \AppBundle\Entity\Master
     */
    public function getPendidikanTertinggi()
    {
        return $this->pendidikanTertinggi;
    }

    /**
     * Set institusiInduk
     *
     * @param \AppBundle\Entity\Master $institusiInduk
     *
     * @return Dosen
     */
    public function setInstitusiInduk(\AppBundle\Entity\Master $institusiInduk = null)
    {
        $this->institusiInduk = $institusiInduk;

        return $this;
    }

    /**
     * Get institusiInduk
     *
     * @return \AppBundle\Entity\Master
     */
    public function getInstitusiInduk()
    {
        return $this->institusiInduk;
    }

    /**
     * Set statusKerja
     *
     * @param \AppBundle\Entity\Master $statusKerja
     *
     * @return Dosen
     */
    public function setStatusKerja(\AppBundle\Entity\Master $statusKerja = null)
    {
        $this->statusKerja = $statusKerja;

        return $this;
    }

    /**
     * Get statusKerja
     *
     * @return \AppBundle\Entity\Master
     */
    public function getStatusKerja()
    {
        return $this->statusKerja;
    }

    /**
     * Set pangkatGolongan
     *
     * @param \AppBundle\Entity\Master $pangkatGolongan
     *
     * @return Dosen
     */
    public function setPangkatGolongan(\AppBundle\Entity\Master $pangkatGolongan = null)
    {
        $this->pangkatGolongan = $pangkatGolongan;

        return $this;
    }

    /**
     * Get pangkatGolongan
     *
     * @return \AppBundle\Entity\Master
     */
    public function getPangkatGolongan()
    {
        return $this->pangkatGolongan;
    }

    /**
     * Add pendidikan
     *
     * @param \AppBundle\Entity\DosenPendidikan $pendidikan
     *
     * @return Dosen
     */
    public function addPendidikan(\AppBundle\Entity\DosenPendidikan $pendidikan)
    {
        $this->pendidikan[] = $pendidikan;

        return $this;
    }

    /**
     * Remove pendidikan
     *
     * @param \AppBundle\Entity\DosenPendidikan $pendidikan
     */
    public function removePendidikan(\AppBundle\Entity\DosenPendidikan $pendidikan)
    {
        $this->pendidikan->removeElement($pendidikan);
    }

    /**
     * Get pendidikan
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPendidikan()
    {
        return $this->pendidikan;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Dosen
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add pelatihan
     *
     * @param \AppBundle\Entity\DosenPelatihan $pelatihan
     *
     * @return Dosen
     */
    public function addPelatihan(\AppBundle\Entity\DosenPelatihan $pelatihan)
    {
        $this->pelatihan[] = $pelatihan;

        return $this;
    }

    /**
     * Remove pelatihan
     *
     * @param \AppBundle\Entity\DosenPelatihan $pelatihan
     */
    public function removePelatihan(\AppBundle\Entity\DosenPelatihan $pelatihan)
    {
        $this->pelatihan->removeElement($pelatihan);
    }

    /**
     * Get pelatihan
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPelatihan()
    {
        return $this->pelatihan;
    }

    /**
     * Add makulDiampu
     *
     * @param \AppBundle\Entity\DosenPengampu $makulDiampu
     *
     * @return Dosen
     */
    public function addMakulDiampu(\AppBundle\Entity\DosenPengampu $makulDiampu)
    {
        $this->makulDiampu[] = $makulDiampu;

        return $this;
    }

    /**
     * Remove makulDiampu
     *
     * @param \AppBundle\Entity\DosenPengampu $makulDiampu
     */
    public function removeMakulDiampu(\AppBundle\Entity\DosenPengampu $makulDiampu)
    {
        $this->makulDiampu->removeElement($makulDiampu);
    }

    /**
     * Get makulDiampu
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMakulDiampu()
    {
        return $this->makulDiampu;
    }

    /**
     * Set nidn
     *
     * @param string $nidn
     *
     * @return Dosen
     */
    public function setNidn($nidn)
    {
        $this->nidn = $nidn;

        return $this;
    }

    /**
     * Get nidn
     *
     * @return string
     */
    public function getNidn()
    {
        return $this->nidn;
    }
}
