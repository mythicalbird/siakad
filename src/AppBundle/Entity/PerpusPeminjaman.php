<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PerpusPeminjaman
 *
 * @ORM\Table(name="perpus_peminjaman")
 * @ORM\Entity
 */
class PerpusPeminjaman
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tanggal", type="datetime", nullable=true)
     */
    private $tanggal; 

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tanggal_kembali", type="datetime", nullable=true)
     */
    private $tanggalKembali; 


    /**
     * @ORM\ManyToOne(targetEntity="PerpusPustaka")
     * @ORM\JoinColumn(name="id_pustaka", referencedColumnName="id")
     */
    private $pustaka;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="id_anggota", referencedColumnName="id")
     */
    private $anggota;

    /**
     * @ORM\Column(name="sudah_kembali", type="boolean")
     */
    private $sudahKembali; 

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return PerpusPeminjaman
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }

    /**
     * Set pustaka
     *
     * @param \AppBundle\Entity\PerpusPustaka $pustaka
     *
     * @return PerpusPeminjaman
     */
    public function setPustaka(\AppBundle\Entity\PerpusPustaka $pustaka = null)
    {
        $this->pustaka = $pustaka;

        return $this;
    }

    /**
     * Get pustaka
     *
     * @return \AppBundle\Entity\PerpusPustaka
     */
    public function getPustaka()
    {
        return $this->pustaka;
    }

    /**
     * Set anggota
     *
     * @param \AppBundle\Entity\User $anggota
     *
     * @return PerpusPeminjaman
     */
    public function setAnggota(\AppBundle\Entity\User $anggota = null)
    {
        $this->anggota = $anggota;

        return $this;
    }

    /**
     * Get anggota
     *
     * @return \AppBundle\Entity\User
     */
    public function getAnggota()
    {
        return $this->anggota;
    }

    /**
     * Set sudahKembali
     *
     * @param boolean $sudahKembali
     *
     * @return PerpusPeminjaman
     */
    public function setSudahKembali($sudahKembali)
    {
        $this->sudahKembali = $sudahKembali;

        return $this;
    }

    /**
     * Get sudahKembali
     *
     * @return boolean
     */
    public function getSudahKembali()
    {
        return $this->sudahKembali;
    }

    /**
     * Set tanggalKembali
     *
     * @param \DateTime $tanggalKembali
     *
     * @return PerpusPeminjaman
     */
    public function setTanggalKembali($tanggalKembali)
    {
        $this->tanggalKembali = $tanggalKembali;

        return $this;
    }

    /**
     * Get tanggalKembali
     *
     * @return \DateTime
     */
    public function getTanggalKembali()
    {
        return $this->tanggalKembali;
    }
}
