<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TahunAkademik
 *
 * @ORM\Table(name="tahun_akademik")
 * @ORM\Entity
 */
class TahunAkademik
{
    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="string", length=20)
     */
    private $kode;

    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255)
     */
    private $nama;

    /**
     * @var string
     *
     * @ORM\Column(name="tahun", type="string", length=5)
     */
    private $tahun;

    /**
     * @var string
     *
     * @ORM\Column(name="semester", type="integer", length=2)
     */
    private $kodeSemester;

    /**
     * @var string
     *
     * @ORM\Column(name="aktif", type="boolean")
     */
    private $aktif;
  
    /**
     * @ORM\Column(name="batas_krs", type="string", length=150, nullable=true)
     */
    private $batasKrs;
  
    /**
     * @ORM\Column(name="batas_krs_online", type="string", length=150, nullable=true)
     */
    private $batasKrsOnline;
    /**
     * @ORM\Column(name="batas_ubah_krs", type="string", length=150, nullable=true)
     */
    private $batasUbahKrs;

    /**
     * @ORM\Column(name="batas_cetak_kss", type="string", length=150, nullable=true)
     */
    private $batasCetakKss;

    /**
     * @ORM\Column(name="batas_pembayaran", type="string", length=150, nullable=true)
     */
    private $batasPembayaran;
  
    /**
     * @ORM\Column(name="perkuliahan", type="string", length=150, nullable=true)
     */
    private $perkuliahan;
  
    /**
     * @ORM\Column(name="batas_uts", type="string", length=150, nullable=true)
     */
    private $batasUts;
  
    /**
     * @ORM\Column(name="batas_uas", type="string", length=150, nullable=true)
     */
    private $batasUas;

    /**
     * @ORM\Column(name="boleh_cuti", type="date", nullable=true)
     */
    private $bolehCuti;

    /**
     * @ORM\Column(name="boleh_mundur", type="date", nullable=true)
     */
    private $bolehMundur;

    /**
     * @ORM\Column(name="akhir_kss", type="date", nullable=true)
     */
    private $akhirKss;  
  
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="string", length=100)
     */
    private $status = 'publish';



    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return TahunAkademik
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return TahunAkademik
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set tahun
     *
     * @param string $tahun
     *
     * @return TahunAkademik
     */
    public function setTahun($tahun)
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get tahun
     *
     * @return string
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Set kodeSemester
     *
     * @param integer $kodeSemester
     *
     * @return TahunAkademik
     */
    public function setKodeSemester($kodeSemester)
    {
        $this->kodeSemester = $kodeSemester;

        return $this;
    }

    /**
     * Get kodeSemester
     *
     * @return integer
     */
    public function getKodeSemester()
    {
        return $this->kodeSemester;
    }

    /**
     * Set aktif
     *
     * @param boolean $aktif
     *
     * @return TahunAkademik
     */
    public function setAktif($aktif)
    {
        $this->aktif = $aktif;

        return $this;
    }

    /**
     * Get aktif
     *
     * @return boolean
     */
    public function getAktif()
    {
        return $this->aktif;
    }

    /**
     * Set batasKrs
     *
     * @param string $batasKrs
     *
     * @return TahunAkademik
     */
    public function setBatasKrs($batasKrs)
    {
        $this->batasKrs = $batasKrs;

        return $this;
    }

    /**
     * Get batasKrs
     *
     * @return string
     */
    public function getBatasKrs()
    {
        return $this->batasKrs;
    }

    /**
     * Set batasKrsOnline
     *
     * @param string $batasKrsOnline
     *
     * @return TahunAkademik
     */
    public function setBatasKrsOnline($batasKrsOnline)
    {
        $this->batasKrsOnline = $batasKrsOnline;

        return $this;
    }

    /**
     * Get batasKrsOnline
     *
     * @return string
     */
    public function getBatasKrsOnline()
    {
        return $this->batasKrsOnline;
    }

    /**
     * Set batasUbahKrs
     *
     * @param string $batasUbahKrs
     *
     * @return TahunAkademik
     */
    public function setBatasUbahKrs($batasUbahKrs)
    {
        $this->batasUbahKrs = $batasUbahKrs;

        return $this;
    }

    /**
     * Get batasUbahKrs
     *
     * @return string
     */
    public function getBatasUbahKrs()
    {
        return $this->batasUbahKrs;
    }

    /**
     * Set batasCetakKss
     *
     * @param string $batasCetakKss
     *
     * @return TahunAkademik
     */
    public function setBatasCetakKss($batasCetakKss)
    {
        $this->batasCetakKss = $batasCetakKss;

        return $this;
    }

    /**
     * Get batasCetakKss
     *
     * @return string
     */
    public function getBatasCetakKss()
    {
        return $this->batasCetakKss;
    }

    /**
     * Set batasPembayaran
     *
     * @param string $batasPembayaran
     *
     * @return TahunAkademik
     */
    public function setBatasPembayaran($batasPembayaran)
    {
        $this->batasPembayaran = $batasPembayaran;

        return $this;
    }

    /**
     * Get batasPembayaran
     *
     * @return string
     */
    public function getBatasPembayaran()
    {
        return $this->batasPembayaran;
    }

    /**
     * Set perkuliahan
     *
     * @param string $perkuliahan
     *
     * @return TahunAkademik
     */
    public function setPerkuliahan($perkuliahan)
    {
        $this->perkuliahan = $perkuliahan;

        return $this;
    }

    /**
     * Get perkuliahan
     *
     * @return string
     */
    public function getPerkuliahan()
    {
        return $this->perkuliahan;
    }

    /**
     * Set batasUts
     *
     * @param string $batasUts
     *
     * @return TahunAkademik
     */
    public function setBatasUts($batasUts)
    {
        $this->batasUts = $batasUts;

        return $this;
    }

    /**
     * Get batasUts
     *
     * @return string
     */
    public function getBatasUts()
    {
        return $this->batasUts;
    }

    /**
     * Set batasUas
     *
     * @param string $batasUas
     *
     * @return TahunAkademik
     */
    public function setBatasUas($batasUas)
    {
        $this->batasUas = $batasUas;

        return $this;
    }

    /**
     * Get batasUas
     *
     * @return string
     */
    public function getBatasUas()
    {
        return $this->batasUas;
    }

    /**
     * Set bolehCuti
     *
     * @param \DateTime $bolehCuti
     *
     * @return TahunAkademik
     */
    public function setBolehCuti($bolehCuti)
    {
        $this->bolehCuti = $bolehCuti;

        return $this;
    }

    /**
     * Get bolehCuti
     *
     * @return \DateTime
     */
    public function getBolehCuti()
    {
        return $this->bolehCuti;
    }

    /**
     * Set bolehMundur
     *
     * @param \DateTime $bolehMundur
     *
     * @return TahunAkademik
     */
    public function setBolehMundur($bolehMundur)
    {
        $this->bolehMundur = $bolehMundur;

        return $this;
    }

    /**
     * Get bolehMundur
     *
     * @return \DateTime
     */
    public function getBolehMundur()
    {
        return $this->bolehMundur;
    }

    /**
     * Set akhirKss
     *
     * @param \DateTime $akhirKss
     *
     * @return TahunAkademik
     */
    public function setAkhirKss($akhirKss)
    {
        $this->akhirKss = $akhirKss;

        return $this;
    }

    /**
     * Get akhirKss
     *
     * @return \DateTime
     */
    public function getAkhirKss()
    {
        return $this->akhirKss;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TahunAkademik
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
