<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wilayah
 *
 * @ORM\Table(name="wilayah")
 * @ORM\Entity
 */
class Wilayah
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255, nullable=true)
     */
    private $nama;

    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="string", length=255, nullable=true)
     */
    private $kode;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_level", referencedColumnName="id")
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity="Wilayah", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Wilayah", inversedBy="children")
     * @ORM\JoinColumn(name="id_induk", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\Column(name="raw", type="array", nullable=true)
     */
    private $raw;

    public function __construct() {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return Wilayah
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return Wilayah
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set level
     *
     * @param \AppBundle\Entity\Master $level
     *
     * @return Wilayah
     */
    public function setLevel(\AppBundle\Entity\Master $level = null)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return \AppBundle\Entity\Master
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Wilayah $child
     *
     * @return Wilayah
     */
    public function addChild(\AppBundle\Entity\Wilayah $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Wilayah $child
     */
    public function removeChild(\AppBundle\Entity\Wilayah $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Wilayah $parent
     *
     * @return Wilayah
     */
    public function setParent(\AppBundle\Entity\Wilayah $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Wilayah
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set raw
     *
     * @param array $raw
     *
     * @return Wilayah
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return array
     */
    public function getRaw()
    {
        return $this->raw;
    }
}
