<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DosenPenelitian
 *
 * @ORM\Table(name="dosen_pelatihan")
 * @ORM\Entity
 */
class DosenPelatihan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Dosen", inversedBy="pelatihan")
     * @ORM\JoinColumn(name="id_dosen", referencedColumnName="id")
     */
    private $dosen;

    /**
     * @var string
     *
     * @ORM\Column(name="tahun", type="string", length=20, nullable=true)
     */
    private $tahun;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tanggal", type="date", nullable=true)
     */
    private $tanggal;

    /**
     * @var string
     *
     * @ORM\Column(name="biaya", type="string", length=255, nullable=true)
     */
    private $biaya;

    /**
     * @var string
     *
     * @ORM\Column(name="judul", type="string", length=255, nullable=true)
     */
    private $judul;

    /**
     * @var string
     *
     * @ORM\Column(name="penyelenggara", type="string", length=255, nullable=true)
     */
    private $penyelenggara;

    /**
     * @var string
     *
     * @ORM\Column(name="lamaPelatihan", type="string", length=255, nullable=true)
     */
    private $lamaPelatihan;

    /**
     * @var string
     *
     * @ORM\Column(name="hasil", type="text", nullable=true)
     */
    private $hasil;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tahun
     *
     * @param string $tahun
     *
     * @return DosenPenelitian
     */
    public function setTahun($tahun)
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get tahun
     *
     * @return string
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Set tanggal
     *
     * @param \DateTime $tanggal
     *
     * @return DosenPenelitian
     */
    public function setTanggal($tanggal)
    {
        $this->tanggal = $tanggal;

        return $this;
    }

    /**
     * Get tanggal
     *
     * @return \DateTime
     */
    public function getTanggal()
    {
        return $this->tanggal;
    }

    /**
     * Set biaya
     *
     * @param string $biaya
     *
     * @return DosenPenelitian
     */
    public function setBiaya($biaya)
    {
        $this->biaya = $biaya;

        return $this;
    }

    /**
     * Get biaya
     *
     * @return string
     */
    public function getBiaya()
    {
        return $this->biaya;
    }

    /**
     * Set judul
     *
     * @param string $judul
     *
     * @return DosenPenelitian
     */
    public function setJudul($judul)
    {
        $this->judul = $judul;

        return $this;
    }

    /**
     * Get judul
     *
     * @return string
     */
    public function getJudul()
    {
        return $this->judul;
    }

    /**
     * Set penyelenggara
     *
     * @param string $penyelenggara
     *
     * @return DosenPenelitian
     */
    public function setPenyelenggara($penyelenggara)
    {
        $this->penyelenggara = $penyelenggara;

        return $this;
    }

    /**
     * Get penyelenggara
     *
     * @return string
     */
    public function getPenyelenggara()
    {
        return $this->penyelenggara;
    }

    /**
     * Set lamaPelatihan
     *
     * @param string $lamaPelatihan
     *
     * @return DosenPenelitian
     */
    public function setLamaPelatihan($lamaPelatihan)
    {
        $this->lamaPelatihan = $lamaPelatihan;

        return $this;
    }

    /**
     * Get lamaPelatihan
     *
     * @return string
     */
    public function getLamaPelatihan()
    {
        return $this->lamaPelatihan;
    }

    /**
     * Set hasil
     *
     * @param string $hasil
     *
     * @return DosenPenelitian
     */
    public function setHasil($hasil)
    {
        $this->hasil = $hasil;

        return $this;
    }

    /**
     * Get hasil
     *
     * @return string
     */
    public function getHasil()
    {
        return $this->hasil;
    }

    /**
     * Set dosen
     *
     * @param \AppBundle\Entity\Dosen $dosen
     *
     * @return DosenPelatihan
     */
    public function setDosen(\AppBundle\Entity\Dosen $dosen = null)
    {
        $this->dosen = $dosen;

        return $this;
    }

    /**
     * Get dosen
     *
     * @return \AppBundle\Entity\Dosen
     */
    public function getDosen()
    {
        return $this->dosen;
    }
}
