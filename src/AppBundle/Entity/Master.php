<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Master
 *
 * @ORM\Table(name="master")
 * @ORM\Entity
 */
class Master
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;
  
    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="string", length=50, nullable=true)
     */
    private $kode;

    /**
     * @var string
     *
     * @ORM\Column(name="nama", type="string", length=255, nullable=true)
     */
    private $nama;

    /**
     * @ORM\Column(name="ket", type="text", nullable=true)
     */
    private $ket;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_1", type="string", length=100, nullable=true)
     */
    private $custom1;
  
    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_2", type="string", length=100, nullable=true)
     */
    private $custom2;
  
    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_3", type="string", length=100, nullable=true)
     */
    private $custom3;
  
    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_4", type="string", length=100, nullable=true)
     */
    private $custom4;
  
    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_5", type="string", length=100, nullable=true)
     */
    private $custom5;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_6", type="string", length=100, nullable=true)
     */
    private $custom6;
  
    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_7", type="string", length=100, nullable=true)
     */
    private $custom7;
  
    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_8", type="string", length=100, nullable=true)
     */
    private $custom8;
  
    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_9", type="string", length=100, nullable=true)
     */
    private $custom9;
  
    /**
     * @var string
     *
     * @ORM\Column(name="custom_field_10", type="string", length=100, nullable=true)
     */
    private $custom10;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status = 'publish';

    /**
     * @var string
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="raw", type="text", nullable=true)
     */
    private $raw;

    /**
     * @ORM\OneToMany(targetEntity="Master", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Master", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;
    // ...

    public function __construct() {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }
  

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Master
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return Master
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set nama
     *
     * @param string $nama
     *
     * @return Master
     */
    public function setNama($nama)
    {
        $this->nama = $nama;

        return $this;
    }

    /**
     * Get nama
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Set ket
     *
     * @param string $ket
     *
     * @return Master
     */
    public function setKet($ket)
    {
        $this->ket = $ket;

        return $this;
    }

    /**
     * Get ket
     *
     * @return string
     */
    public function getKet()
    {
        return $this->ket;
    }

    /**
     * Set custom1
     *
     * @param string $custom1
     *
     * @return Master
     */
    public function setCustom1($custom1)
    {
        $this->custom1 = $custom1;

        return $this;
    }

    /**
     * Get custom1
     *
     * @return string
     */
    public function getCustom1()
    {
        return $this->custom1;
    }

    /**
     * Set custom2
     *
     * @param string $custom2
     *
     * @return Master
     */
    public function setCustom2($custom2)
    {
        $this->custom2 = $custom2;

        return $this;
    }

    /**
     * Get custom2
     *
     * @return string
     */
    public function getCustom2()
    {
        return $this->custom2;
    }

    /**
     * Set custom3
     *
     * @param string $custom3
     *
     * @return Master
     */
    public function setCustom3($custom3)
    {
        $this->custom3 = $custom3;

        return $this;
    }

    /**
     * Get custom3
     *
     * @return string
     */
    public function getCustom3()
    {
        return $this->custom3;
    }

    /**
     * Set custom4
     *
     * @param string $custom4
     *
     * @return Master
     */
    public function setCustom4($custom4)
    {
        $this->custom4 = $custom4;

        return $this;
    }

    /**
     * Get custom4
     *
     * @return string
     */
    public function getCustom4()
    {
        return $this->custom4;
    }

    /**
     * Set custom5
     *
     * @param string $custom5
     *
     * @return Master
     */
    public function setCustom5($custom5)
    {
        $this->custom5 = $custom5;

        return $this;
    }

    /**
     * Get custom5
     *
     * @return string
     */
    public function getCustom5()
    {
        return $this->custom5;
    }

    /**
     * Set custom6
     *
     * @param string $custom6
     *
     * @return Master
     */
    public function setCustom6($custom6)
    {
        $this->custom6 = $custom6;

        return $this;
    }

    /**
     * Get custom6
     *
     * @return string
     */
    public function getCustom6()
    {
        return $this->custom6;
    }

    /**
     * Set custom7
     *
     * @param string $custom7
     *
     * @return Master
     */
    public function setCustom7($custom7)
    {
        $this->custom7 = $custom7;

        return $this;
    }

    /**
     * Get custom7
     *
     * @return string
     */
    public function getCustom7()
    {
        return $this->custom7;
    }

    /**
     * Set custom8
     *
     * @param string $custom8
     *
     * @return Master
     */
    public function setCustom8($custom8)
    {
        $this->custom8 = $custom8;

        return $this;
    }

    /**
     * Get custom8
     *
     * @return string
     */
    public function getCustom8()
    {
        return $this->custom8;
    }

    /**
     * Set custom9
     *
     * @param string $custom9
     *
     * @return Master
     */
    public function setCustom9($custom9)
    {
        $this->custom9 = $custom9;

        return $this;
    }

    /**
     * Get custom9
     *
     * @return string
     */
    public function getCustom9()
    {
        return $this->custom9;
    }

    /**
     * Set custom10
     *
     * @param string $custom10
     *
     * @return Master
     */
    public function setCustom10($custom10)
    {
        $this->custom10 = $custom10;

        return $this;
    }

    /**
     * Get custom10
     *
     * @return string
     */
    public function getCustom10()
    {
        return $this->custom10;
    }

    /**
     * Set raw
     *
     * @param string $raw
     *
     * @return Master
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return string
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     *
     * @return Master
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Master
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Master $child
     *
     * @return Master
     */
    public function addChild(\AppBundle\Entity\Master $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Master $child
     */
    public function removeChild(\AppBundle\Entity\Master $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Master $parent
     *
     * @return Master
     */
    public function setParent(\AppBundle\Entity\Master $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Master
     */
    public function getParent()
    {
        return $this->parent;
    }
}
