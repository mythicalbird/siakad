<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dosen
 *
 * @ORM\Table(name="nilai")
 * @ORM\Entity
 */
class Nilai
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Mahasiswa")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $mahasiswa;

    /**
     * @ORM\ManyToOne(targetEntity="KurikulumMakul")
     * @ORM\JoinColumn(name="id_kurikulum_makul", referencedColumnName="id")
     */
    private $makul;

    /**
     * @ORM\Column(name="semester", type="integer", length=2)
     */
    private $semester = 1;

    /**
     * @ORM\ManyToOne(targetEntity="Kelas")
     * @ORM\JoinColumn(name="id_kelas", referencedColumnName="id")
     */
    private $kelas;

    /**
     * @ORM\ManyToOne(targetEntity="NilaiPerkuliahan", inversedBy="nilai")
     * @ORM\JoinColumn(name="id_nilai_perkuliahan", referencedColumnName="id")
     */
    private $nilaiAspek;

    /**
     * @var string
     *
     * @ORM\Column(name="nilai_huruf", type="string", length=1, nullable=true)
     */
    private $nilaiHuruf;

    /**
     * @var string
     *
     * @ORM\Column(name="nilai_angka", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $nilaiAngka;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status = 'publish';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set semester
     *
     * @param integer $semester
     *
     * @return Nilai
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set nilaiHuruf
     *
     * @param string $nilaiHuruf
     *
     * @return Nilai
     */
    public function setNilaiHuruf($nilaiHuruf)
    {
        $this->nilaiHuruf = $nilaiHuruf;

        return $this;
    }

    /**
     * Get nilaiHuruf
     *
     * @return string
     */
    public function getNilaiHuruf()
    {
        return $this->nilaiHuruf;
    }

    /**
     * Set nilaiAngka
     *
     * @param string $nilaiAngka
     *
     * @return Nilai
     */
    public function setNilaiAngka($nilaiAngka)
    {
        $this->nilaiAngka = $nilaiAngka;

        return $this;
    }

    /**
     * Get nilaiAngka
     *
     * @return string
     */
    public function getNilaiAngka()
    {
        return $this->nilaiAngka;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Nilai
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return Nilai
     */
    public function setMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa = null)
    {
        $this->mahasiswa = $mahasiswa;

        return $this;
    }

    /**
     * Get mahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }

    /**
     * Set makul
     *
     * @param \AppBundle\Entity\KurikulumMakul $makul
     *
     * @return Nilai
     */
    public function setMakul(\AppBundle\Entity\KurikulumMakul $makul = null)
    {
        $this->makul = $makul;

        return $this;
    }

    /**
     * Get makul
     *
     * @return \AppBundle\Entity\KurikulumMakul
     */
    public function getMakul()
    {
        return $this->makul;
    }

    /**
     * Set kelas
     *
     * @param \AppBundle\Entity\Kelas $kelas
     *
     * @return Nilai
     */
    public function setKelas(\AppBundle\Entity\Kelas $kelas = null)
    {
        $this->kelas = $kelas;

        return $this;
    }

    /**
     * Get kelas
     *
     * @return \AppBundle\Entity\Kelas
     */
    public function getKelas()
    {
        return $this->kelas;
    }

    /**
     * Set nilaiAspek
     *
     * @param \AppBundle\Entity\NilaiPerkuliahan $nilaiAspek
     *
     * @return Nilai
     */
    public function setNilaiAspek(\AppBundle\Entity\NilaiPerkuliahan $nilaiAspek = null)
    {
        $this->nilaiAspek = $nilaiAspek;

        return $this;
    }

    /**
     * Get nilaiAspek
     *
     * @return \AppBundle\Entity\NilaiPerkuliahan
     */
    public function getNilaiAspek()
    {
        return $this->nilaiAspek;
    }
}
