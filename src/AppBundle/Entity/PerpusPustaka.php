<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pustaka
 *
 * @ORM\Table(name="perpus_pustaka")
 * @ORM\Entity
 */
class PerpusPustaka
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_klasifikasi", referencedColumnName="id")
     */
    private $klasifikasi;

    /**
     * @var string
     *
     * @ORM\Column(name="kode", type="string", length=255)
     */
    private $kode;

    /**
     * @var string
     *
     * @ORM\Column(name="isbn", type="string", length=255, nullable=true)
     */
    private $isbn;

    /**
     * @var string
     *
     * @ORM\Column(name="judul", type="string", length=255, nullable=true)
     */
    private $judul;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_format", referencedColumnName="id")
     */
    private $format;

    /**
     * @ORM\ManyToOne(targetEntity="Master")
     * @ORM\JoinColumn(name="id_lokasi", referencedColumnName="id")
     */
    private $lokasi;

    /**
     * @var string
     *
     * @ORM\Column(name="pengarang", type="string", length=255, nullable=true)
     */
    private $pengarang;

    /**
     * @var string
     *
     * @ORM\Column(name="penerbit", type="string", length=255, nullable=true)
     */
    private $penerbit;

    /**
     * @var string
     *
     * @ORM\Column(name="tpt_terbit", type="string", length=255, nullable=true)
     */
    private $tptTerbit;

    /**
     * @var string
     *
     * @ORM\Column(name="tahun_terbit", type="string", length=255, nullable=true)
     */
    private $tahunTerbit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_terima", type="date")
     */
    private $tglTerima;

    /**
     * @var string
     *
     * @ORM\Column(name="edisi", type="string", length=255, nullable=true)
     */
    private $edisi;

    /**
     * @var string
     *
     * @ORM\Column(name="harga", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $harga;

    /**
     * @var string
     *
     * @ORM\Column(name="jumlah", type="string", length=255)
     */
    private $jumlah;

    /**
     * @var string
     *
     * @ORM\Column(name="sumber_dari", type="string", length=255, nullable=true)
     */
    private $sumberDari;

    /**
     * @var string
     *
     * @ORM\Column(name="keterangan", type="text", nullable=true)
     */
    private $ket;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kode
     *
     * @param string $kode
     *
     * @return Pustaka
     */
    public function setKode($kode)
    {
        $this->kode = $kode;

        return $this;
    }

    /**
     * Get kode
     *
     * @return string
     */
    public function getKode()
    {
        return $this->kode;
    }

    /**
     * Set isbn
     *
     * @param string $isbn
     *
     * @return Pustaka
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * Get isbn
     *
     * @return string
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * Set judul
     *
     * @param string $judul
     *
     * @return Pustaka
     */
    public function setJudul($judul)
    {
        $this->judul = $judul;

        return $this;
    }

    /**
     * Get judul
     *
     * @return string
     */
    public function getJudul()
    {
        return $this->judul;
    }

    /**
     * Set pengarang
     *
     * @param string $pengarang
     *
     * @return Pustaka
     */
    public function setPengarang($pengarang)
    {
        $this->pengarang = $pengarang;

        return $this;
    }

    /**
     * Get pengarang
     *
     * @return string
     */
    public function getPengarang()
    {
        return $this->pengarang;
    }

    /**
     * Set penerbit
     *
     * @param string $penerbit
     *
     * @return Pustaka
     */
    public function setPenerbit($penerbit)
    {
        $this->penerbit = $penerbit;

        return $this;
    }

    /**
     * Get penerbit
     *
     * @return string
     */
    public function getPenerbit()
    {
        return $this->penerbit;
    }

    /**
     * Set tptTerbit
     *
     * @param string $tptTerbit
     *
     * @return Pustaka
     */
    public function setTptTerbit($tptTerbit)
    {
        $this->tptTerbit = $tptTerbit;

        return $this;
    }

    /**
     * Get tptTerbit
     *
     * @return string
     */
    public function getTptTerbit()
    {
        return $this->tptTerbit;
    }

    /**
     * Set tahunTerbit
     *
     * @param string $tahunTerbit
     *
     * @return Pustaka
     */
    public function setTahunTerbit($tahunTerbit)
    {
        $this->tahunTerbit = $tahunTerbit;

        return $this;
    }

    /**
     * Get tahunTerbit
     *
     * @return string
     */
    public function getTahunTerbit()
    {
        return $this->tahunTerbit;
    }

    /**
     * Set tglTerima
     *
     * @param \DateTime $tglTerima
     *
     * @return Pustaka
     */
    public function setTglTerima($tglTerima)
    {
        $this->tglTerima = $tglTerima;

        return $this;
    }

    /**
     * Get tglTerima
     *
     * @return \DateTime
     */
    public function getTglTerima()
    {
        return $this->tglTerima;
    }

    /**
     * Set edisi
     *
     * @param string $edisi
     *
     * @return Pustaka
     */
    public function setEdisi($edisi)
    {
        $this->edisi = $edisi;

        return $this;
    }

    /**
     * Get edisi
     *
     * @return string
     */
    public function getEdisi()
    {
        return $this->edisi;
    }

    /**
     * Set harga
     *
     * @param string $harga
     *
     * @return Pustaka
     */
    public function setHarga($harga)
    {
        $this->harga = $harga;

        return $this;
    }

    /**
     * Get harga
     *
     * @return string
     */
    public function getHarga()
    {
        return $this->harga;
    }

    /**
     * Set jumlah
     *
     * @param string $jumlah
     *
     * @return Pustaka
     */
    public function setJumlah($jumlah)
    {
        $this->jumlah = $jumlah;

        return $this;
    }

    /**
     * Get jumlah
     *
     * @return string
     */
    public function getJumlah()
    {
        return $this->jumlah;
    }

    /**
     * Set sumberDari
     *
     * @param string $sumberDari
     *
     * @return Pustaka
     */
    public function setSumberDari($sumberDari)
    {
        $this->sumberDari = $sumberDari;

        return $this;
    }

    /**
     * Get sumberDari
     *
     * @return string
     */
    public function getSumberDari()
    {
        return $this->sumberDari;
    }

    /**
     * Set ket
     *
     * @param string $ket
     *
     * @return Pustaka
     */
    public function setKet($ket)
    {
        $this->ket = $ket;

        return $this;
    }

    /**
     * Get ket
     *
     * @return string
     */
    public function getKet()
    {
        return $this->ket;
    }

    /**
     * Set klasifikasi
     *
     * @param \AppBundle\Entity\Master $klasifikasi
     *
     * @return Pustaka
     */
    public function setKlasifikasi(\AppBundle\Entity\Master $klasifikasi = null)
    {
        $this->klasifikasi = $klasifikasi;

        return $this;
    }

    /**
     * Get klasifikasi
     *
     * @return \AppBundle\Entity\Master
     */
    public function getKlasifikasi()
    {
        return $this->klasifikasi;
    }

    /**
     * Set format
     *
     * @param \AppBundle\Entity\Master $format
     *
     * @return Pustaka
     */
    public function setFormat(\AppBundle\Entity\Master $format = null)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return \AppBundle\Entity\Master
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set lokasi
     *
     * @param \AppBundle\Entity\Master $lokasi
     *
     * @return Pustaka
     */
    public function setLokasi(\AppBundle\Entity\Master $lokasi = null)
    {
        $this->lokasi = $lokasi;

        return $this;
    }

    /**
     * Get lokasi
     *
     * @return \AppBundle\Entity\Master
     */
    public function getLokasi()
    {
        return $this->lokasi;
    }
}
