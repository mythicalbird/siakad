<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MahasiswaCuti
 *
 * @ORM\Table(name="mahasiswa_aktifitas")
 * @ORM\Entity
 */
class MahasiswaAktifitas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TahunAkademik")
     * @ORM\JoinColumn(name="id_ta", referencedColumnName="id")
     */
    private $ta; // sebagai semester

    /**
     * @var string
     *
     * @ORM\Column(name="semester", type="integer", nullable=true)
     */
    private $semester; // sebagai konfirmasi aja

    /**
     * @var string
     *
     * @ORM\Column(name="jumlah_sks", type="integer", nullable=true)
     */
    private $jumlahSks; // dalam semester sekarang

    /**
     * @ORM\ManyToOne(targetEntity="Mahasiswa")
     * @ORM\JoinColumn(name="id_mahasiswa", referencedColumnName="id")
     */
    private $mahasiswa;

    /**
     * @var string
     *
     * @ORM\Column(name="ips", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $ips; //nilai baku

    /**
     * @var string
     *
     * @ORM\Column(name="ipk", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $ipk; //nilai baku

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20, nullable=true)
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set semester
     *
     * @param integer $semester
     *
     * @return MahasiswaAktifitas
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return integer
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set jumlahSks
     *
     * @param integer $jumlahSks
     *
     * @return MahasiswaAktifitas
     */
    public function setJumlahSks($jumlahSks)
    {
        $this->jumlahSks = $jumlahSks;

        return $this;
    }

    /**
     * Get jumlahSks
     *
     * @return integer
     */
    public function getJumlahSks()
    {
        return $this->jumlahSks;
    }

    /**
     * Set ips
     *
     * @param string $ips
     *
     * @return MahasiswaAktifitas
     */
    public function setIps($ips)
    {
        $this->ips = $ips;

        return $this;
    }

    /**
     * Get ips
     *
     * @return string
     */
    public function getIps()
    {
        return $this->ips;
    }

    /**
     * Set ipk
     *
     * @param string $ipk
     *
     * @return MahasiswaAktifitas
     */
    public function setIpk($ipk)
    {
        $this->ipk = $ipk;

        return $this;
    }

    /**
     * Get ipk
     *
     * @return string
     */
    public function getIpk()
    {
        return $this->ipk;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return MahasiswaAktifitas
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set ta
     *
     * @param \AppBundle\Entity\TahunAkademik $ta
     *
     * @return MahasiswaAktifitas
     */
    public function setTa(\AppBundle\Entity\TahunAkademik $ta = null)
    {
        $this->ta = $ta;

        return $this;
    }

    /**
     * Get ta
     *
     * @return \AppBundle\Entity\TahunAkademik
     */
    public function getTa()
    {
        return $this->ta;
    }

    /**
     * Set mahasiswa
     *
     * @param \AppBundle\Entity\Mahasiswa $mahasiswa
     *
     * @return MahasiswaAktifitas
     */
    public function setMahasiswa(\AppBundle\Entity\Mahasiswa $mahasiswa = null)
    {
        $this->mahasiswa = $mahasiswa;

        return $this;
    }

    /**
     * Get mahasiswa
     *
     * @return \AppBundle\Entity\Mahasiswa
     */
    public function getMahasiswa()
    {
        return $this->mahasiswa;
    }
}
