<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Service\AppService;

class FeederService
{
	protected $em;
	protected $token_storage;
	protected $request;
	protected $templating;
	protected $appService;

	public function __construct(EntityManager $em, AppService $appService, $request, $token_storage, $templating) {
		$this->em = $em;	
		$this->token_storage = $token_storage;
		$this->request = $request;
		$this->templating = $templating;
		$this->appService = $appService;
	}

	public function ws($method, $params = array(), $use_token = true)
	{
		$res = array();
		if ( isset($token['error']) ) {
			$res['error'] = $token['error'];
		} else {
			
			$params['token'] = $this->getToken();
			$client = $this->getClient();

	        $result = $client->call($method, $params, '', '', false, true);

	        if ( !empty($result['error_code']) ) {
	        	$this->generateToken();
	        	$params['token'] = $this->getToken();
	        	$result = $client->call($method, $params, '', '', false, true);
	        } 

	        return $result;

		}

        return $res;
	}

	public function updateRecord($table, $key, $data)
	{
		$res = array();
		if ( isset($token['error']) ) {
			$res['error'] = $token['error'];
		} else {
			
			$token = $this->getToken();
			$record = array('key'=>$key, 'data'=>$data);
			$client = $this->getClient();

	        $result = $client->call(
	        	'UpdateRecord', 
	        	array(
		        	'token'		=> $token,
		        	'table'		=> $table,
		        	// 'record'	=> json_encode($record),
		        	// 'key'		=> $key,
		        	'data'		=> json_encode($record)
	        	)
	        );

	        return $result;

		}

        return $res;
	}

	public function getClient()
	{
		$res = array();
		$server = $this->appService->getSetting('server_feeder_dikti', true);
      	if ($server == '') {
      		$res['error'] = 'Server feeder belum diatur!';
      	} else {

	        $url = rtrim($server['url'], '/');
	        $url = $url . '/ws/' . $server['server'] . '.php?wsdl';
	        $username = $server['username']; 
	        $password = $server['password']; 

	        $client = new \nusoap_client($url, true);
			// Check for a fault
			if ($client->fault) {
				$res['error'] = 'Fault: ' . json_encode($result);
			} else {
				// Check for errors
				$err = $client->getError();
				if ($err) {
					// Display the error
					$res['error'] = json_encode($err);
				} else {
					$res = $client;
				}
			}
      	}
		return $res;
	}

	public function getFeederWsToken() 
	{
		$res = array();
      	$server = $this->appService->getSetting('server_feeder_dikti', true);
      	if ($server == '') {
      		$res['error'] = 'Server feeder belum diatur!';
      	} else {

	        $url = rtrim($server['url'], '/');
	        $url = $url . '/ws/' . $server['server'] . '.php?wsdl';
	        $username = $server['username']; 
	        $password = $server['password']; 

	        $client = new \nusoap_client($url, true);
			$result = $client->call('GetToken', array('username'=>$username, 'password'=>$password), '', '', false, true);
			// Check for a fault
			if ($client->fault) {
				$res['error'] = 'Fault: ' . json_encode($result);
			} else {
				// Check for errors
				$err = $client->getError();
				if ($err) {
					// Display the error
					$res['error'] = json_encode($err);
				} else {
					if ( strpos( strtolower($result), 'error') !== false ) {
						$res['error'] = $result;
					} else {
						$this->appService->updateSetting('feeder_ws_token', $result);
						$res['result'] = $result;
						$res['client'] = $client;
					}
				}
			}
      	}
		return $res;
	}

	public function generateToken() 
	{
		$res = array();
      	$server = $this->appService->getSetting('server_feeder_dikti', true);
      	if ($server == '') {
      		$res['error'] = 'Server feeder belum diatur!';
      	} else {

	        $url = rtrim($server['url'], '/');
	        $url = $url . '/ws/' . $server['server'] . '.php?wsdl';
	        $username = $server['username']; 
	        $password = $server['password']; 

	        $client = new \nusoap_client($url, true);
			$result = $client->call('GetToken', array('username'=>$username, 'password'=>$password), '', '', false, true);
			// Check for a fault
			if ($client->fault) {
				$res['error'] = 'Fault: ' . json_encode($result);
			} else {
				// Check for errors
				$err = $client->getError();
				if ($err) {
					// Display the error
					$res['error'] = json_encode($err);
				} else {
					if ( strpos( strtolower($result), 'error') !== false ) {
						$res['error'] = $result;
					} else {
						$this->appService->updateSetting('feeder_ws_token', array($result));
						$res['result'] = $result;
					}
				}
			}
      	}
		return $res;
	}

	public function getToken() 
	{
		$setting = $this->em->getRepository('AppBundle:Setting')
			->findOneByName('feeder_ws_token');
		$now = new\DateTime();
		if( ! $setting ) {
			$newToken = $this->generateToken();
			$token = $newToken['result'];
			$setting = new \AppBundle\Entity\Setting();
			$setting->setName( 'feeder_ws_token' );
			$setting->setModifiedAt( $now );
			$setting->setValue( array( $token ) );
			$this->em->persist($setting);
			$this->em->flush();

			return $token;
		} else {
			$token = $setting->getValue();
			return $token[0];
		}
	}

	public function importDosen() {
		$default_user_params = array(
			'nama',
			'gelarDepan',
			'gelarBelakang',
			'tptLahir',
			'tglLahir',
			'ktp',
			'noKk',
			'npwp',
			'jk',
			'agama',
			'wargaNegara',
			'statusSipil',
			'telp',
			'hp',
			'email',
			'prodi',
			'alamat',
			'kota',
			'propinsi',
			'rt',
			'rw',
			'dusun',
			'desa',
			'kecamatan',
			'hakAkses',
			'username',
			'password',
			'tglMasuk',
			'tglKeluar',
			'dataDosen',
			'updatedAt',
		);

		$default_dosen_params = array(
			'nip',
			'tglMulaiKerja',
			'semesterMulaiKerja',
			'pendidikanTertinggi',
			'institusiInduk',
			'status',
			'statusKerja',
			'jabatanAkademik',
			'jabatanFungsional',
			'jabatanStruktural',
			'pangkatGolongan',
			'user',
			'uuid',
		);
	}

	public function tambahDosen($params=array()) {

	}

	public function tambahDosenPendidikan($nidn, $listPendidikan=array()) {
		
	}

	public function updateDosen($nidn, $params=array()) {

	}


	public function tambahUser($data=array()) {
		$now = new\DateTime();

		$user = new \AppBundle\Entity\User();
		$user->setNama($data['nama']);
		$user->setGelarDepan($data['gelarDepan']);
		$user->setGelarBelakang($data['gelarBelakang']);
		$user->setTptLahir($data['tptLahir']);
		$user->setTglLahir($data['tglLahir']);
		$user->setKtp($data['ktp']);
		$user->setNoKk($data['noKk']);
		$user->setNpwp($data['npwp']);
		$user->setJk($data['jk']);
		$user->setAgama($data['agama']);
		$user->setWargaNegara($data['wargaNegara']);
		$user->setStatusSipil($data['statusSipil']);
		$user->setTelp($data['telp']);
		$user->setHp($data['hp']);
		$user->setEmail($data['email']);
		$user->setProdi($data['prodi']);
		$user->setAlamat($data['alamat']);
		$user->setRt($data['rt']);
		$user->setRw($data['rw']);
		$user->setDusun($data['dusun']);
		$user->setDesa($data['desa']);
		$user->setKecamatan($data['kecamatan']);
		$user->setPos($data['pos']);
		$user->setHakAkses($data['hakAkses']);
		$user->setUsername($data['username']);
		$user->setPassword($data['password']);
		$user->setTglMasuk($data['tglMasuk']);
		$user->setTglKeluar($data['tglKeluar']);
		$user->setDataMahasiswa($data['dataMahasiswa']);
		$user->setDataDosen($data['dataDosen']);
		$user->setDataPegawai($data['dataPegawai']);
		$user->setUpdatedAt($now);

		$this->em->persist($user);
		$this->em->flush();

		return $user;
	}

	public function updateUser($username, $data=array(), $insertIfNull=false) {
		$user = $this->em->getRepository('AppBundle:User')
			->findOneByUsername($username);
		if ( ! $user ) {
			if ( $insertIfNull === true ) {
				$user = new \AppBundle\Entity\User();
			} else {
				return;
			}
		}

		$now = new\DateTime();

		$user->setNama($data['nama']);
		$user->setGelarDepan($data['gelarDepan']);
		$user->setGelarBelakang($data['gelarBelakang']);
		$user->setTptLahir($data['tptLahir']);
		$user->setTglLahir($data['tglLahir']);
		$user->setKtp($data['ktp']);
		$user->setNoKk($data['noKk']);
		$user->setNpwp($data['npwp']);
		$user->setJk($data['jk']);
		$user->setAgama($data['agama']);
		$user->setWargaNegara($data['wargaNegara']);
		$user->setStatusSipil($data['statusSipil']);
		$user->setTelp($data['telp']);
		$user->setHp($data['hp']);
		$user->setEmail($data['email']);
		$user->setProdi($data['prodi']);
		$user->setAlamat($data['alamat']);
		$user->setRt($data['rt']);
		$user->setRw($data['rw']);
		$user->setDusun($data['dusun']);
		$user->setDesa($data['desa']);
		$user->setKecamatan($data['kecamatan']);
		$user->setPos($data['pos']);
		$user->setHakAkses($data['hakAkses']);
		$user->setUsername($data['username']);
		$user->setPassword($data['password']);
		$user->setTglMasuk($data['tglMasuk']);
		$user->setTglKeluar($data['tglKeluar']);
		$user->setDataMahasiswa($data['dataMahasiswa']);
		$user->setDataDosen($data['dataDosen']);
		$user->setDataPegawai($data['dataPegawai']);
		$user->setUpdatedAt($now);

		$this->em->persist($user);
		$this->em->flush();

		return $user;
	}
}