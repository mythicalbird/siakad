<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use AppBundle\Entity\Setting;
use AppBundle\Entity\Master;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Mahasiswa;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AppService 
{
	protected $em;
	protected $token_storage;
	protected $request;
	protected $templating;
	protected $encoder;

	public function __construct(EntityManager $em, $request, $token_storage, $templating, $encoder) {
		$this->em = $em;	
		$this->token_storage = $token_storage;
		$this->request = $request;
		$this->templating = $templating;
		$this->encoder = $encoder;
	}

	public function getSetting($settingName, $single = false)
	{
			$setting = $this->em->getRepository(Setting::class)
				->findOneByName($settingName);
			$now = new\DateTime();
			if( ! $setting ) {
				$setting = new Setting();
				$setting->setName($settingName);
				$setting->setModifiedAt($now);
				$this->em->persist($setting);
				$this->em->flush();
				$result = '';
			} else {
				$result = $setting->getValue();
				$result = ( is_array($result) ) ? $result : '';
				if( false !== $single ) {
					if (isset($result[0])) {
						$result = $result[0];
					}
				}
			}
			return $result;
	}

	public function updateSetting($settingName, $settingValue)
	{
			$setting = $this->em->getRepository(Setting::class)
				->findOneByName($settingName);
			$now = new\DateTime();
			if( ! $setting ) {
				$setting = new Setting();
				$setting->setName($settingName);
				$value = ( is_array($settingValue) ) ? $settingValue : array($settingValue);
			} else {
				$value = ( is_array($settingValue) ) ? $settingValue : array($settingValue);
			}
			$setting->setValue($value);
			$setting->setModifiedAt($now);
			$this->em->persist($setting);
			return $this->em->flush();
	}

	public function slugify($str, $sep = '_') 
	{
		$slug = preg_replace('/[^A-Za-z0-9\-]/', $sep, $str);
      	$slug = str_replace(' ', $sep, strtolower($slug));
      	return $slug;
	}

	public function getDataKurikulum($prodi, $tahun = null) 
	{
		$result = array();
		if ( null !== $tahun ) {
			$tahunAkademik = $this->getTahunAkademik();
			$tahun = $tahunAkademik->getTahun();;
		}
		$data = $this->em->getRepository('AppBundle:ProdiKurikulum')
			->findOneBy(array(
				'prodi'		=> $prodi,
				'tahun'		=> $tahun
			));
		if ( $data ) {
			$result['kurikulum'] = $data->getKurikulum();
			$result['prodi']	 = $prodi;
			$result['tahun']	 = $tahun;
			$result['makul']	 = $data->getKurikulum()->getListMakul();
		}

		return $result;
	}

	public function getMasterTermObject($type, $kode = null) 
	{
		if ( null !== $kode ) {
			$object = $this->em->getRepository('AppBundle:Master')
				->findOneBy(array(
					'type'	=> $type,
					'kode'	=> trim($kode),
				));
		} else { 
			$object = $this->em->getRepository('AppBundle:Master')
				->findByType($type);
		}
		return $object;
	}

	public function getMasterTermObjectByName($type, $nama = null) 
	{
		$object = $this->em->getRepository('AppBundle:Master')
			->findOneBy(array(
				'type'	=> $type,
				'nama'	=> $nama,
			));
		return $object;
	}

	public function getMasterTermObjectById($id) 
	{
		$object = $this->em->getRepository('AppBundle:Master')
			->find($id);
		return ($object) ? $object : null;
	}

	public function updateMahasiswa($nim, $params) 
	{
		$mhs = $this->em->getRepository('AppBundle:User')
			->findOneByUsername($nim);
		if ($mhs) {
			$defaultArgs = array(
	            'id_pd' 				=> '',
	            'id_jns_daftar' 		=> 1, // 1 = BARU, 2 = PINDAHAN
	            'id_jns_keluar' 		=> null,
	            'mulai_smt' 			=> null,
	            'sks_diakui' 			=> '',
	            'jalur_skripsi' 		=> '',
	            'judul_skripsi' 		=> '',
	            'bln_awal_bimbingan' 	=> '',
	            'bln_akhir_bimbingan' 	=> '',
	            'sk_yudisium' 			=> '',
	            'tgl_sk_yudisium' 		=> '',
	            'ipk' 					=> null,
	            'no_seri_ijazah'		=> '',
	            'sert_prof' 			=> '',
	            'kode_pt_asal' 			=> '',
	            'nm_pt_asal' 			=> '',
				'nm_prodi_asal'			=> '',
				'jenjang_asal'			=> '',
				'nim_asal'				=> '',
				'sks_diakui_asal'		=> '',
				'nisn'					=> '',			
				'jk'					=> '',
				'id_agama'				=> '',
				'id_pembiayaan'			=> null,
				'kode_sekolah'			=> '',
				'nama_sekolah'			=> '',
				'nis'					=> '',
				'nilai_un'				=> '',
				'id_status'				=> 'X',
				'id_dosen_pa'			=> null,
				'tahun_angkatan'		=> '',
				'maba'					=> 0,
				'orangtua'				=> null,
				'pendidikan'			=> null,
				'berkas'				=> null,
				'id_kelas'				=> null,
				'id_semester'			=> null,
			);
			foreach ($defaultArgs as $key => $value) {
				if ( !isset($args[$key]) ) {
					$args[$key] = $value;
				}
			}

			if ( !empty('id_pd') ) {
				$mhs->setUuid($args['id_pd']);
			}

			if ( null !== $args['id_jns_keluar'] ) {
				$jenisKeluar = $this->getMasterTermObject('jenis_keluar', $args['id_jns_keluar']);
				if ($jenisKeluar)
					$mhs->setJenisKeluar($jenisKeluar);
				if (null !== $args['tgl_keluar'])
					$mhs->setTglKeluar($args['tgl_keluar']);
			}

			$status = $this->getMasterTermObject('status_mahasiswa', $args['id_status']);
			if ($status)
				$mhs->setStatus($status);

			// set orangtua
			if ( null !== $args['orangtua'] ) {
				$mhs->setOrangtua($args['orangtua']);
			}

			// set semester
			if (null !== $args['id_semester']) {
				$semester = $this->em->getRepository('AppBundle:Semester')
					->find($args['id_semester']);
				if ($semester)
					$mhs->setSemester($semester);
			}

			// set kelas
			if (null !== $args['id_kelas']) {
				$kelas = $this->em->getRepository('AppBundle:Kelas')
					->find($args['id_kelas']);
				if ($kelas)
					$mhs->setKelas($kelas);
			}

			// set pembimbing akademik
			if ( null !== $args['id_dosen_pa'] ) {
				$dosenPa = $this->em->getRepository('AppBundle:Dosen')
					->find($args['id_dosen_pa']);
				if ($dosenPa)
					$mhs->setPa($dosenPa);
			}

			if ( !empty($args['tahun_angkatan']) ) {
				$mhs->setAngkatan($args['tahun_angkatan']);
			}

			$this->em->persist($mhs);
			$this->em->flush();

			return $mhs;
		}
		return true;
	}

	public function generateNpm($periode, $noUrut, $lanjutan = false)
	{
		/**
		 * FORMAT: TAHUN MASUK.KODE PRODI.NO URUT
		 * Contoh:
		 * 18.012.001 Prodi Bisnis
		 * 18.011.001 Prodi Public
		 * 18.111.001 Lanjutan / Pindahan Publik
		 * 18.112.001 Lanjutan / Pindahan Bisnis
		 */

		$prodi = $periode->getProdi();
		$tahun = $periode->getTahun();

		$npm = '';
		$npm .= substr($tahun, 2, 2);
		$npm .= '.';
		// $npm .= substr($jurusan, 0, 3);
		$npm .= ( !$lanjutan ) ? '0' : '1';
		$npm .= substr($prodi->getKode(), -2);
		$npm .= '.';
		$npm .= str_pad($noUrut, 3, 0, STR_PAD_LEFT);
		return $npm;
	}

	// public function dateFormat($date, $toDb = false) {
	// 	if ( ! $toDb ) {
	// 		$result = 
	// 	} else {
	// 		$data = 
	// 	}
	// 	return $result;
	// }

	public function newMahasiswa($args, $ta=null, $maba=0)
	{
		if ( is_null($ta) ) {
			$ta = $this->getTahunAkademik();
		}
		$defaultArgs = array(
			'gelarDepan' 	=> '',
			'gelarBelakang' => '',
			'wargaNegara' 	=> '',
			'jenisDaftar' 	=> null,
			'jenisKeluar' 	=> null,
			'status' 		=> '',
			'orangtua' 		=> array(),
			'semester' 		=> '',
			'semesterMasuk' => '',
			'kelas' 		=> '',
			'pa' 			=> '',
			'jalurMasuk' 	=> '',
			'angkatan' 		=> '',
			'uuid' 			=> '',
			'regPd' 		=> '',
			'statusMasuk' 	=> '',
			'wilayah'		=> null,
			'tglMasuk'		=> null,
			'tglKeluar'		=> null,
			'hp'			=> '',
			'prodi'			=> null,
			'angkatan'		=> date('Y'),
			'status'		=> 'X',
			'smt'			=> 0,
			'kelas'			=> null,
			'berkas'		=> null,
			'pa'			=> null,
			'pembiayaan'	=> null,
			'raw'			=> null,
			'semester'		=> 1,
			'ipk'			=> '',
			'sks'			=> '',
			'periode'		=> null,
			'rt'			=> '',
			'rw'			=> '',
			'dusun'			=> '',
			'desa'			=> '',
			'pos'			=> '',
			'maba'			=> $maba,
			'riwayatPendidikan'	=> null,
			'asalPt'		=> null
		);
		foreach ($defaultArgs as $key => $value) {
			if ( !isset($args[$key]) ) {
				$args[$key] = $value;
			}
		}

		if ( null !== $args['prodi'] ) {
			$prodi = $args['prodi'];
		}

		if ( null !== $args['periode'] ) {
			$periode = $args['periode'];
			$prodi = $periode->getProdi();
		} else {
	      $periode = $this->em->getRepository('AppBundle:PmbPeriode')
	        ->findOneByTahun($this->getTahunAkademik()->getTahun());
	      if ( !$periode ) {
	        $periode = new \AppBundle\Entity\PmbPeriode();
	        $periode->setTahun($this->getTahunAkademik()->getTahun());
	        $periode->setStatus(0);
	        $periode->setProdi( ( isset($prodi) ? $prodi : null ) );
	        $this->em->persist($periode);
	        $this->em->flush();
	      }
		}

		$no_urut = $this->generateNoUrutPmb($periode);

		$data = array();
		$data['pmb'] = array(
			'noUrut'	=> $no_urut,
			'periode'	=> $periode,
			'jalur'		=> $args['jalur'],
			'nilaiUsm'	=> 0,
			'lulus'		=> 0
		);

		$angkatan = $args['angkatan']; // tahun

		$semesterMasuk = $this->em->getRepository('AppBundle:TahunAkademik')
			->findOneBy(array(
				'kode'			=> $angkatan . 1
			));

		$user = new \AppBundle\Entity\User();
		$user->setNama( $args['nama'] );
		$user->setGelarDepan( $args['gelarDepan'] );
		$user->setGelarBelakang( $args['gelarBelakang'] );
		$user->setTptLahir( $args['tptLahir'] );
		$user->setTglLahir( $args['tglLahir'] );
		$user->setKtp( $args['ktp'] );
		$user->setNoKk( $args['noKk'] );
		$user->setNpwp( $args['npwp'] );
		$user->setJk( $args['jk'] );
		$user->setAgama( $args['agama'] );
		$user->setWargaNegara( $args['wargaNegara'] );
		$user->setStatusSipil( $args['statusSipil'] );
		$user->setTelp( $args['telp'] );
		$user->setHp( ( !empty($args['hp']) ) ? $args['hp'] : $args['telp'] );
		$user->setEmail( $args['email'] );
		$user->setProdi( $periode->getProdi() );
		$user->setAlamat( $args['alamat'] );
		$user->setRt( $args['rt'] );
		$user->setRw( $args['rw'] );
		$user->setDusun( $args['dusun'] );
		$user->setDesa( $args['desa'] );
		$user->setWilayah( $args['wilayah'] );
		$user->setPos( $args['pos'] );
		$user->setHakAkses( $this->getMasterTermObject('hak_akses', 4) );

		$lanjutan = ( $args['statusMasuk'] == 'PINDAHAN' ) ? true : false;
		if ( isset( $args['nim'] ) ) {
			$username = $args['nim'];
		} else {
			$username = $this->generateNpm($periode, $no_urut, $lanjutan); //nim
		}
		$password = $this->encoder->encodePassword($user, $username);

		$user->setUsername( $username );
		$user->setPassword( $password );
		$user->setTglMasuk( $args['tglMasuk'] );
		$user->setTglKeluar( $args['tglKeluar'] );


		$args['jenisDaftar'] = ( $args['statusMasuk'] == 'PINDAHAN' ) ? 2 : 1;

		$mhs = new \AppBundle\Entity\Mahasiswa();
		$mhs->setJenisDaftar( $this->getMasterTermObject('jenis_pendaftaran', $args['jenisDaftar']) ); // entity

		// if ( null !== $args['jenisKeluar'] ) {
		// 	$mhs->setJenisKeluar( $this->getMasterTermObject('jenis_keluar', $args['jenisKeluar']) ); // entity
		// }
		$mhs->setSemester( $args['semester'] ); // entity
		$mhs->setSemesterMasuk( $semesterMasuk ); // entity
		$mhs->setPeriode($periode);
		$mhs->setIpk( $args['ipk'] );
		$mhs->setSks( $args['sks'] );
		// $mhs->setJalurMasuk( $this->getMasterTermObject('jenis_pendaftaran', $args['jalur_masuk']) ); // entity
		$mhs->setOrangtua( $args['orangtua'] );
		$mhs->setAngkatan( $angkatan );
		$mhs->setData( $data );
		$mhs->setUuid( $args['uuid'] );
		$mhs->setRegPd( $args['regPd'] );
		$mhs->setNoSeriIjazah( null );
		$mhs->setTglIjazah( null );
		$mhs->setNoSkYudisium( null );
		$mhs->setTglSkYudisium( null );
		$mhs->setKodeSekolah( $args['kodeSekolah'] );
		$mhs->setNamaSekolah( $args['namaSekolah'] );
		$mhs->setNis( $args['nis'] );
		$mhs->setNilaiUn( $args['nilaiUn'] );
		$mhs->setAsalKodePt( $args['asalKodePt'] );
		$mhs->setAsalNamaPt( $args['asalNamaPt'] );
		$mhs->setAsalJenjang( $args['asalJenjang'] );
		$mhs->setAsalProdi( $args['asalProdi'] );
		$mhs->setAsalNim( $args['asalNim'] );
		$mhs->setAsalSksDiakui( $args['asalSksDiakui'] );
		$mhs->setMaba( $args['maba'] );
		if ( null !== $args['asalPt'] ) {
			$asalPt = $this->em->getRepository('AppBundle:SatuanPendidikan')
				->find($args['asalPt']);
			if ( $asalPt ) {
				$mhs->setAsalPt( $asalPt );
			}
		}
		$status = $this->getMasterTermObject('status_mahasiswa', $args['status']);
		$mhs->setStatus($status);			

		// set orangtua
		if ( null !== $args['orangtua'] && is_array($args['orangtua']) ) {
			$mhs->setOrangtua($args['orangtua']);
		}
		// set pendidikan
		if ( null !== $args['riwayatPendidikan'] ) {
            $riwayatPendidikan = $args['riwayatPendidikan'];
            	for ($i=0; $i < count($riwayatPendidikan['tahun']); $i++) { 
            		if ( !empty($riwayatPendidikan['tahun'][$i]) ) {
			            $pendidikan = new \AppBundle\Entity\MahasiswaPendidikan();
			            $pendidikan->setMahasiswa($mhs);
			            $pendidikan->setTahun($riwayatPendidikan['tahun'][$i]);
			            $pendidikan->setTglIjazah($riwayatPendidikan['tglIjazah'][$i]);
			            $pendidikan->setGelarAkademik($riwayatPendidikan['gelarAkademik'][$i]);
			            $pendidikan->setJenjang($riwayatPendidikan['jenjangPendidikan'][$i]);
			            $pendidikan->setBidangKeilmuan($riwayatPendidikan['bidangKeilmuan'][$i]);
			            $this->em->persist($pendidikan);
            		}
            }
		}
		// set kelas
		if (null !== $args['kelas']) {
			$mhs->setKelas($args['kelas']);
		}
		// // set berkas
		if ( null !== $args['berkas'] ) {
	        foreach ($args['berkas'] as $berkas) {
	        	$berkas->setMahasiswa($mhs);
	          	$this->em->persist($berkas);
	        }
		}
		// set pembimbing akademik
		if ( null !== $args['pa'] ) {
			$mhs->setPa($args['pa']);
		}
		if ( null !== $args['pembiayaan'] ) {
			$mhs->setPembiayaan($args['pembiayaan']);
		}
		if ( null !== $args['raw'] ) {
			$mhs->setRaw($args['raw']);
		}
		$this->em->persist($mhs);
		$this->em->persist($user);
		$this->em->flush();
		$user->setDataMahasiswa( $mhs );
		$mhs->setUser($user);
		$this->em->persist($mhs);
		$this->em->persist($user);
		$this->em->flush();

		return $user;
	}

	public function getSemesterObj($ta, $tahun, $smt = 1)
	{
		$kode = $tahun . $ta->getKodeSemester();
		$semester = $this->em->getRepository('AppBundle:Semester')
			->findOneBy(array(
				'tahunAkademik'	=> $ta,
				'kode'			=> $kode,
				'semester'		=> $smt
			));
        if ( ! $semester ) {
            $semester = new \AppBundle\Entity\Semester();
            $semester->setKode($kode);
            $namaSemester = $ta->getTahun() . '/';
            $namaSemester .= $namaSemester+1 . ' ';
            $namaSemester .= ( $ta->getKodeSemester() == 1 ) ? 'Ganjil' : 'Genap';
            $semester->setNama($namaSemester);
            $semester->setSemester($smt);
        }
        $semester->setTahunAkademik($ta);
        $this->em->persist($semester);
        $this->em->flush();
        return $semester;
	}

	public function generateFieldValue($value = null, $dataType = null)
	{
		if ( $dataType = 'date' ) {
			$value = ( !empty( trim($value) ) ) ? new\DateTime($value) : null;
		}
		return !empty($value) ? $value : null;
	}

	public function generateNoUrutPmb($periode) {
		$no_urut = $this->getSetting('no_urut_pmb_'.$periode->getId(), true);
		if ( $no_urut > $periode->getKapasitas() ) {
			$no_urut = 1;
			$this->updateSetting('no_urut_pmb_'.$periode->getId(), $no_urut);
		} else {
			$this->updateSetting('no_urut_pmb_'.$periode->getId(), $no_urut+1);
		}
		// $data = $this->em->getRepository('AppBundle:Pmb')
		// 	->findByPeriode($periode);
		// if ($data) {
		// 	$no_urut = count($data);
		// 	return (int) $no_urut+1;
		// }
		return $no_urut;
	}

	public function registerUpdateDosen($nidn, $params = array()) 
	{
		//key = email

		$username = $nidn;
		$email = ($params['email'] != '') ? $params['email'] : $username;
		$password = ( !empty($params['nip']) ) ? $params['nip'] : $username;

		$registerUser = $this->registerUser($email, $username, $password, 'dosen');
		if (!$registerUser) {
			$dosen = new Dosen();
		} else {				
			$dosen = $this->em->getRepository('AppBundle:Dosen')
				->findOneByNidn($username);
		}

		if ($dosen) {

			//set agama
			if ( isset($params['id_agama']) ) {
				$agama = $this->getMasterTermObject('agama', $params['id_agama']);
			} else {
				$agama = $params['agama']; //object
			}

			//set prodi
			if ( isset($params['id_prodi']) ) {
				$prodi = $this->em->getRepository('AppBundle:ProgramStudi')
					->findOneByKode($params['id_prodi']);
			} else {
				$prodi = $params['prodi']; //object
			}

			//set pangkat_golongan
			if ( isset($params['id_pangkat_gol']) ) {
				$pangkatGolongan = $this->getMasterTermObject('pangkat_golongan', $params['id_pangkat_gol']);
			} else {
				$pangkatGolongan = null; //object
			}

          	$dosen->setNidn($username);
          	$dosen->setNip( $params['nip'] );
          	$dosen->setNama( $params['nama'] );
          	$dosen->setTptLahir( $params['tpt_lahir'] );
          	$dosen->setTglLahir( new\DateTime($params['tgl_lahir']) );
          	$dosen->setGender( strtolower($params['gender']) );
          	$dosen->setAgama( $agama );
          	$dosen->setNoKtp( $params['ktp'] );
          	$dosen->setTelp( $params['telp']);
          	$dosen->setHp( $params['hp']);
          	$dosen->setEmail( $email );
          	$dosen->setProdi( $prodi );
          	$dosen->setAlamat( $params['alamat']);
          	$dosen->setKota(null);
          	$dosen->setProvinsi(null);
          	$dosen->setKodePos( $params['kode_pos']);
          	$dosen->setPangkatGolongan( $pangkatGolongan );

          	$this->em->persist($dosen);
          	$this->em->flush();
          	return $dosen;
		}

	}

	public function getTahunAkademik() {
		$tahunAkademik = $this->em->getRepository('AppBundle:TahunAkademik')
            ->findOneBy(array(
            	'aktif'		=> 1,
            	'status'	=> 'publish',
            ));
        if ( ! $tahunAkademik ) {
        	$now = new\DateTime();
        	$year = $now->format('Y');
        	$tahunAkademik = $this->em->getRepository('AppBundle:TahunAkademik')
            ->findOneByTahun($year);
            if ($tahunAkademik) {
            	$tahunAkademik->setStatus('publish');
            	$tahunAkademik->setAktif(1);
            	$this->em->persist($tahunAkademik);
            	$this->em->flush();
            }
        }
		return $tahunAkademik;
	}

	public function getTa($kode, $allow_create=false) {
		// $kode = id_smt di forlap (contoh: 20171, 20172, 20173)
		$ta = $this->em->getRepository('AppBundle:TahunAkademik')
            ->findOneBy(array(
            	'kode'		=> $kode,
            	'status'	=> 'publish'
            ));
        if ( ! $ta ) {
        	if ( $allow_create ) {
	        	$tahun = substr($kode, 0,4);
	        	$kodeSemester = substr($kode, -1);
	        	$nama = $tahun . "/" . $tahun+1;
	        	if ( $kodeSemester == 1 ) {
	        		$nama .= " Ganjil";
	        	} elseif ( $kodeSemester == 2 ) {
	        		$nama .= " Genap";
	        	} elseif ( $kodeSemester == 3 ) {
	        		$nama .= " Pendek";
	        	}
	        	$ta = new \AppBundle\Entity\TahunAkademik();
	        	$ta->setKode( $kode );
	        	$ta->setNama( $nama );
	        	$ta->setTahun( $tahun );
	        	$ta->setKodeSemester( $kodeSemester );
	        	$ta->setAktif(0);
	        	$ta->setStatus('publish');
	        	$this->em->persist($ta);
	        	$this->em->flush();
        	}
        }
		return $ta;
	}

	// public function getSemesterObj($kode, $smt = '')
	// {
 //        $semester = $this->em->getRepository('AppBundle:Semester')
 //            ->findOneByKode($kode);
 //        if ( ! $semester ) {
 //            $semester = new \AppBundle\Entity\Semester();
	//         $semester->setKode(trim($kode));

	//         $tahun = substr(trim($kode), 0, 4);
	//         $taKodeSemester = str_replace($tahun, '', $kode);

	//         $namaSemester = $tahun . '/';
	//         $namaSemester .= $namaSemester+1 . ' ';
	//         $namaSemester .= ( $taKodeSemester == 1 ) ? 'Ganjil' : 'Genap';

	//         $semester->setNama($namaSemester);
	//         $smt = ( $smt != '' ) ? $smt : $taKodeSemester;
	//         $semester->setSemester($smt);

	//         $ta = $this->em->getRepository('AppBundle:TahunAkademik')
	//         	->findOneBy(array(
	//         		'kode'			=> $tahun,
	//         		'kodeSemester'	=> $taKodeSemester
	//         	));
	//         if ( !$ta ) {
	//         	$ta = $this->getTahunAkademik();
	//         }
	//         $semester->setTahunAkademik($ta);
	//         $this->em->persist($semester);
	//         $this->em->flush();
 //        }

 //        return $semester;
	// }

	/**
	 * @param $persemester | bool | berdasarkan semester ganjil/genap atau semua
	 * @param $max | maximal semester atau batas sesi dari prodi
	 * @return array
	 */
	public function getSemesterList($persemester = true, $max = 14)
	{
		$ta = $this->getTahunAkademik();
		$results = array(); // return id, semester, nama

		$params = array();
		$kodeSemester = $ta->getTahun().$ta->getKodeSemester(); // ganjgen

		if ( false !== $persemester ) {
			$params['tahunAkademik'] = $ta;
			$params['kode'] = $kodeSemester;

	        $semesterList = $this->em->getRepository('AppBundle:Semester')
	            ->findBy($params);

		} else {
			$params['tahun'] = $ta->getTahun();

	        $semesterList = $this->em->createQueryBuilder()
				->select('s')
		        ->from('AppBundle:Semester', 's')
		        ->where('s.kode= :kode1 OR s.kode = :kode2')
		        ->setParameter('kode1', $ta->getTahun().'1')
		        ->setParameter('kode2', $ta->getTahun().'2')
		        ->getQuery()
		        ->getResult();
		}

        $tmp_result = array(); // hanya penyesuaian
        foreach ($semesterList as $semester) {

        	if ( !isset($tmp_result[$semester->getSemester()]) ) {
	        	$results[] = array(
	        		'id'		=> $semester->getId(),
	        		'nama'		=> 'Semester ' . $semester->getSemester(),
	        		'semester'	=> $semester->getSemester(),
	        		'object'	=> $semester
	        	);

	        	$tmp_result[$semester->getSemester()] = $semester->getSemester();
        	}
        }
        return $results;
	}

	public function hari($datetimeObj) {
		$hari = '';
		if ( null !== $datetimeObj ) {
			$day = $datetimeObj->format('l');
			if ( $day == 'Sunday' ) {
				$hari =  'Minggu';
			} elseif ($day == 'Monday') {
				$hari =  'Senin';
			} elseif ($day == 'Tuesday') {
				$hari =  'Selasa';
			} elseif ($day == 'Wednesday') {
				$hari =  'Rabu';
			} elseif ($day == 'Thursday') {
				$hari =  'Kamis';
			} elseif ($day == 'Friday') {
				$hari =  'Jumat';
			} elseif ($day == 'Saturday') {
				$hari =  'Sabtu';
			} else {
				$hari = $day;
			}
		}
		return $hari;
	}

	public function getKelasList($params = array())
	{
		
		$kelasList = $this->em->getRepository('AppBundle:Kelas')
	        ->findAll();
        return $kelasList;
	}

	public function getCurrentProdi() 
	{	
		if ( "anon." != $this->token_storage->getToken()->getUser() ) {
			$roles = $this->token_storage->getToken()->getUser()->getRoles();
			if ( in_array('ROLE_MAHASISWA', $roles) ) {
				
			} else {
				// kode prodi disimpan di setting (hanya kode prodi)
				$userId = $this->token_storage->getToken()->getUser()->getId();
				$param = '__' . $userId . '_current_prodi';
				$kodeProdi = $this->getSetting($param);
				return $this->em->getRepository('AppBundle:ProgramStudi')
					->findOneByKodeProdi($kodeProdi);
			}
		}
		return null;


			// $param = '__' . $userId . '_current_prodi';
			// $currentProdi = $this->getSetting($param);
			// if( false !== $object && isset($currentProdi['id']) ) {
			// 		$prodi = $this->em->getRepository('AppBundle:ProgramStudi')
			// 			->find($currentProdi['id']);
			// 		return $prodi;
			// } else {
			// 		return ( isset($prodi[$value]) ) ? $prodi[$value] : null;
			// }
	}

	public function getGlobalOption() {
        $options = array();

        $options['prodi'] = $this->getCurrentProdi();

        $themeOptions = $this->em->getRepository('AppBundle:Setting')
          ->findOneByName('theme_option');
        if ($themeOptions) {
            $options = array_merge($options, $themeOptions->getValue());
        }
        if ( !isset($options['theme_skin']) ) {
            $options['theme_skin']['kode'] = 'blue';
        }
        $ptOptions = $this->em->getRepository('AppBundle:Setting')
          ->findOneByName('data_perguruan_tinggi');
        if ($ptOptions) {
            $ptOptionsVal = $ptOptions->getValue();
            if (isset($ptOptionsVal['nama_perguruan_tinggi'])) {
                $options['pt'] = array(
                	'kode'		=> $ptOptionsVal['kode_perguruan_tinggi'],
                	'nama'		=> $ptOptionsVal['nama_perguruan_tinggi'],
                	'alamat'	=> $ptOptionsVal['alamat_utama'],
                	'telp'		=> $ptOptionsVal['telp'],
                	'fax'		=> $ptOptionsVal['fax'],
                	'email'		=> $ptOptionsVal['email'],
                	'website'	=> $ptOptionsVal['website'],
                );
            }
        }
        $options['ta'] = $this->getTahunAkademik();
		return $options;
	}


	/**
	 * Registrasi mahasiswa dari form pmb atau tambah mahasiswa
	 * @return id mahasiswa
	 */
	public function registerMahasiswa($userEntityObj, $periode)
	{
        $defaultStatus = $this->getMasterTermObject('status_mahasiswa', 'X');
        
        $dataMahasiswa = $userEntityObj->getDataMahasiswa();
        $pmb = $dataMahasiswa->getPmb();

        $pmb->setPeriode($periode);

        // set jenis daftar 
        if ( $pmb->getStatusMasuk() == 'PINDAHAN' ) {
          $jenisDaftar = $this->getMasterTermObject('jenis_pendaftaran', 2);
        } else {
          $jenisDaftar = $this->getMasterTermObject('jenis_pendaftaran', 1);
        }

        if ( $jenisDaftar ) {
          $dataMahasiswa->setJenisDaftar($jenisDaftar);
        }

        $tahun = $pmb->getPeriode()->getTahunAngkatan()->getTahun();

        foreach ($pmb->getBerkas() as $berkas) {
          $berkas->setPmb($pmb);
          $this->em->persist($berkas);
        }

        $prodi = $periode->getProdi();
        if ( $prodi ) {
          $userEntityObj->setProdi($prodi);
        }

        $dataMahasiswa->setAngkatan($periode->getTahunAngkatan()->getTahun());
        $dataMahasiswa->setSemester(null);
        $dataMahasiswa->setStatus($defaultStatus);
        $dataMahasiswa->setKelas(null);
        $dataMahasiswa->setMaba(1);

        // set pendidikan
        $riwayatPendidikan = $request->get('riwayatPendidikan');
        if ( is_array($riwayatPendidikan) && count($riwayatPendidikan) > 0 ) {
          for ( $i = 0; $i < count($riwayatPendidikan['tahun']); $i++ ) {
            $pendidikan = new \AppBundle\Entity\MahasiswaPendidikan();
            $pendidikan->setMahasiswa($dataMahasiswa);
            $pendidikan->setTahun($riwayatPendidikan['tahun'][$i]);
            $pendidikan->setTglIjazah($riwayatPendidikan['tglIjazah'][$i]);
            $pendidikan->setGelarAkademik($riwayatPendidikan['gelarAkademik'][$i]);
            $pendidikan->setJenjang($riwayatPendidikan['jenjangPendidikan'][$i]);
            $pendidikan->setBidangKeilmuan($riwayatPendidikan['bidangKeilmuan'][$i]);
            $this->em->persist($pendidikan);
          }
        }

        $em->persist($dataMahasiswa);
        $em->persist($pmb);

        // set nim
        $nim = $this->setNoIndukMahasiswa($prodi, $pmb, $tahun);
        $userEntityObj->setUsername($nim);
        
        $password = md5($nim);
        $userEntityObj->setPassword($password);

        $this->em->persist($userEntityObj);
        $this->em->flush();

        return $userEntityObj->getId();
	}

	public function registerUpdateMahasiswaOld($nim, $params = array()) 
	{
		$email = ($params['email'] != '') ? $params['email'] : $nim;
		$password = ( !empty($params['nip']) ) ? $params['nip'] : $nim;

		$registerUser = $this->registerUser($email, $nim, $password, 'mahasiswa');
		if (!$registerUser) {
			$mhs = new Mahasiswa();
		} else {				
			$mhs = $this->em->getRepository('AppBundle:Mahasiswa')
				->findOneByNim($nim);
		}

		if ($mhs) {

			//set agama
			if ( isset($params['id_agama']) ) {
				$agama = $this->getMasterTermObject('agama', $params['id_agama']);
			} else {
				$agama = $params['agama']; //object
			}

			//set prodi
			if ( isset($params['id_prodi']) ) {
				$prodi = $this->em->getRepository('AppBundle:ProgramStudi')
					->findOneByKode($params['id_prodi']);
			} else {
				$prodi = $params['prodi']; //object
			}

			//set pangkat_golongan
			if ( isset($params['id_pangkat_gol']) ) {
				$pangkatGolongan = $this->getMasterTermObject('pangkat_golongan', $params['id_pangkat_gol']);
			} else {
				$pangkatGolongan = null; //object
			}

          	$mhs->setNidn($username);
          	$mhs->setNip( $params['nip'] );
          	$mhs->setNama( $params['nama'] );
          	$mhs->setTptLahir( $params['tpt_lahir'] );
          	$mhs->setTglLahir( new\DateTime($params['tgl_lahir']) );
          	$mhs->setGender( strtolower($params['gender']) );
          	$mhs->setAgama( $agama );
          	$mhs->setNoKtp( $params['ktp'] );
          	$mhs->setTelp( $params['telp']);
          	$mhs->setHp( $params['hp']);
          	$mhs->setEmail( $email );
          	$mhs->setProdi( $prodi );
          	$mhs->setAlamat( $params['alamat']);
          	$mhs->setKota(null);
          	$mhs->setProvinsi(null);
          	$mhs->setKodePos( $params['kode_pos']);
          	$mhs->setPangkatGolongan( $pangkatGolongan );

          	$this->em->persist($mhs);
          	$this->em->flush();
          	return $mhs;
		}

	}

	public function hapusDosen($nidn)
	{
		$user = $this->em->getRepository('AppBundle:User')
			->findOneByUsername($nidn);
		if ( $user ) {

			if ( null !== $user->getDataDosen() ) {

				$dosen = $user->getDataDosen();

				// remove pa
				$mhsByPa = $this->em->createQueryBuilder()
						->select('m')
				        ->from('AppBundle:Mahasiswa', 'm')
				        ->where('m.pa= :pa')
				        ->setParameter('pa', $dosen)
				        ->getQuery()
				        ->getResult();
				if ($mhsByPa) {
					foreach ($mhsByPa as $mhs) {
						$mhs->setPa(null);
						$this->em->persist($mhs);
					}
				}

				// remove pengampiu
				$dosenPengampu = $this->em->createQueryBuilder()
						->select('d')
				        ->from('AppBundle:DosenPengampu', 'd')
				        ->where('d.dosen= :dosen')
				        ->setParameter('dosen', $dosen)
				        ->getQuery()
				        ->getResult();
				if ($dosenPengampu) {
					foreach ($dosenPengampu as $dp) {
						$this->em->remove($dp);
					}
				}
			
				// remove presensi
				$presensi = $this->em->createQueryBuilder()
						->select('p')
				        ->from('AppBundle:PresensiDosen', 'p')
				        ->where('p.dosen= :dosen')
				        ->setParameter('dosen', $dosen)
				        ->getQuery()
				        ->getResult();
				if ( $presensi ) {
					foreach ($presensi as $pres) {
						$this->em->remove($pres);
					}
				}

				$this->em->remove($dosen);

			}

			$this->em->remove($user);
			$this->em->flush();
		}
	}

	public function hapusMahasiswa($nim)
	{
		$user = $this->em->getRepository('AppBundle:User')
			->findOneByUsername($nim);
		if ($user) {

			if ( null !== $user->getDataMahasiswa() ) {
				$mhs = $user->getDataMahasiswa();

				//remove tugas akhir
				$tugasAkhir = $this->em->getRepository('AppBundle:TugasAkhir')
					->findOneByMahasiswa($mhs);
				if ($tugasAkhir) {
					$this->em->remove($tugasAkhir);
				}

				// remove presensi
				$presensi = $this->em->createQueryBuilder()
						->select('p')
				        ->from('AppBundle:PresensiMahasiswa', 'p')
				        ->where('p.mahasiswa= :mhs')
				        ->setParameter('mhs', $mhs)
				        ->getQuery()
				        ->getResult();
				if ($presensi) {
					foreach ($presensi as $pres) {
						$this->em->remove($pres);
					}
				}

				// remove pendidikan
				$pendidikan = $this->em->createQueryBuilder()
						->select('p')
				        ->from('AppBundle:MahasiswaPendidikan', 'p')
				        ->where('p.mahasiswa= :mahasiswa')
				        ->setParameter('mahasiswa', $mhs)
				        ->getQuery()
				        ->getResult();
				if ($pendidikan) {
					foreach ($pendidikan as $pend) {
						$this->em->remove($pend);
					}
				}

				// remove data cuti
				$cuti = $this->em->createQueryBuilder()
						->select('p')
				        ->from('AppBundle:MahasiswaCuti', 'p')
				        ->where('p.mahasiswa= :mahasiswa')
				        ->setParameter('mahasiswa', $mhs)
				        ->getQuery()
				        ->getResult();
				if ($cuti) {
					foreach ($cuti as $ct) {
						$this->em->remove($ct);
					}
				}

				// remove data krs
				$krs = $this->em->createQueryBuilder()
						->select('p')
				        ->from('AppBundle:Krs', 'p')
				        ->where('p.mahasiswa= :mahasiswa')
				        ->setParameter('mahasiswa', $mhs)
				        ->getQuery()
				        ->getResult();
				if ($krs) { 
					foreach ($krs as $k) {
						$nilaiPerkuliahan = $this->em->createQueryBuilder()
								->select('n')
						        ->from('AppBundle:NilaiPerkuliahan', 'n')
						        ->where('n.krs= :krs')
						        ->setParameter('krs', $k)
						        ->getQuery()
						        ->getResult();
						foreach ($nilaiPerkuliahan as $np) {
							$this->em->remove($np);
						}
						$k->setStatus('trash');
						$this->em->persist($k);
						$this->em->flush();
					}
				}

				// remove data perpuspeminjaman
				$pPeminjaman = $this->em->createQueryBuilder()
						->select('p')
				        ->from('AppBundle:PerpusPeminjaman', 'p')
				        ->where('p.anggota= :anggota')
				        ->setParameter('anggota', $user)
				        ->getQuery()
				        ->getResult();
				if ($pPeminjaman) {
					foreach ($pPeminjaman as $pp) {
						$this->em->remove($pp);
					}
				}

				// remove data transaksi pemb
				$pembayaranTransaksi = $this->em->createQueryBuilder()
						->select('p')
				        ->from('AppBundle:PembayaranTransaksi', 'p')
				        ->where('p.mahasiswa= :mahasiswa')
				        ->setParameter('mahasiswa', $user->getDataMahasiswa())
				        ->getQuery()
				        ->getResult();
				if ($pembayaranTransaksi) {
					foreach ($pembayaranTransaksi as $trx) {
						$this->em->remove($trx);
					}
				}

				// remove wisuda
				$wisuda = $this->em->createQueryBuilder()
						->select('p')
				        ->from('AppBundle:Wisudawan', 'p')
				        ->where('p.mahasiswa= :mahasiswa')
				        ->setParameter('mahasiswa', $user->getDataMahasiswa())
				        ->setMaxResults(1)
				        ->getQuery()
				        ->getOneOrNullResult();
				if ($wisuda) {
					$this->em->remove($wisuda);
				}

				// remove berkas
				if ( null !== $mhs->getBerkas() ) {
		            foreach ($mhs->getBerkas() as $berkas) {
		              $this->em->remove($berkas);
		            }
				}

				$mhs->setTrashed(1);
				$user->setStatus('trash');
				$this->em->persist($user);
				$this->em->flush();

			}
		}
	}

	public function hapusProdi($kode)
	{
		$prodi = $this->em->getRepository('AppBundle:ProgramStudi')
			->findOneByKodeProdi($kode);
		if ( $prodi ) {

			// clear pmb periode
			$pmbPeriode = $this->em->createQueryBuilder()
					->select('p')
			        ->from('AppBundle:PmbPeriode', 'p')
			        ->where('p.prodi= :prodi')
			        ->setParameter('prodi', $prodi)
			        ->getQuery()
			        ->getResult();
			if ($pmbPeriode) {
				foreach ($pmbPeriode as $pp) {
					$pp->setProdi(null);
					$this->em->persist($pp);
					$this->em->flush();
				}
			}

			// clear jam kuliah
			$jamKuliah = $this->em->createQueryBuilder()
					->select('j')
			        ->from('AppBundle:JamKuliah', 'j')
			        ->where('j.prodi= :prodi')
			        ->setParameter('prodi', $prodi)
			        ->getQuery()
			        ->getResult();
			if ($jamKuliah) {
				foreach ($jamKuliah as $jam) {
					$jam->setProdi(null);
					$this->em->persist($jam);
					$this->em->flush();
				}
			}

			// clear pmb
			$pmbData = $this->em->createQueryBuilder()
					->select('p')
			        ->from('AppBundle:Pmb', 'p')
			        ->where('p.prodi= :prodi')
			        ->setParameter('prodi', $prodi)
			        ->getQuery()
			        ->getResult();
			if ($pmbData) {
				foreach ($pmbData as $pmb) {
					$pmb->setProdi(null);
					$this->em->persist($pmb);
					$this->em->flush();
				}
			}

			// clear kurikulum
			$kurikulum = $this->em->createQueryBuilder()
					->select('k')
			        ->from('AppBundle:Kurikulum', 'k')
			        ->where('k.prodi= :prodi')
			        ->setParameter('prodi', $prodi)
			        ->getQuery()
			        ->getResult();
			if ($kurikulum) {
				foreach ($kurikulum as $krk) {
					$krk->setProdi(null);
					$this->em->persist($krk);
					$this->em->flush();
				}
			}

			// clear makul
			$mataKuliah = $this->em->createQueryBuilder()
					->select('m')
			        ->from('AppBundle:Makul', 'm')
			        ->where('m.prodi= :prodi')
			        ->setParameter('prodi', $prodi)
			        ->getQuery()
			        ->getResult();
			if ($mataKuliah) {
				foreach ($mataKuliah as $makul) {
					$makul->setProdi(null);
					$this->em->persist($makul);
					$this->em->flush();
				}
			}

			// clear bobot nilai
			$bobotNilai = $this->em->createQueryBuilder()
					->select('bn')
			        ->from('AppBundle:BobotNilai', 'bn')
			        ->where('bn.prodi= :prodi')
			        ->setParameter('prodi', $prodi)
			        ->getQuery()
			        ->getResult();
			if ($bobotNilai) {
				foreach ($bobotNilai as $bn) {
					$bn->setProdi(null);
					$this->em->persist($bn);
					$this->em->flush();
				}
			}

			// clear mahasiswa
			$mahasiswa = $this->em->createQueryBuilder()
					->select('m')
			        ->from('AppBundle:Mahasiswa', 'm')
			        ->where('m.prodi= :prodi')
			        ->setParameter('prodi', $prodi)
			        ->getQuery()
			        ->getResult();
			if ($mahasiswa) {
				foreach ($mahasiswa as $mhs) {
					$mhs->setProdi(null);
					$this->em->persist($mhs);
					$this->em->flush();
				}
			}

			// clear prodi kurikulum
			$prodiKrk = $this->em->createQueryBuilder()
					->select('p')
			        ->from('AppBundle:ProdiKurikulum', 'p')
			        ->where('p.prodi= :prodi')
			        ->setParameter('prodi', $prodi)
			        ->getQuery()
			        ->getResult();
			if ($prodiKrk) {
				foreach ($prodiKrk as $pk) {
					$pk->setProdi(null);
					$this->em->persist($pk);
					$this->em->flush();
				}
			}

			$this->em->remove($prodi);
			$this->em->flush();
		}
	}

	public function isModuleActivated($mod)
	{
		$module = $this->em->getRepository('AppBundle:Module')
			->findOneBySlug($mod);
		if( $module ) {
			return $module->getActive();
			// return ( $module->getActive() == 1 ) ? true : false;
		}
		return false;
	}

	public function load($view, $parameters = array())
	{
		$template = $view;
		$_route = $this->request->getCurrentRequest()->get('_route');
		if ( "anon." != $this->token_storage->getToken()->getUser() ) {
			$_role = $this->token_storage->getToken()->getUser()->getRoles();
			$_role = $_role[0];
			if ( $_role != 'ROLE_SUPER_ADMIN' ) {
				$module = $this->em->getRepository('AppBundle:Module')
					->findOneBySlug($_route);
				if( $module ) {
					if ( null !== $module->getParent() ) {
						
						if ( !in_array($_role, $module->getParent()->getRoles()) ) {
							$template = "default/404.html.twig";
						}

					} elseif ( !in_array($_role, $module->getRoles()) ) {
						$template = "default/404.html.twig";
					}
				}
			}
		}
		$content = $this->templating->render($template, $parameters);

		return $response = new Response($content);
	}

	/**
	 * @param status: A, C, N, D, G, K, L, X
	 * Default 
	 */
	public function getListMahasiswa($args = [], $period = true, $results = [])
	{
		$user_params = array(
			'status'	=> 'active',
			'hakAkses'	=> $this->getMasterTermObject('hak_akses', 4)
		);
		if ( !empty($args['prodi']) ) {
			$user_params['prodi'] = $args['prodi'];
		}
		$dataMahasiswaUser = $this->em->getRepository('AppBundle:User')
			->findBy($user_params);

		foreach ($dataMahasiswaUser as $user) {
			if ( null !== $user->getDataMahasiswa() ) {
				$results[] = $user->getDataMahasiswa()->getId();
			}
		}

		unset($args['prodi']);

		$params = array(
			'maba'		=> 0,
			'trashed'	=> 1
		);
		$whereQuery = 'm.maba=:maba AND m.trashed!=:trashed';
		foreach ($args as $k => $v) {
			$whereQuery .= ' ';
			if ( is_array($v) ) {
				$whereQuery .= ( isset($v[2]) ) ? $v[2] : 'AND';
				$whereQuery .= ' m.' . $k;
				$whereQuery .= ( isset($v[1]) ) ? $v[1] : '=';
				$whereQuery .= ':' . $k;

				if ( $k == 'kelas' ) {
					$kelas = $this->em->getRepository('AppBundle:Kelas')
						->find($v[0]);
					$params[$k] = $kelas;
					
				} else {
					$params[$k] = $v[0];
				}
			} else {
				$whereQuery .= 'AND ';
				$whereQuery .= 'm.' . $k . '=:' . $k;

				if ( $k == 'kelas' ) {
					$kelas = $this->em->getRepository('AppBundle:Kelas')
						->find($v[0]);
					$params[$k] = $kelas;
				} else {
					$params[$k] = $v;
				}
			}
		}
		$whereQuery = ltrim($whereQuery, ' AND');
		$whereQuery = ltrim($whereQuery, ' OR');
		$whereQuery = trim($whereQuery);

		if ( !empty($whereQuery) ) {
			$whereQuery .= ' AND ';
		}
		if ( !empty( $args['status'] ) ) {
			$status = $this->em->getRepository('AppBundle:Master')
				->findOneByKode($args['status']);
			if ( $status ) {
				$whereQuery .= 'm.status=:status';
				$params['status'] = $status;
			}
		} else {
			$dataStatusMahasiswa = $this->em->createQueryBuilder()
				->select('s')
				->from('AppBundle:Master', 's')
				->where('s.status!=:status_trash and s.type=:type and s.kode in (:kode)')
				->setParameters(array(
					'status_trash'	=> 'trash',
					'type'			=> 'status_mahasiswa',
					'kode'			=> array('A','C','D','G','K')
				))
				->getQuery()
				->getResult();
			$whereQuery .= 'm.status IN (:status)';
			$params['status'] = $dataStatusMahasiswa;
		}

		if ( count($results) > 0 ) {
			$whereQuery .= ' AND m.id IN (:users)';
			$params['users'] = $results;
		}

		if ( $period ) {
			$ta = $this->getTahunAkademik();
			if ( $ta->getKodeSemester() == 2 ) {
				$dataSemester = [2,4,6,8,10,12,14];
			} else {
				$dataSemester = [1,3,5,7,9,11,13];
			}
			$whereQuery .= ' AND m.semester IN (:semester)';
			$params['semester'] = $dataSemester;
		}
		// $limit = ( !empty($args['limit']) ) ? $args
		$qb = $this->em->createQueryBuilder();
        $qb
            ->select('m')
            ->from('AppBundle:Mahasiswa', 'm')
            ->where($whereQuery)
            ->setParameters($params);
            // ->setMaxResults(3);
        $data = $qb->getQuery()->getResult();
        return $data;
	}		

	public function getListMahasiswaBaru($id_periode_pmb = null)
	{
		$data = $this->em->getRepository('AppBundle:Pmb')
	    	->findAll();
       	if ( null !== $id_periode_pmb ) {
       		$periode = $this->em->getRepository('AppBundle:PmbPeriode')
       			->find($id_periode_pmb);
	       	if ($periode) {
		       	$data = $this->em->getRepository('AppBundle:Pmb')
		       		->findByPeriode($periode);
	       	}
       	}
        return $data;
	}

	/**
	 * @param $entity=Dosen
	 */
	public function getDosenMakul($dosenPa, $kelas = null)
	{
		$results = array();
		$tmp_result = array();

		$params = array();
		$params['dosen'] = $dosenPa;
		if ( null !== $kelas ) {
			$params['kelas'] = $kelas;
		}
		$data = $this->em->getRepository('AppBundle:DosenPengampu')
			->findBy($params);
		if ( $data ) {
			foreach ($data as $dp) {
				
				if ( null !== $dp->getMakul() ) {
					if ( !isset($tmp_result[$dp->getMakul()->getId()]) ) {
						$results[] = $dp->getMakul();
					}
					$tmp_result[$dp->getMakul()->getId()] = 1;
				}

			}
		}

		return $results;
	}

	public function getSemesterChoices($prodi=null) {
      	$semesterChoices = array();
      	for ($i=1; $i <= 8; $i++) { 
        	$semesterChoices['Semester ' . $i] = $i;
      	}
      	return $semesterChoices;
	}

	public function getTahunChoices($start = 1980, $end = null) {
		if ( null === $end ) {
			$end = date('Y')+5;
		}
      	$tahunChoices = array();
      	for ($i=$end; $i > $start; $i--) { 
        	$tahunChoices[$i] = $i;
      	}
      	return $tahunChoices;
	}

	public function getMasterChoices($type, $choice_value = 'kode', $choices = array()) 
	{
        $dataChoices = $this->em->getRepository('AppBundle:Master')
          ->findByType( $type );
        foreach ($dataChoices as $data) {
        	if ( $choice_value == 'nama' ) {
        		$choices[$data->getNama()] = $data->getNama();
        	} elseif ( $choice_value == 'kode' ) {
        		$choices[$data->getNama()] = $data->getKode();
        	} else {
        		$choices[$data->getNama()] = $data->getId();
        	}
        }
        return $choices;
	}

	public function getDosenChoices($choice_value = 'nama', $choices = array()) 
	{
        $dataChoices = $this->em->getRepository('AppBundle:Dosen')
          ->findAll();
        foreach ($dataChoices as $data) {
        	if ( null !== $data->getUser() ) {
        	
	        	if ( $choice_value == 'nama' ) {
	        		$choices[$data->getUser()->getNama()] = $data->getUser()->getNama();
	        	} else {
	        		$choices[$data->getUser()->getNama()] = $data->getId();
	        	}
	        }
        }
        return $choices;
	}

	public function getWilayahChoices($wilayah = array()) 
	{
        $dataWilayah = $this->em->getRepository('AppBundle:Wilayah')
          ->findByLevel( $this->getMasterTermObject('level_wilayah', 3) );
        foreach ($dataWilayah as $wil) {
          if ( null !== $wil->getParent() ) {
            $parent = $wil->getParent();
            $nama = '';
            $nama .= trim($wil->getNama()) . ', ' . trim($parent->getNama());
            if ( null !== $parent->getParent() ) {
              $nama .= ', ' . trim($parent->getParent()->getNama());
            }
            $wilayah[$nama] = $nama;
          }
        }
        return $wilayah;
	}

	public function getWilayahName($kode_kecamatan) 
	{
		$nama = '';
        $wilayah = $this->em->getRepository('AppBundle:Wilayah')
          ->findOneByKode( $kode_kecamatan );
        if ( null !== $wilayah->getParent() ) {
            $parent = $wilayah->getParent();
            $nama .= trim($wilayah->getNama()) . ', ' . trim($parent->getNama());
            if ( null !== $parent->getParent() ) {
              $nama .= ', ' . trim($parent->getParent()->getNama());
            }
        }
        return $nama;
	}

	public function getOrangtuaMahasiswa($nim)
	{
		$default = array(
			'nama'			=> '',
			'nik'			=> '',
			'agama'			=> null,
			'pendidikan'	=> '',
			'pekerjaan'		=> null,
			'penghasilan'	=> null,
			'status'		=> '',
			'alamat'		=> '',
			'provinsi'		=> '',
			'kota'			=> '',
			'pos'			=> '',
			'telp'			=> '',
			'hp'			=> '',
			'email'			=> '',
			'fk_id_agama'	=> null,
			'fk_id_pendidikan'	=> null,
			'fk_id_pekerjaan'	=> null,
			'fk_id_penghasilan'	=> null,
		);
		$result = array(
			'ayah'	=> $default,
			'ibu'	=> $default,
		);

		$user = $this->em->getRepository('AppBundle:User')
			->findOneByUsername($nim);

		if ( ! $user ) {
			return $result;
		}

		if ( null === $user->getDataMahasiswa() ) {
			return $result;
		}

		$mahasiswa = $user->getDataMahasiswa();

		if ( null === $mahasiswa->getOrangtua() ) {
			return $result;
		}

		$orangtua = $mahasiswa->getOrangtua();

		if ( isset($orangtua['ayah']) ) {
			$result['ayah'] = array_merge($result['ayah'], $orangtua['ayah']);

			if ( $result['ayah']['pendidikan'] == 'TAMAT SMA' ) {
				$result['ayah']['fk_id_pendidikan'] = 6;
			} 
			elseif ( $result['ayah']['pendidikan'] == 'TAMAT SMP' ) {
				$result['ayah']['fk_id_pendidikan'] = 5;
			}
			elseif ( $result['ayah']['pendidikan'] == 'TAMAT SD' ) {
				$result['ayah']['fk_id_pendidikan'] = 4;
			}
			elseif ( $result['ayah']['pendidikan'] == 'TIDAK TAMAT SD' ) {
				$result['ayah']['fk_id_pendidikan'] = 3;
			}

			if ( isset($result['ayah']['agama']) && !is_null($result['ayah']['agama']) ) {
				$result['ayah']['fk_id_agama'] = (int)$result['ayah']['agama']->getKode();
			}
			if ( isset($result['ayah']['pekerjaan']) && !is_null($result['ayah']['pekerjaan']) ) {
				$result['ayah']['fk_id_pekerjaan'] = (int)$result['ayah']['pekerjaan']->getKode();
			}
			if ( isset($result['ayah']['penghasilan']) && !is_null($result['ayah']['penghasilan']) ) {
				$result['ayah']['fk_id_penghasilan'] = (int)$result['ayah']['penghasilan']->getKode();
			}
		}
		if ( isset($orangtua['ibu']) ) {
			$result['ibu'] = array_merge($result['ibu'], $orangtua['ibu']);
			if ( $result['ibu']['pendidikan'] == 'TAMAT SMA' ) {
				$result['ibu']['fk_id_pendidikan'] = 6;
			} 
			elseif ( $result['ibu']['pendidikan'] == 'TAMAT SMP' ) {
				$result['ibu']['fk_id_pendidikan'] = 5;
			}
			elseif ( $result['ibu']['pendidikan'] == 'TAMAT SD' ) {
				$result['ibu']['fk_id_pendidikan'] = 4;
			}
			elseif ( $result['ibu']['pendidikan'] == 'TIDAK TAMAT SD' ) {
				$result['ibu']['fk_id_pendidikan'] = 3;
			}

			if ( isset($result['ibu']['agama']) && !is_null($result['ibu']['agama']) ) {
				$result['ibu']['fk_id_agama'] = (int)$result['ibu']['agama']->getKode();
			}
			if ( isset($result['ibu']['pekerjaan']) && !is_null($result['ibu']['pekerjaan']) ) {
				$result['ibu']['fk_id_pekerjaan'] = (int)$result['ibu']['pekerjaan']->getKode();
			}
			if ( isset($result['ibu']['penghasilan']) && !is_null($result['ibu']['penghasilan']) ) {
				$result['ibu']['fk_id_penghasilan'] = (int)$result['ibu']['penghasilan']->getKode();
			}
		}

		// $ayah = $orangtua['ayah'];
		// $ibu = $orangtua['ibu'];

		// if ( $ayah->get ) {
		// 	# code...
		// }

		return $result;
	}

	/**
	 * @param $mhs = object, $semester = int, $ret = huruf atau angka
	 * @return IPS
	 */
	public function getIpsMahasiswaOld($mhs, $semester=1, $ret='angka') 
	{
		$nilai_ips = 0;
		// if ( null !== $mhs ) {
		// 	if ( null !== $mhs->getUser() ) {
		// 		$dataKrs = $this->em->getRepository('AppBundle:Krs')
		// 			->findBy(array(
		// 				'mahasiswa'	=> $mhs,
		// 				'semester'	=> $semester
		// 			));
		// 		foreach ($dataKrs as $krs) {
		// 			$nilai_bobot = 0;
		// 			if ( null !== $krs->getMakul() ) {
		// 				$makul = $krs->getMakul();
		// 				$jmlSks = $makul->getSksTeori()+$makul->getSksPraktek()+$makul->getSksLapangan();
		// 				$bobot = $this->getBobotNilai();
		// 				// $nilai_bobot += $jmlSks*
		// 			}
		// 			// $nilai_ips +=
		// 		}
		// 	}
		// }

		$args_aktifitas = array(
			'semester'		=> $semester,
			'jumlah_sks'	=> 0,
			'mahasiswa'		=> $mhs,
			'ips'			=> 0,
			'ipk'			=> 0,
			'status'		=> 'publish'
		);

		if ( null !== $mhs ) {
			if ( null !== $mhs->getUser() ) {
				$aspekNilai = $this->em->getRepository('AppBundle:AspekNilai')
					->findAll();
				foreach ($aspekNilai as $aspek) {
					$dataKrs = $this->em->getRepository('AppBundle:Krs')
						->findBy(array(
							'mahasiswa'	=> $mhs,
							'semester'	=> $semester
						));
					foreach ($dataKrs as $krs) {
						$jumlah_sks = 0;
						$nilai_ips = 0;
						if ( null !== $krs->getMakul() ) {
							$makul = $krs->getMakul();
							$nilaiPerkuliahan = $this->getNilaiPerkuliahan($mhs, $makul, $aspek);
							$jumlah_sks = $makul->getSksTeori()+$makul->getSksPraktek()+$makul->getSksLapangan();
							// $bobot = $this->getBobotNilai();
							// $nilai_bobot += $jmlSks*
							$nilai_ips += $nilaiPerkuliahan[1];
						}
					}
					$args_aktifitas['jumlah_sks'] += $jumlah_sks;
					$args_aktifitas['ips'] += $nilai_ips;
				}
			}
		}

		$aktifitas = $this->em->getRepository('AppBundle:MahasiswaAktifitas')
			->findOneBy(array(
				'mahasiswa'	=> $mhs,
				'semester'	=> $semester
			));
		if ( !$aktifitas ) {
			$aktifitas = new \AppBundle\Entity\MahasiswaAktifitas();
			$aktifitas->setMahasiswa($mhs);
		}
		$aktifitas->setSemester($args_aktifitas['semester']);
		$aktifitas->setJumlahSks($args_aktifitas['jumlah_sks']);
		$aktifitas->setIps( $this->getBobotNilai($args_aktifitas['ips']) );
		$aktifitas->setIpk( $this->getBobotNilai($args_aktifitas['ipk']) );
		$aktifitas->setStatus($args_aktifitas['status']);
		$this->em->persist($aktifitas);
		$this->em->flush();
		return $aktifitas->getIps();

	}

	/**
	 * @param $mhs = object, $semester = int, $ret = huruf atau angka
	 * @return IPS
	 */
	public function getIpsMahasiswa($mhs=null, $semester=1) 
	{
		$nilai_ips = 0;
		$total_sks = 0;
		$total_nilai = 0;
		if ( null !== $mhs ) {
			if ( null !== $mhs->getUser() ) {
				$dataKrs = $this->em->getRepository('AppBundle:Krs')
					->findBy(array(
						'mahasiswa'	=> $mhs,
						'semester'	=> $semester,
						// 'status'	=> 'aktif',
					));
				foreach ($dataKrs as $krs) {
					if ( null !== $krs->getMakul() && null !== $krs->getMakul()->getMakul() ) {
						$makul = $krs->getMakul()->getMakul();
						$jumlah_sks_makul = $makul->getSksPraktek()+$makul->getSksPraktek()+$makul->getSksLapangan();
						$total_sks += $jumlah_sks_makul;
						$total_nilai += $jumlah_sks_makul*$krs->getNilaiAngka();
					}
				}
				$nilai_ips = $total_nilai/$total_sks;
			}
		}
		// $nilai_ips = ( $nilai_ips > 0 ) ? ($total_nilai/$total_sks) : 0;
		return round($nilai_ips, 2);
	}

	/**
	 * @param $mhs = object
	 * @return IPK
	 */
	public function getIpkMahasiswa($mhs, $semester=1) 
	{
		$ipk = 0;
		if ( null !== $mhs ) {
			if ( null !== $mhs->getUser() ) {
				if ( $semester == 8 ) {
					$ips_s1 = $this->getIpsMahasiswa($mhs, 1);
					$ips_s2 = $this->getIpsMahasiswa($mhs, 2);
					$ips_s3 = $this->getIpsMahasiswa($mhs, 3);
					$ips_s4 = $this->getIpsMahasiswa($mhs, 4);
					$ips_s5 = $this->getIpsMahasiswa($mhs, 5);
					$ips_s6 = $this->getIpsMahasiswa($mhs, 6);
					$ips_s7 = $this->getIpsMahasiswa($mhs, 7);
					$ips_s8 = $this->getIpsMahasiswa($mhs, $semester);
					$ipk += $ips_s1+$ips_s2+$ips_s3+$ips_s4+$ips_s5+$ips_s6+$ips_s7+$ips_s8;
					$ipk = $ipk/8;
				} elseif ( $semester == 7 ) {
					$ips_s1 = $this->getIpsMahasiswa($mhs, 1);
					$ips_s2 = $this->getIpsMahasiswa($mhs, 2);
					$ips_s3 = $this->getIpsMahasiswa($mhs, 3);
					$ips_s4 = $this->getIpsMahasiswa($mhs, 4);
					$ips_s5 = $this->getIpsMahasiswa($mhs, 5);
					$ips_s6 = $this->getIpsMahasiswa($mhs, 6);
					$ips_s7 = $this->getIpsMahasiswa($mhs, $semester);
					$ipk += $ips_s1+$ips_s2+$ips_s3+$ips_s4+$ips_s5+$ips_s6+$ips_s7;
					$ipk = $ipk/7;
				} elseif ( $semester == 6 ) {
					$ips_s1 = $this->getIpsMahasiswa($mhs, 1);
					$ips_s2 = $this->getIpsMahasiswa($mhs, 2);
					$ips_s3 = $this->getIpsMahasiswa($mhs, 3);
					$ips_s4 = $this->getIpsMahasiswa($mhs, 4);
					$ips_s5 = $this->getIpsMahasiswa($mhs, 5);
					$ips_s6 = $this->getIpsMahasiswa($mhs, $semester);
					$ipk += $ips_s1+$ips_s2+$ips_s3+$ips_s4+$ips_s5+$ips_s6;
					$ipk = $ipk/6;
				} elseif ( $semester == 5 ) {
					$ips_s1 = $this->getIpsMahasiswa($mhs, 1);
					$ips_s2 = $this->getIpsMahasiswa($mhs, 2);
					$ips_s3 = $this->getIpsMahasiswa($mhs, 3);
					$ips_s4 = $this->getIpsMahasiswa($mhs, 4);
					$ips_s5 = $this->getIpsMahasiswa($mhs, $semester);
					$ipk += $ips_s1+$ips_s2+$ips_s3+$ips_s4+$ips_s5;
					$ipk = $ipk/5;
				} elseif ( $semester == 4 ) {
					$ips_s1 = $this->getIpsMahasiswa($mhs, 1);
					$ips_s2 = $this->getIpsMahasiswa($mhs, 2);
					$ips_s3 = $this->getIpsMahasiswa($mhs, 3);
					$ips_s4 = $this->getIpsMahasiswa($mhs, $semester);
					$ipk += $ips_s1+$ips_s2+$ips_s3+$ips_s4;
					$ipk = $ipk/4;
				} elseif ( $semester == 3 ) {
					$ips_s1 = $this->getIpsMahasiswa($mhs, 1);
					$ips_s2 = $this->getIpsMahasiswa($mhs, 2);
					$ips_s3 = $this->getIpsMahasiswa($mhs, $semester);
					$ipk += $ips_s1+$ips_s2+$ips_s3;
					$ipk = $ipk/3;
				} elseif ( $semester == 2 ) {
					$ips_s1 = $this->getIpsMahasiswa($mhs, 1);
					$ips_s2 = $this->getIpsMahasiswa($mhs, $semester);
					$ipk += $ips_s1+$ips_s2;
					$ipk = $ipk/2;
				} else {
					$ips_s1 = $this->getIpsMahasiswa($mhs, $semester);
					$ipk = $ips_s1;
				}
			}
		}
		return round($ipk, 2);

	}

    public function getKrsNilaiAspek($krs = null, $aspekNilai = null, $persentase = false, $id_np = false){ 

        if ( null === $krs ) {
            return 0;
        }

        $mhs = $krs->getMahasiswa();
        $nilai = 0;

        if ( $aspekNilai->getNama() == 'Absensi' ) {

            $presensi = $this->em->getRepository('AppBundle:PresensiMahasiswa')
                ->findBy(array(
                    // 'tahunAkademik' => $ta,
                    // 'makul'     => $makul,
                    'mahasiswa'      => $mhs,
                    'ket'       => 'hadir'
                ));
            $presensiCount = count($presensi);
            $jmlPertemuan = 14;
            $nilai_persen = (( ( $presensiCount/$jmlPertemuan )*$aspekNilai->getPersen() )/100)*100;
            $nilai = ( $persentase ) ? round($nilai_persen, 2) : $presensiCount;

        } else {

            $np = $this->em->getRepository('AppBundle:NilaiPerkuliahan')
                ->findOneBy(array(
                    'krs'           => $krs,
                    'aspekNilai'    => $aspekNilai
                ));

            if ( !$np ) {
                $np = new \AppBundle\Entity\NilaiPerkuliahan();
                $np->setKrs($krs);
                $np->setAspekNilai($aspekNilai);
                $np->setNilai(0);
                $this->em->persist($np);
                $this->em->flush();
            }
            $nilai_persen = ($np->getNilai()*$aspekNilai->getPersen())/100;
            if ( $id_np ) {
            	$nilai = $np->getId();
            } else {
            	$nilai = ( $persentase ) ? round($nilai_persen, 2) : $np->getNilai();
            }
        }

        return $nilai;
    }

    public function getListMakulByKelas($kelas, $prodi=null) 
    {
    	$results = array();
        $dataDosenPengampu = $dosen->getMakulDiampu();
        foreach ($dataDosenPengampu as $dp) {
          // if ( null !== $dp->getMakul() && null !== $dp->getMakul()->getMakul() && !in_array($dp->getMakul()->getMakul()->getId(), $unset) ) {
          	$makul = $dp->getMakul()->getMakul();
            $results[] = array(
            	'id'			=> $dp->getMakul()->getId(),
            	'id_mk'			=> $makul->getId(),
            	'kode_mk'		=> $makul->getKode(),
            	'nama_mk'		=> $makul->getNama(),
            	'id_kls'		=> ( null !== $dp->getMakul()->getKelas() ) ? $dp->getMakul()->getKelas()->getId() : '',
            	'nm_kls'		=> ( null !== $dp->getMakul()->getKelas() ) ? $dp->getMakul()->getKelas()->getNama() : '',
            );
            // array_push($unset, $dp->getMakul()->getMakul()->getId());
          // }
        }
        return $results;
    }

    public function getListMakulByDosen($dosen, $prodi=null) 
    {
    	$results = array();
        $unset = array();
        $dataDosenPengampu = $dosen->getMakulDiampu();
        foreach ($dataDosenPengampu as $dp) {
          // if ( null !== $dp->getMakul() && null !== $dp->getMakul()->getMakul() && !in_array($dp->getMakul()->getMakul()->getId(), $unset) ) {
          	$makul = $dp->getMakul()->getMakul();
            $results[] = array(
            	'id'			=> $dp->getMakul()->getId(),
            	'id_mk'			=> $makul->getId(),
            	'kode_mk'		=> $makul->getKode(),
            	'nama_mk'		=> $makul->getNama(),
            	'id_kls'		=> ( null !== $dp->getMakul()->getKelas() ) ? $dp->getMakul()->getKelas()->getId() : '',
            	'nm_kls'		=> ( null !== $dp->getMakul()->getKelas() ) ? $dp->getMakul()->getKelas()->getNama() : '',
            );
            // array_push($unset, $dp->getMakul()->getMakul()->getId());
          // }
        }
        return $results;
    }

    public function getListKelasByMakul($makul, $prodi, $dosen=null) 
    {
    	// $makul = object makul
    	$results = array();
        if ( null !== $dosen ) {
        	
        	$dataMakulDosen = $this->getListMakulByDosen($dosen, $prodi);
	        foreach ($dataMakulDosen as $mk) {
	            $results[] = array(
	                'id'    => $mk['id_kls'],
	                'nama'  => 'yes',
	            );
	        }

        } else {

	        $dataMakulKurikulum = $this->em->getRepository('AppBundle:KurikulumMakul')
		        ->findBy(array(
		          // 'prodi'   => $prodi,
		          'makul'   => $makul
		        ));
	        foreach ($dataMakulKurikulum as $mk) {
	          if ( null !== $mk->getKelas() ) {
	              $results[] = array(
	                'id'    => $mk->getKelas()->getId(),
	                'nama'  => $mk->getKelas()->getNama()
	              );
	          }
	        }

        }
        return $results;
    }

    public function getNilaiPerkuliahan($mhs, $makul, $aspekNilai, $aspekNilaiDosen = null, $ta = null){ 

        // $krs = $this->em->getRepository('AppBundle:Krs')
        //     ->findOneBy(array(
        //         'mahasiswa' => $mhs,
        //         'makul'     => $makul
        //     ));
        // if ( !$krs ) {
        //     return 0;
        // }

        if ( $aspekNilai->getNama() == 'Absensi' ) {

            $presensi = $this->em->getRepository('AppBundle:PresensiMahasiswa')
                ->findBy(array(
                    // 'tahunAkademik' => $ta,
                    'semester'  => $mhs->getSemester(),
                    'kelas'     => $mhs->getKelas(),
                    'makul'     => $makul,
                    'user'      => $mhs->getUser(),
                    'ket'       => 'hadir'
                ));
            $presensiCount = count($presensi);
            $jmlPertemuan = 14;

            $final_np = (( ( $presensiCount/$jmlPertemuan )*$aspekNilai->getPersen() )/100)*100;

            $np = array(
                $presensiCount,
                round($final_np, 2), // setelah di persen
                0
            );

        } else {

            $nilai = $this->em->getRepository('AppBundle:NilaiPerkuliahan')
                ->findOneBy(array(
                    // 'krs'           => $krs,
                    'aspekNilai'    => $aspekNilai
                ));

            if ( !$nilai ) {
                $nilai = new \AppBundle\Entity\NilaiPerkuliahan();
                // $nilai->setKrs($krs);
                if ( null !== $aspekNilaiDosen ) {
                    $nilai->setAspekNilaiDosen($aspekNilaiDosen);
                } else {
                    $nilai->setAspekNilai($aspekNilai);
                }
                $nilai->setNilai(0);
                $this->em->persist($nilai);
                $this->em->flush();
            }
            $final_np = ($nilai->getNilai()*$aspekNilai->getPersen())/100;
            $np = array(
                $nilai->getNilai(),
                round($final_np, 2), // setelah di persen
                $nilai->getId()
            );

        }

        return $np; // nilai real, nilai persenstasi, id
    }

    public function getBobotNilai($nilai, $huruf = false, $lulus = false)
    {
        $bobot = $this->em->createQueryBuilder()
            ->select('b')
            ->from('AppBundle:BobotNilai', 'b')
            ->where('b.global = :global AND b.min <= :nilai AND b.max >= :nilai')
            ->setParameter('global', 1)
            ->setParameter('nilai', $nilai)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
        if ( !$bobot ) {
            return '';
        }
        if ( $lulus ) {
            return ( $bobot->getLulus() ) ? 'Ya' : 'Tidak';
        }
        if ( $huruf ) {
            return $bobot->getPredikat();
        }
        return $bobot->getBobot();
    }


    public function tambahAktifitasUser($user, $data) 
    {
    	$aktifitas = new \AppBundle\Entity\Aktifitas();
    	$aktifitas->setUser($user);
    	$aktifitas->setJudul($data['judul']);
    	$aktifitas->setKet($data['ket']);
    	$aktifitas->setStatus( ( !empty($data['status']) ) ? $data['status'] : 'publish' );
    	$aktifitas->setCreatedAt(new\DateTime());

    	$this->em->persist($aktifitas);
    	$this->em->flush();
    	return $aktifitas;
    }

    public function getCountPresensiMahasiswa($mhs, $makul, $ket) 
    {
        $params = array(
            'makul'         => $makul,
            'mahasiswa'     => $mhs,
            'ket'           => strtolower($ket)
        );
        $presensi = $this->em->getRepository('AppBundle:PresensiMahasiswa')
            ->findBy($params);
        $jumlah = count($presensi);
        return ($jumlah >  0) ? $jumlah : '-';
    }

    public function getCountPresensiDosen($dosen, $ket, $ta = null) 
    {
    	if ( is_null($ta) ) {
    		$ta = $this->getTahunAkademik();
    	}
        $params = array(
            'ta'		=> $ta,
            'dosen'     => $dosen,
            'ket'       => strtolower($ket)
        );
        $presensi = $this->em->getRepository('AppBundle:PresensiDosen')
            ->findBy($params);
        $jumlah = count($presensi);
        return ($jumlah >  0) ? $jumlah : '-';
    }

    public function getKrsJadwalKuliah($krs = null)
    {
		$result = array(
			'hari' 	=> '',
			'jam'	=> '',
			'ruang'	=> '',
		);
    	if ( null !== $krs ) {
    		$jadwal = $this->em->getRepository('AppBundle:Jadwal')
    			->findOneBy(array(
    				// 'ta'			=> $krs->getMakul()->getTa(),
    				// 'semester'		=> $krs->getMakul()->getSemester(),
    				// 'kelas'			=> $krs->getMakul()->getKelas()->getNama(),
    				'makul'			=> $krs->getMakul(),
    			));
    		if ( $jadwal ) {
	    		$jamKuliah = $jadwal->getJam();
    			$result = array(
    				'hari' 	=> ( null !== $jadwal->getHari() ) ? $jadwal->getHari()->getNama() : '',
    				'jam'	=> ( !$jamKuliah ) ? $jadwal->getJam() : $jamKuliah->getDari()->format('H:i') . " - " . $jamKuliah->getSampai()->format('H:i') . " WIB",
    				'ruang'	=> ( null !== $jadwal->getKelas() ) ? $jadwal->getKelas()->getNama() : ''
    			);
    		}
    	}
    	return $result;
    }

}