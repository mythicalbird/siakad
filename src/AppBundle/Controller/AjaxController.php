<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Service\AppService;
use AppBundle\Service\DosenPengampu;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AjaxController extends Controller
{
		
		protected $appService;
	
		public function __construct(AppService $appService) {
			$this->appService = $appService;
		}
	
    /**
     * @Route("/_ajax/edit/nilai_usm", name="nilai_usm_update")
		 * @Method({"POST"})
     */
    public function nilaiUsmEditAction(Request $request)
    {
		    $response = new JsonResponse();
		    $em = $this->getDoctrine()->getManager();
        $mhs = $em->getRepository('AppBundle:Mahasiswa')
            ->find($request->get('pk'));
        if ( $mhs ) {

            $lulus = 0;
            if ( null !== $mhs->getData() ) {
              $data = $mhs->getData();
              $nilaiMinimal = $mhs->getPeriode()->getNilaiMinimal();
              $lulus = ( $request->get('value') >= $nilaiMinimal ) ? 1 : 0;

              $data['pmb']['lulus'] = $lulus; 
              $data['pmb']['nilaiUsm'] = $request->get('value'); 
              $mhs->setData($data);
            }
						$em->persist($mhs);
						$em->flush();
					
						$response->setData(array(
							'id'       => $request->get('pk'),
							'success'  => 1,
              'lulus'    => $lulus
						));
        }
				return $response;
    }

	
    /**
     * @Route("/_ajax/edit/kelas_mahasiswa", name="kelas_mahasiswa_update")
		 * @Method({"POST"})
     */
    public function kelasMahasiswaEditAction(Request $request)
    {
				$response = new JsonResponse();
				$em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:Mahasiswa')->find($request->get('pk'));
        if ( $data ) {
						$kelas = $em->getRepository('AppBundle:Kelas')->find($request->get('value'));
						if( $kelas ) {
							$data->setKelas($kelas);
							$em->persist($data);
							$em->flush();
							
							$response->setData(array(
								'id' => $request->get('pk'),
								'success' => 1
							));							
						}
        }
				return $response;
    }

    /**
     * @Route("/_ajax/check/email", name="email_check")
		 * @Method({"POST"})
     */
    public function emailCheckAction(Request $request)
    {
				$response = new JsonResponse();
				$userManager = $this->get('fos_user.user_manager');
				$email_exist = $userManager->findUserByEmail($request->get('email'));
				$response->setData(array(
					'registered' => ($email_exist) ? 1 : 0
				));
				return $response;
    }
	
    /**
     * @Route("/_ajax/hapus/data_entity", name="hapus_data_entity")
		 * @Method({"POST"})
     */
    public function hapusDataEntityAction(Request $request)
    {
				$response = new JsonResponse();
				$entity = $request->get('entity');
				$em = $this->getDoctrine()->getManager();
				$data = $em->getRepository('AppBundle:'.$entity)
          ->find($request->get('id'));
				if( $data ) {

            if ( $entity == 'Mahasiswa' || $entity == 'Dosen' || $entity == 'Pegawai' ) {

              if ( null !== $entity->getUser() ) {
                $user = $entity->getUser();
                $user->setStatus('trash');
                $em->persist($user);
              }

            }
            elseif ( $entity == 'TahunAkademik' || $entity == 'PmbPeriode' || $entity == 'Master' ) {

              $data->setStatus('trash');
              $em->persist($data);

            } 
            else {

              if ( $entity == 'Krs' ) {

                if ( null !== $data->getNilaiAspek() ) {
                  $em->remove($data->getNilaiAspek());
                }

              } elseif ( $entity == 'KurikulumMakul' ) {
                
                if ( null !== $data->getDosenPengampu() ) {
                  foreach ($data->getDosenPengampu() as $dp) {
                    $em->remove($dp);
                  }
                }

              } 

              $em->remove($data);

            }

					
  					$em->flush();
  					$response->setData(array(
  						'success' => true,
  						'message'	=> ''
  					));

				} else {
  					$response->setData(array(
  						'success' => false,
  						'message'	=> 'Terjadi kesalahan'
  					));
				}
        
				return $response;
    }

    /**
     * @Route("/_ajax/cari/mahasiswa", name="cari_mahasiswa")
     * @Method({"POST"})
     */
    public function cariMahasiswaAction(Request $request)
    {
        $response = new JsonResponse();
        $mahasiswa = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('nim'));
        if ($mahasiswa) {
          if( null !== $mahasiswa->getProdi() ) {
            $prodi = $mahasiswa->getProdi()->getNamaProdi();
          } else {
            $prodi = 'Tidak ditemukan';
          }
          $html = '<div class="row"><div class="col-md-12"><table class="table table-bordered table-responsive">';
          $html .= '<tr>';
          $html .= '<td>NIM</td>';
          $html .= '<td><em>'.$mahasiswa->getUsername().'</em></td>';
          $html .= '</tr>';
          $html .= '<tr>';
          $html .= '<td>Nama Mahasiswa</td>';
          $html .= '<td><strong>'.$mahasiswa->getNama().'</strong></td>';
          $html .= '</tr>';
          $html .= '<tr>';
          $html .= '<td>Jurusan</td>';
          $html .= '<td>'.$mahasiswa->getNama().'</td>';
          $html .= '</tr>';
          $html .= '<tr><td>Program Studi</td>';
          $html .= '<td>'.$prodi.'</td>';
          $html .= '</tr>';
          $html .= '</table></div></div>';
          $html .= '<div class="clearfix"></div>';
          $response->setData($html);
        } else {
          $response->setData('null');
        }
        return $response;
    }

    /**
     * @Route("/_ajax/ambil_data/user", name="ambil_data_user")
     * @Method({"POST"})
     */
    public function ambilDataUserAction(Request $request)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        if ( !empty( $request->get('username') ) ) {
          $user = $em->getRepository('AppBundle:User')
            ->findOneByUsername( $request->get('username') );
        } else {
          $user = $em->getRepository('AppBundle:User')
            ->find( $request->get('id') );
        }

        if ($user) {

            $result = array(
                'nama'      => $user->getNama(),
                'username'  => $user->getUsername(),
                'prodi'     => ( null !== $user->getProdi() ) ? $user->getProdi()->getNamaProdi() : '',
            );

            if ( in_array('ROLE_MAHASISWA', $user->getRoles()) ) {
              $data = $user->getDataMahasiswa();
              $result['data'] = array(
                'angkatan'  => $data->getAngkatan(),
                'semester'  => $data->getSemester()
              );
            }

            $response->setData($result);

        }
        return $response;
    }

    /**
     * @Route("/_ajax/ambil_data/mahasiswa", name="ambil_data_mahasiswa")
     * @Method({"POST"})
     */
    public function ambilDataMahasiswaAction(Request $request)
    {
        $id = $request->get('id'); // id mahasiswa
        $jenis = $request->get('jenis'); // berkas, pendidikan
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();
        $mahasiswa = $em->getRepository('AppBundle:Mahasiswa')
          ->find($request->get('id'));

        if ($mahasiswa) {

          if ( $jenis == 'pendidikan' ) {
            $data = $em->createQueryBuilder()
              ->select('m')
              ->from('AppBundle:MahasiswaPendidikan', 'm')
              ->where('m.mahasiswa= :mahasiswa')
              ->setParameter('mahasiswa', $mahasiswa)
              ->getQuery()
              ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            $response->setData($data);
          
          } elseif ( $jenis == 'berkas' ) {
            
            $data = array();

            if ( $mahasiswa->getBerkas() !== null ) {
              foreach ($mahasiswa->getBerkas() as $berkas) {
                if ( null !== $berkas ) {
                  $data[] = $berkas->getImageName();
                }
              }
            }

            $dataBerkas = array();
            $typeBerkas = $em->createQueryBuilder()
              ->select('m')
              ->from('AppBundle:Master', 'm')
              ->where('m.type= :type')
              ->setParameter('type', 'berkas_pmb')
              ->getQuery()
              ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            for ($i=0; $i < count($typeBerkas); $i++) {
              if (isset($data[$i])) {
                $dataBerkas[] = array(
                  'nama'  => $typeBerkas[$i]['kode'],
                  'file'  => $data[$i],
                  'url'   => $this->generateUrl( 'dashboard', array(), UrlGeneratorInterface::ABSOLUTE_URL )
                );
              }
            }

            $response->setData($dataBerkas);

          }

        }
        return $response;
    }

    /**
     * @Route("/_ajax/ambil_data/pustaka", name="ambil_data_pustaka")
     * @Method({"POST"})
     */
    public function ambilDataPustakaAction(Request $request)
    {
        $kode = $request->get('id'); // id mahasiswa
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();
        $data = $em->createQueryBuilder()
          ->select('u')
          ->from('AppBundle:PerpusPustaka', 'u')
          ->where('u.kode= :kode')
          ->setParameter('kode', $kode)
          ->getQuery()
          ->setMaxResults(1)
          ->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if ($data) {

            $now = new\DateTime();
            $response->setData($data);
            $peminjaman = new \AppBundle\Entity\PerpusPeminjaman();
            $peminjaman->setTanggal($now->format('Y-m-d'));
            $peminjaman->setJam($now->format('H:i:s'));
            $peminjaman->setAnggota($this->getUser());
            $em->persist($peminjaman);
            $em->flush();

        }
        return $response;
    }



    /**
     * @Route("/_ajax/status_krs/update", name="status_krs_update")
     * @Method({"POST"})
     */
    public function statusKrsUpdateAction(Request $request)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:Krs')->find($request->get('pk'));
        if ( $data ) {
            $data->setStatus($request->get('value'));
            $em->persist($data);
            $em->flush();
            
            $response->setData(array(
              'id' => $request->get('pk'),
              'success' => 1
            ));
        }
        return $response;
    }


    /**
     * @Route("/_ajax/list/mahasiswa", name="data_list_mahasiswa")
     */
    public function dataListMahasiswaAction(Request $request)
    {
        $fields = array(); 
        $result = array();
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();

        $data = $em->getRepository('AppBundle:Mahasiswa')
          ->findAll();

        // if ( !empty($request->get('semester')) ) {
        //   $semester = $em->getRepository('AppBundle:Semester')
        //     ->find($request->get('semester'));
        //   if ($semester) {
        //     $data = $em->getRepository('AppBundle:Mahasiswa')
        //       ->findBySemester($semester);
        //   }
        // }
        if ( $data ) {
            for ($i = 0; $i < count($data); $i++) {
              if ( $data[$i]->getPmb() === null ) {
                $resultData = array(
                  'id_user'       => ( null !== $data[$i]->getUser() ) ? $data[$i]->getUser()->getId() : '-',
                  'id_mhs'        => $data[$i]->getId(),
                  'id_pmb'        => ( null !== $data[$i]->getPmb() ) ? $data[$i]->getPmb()->getId() : '-',
                  'nim'           => ( null !== $data[$i]->getUser() ) ? $data[$i]->getUser()->getUsername() : '-',
                  'nama'          => ( null !== $data[$i]->getUser() ) ? $data[$i]->getUser()->getNama() : '-',
                  'angkatan'      => $data[$i]->getAngkatan(),
                  'batas_studi'   => ( null !== $data[$i]->getSemester() ) ? $data[$i]->getSemester()->getSemester() : '-',
                  'status_masuk'  => ( null !== $data[$i]->getUser() ) ? $data[$i]->getStatusMasuk() : '-',
                );
                $result[] = $resultData;
              }
            }
            $response->setData(array('data'=>$result));
        }
        return $response;
    }

    /**
     * @Route("/_ajax/datatable/biaya_kuliah", name="data_list_biaya_kuliah")
     */
    public function dataListBiayaKuliahAction(Request $request)
    {
        $result = array();
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:BiayaKuliahRincian')
          ->findAll();
        if ( $data ) {
            for ($i = 0; $i < count($data); $i++) {
                $resultData = array(
                  'nama'            => $data[$i]->getNama(),
                  'semester'        => $data[$i]->getSemester(),
                  'batas_bayar'     => ( null !== $data[$i]->getBatasBayar() ) ? 'dsak' : '',
                  'syarat_krs'      => $data[$i]->getSyaratKrs(),
                );
                $result[] = $resultData;
            }
            $response->setData(array('data'=>$result));
        }
        return $response;
    }

    /**
     * @Route("/_ajax/datatable/transaksi_pembayaran", name="data_list_transaksi_pembayaran")
     */
    public function dataListTransaksiPembayaranAction(Request $request)
    {
        $result = array();
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:Pembayaran')
          ->findAll();
        if ( $data ) {
            for ($i = 0; $i < count($data); $i++) {
                if ( null !== $data[$i]->getTanggal() ) {
                  $tanggal = $data[$i]->getTanggal();
                } else {
                  $tanggal = '';
                }
                $resultData = array(
                  'tanggal'    => $tanggal,
                  'ket'        => $data[$i]->getKet(),
                  'jumlah'     => $data[$i]->getJumlah(),
                  'tes'       => 'test'
                );
                $result[] = $resultData;
            }
            $response->setData(array('data'=>$result));
        }
        return $response;
    }

    /**
     * @Route("/_ajax/peminjaman/tambah", name="tambah_peminjaman_pustaka")
     */
    public function tambahPeminjamanPustakaAction(Request $request)
    {
        $id_anggota = $request->get('anggota'); 
        $id_pustaka = $request->get('pustaka');
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')
          ->findOneByUsername($id_anggota);
        $pustaka = $em->getRepository('AppBundle:PerpusPustaka')
          ->findOneByKode($id_pustaka);

        if ( $user && $pustaka ) {

          $peminjamanSkrg = $em->getRepository('AppBundle:PerpusPeminjaman')
            ->findOneBy(array('pustaka' => $pustaka, 'anggota' => $anggota));

          if ( ! $peminjamanSkrg ) {
            $now = new\DateTime();
            $peminjaman = new \AppBundle\Entity\PerpusPeminjaman();
            $peminjaman->setTanggal($now);
            $peminjaman->setAnggota($user);
            $peminjaman->setPustaka($pustaka);
            $em->persist($peminjaman);
            $em->flush();

            $response->setData(array(
                'no'              => '000',
                'kode'            => '',
                'pustaka'         => '',
                'tanggal'         => '',
                'jam'             => 'Jam'
            ));
          } else {
            $response->setData(array(
              'success' => 0
            ));
          }

        }

        return $response;
    }

    /**
     * @Route("/_ajax/list/peminjaman", name="data_list_peminjaman")
     */
    public function dataListPeminjamanAction(Request $request)
    {
        $result = array();
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:PerpusPeminjaman')
          ->findAll();
        if ( $data ) {
            $no = 1;
            for ($i = 0; $i < count($data); $i++) {
              $resultData = array(
                'no'              => $no,
                'kode'            => ( null !== $data[$i]->getPustaka() ) ? $data[$i]->getPustaka()->getKode() : '-',
                'pustaka'         => ( null !== $data[$i]->getAnggota() ) ? $data[$i]->getAnggota()->getNama() : '-',
                'tanggal'         => 'Tanggal',
                'jam'             => 'Jam',
              );
              $result[] = $resultData;
              $no++;
            }
            $response->setData(array('data'=>$result));
        }
        return $response;
    }

    /**
     * @Route("/_ajax/list/paket_krs", name="data_list_paket_krs")
     */
    public function dataListPaketKrsAction(Request $request)
    {
        $result = array();
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $mhs = $em->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('nim'));
        if ( $mhs && null !== $mhs->getProdi() && null !== $mhs->getProdi()->getKurikulum() ) {
            $data = $mhs->getProdi()->getKurikulum()->getListMakul();
            for ($i = 0; $i < count($data); $i++) {
              $resultData = array(
                'id'              => $data[$i]->getId(),
                'kode'            => $data[$i]->getKode(),
                'nama'            => $data[$i]->getNama(),
                'dosen'           => '',
                'skst'            => '',
                'sksp'            => '',
                'sksl'            => '',
              );
              $result[] = $resultData;
              $no++;
            }
            $response->setData($result);
        }
        return $response;
    }

    /**
     * @Route("/_ajax/perpus/peminjamananggota", name="_data_peminjaman_anggota")
     * @Method({"POST"})
     */
    public function dataPeminjamanAnggotaAction(Request $request)
    {
        $response = new JsonResponse();
        $aksi = $request->get('aksi');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository('AppBundle:User')
          ->find($request->get('id'));
        if ($user) {

          $now = new\DateTime();

          if ( $aksi == 'insert' ) {

            if ( !empty($request->get('id_pustaka')) ) {
              $pustaka = $em->getRepository('AppBundle:PerpusPustaka') 
                ->find($request->get('id_pustaka'));

              if ( $pustaka ) {

                // cek jika amggota sedang meminjam buku
                $peminjaman = $em->getRepository('AppBundle:PerpusPeminjaman')
                  ->findOneBy(array(
                      'pustaka'       => $pustaka,
                      'anggota'       => $user
                  ));

                if ( ! $peminjaman ) {

                  $peminjaman = new \AppBundle\Entity\PerpusPeminjaman();

                } else {
                  
                  if ( $peminjaman->sudahKembali() === 1 ) {
                    $peminjaman = new \AppBundle\Entity\PerpusPeminjaman();
                  }

                }

                $peminjaman->setPustaka($pustaka);
                $peminjaman->setTanggal($now);
                $peminjaman->setAnggota($user);
                $peminjaman->setSudahKembali(0);

                $em->persist($peminjaman);
                $em->flush();

              }

            }
          }

          $data = $this->getDoctrine()->getRepository('AppBundle:PerpusPeminjaman')
            ->findByAnggota($user);

          if ( $data ) {
            $html = '';
            $no = 1;

            foreach ($data as $d) {
              $html .= '<tr>';
              $html .= '<td>'.$no.'</td>';
              $html .= '<td>'.$d->getPustaka()->getKode().'</td>';
              $html .= '<td>'.$d->getPustaka()->getJudul().'</td>';
              $html .= '<td>';
              $html .= ( null !== $d->getPustaka()->getKlasifikasi() ) ? $d->getPustaka()->getKlasifikasi()->getNama() : '';
              $html .= '</td>';
              $html .= '<td><a href="#" class="btn btn-sm btn-danger hapus-btn" data-id="'.$d->getId().'" data-entity="PerpusPeminjaman"><i class="fa fa-trash"></i></a></td>';
              $html .= '</tr>';
              $no++;
            }
            $response->setData($html);
          }

        }
        return $response;
    }


    /**
     * @Route("/_ajax/remote_data/user/{role}", name="ajax_remote_data_user")
     * @Method({"GET","POST"})
     */
    public function ajaxRemoteDataUserAction(Request $request, $role = '')
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        /**
         * Autocomplete Select2 Ajax Remote Data
         */
        $whereQuery = '';
        $whereQuery .= 'u.status=:status';
        $params = array( 'status' => 'active' );
        if ( !empty($role) ) {
          if ( $role == 'mahasiswa' ) {
            $whereQuery .= ' AND u.hakAkses=:hakAkses';
            $params['hakAkses'] = $this->appService->getMasterTermObject('hak_akses', 4);
          } elseif ( $role == 'dosen' ) {
            $whereQuery .= ' AND u.hakAkses=:hakAkses';
            $params['hakAkses'] = $this->appService->getMasterTermObject('hak_akses', 3);
          }
        }
        $whereQuery .= ' AND u.nama LIKE :nama OR u.username LIKE :username';

        $data = array();
        if ( !empty($request->get('term')) ) {
          $params['nama'] = '%'.$request->get('term').'%';
          $params['username'] = '%'.$request->get('term').'%';
          $dataUser = $em->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:User', 'u')
            ->where($whereQuery)
            ->setParameters($params)
            ->getQuery()
            ->getResult();
          foreach($dataUser as $user) {
            $data[] = array(
              'id'        => $user->getId(),
              'name'      => $user->getNama(),
              'full_name' => $user->getNama(),
              "custom"    => $user->getUsername(),
            );
          }

        }

        $result = array(
          'total_count'         => count($data),
          'incomplete_results'  => true,
          'items'               => $data,
        );
        $response->setData($result);

        return $response;
    }

    /**
     * @Route("/_ajax/remote_data/pustaka", name="ajax_remote_data_pustaka")
     * @Method({"GET","POST"})
     */
    public function ajaxRemoteDataPustakaAction(Request $request)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $data = array();
        if ( !empty($request->get('term')) ) {
          $term = $request->get('term');
          $dataPustaka = $em->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:PerpusPustaka', 'u')
            ->where('u.judul LIKE :judul')
            ->setParameter('judul', '%'.$term.'%')
            ->getQuery()
            ->getResult();
          foreach($dataPustaka as $pustaka) {
            $data[] = array(
              'id'        => $pustaka->getId(),
              'name'      => $pustaka->getJudul(),
              'full_name' => $pustaka->getJudul(),
              "custom"    => $pustaka->getKode(),
            );
          }

        }

        $result = array(
          'total_count'         => count($data),
          'incomplete_results'  => true,
          'items'               => $data,
        );
        $response->setData($result);

        return $response;
    }
}
