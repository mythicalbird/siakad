<?php

namespace AppBundle\Controller\Master\Civitas;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\Type\PegawaiFormType;
use AppBundle\Service\AppService;
// use AppBundle\Form\GantiPasswordType;
// use AppBundle\Form\Model\GantiPassword;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PegawaiController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/master/civitas/pegawai", name="pegawai_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        // $dataPegawai = $em->createQueryBuilder()
        //   ->select('u')
        //   ->from('AppBundle:User', 'u')
        //   ->where('u.hakAkses!=:role_dosen AND u.hakAkses!=:role_mahasiswa AND u.hakAkses!=:role_superadmin AND u.hakAkses!=:role_admin AND u.status=:status')
        //   ->setParameters(array(
        //     'role_dosen'        => $this->appService->getMasterTermObject('hak_akses', 3),
        //     'role_mahasiswa'    => $this->appService->getMasterTermObject('hak_akses', 4),
        //     'role_superadmin'   => $this->appService->getMasterTermObject('hak_akses', 1),
        //     'role_admin'        => $this->appService->getMasterTermObject('hak_akses', 2),
        //     'status'            => 'active'
        //   ))
        //   ->getQuery()
        //   ->getResult();
        $dataPegawai = $em->createQueryBuilder()
          ->select('u')
          ->from('AppBundle:Pegawai', 'u')
          ->getQuery()
          ->getResult();

        foreach ($dataPegawai as $pgw) {
          if ( null !== $pgw->getUser() && $pgw->getUser()->getStatus() !== 'trash' ) {
            $user = $pgw->getUser();
            $this->response['result'][] = array(
              'id'        => $pgw->getId(),
              'id_user'   => $user->getId(),
              'nama'      => $user->getNama(),
              'email'     => $user->getEmail(),
              'bidang'    => $user->getGelarBelakang(),
              'umur'      => ( null !== $user->getTglLahir() ) ? (date('Y')-$user->getTglLahir()->format('Y')) : '',
              'tgl_masuk' => $user->getTglMasuk(),
              'lama_kerja'=> 0,
              'status'    => $user->getStatus()
            );
          }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
          $dataBidang = $em->createQueryBuilder()
            ->select('m')
            ->from('AppBundle:Master', 'm')
            ->where('m.type=:type AND m.status!=:status')
            ->setParameters(array(
              'type'      => 'bidang',
              'status'    => 'trash'
            ))
            ->getQuery()
            ->getResult();
          return $this->appService->load('master/civitas/pegawai_index.html.twig', array(
            'target'  => ( !empty($request->get('target')) ) ? $request->get('target') : 'tab_pegawai',
            'dataBidang'  => $dataBidang,
            'data'        => $this->response
          ));
        }

    }

    /**
     * @Route("/master/civitas/pegawai/edit", name="pegawai_edit")
     */
    public function editAction(Request $request)
    {
        if( !empty($request->get('id')) ) {
          $data = $this->getDoctrine()->getRepository('AppBundle:User')
            ->find($request->get('id'));
          // if ( !$data ) {
          //   $data = new \AppBundle\Entity\User();
          // }
        } else {
          $data = new \AppBundle\Entity\User();
        }
        $builder = $this->createFormBuilder($data);
        $builder
          ->add('username', null, array(
              'label'         => 'Username',
          ))
          ->add('nama', null, array(
              'label' => 'Nama Pegawai'
          ))
          ->add('gelarDepan', null, array(
              'label' => 'Gelar Akademik'
          ))
          // ->add('gelarBelakang', ChoiceType::class, array(
          //     'label'     => 'Bidang',
          //     'choices'   => $this->appService->getMasterChoices('bidang'),
          //     'attr'      => array(
          //       'class' => 'form-control'
          //     )
          // ))
          ->add('tptLahir', null, array(
              'label' => 'Tempat Lahir'
          ))
          ->add('tglLahir', DateType::class, array(
              'required'    => false,
              'label' => 'Tanggal Lahir',
              'widget'=> 'single_text',
              // prevents rendering it as type="date", to avoid HTML5 date pickers
              'html5' => false,
              'format' => 'dd-MM-yyyy',
              // adds a class that can be selected in JavaScript
              'attr' => ['class' => 'js-datepicker'],
          ))
          ->add('jk', ChoiceType::class, array(
              'label' => 'Jenis Kelamin',
              'attr'  => array(
                'class' => 'form-control',
              ),
              'choices'   => array(
                  'Laki-laki' => 'L',
                  'Perempuan' => 'P'
              ),
              'placeholder'   => '-- Pilih --',
          ))
          ->add('hakAkses', EntityType::class, array(
              'label' => 'Bidang/Jabatan',
              'attr'  => array(
                'class' => 'form-control',
              ),
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type AND m.kode!=:role_suadmin AND m.kode!=:role_admin AND m.kode!=:role_dosen AND m.kode!=:role_mhs AND m.kode!=:role_user AND m.status=:status')
                      ->setParameters(array(
                        'type'          => 'hak_akses',
                        'role_suadmin'  => 1,
                        'role_admin'    => 2,
                        'role_dosen'    => 3,
                        'role_mhs'      => 4,
                        'role_user'     => 5,
                        'status'        => 'publish'
                      ));
              },
              'choice_label' => function(Master $entity = null) {
                return strtoupper($entity->getParent()->getNama()) . " - " . $entity->getNama();
              },
              'placeholder'   => '-- Pilih --',
          ))
          ->add('agama', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'attr'  => array(
                'class' => 'form-control',
              ),
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'agama');
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('ktp', null, array(
              'required'  => false, 
              'label'     => 'Nomor KTP',
          ))
          ->add('hp', null, array(
              'label' => 'Nomor Telp/HP'
          ))
          ->add('email', EmailType::class)
          ->add('alamat', null, array(
              'required'  => false, 
              'label'     => 'Alamat',
              'attr'      => array(
                'class'   => 'form-control'
              )
          ))
          ->add('dataPegawai', PegawaiFormType::class)
          ->add('submit', SubmitType::class, array(
            'label' => 'Simpan',
            'attr'  => array(
                'class' => 'btn btn-primary'
            )
        ));
        if ( !empty($request->get('aksi')) && $request->get('aksi') == 'tambah' ) {
            $builder
              ->add('password', RepeatedType::class, array(
                  'type' => PasswordType::class,
                  'invalid_message' => 'The password fields must match.',
                  'options' => array('attr' => array('class' => 'password-field')),
                  'required' => true,
                  'first_options'  => array('label' => 'Kata Sandi'),
                  'second_options' => array('label' => 'Ulangi Kata Sandi'),
              ))
            ;
        }

        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data->setStatus('active');
            $data->setWargaNegara('ID');
            $data->setTelp($data->getHp());
            $data->setProdi($this->getUser()->getProdi());
            $em->persist($data->getDataPegawai());
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('pegawai_index');
        }
        return $this->appService->load('master/civitas/pegawai_edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/master/civitas/pegawai/jabatan/edit", name="pegawai_jabatan")
     */
    public function jabatanAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ( !empty($request->get('id')) ) {
          $data = $em->getRepository('AppBundle:Master')
            ->findOneBy(array(
              'type'  => 'hak_akses',
              'id'    => $request->get('id')
            ));
          if ( !$data ) {
            $data = new \AppBundle\Entity\Master();
            $data->setType('hak_akses');
          }
        } else {
          $data = new \AppBundle\Entity\Master();
          $data->setType('hak_akses');
        }

        $form = $this->createFormBuilder($data)
          ->add('parent', EntityType::class, array(
              'label' => 'Bidang',
              'attr'  => array(
                'class' => 'form-control',
              ),
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameters(array(
                        'type'          => 'bidang'
                      ));
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('nama', null, array(
            'label' => 'Nama Jabatan'
          ))          
          ->add('submit', SubmitType::class, array(
            'attr'    => array(
              'class' => 'btn btn-primary'
            )
          ))
          ->getForm();
        $form->handleRequest($request);
        if ( $form->isSubmitted() && $form->isValid() ) {
          if ( empty($request->get('id')) ) {
            $kode = $this->appService->slugify($data->getNama());
            $data->setKode($kode);
          }
          $em->persist($data);
          $em->flush();

          $this->addFlash('success', 'Data Jabatan berhasil disimpan.');
          return $this->redirectToRoute('pegawai_index', array('target' => 'tab_jabatan'));
        }
        return $this->appService->load('master/civitas/pegawai_jabatan_edit.html.twig', array(
          'form'        => $form->createView()
        ));
    }

    /**
     * @Route("/master/pegawai/bidang/edit", name="pegawai_bidang")
     */
    public function bidangAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ( !empty($request->get('id')) ) {
          $data = $em->getRepository('AppBundle:Master')
            ->findOneBy(array(
              'type'  => 'bidang',
              'id'    => $request->get('id')
            ));
          if ( !$data ) {
            $data = new \AppBundle\Entity\Master();
            $data->setType('bidang');
          }
        } else {
          $data = new \AppBundle\Entity\Master();
          $data->setType('bidang');
        }

        $form = $this->createFormBuilder($data)
          ->add('nama', null, array(
            'label' => 'Nama Bidang'
          ))
          ->add('ket', null, array(
            'label' => 'Keterangan'
          ))
          ->add('submit', SubmitType::class, array(
            'attr'    => array(
              'class' => 'btn btn-primary'
            )
          ))
          ->getForm();
        $form->handleRequest($request);
        if ( $form->isSubmitted() && $form->isValid() ) {
          if ( empty($request->get('id')) ) {
            $kode = $this->appService->slugify($data->getNama());
            $data->setKode($kode);
          }
          $data->setStatus('publish');
          $em->persist($data);
          $em->flush();

          $this->addFlash('success', 'Data bidang berhasil disimpan.');
          return $this->redirectToRoute('pegawai_index', array('target' => 'tab_bidang'));
        }
        return $this->appService->load('master/civitas/pegawai_bidang_edit.html.twig', array(
          'form'        => $form->createView()
        ));

    }
}
