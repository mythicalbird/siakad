<?php

namespace AppBundle\Controller\Master\Civitas;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\User;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Mahasiswa;
use AppBundle\Entity\Wilayah;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\Type\MahasiswaFormType;
use AppBundle\Service\AppService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MahasiswaController extends Controller
{

    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/master/mahasiswa", name="mahasiswa_index")
     */
    public function indexAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        // $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // $writer->save('file.xls');
        $em = $this->getDoctrine()->getManager();
        $ta = $this->appService->getTahunAkademik();
        if ( $ta->getKodeSemester() ) {
          # code...
        }
        $params = array( 
          'maba'      => 0,
          'prodi'     => $this->getUser()->getProdi()
        );
        if ( !empty($request->get('angkatan')) ) {
          $params['angkatan'] = $request->get('angkatan');
        }
        $dataMahasiswa = $this->appService->getListMahasiswa($params);
        $no = 1;
        foreach ($dataMahasiswa as $mhs) {
          if ( null !== $mhs->getUser() && null !== $mhs->getUser()->getProdi() && $this->getUser()->getProdi() == $mhs->getUser()->getProdi() ) {
            if ( $mhs->getStatus() != $this->appService->getMasterTermObject('status_mahasiswa', 'L') ) {
              if ( !empty($request->get('dt')) && $request->get('dt') == "true" ) {
                $button_html = '';
                $button_html .= '<div class="dropdown">';
                $button_html .= '
                  <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Aksi
                    <span class="caret"></span>
                  </button>
                ';
                $button_html .= '<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">';
                $button_html .= '<li><a href="'.$this->generateUrl('mahasiswa_edit', array('id' => $mhs->getUser()->getId())).'"><i class="fa fa-user"></i> Edit Profil</a></li> ';
                $button_html .= '<li><a href="#" data-toggle="modal" data-target="#myModal" data-aksi="pendidikan" data-id="'.$mhs->getId().'" data-nim="'.$mhs->getUser()->getUsername().'" data-nama="'.$mhs->getUser()->getNama().'" data-prodi="'.$mhs->getUser()->getProdi()->getNamaProdi().'"><i class="fa fa-mortar-board"></i> Pendidikan</a></li>';
                $button_html .= '<li><a href="'.$this->generateUrl('mahasiswa_cetak', array('id' => $mhs->getId())).'" target="_blank"><i class="fa fa-print"></i> Cetak Data</a></li>';
                $button_html .= '<li><a href="'.$this->generateUrl('mahasiswa_ktm_cetak', array('nim' => $mhs->getUser()->getUsername())).'" target="_blank"><i class="fa fa-id-card"></i> Cetak KTM</a></li>';
                $button_html .= '<li role="separator" class="divider"></li>';
                $button_html .= '<li><a href="'.$this->generateUrl('mahasiswa_hapus', array('nim' => $mhs->getUser()->getUsername(), 'angkatan' => ( !empty($request->get('angkatan')) ? $request->get('angkatan') : '' ))).'" onclick="return confirm(\'Yakin ingin menghapus data?\')"><i class="fa fa-trash"></i> Hapus</a></li>';
                $button_html .= '</ul></div>';
                $this->response['result'][] = array(
                  $no,
                  $mhs->getUser()->getNama(),
                  $mhs->getUser()->getUsername(),
                  $mhs->getUser()->getJk(),
                  ( null !== $mhs->getUser()->getAgama() ) ? $mhs->getUser()->getAgama()->getNama() : '',
                  ( null !== $mhs->getUser()->getTglLahir() ) ? $mhs->getUser()->getTptLahir() . ', ' . $mhs->getUser()->getTglLahir()->format('d/m/Y') : '',
                  ( null !== $mhs->getStatus() ) ? $mhs->getStatus()->getNama() : '',
                  $mhs->getAngkatan(),
                  $button_html
                );
                $no++;
              } else {
                $this->response['result'][] = array(
                  'id'        => $mhs->getId(),
                  'id_user'   => $mhs->getUser()->getId(),
                  'nim'       => $mhs->getUser()->getUsername(),
                  'nama'      => $mhs->getUser()->getNama(),
                  'jk'        => $mhs->getUser()->getJk(),
                  'agama'     => ( null !== $mhs->getUser()->getAgama() ) ? $mhs->getUser()->getAgama()->getNama() : '',
                  'tgl_lahir' => '',
                  'angkatan'  => $mhs->getAngkatan()
                );
              }
            }
          }
        }
        $dataStatusMahasiswa = $em->createQueryBuilder()
          ->select('s')
          ->from('AppBundle:Master', 's')
          ->where('s.status=:status and s.type=:type and s.kode in (:kode)')
          ->setParameters(array(
            'status'  => 'publish',
            'type'    => 'status_mahasiswa',
            'kode'    => array('A', 'C', 'D', 'G')
          ))
          ->getQuery()
          ->getResult();
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            if ( !empty($request->get('dt')) && $request->get('dt') == "true" ) {
              $response->setData(array('data'=> $this->response['result']));
            } else {
              $response->setData($this->response);
            }
            return $response;
        } else {
            return $this->appService->load('master/civitas/mahasiswa_index.html.twig', array(
              'dataStatusMahasiswa'=> $dataStatusMahasiswa,
              'data'  => $this->response
            ));
        }
    } 

    /**
     * @Route("/master/mahasiswa/bukuinduk/cetak", name="mahasiswa_bukuinduk_cetak")
     */
    public function bukuIndukCetakAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
          ->findByAngkatan($request->get('angkatan'));
        $params = array( 'maba'  => 0 );
        if ( !empty($request->get('angkatan')) ) {
          $params['angkatan'] = $request->get('angkatan');
        }
        $data = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
          ->findBy($params);
        return $this->appService->load('cetak/mahasiswa_bukuinduk.html.twig', array(
          'data' => $data
        ));
    }

    /**
     * @Route("/master/mahasiswa/ktm/cetak", name="mahasiswa_ktm_cetak")
     */
    public function ktmCetakAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('nim'));
        return $this->appService->load('cetak/mahasiswa_ktm.html.twig', array(
          'mhs' => $data
        ));
    }

    /**
     * @Route("/master/mahasiswa/cetak/{id}", name="mahasiswa_cetak")
     */
    public function cetakAction(Request $request, $id)
    {
        $mhs = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
          ->find($id);

        // data krs blm
        return $this->appService->load('cetak/mahasiswa_data.html.twig', array(
          'mhs' => $mhs
        ));
    }

    /**
     * @Route("/master/mahasiswa/tambah", name="mahasiswa_tambah")
     */
    public function tambahAction(Request $request)
    {
        $data = array();
        $builder = $this->createFormBuilder($data);
        $builder
          ->add('username', null, array(
              'label' => 'NIM'
          ))
          ->add('ktp', null, array(
              'label' => 'No KTP'
          ))
          ->add('noKk', null, array(
              'label' => 'No KK'
          ))
          ->add('npwp', null, array(
              'label' => 'No NPWP'
          ))
          ->add('nama', null, array(
              'label' => 'Nama Mahasiswa'
          ))
          ->add('tptLahir', null, array(
              'label' => 'Tempat Lahir'
          ))
          ->add('tglLahir', DateType::class, array(
              'label' => 'Tanggal Lahir',
              'widget'    => 'single_text',
              'html5'     => false,
              'format'    => 'dd-MM-yyyy',
              'attr'      => ['class' => 'js-datepicker'],
          ))
          ->add('jk', ChoiceType::class, array(
              'label' => 'Jenis Kelamin',
              'choices'   => array(
                  'Laki-laki' => 'L',
                  'Perempuan' => 'P'
              ),
              'placeholder'   => '-- Pilih --' 
          ))
          ->add('agama', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'agama');
              },
              'choice_label' => 'nama',
          ))
          ->add('wargaNegara', ChoiceType::class, array(
            'label'   => 'Warga Negara',
            'choices' => array(
              'WNI' => 'ID',
              'WNA' => 'WNA',
            ),
          ))
          ->add('telp', null, array(
            'required'  => false,
            'label'   => 'No. Telp',
          ))
          ->add('hp', null, array(
            'required'  => false,
            'label'   => 'Nomor HP',
          ))
          ->add('email', null, array(
            'required'  => true,
          ))
          ->add('dosenPa', EntityType::class, array(
              'label' => 'Penasehat Akademik',
              'class' => 'AppBundle:Dosen',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('p');
              },
              'choice_label' => function(Dosen $entity = null) {
                  return (null !== $entity->getUser()) ? $entity->getUser()->getNama() : '';
              },
              'placeholder' => '-- Pilih --',
          ))
          ->add('prodi', EntityType::class, array(
              'label'   => 'Program Studi',
              'class' => 'AppBundle:ProgramStudi',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('p');
              },
              'choice_label' => function(\AppBundle\Entity\ProgramStudi $prodi) {
                  return $prodi->getJenjang() . ' ' . $prodi->getNamaProdi();
              },
              'placeholder' => '-- Pilih --',
          ))
          ->add('jalur', EntityType::class, array(
              'label' => 'Jalur PMB',
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'jalur_pmb');
              },
              'choice_label' => 'nama',
          ))
          ->add('statusMasuk', ChoiceType::class, array(
            'label'   => 'Status Masuk',
            'choices' => array(
              'BARU' => 1,
              'PINDAHAN' => 2,
            ),
            'placeholder'   => '-- Pilih --'
          ))
          // ->add('kodeSekolah', null, array(
          //     'required'  => false,
          //     'label' => 'Kode Sekolah Asal'
          // ))
          ->add('namaSekolah', null, array(
              'required'  => false,
              'label' => 'Nama Sekolah Asal',
          ))          
          ->add('nis', null, array(
              'required'  => false,
              'label' => 'NIS',
          ))
          ->add('nilaiUn', null, array(
              'required'  => false,
              'label' => 'Nilai UN'
          ))
          ->add('asalKodePt', null, array(
              'required'  => false,
              'label' => 'Kode Perguruan Tinggi'
          ))
          ->add('asalNamaPt', null, array(
              'required'  => false,
              'label' => 'Nama Perguruan Tinggi',
          ))          
          ->add('asalProdi', null, array(
              'required'  => false,
              'label' => 'Nama Prodi Asal',
          ))
          ->add('asalJenjang', null, array(
              'required'  => false,
              'label' => 'Jenjang Pendidikan'
          ))
          ->add('asalNim', null, array(
              'required'  => false,
              'label' => 'NIM Asal',
          ))          
          ->add('asalSksDiakui', null, array(
              'required'  => false,
              'label' => 'SKS Diakui',
          ))
          ->add('status', EntityType::class, array(
              'label' => 'Status',
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'status_mahasiswa');
              },
              'choice_label' => 'nama',
              'placeholder' => '-- Pilih --',
          ))
          ->add('statusSipil', ChoiceType::class, array(
            'label'         => 'Status Sipil',
            'choices'       => array(
              'Bujangan'  => 'bujangan',
              'Duda'      => 'duda',
              'Janda'     => 'janda',
              'Menikah'   => 'menikah',
            ),
            'placeholder'   => '-- Pilih --'
          ))
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
        ;

        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
          $data = $form->getData();
          $nim = $data['username'];
          // check nim sudah terdaftar apa belum
          $reg_mhs = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findOneBy(array(
              'username'  => $nim,
              'status'    => 'active',
            ));
          if ( !$reg_mhs ) {
            $data['nim'] = $nim;
            $data['alamat'] = '';
            $data['rt'] = '';
            $data['rw'] = '';
            $data['status'] = $data['status']->getKode();
            $data['nim'] = $data['username'];
            $user = $this->appService->newMahasiswa($data);
            $this->addFlash('success', 'Mahasiswa berhasil ditambah.');
            return $this->redirectToRoute('mahasiswa_edit', array('id' => $user->getId()));
          } else {
            $this->addFlash('error', 'NPM sudah terdaftar!');
          }

        }
        return $this->appService->load('master/civitas/mahasiswa_tambah.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/master/mahasiswa/edit/{id}", name="mahasiswa_edit")
     */
    public function editAction(Request $request, $id)
    {

        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->find($id);
        $builder = $this->createFormBuilder($data);
        $builder
          ->add('username', null, array(
              'label'     => 'NPM',
              'attr'      =>  array(
                'readonly'  => true
              )
          ))
          ->add('nama', null, array(
              'label' => 'Nama Mahasiswa',
              'attr'      =>  array(
                'readonly'  => true
              )
          ))
          ->add('ktp', null, array(
              'label' => 'No KTP'
          ))
          ->add('noKk', null, array(
              'label' => 'No KK'
          ))
          ->add('npwp', null, array(
              'label' => 'NPWP'
          ))
          ->add('tptLahir', null, array(
              'label' => 'Tempat Lahir'
          ))
          ->add('tglLahir', DateType::class, array(
              'label' => 'Tanggal Lahir',
              'widget'    => 'single_text',
              'html5'     => false,
              'format'    => 'dd-MM-yyyy',
              'attr'      => ['class' => 'js-datepicker'],
          ))
          ->add('jk', ChoiceType::class, array(
              'label' => 'Jenis Kelamin',
              'choices'   => array(
                  'Laki-laki' => 'L',
                  'Perempuan' => 'P'
              ),
              'placeholder'   => '-- Pilih --' 
          ))
          ->add('agama', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'agama');
              },
              'choice_label' => 'nama',
          ))
          ->add('wargaNegara', ChoiceType::class, array(
            'required'  => false,
            'label'   => 'Warga Negara',
            'choices' => array(
              'WNI' => 'ID',
              'WNA' => 'WNA',
            ),
          ))
          ->add('telp', null, array(
            'required'  => false,
            'label'   => 'No. Telp',
          ))
          ->add('hp', null, array(
            'required'  => false,
            'label'   => 'Nomor HP',
          ))
          ->add('email')
          ->add('alamat', null, array(
          ))
          ->add('rt', null, array(
              'label' => 'RT',
          ))
          ->add('rw', null, array(
              'label' => 'RW',
          ))
          ->add('dusun')
          ->add('desa', null, array(
            'required'  => false,
            'label'     => 'Desa/Kel.'
          ))
          ->add('wilayah', EntityType::class, array(
              'label' => 'Kec/Kab/Pro',
              'class' => 'AppBundle:Wilayah',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('w')
                      ->where('w.level= :level')
                      ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 3));
              },
              'choice_label' => function( Wilayah $entity = null ){
                $choice_label = $entity->getNama();
                if ( null !== $entity->getParent() ) {
                  $parent = $entity->getParent();
                  $choice_label .= ', ' . $parent->getNama();
                  if ( null !== $parent->getParent() ) {
                    $choice_label .= ', ' . $parent->getParent()->getNama();
                  }
                }
                return $choice_label;
              },
              'choice_value' => function( Wilayah $entity = null ){
                return ( null !== $entity ) ? $entity->getId() : null;
              },
              'attr'  => array(
                'style' => 'width:100%'
              ),
              'placeholder' => '-- Pilih --',
          ))   
          ->add('pos', null, array(
              'label' => 'Kodepos',
          ))
          ->add('statusSipil', ChoiceType::class, array(
            'required'  => false,
            'label'         => 'Status Sipil',
            'choices'       => array(
              'Bujangan'  => 'bujangan',
              'Duda'  => 'duda',
              'Janda' => 'janda',
              'Menikah' => 'menikah',
            ),
            'placeholder'   => '-- Pilih --'
          ))
          ->add('prodi', EntityType::class, array(
              'class' => 'AppBundle:ProgramStudi',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('p');
              },
              'choice_label' => 'namaProdi'
          ))
          ->add('tglMasuk', DateType::class, array(
              'label' => 'Tanggal Masuk',
              'widget'    => 'single_text',
              'html5'     => false,
              'format'    => 'dd-MM-yyyy',
              'attr'      => ['class' => 'js-datepicker'],
          ))
          ->add('dataMahasiswa', MahasiswaFormType::class)

        ;

        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!empty($request->get('aksi')) && $request->get('aksi') == "tambah") {
              $data->setPassword($data->getUsername());
            }
            $mahasiswa= $data->getDataMahasiswa();
            foreach ($mahasiswa->getBerkas() as $berkas) {
              $em->persist($berkas);
            }

            // hidden data
            $mahasiswa->setMaba(0);

            $em->persist($mahasiswa);
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('mahasiswa_index', array('angkatan' => $mahasiswa->getAngkatan()));
        }
        return $this->appService->load('master/civitas/mahasiswa_edit.html.twig', array(
            'form' => $form->createView(),
            'data'  => $data
            // 'wilayah' => json_encode($wilayah)
        ));
    }

    /**
     * @Route("/master/mahasiswa/hapus/{nim}", name="mahasiswa_hapus")
     */
    public function hapusAction(Request $request, $nim)
    { 
        $return = ( !empty($request->get('_ret')) ) ? $request->get('_ret') : 'mahasiswa_index';
        $this->appService->hapusMahasiswa($nim);
        $this->addFlash('success', 'Mahasiswa berhasil dihapus');
        return $this->redirectToRoute($return, array('angkatan' => $request->get('angkatan')));
    }
}
