<?php

namespace AppBundle\Controller\Master\Pusat;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\Setting;
use AppBundle\Entity\User;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Wilayah;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use Doctrine\ORM\EntityRepository;

class DefaultController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/master/badan_hukum", name="master_badan_hukum")
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository(Setting::class)
          ->findOneByName('data_badan_hukum');
        if( ! $data ) {
          $data = new Setting();
          $data->setName('data_badan_hukum');
        }
        $now = new\DateTime();
        $data->setModifiedAt($now);
        $builder = $this->createFormBuilder($data);
        $form = $builder
          ->add(
              $builder->create('value', FormType::class, array('by_reference' => true))
              ->add('kode_badan_hukum', null, array(
                  'required'  => false,
                  'label'   => 'Kode Badan Hukum',
              ))
              ->add('nama_badan_hukum', null, array(
                  'required'  => false,
                  'label'   => 'Nama Badan Hukum',
              ))
              ->add('tanggal_berdiri', DateType::class, array(
                  'required'  => false,
                  'label' => 'Tanggal Berdiri',
                  'widget'=> 'single_text',
                  // prevents rendering it as type="date", to avoid HTML5 date pickers
                  'html5' => false,
                  'format' => 'dd-mm-yyyy',
                  // adds a class that can be selected in JavaScript
                  'attr' => ['class' => 'js-datepicker'],
              ))
              ->add('nomor_akta', null, array(
                  'required'  => false,
                  'label'   => 'Nomor',
              ))
              ->add('tanggal_akta', DateType::class, array(
                  'required'  => false,
                  'label' => 'Tanggal',
                  'widget'=> 'single_text',
                  // prevents rendering it as type="date", to avoid HTML5 date pickers
                  'html5' => false,
                  'format' => 'dd-mm-yyyy',
                  // adds a class that can be selected in JavaScript
                  'attr' => ['class' => 'js-datepicker'],
              ))
              ->add('nomor_pengesahan', null, array(
                  'required'  => false,
                  'label'   => 'Nomor'
              ))
              ->add('tanggal_pengesahan', DateType::class, array(
                  'required'  => false,
                  'label' => 'Tanggal',
                  'widget'=> 'single_text',
                  // prevents rendering it as type="date", to avoid HTML5 date pickers
                  'html5' => false,
                  'format' => 'dd-mm-yyyy',
                  // adds a class that can be selected in JavaScript
                  'attr' => ['class' => 'js-datepicker'],
              ))
              ->add('alamat', TextareaType::class, array(
                  'required'  => false,
              ))
              ->add('kota', EntityType::class, array(
                  'required'  => false,
                  'class' => 'AppBundle:Wilayah',
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('w')
                          ->where('w.level = :level')
                          ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 2));
                  },
                  'attr'          => array( 'class' => 'form-control select2' ),
                  'choice_label'  => 'nama',
                  'choice_value' => function (Wilayah $entity = null) {
                      return $entity ? $entity->getId() : '';
                  },
                  'placeholder'   => '-- PILIH --'
              ))
              ->add('provinsi', EntityType::class, array(
                  'required'  => false,
                  'class' => 'AppBundle:Wilayah',
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('w')
                          ->where('w.level = :level')
                          ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 1));
                  },
                  'attr'          => array( 'class' => 'form-control select2' ),
                  'choice_label'  => 'nama',
                  'choice_value' => function (Wilayah $entity = null) {
                      return $entity ? $entity->getId() : '';
                  },
                  'placeholder'   => '-- PILIH --'
              ))
              ->add('kode_pos', null, array(
                  'required'  => false,
                  'label'   => 'Kode Pos',
              ))
              ->add('telp', null, array(
                  'required'  => false,
                  'label'   => 'Telepon',
              ))
              ->add('fax', null, array(
                  'required'  => false,
                  'label'     => 'Faximili',
              ))
              ->add('email', EmailType::class, array(
                  'required'  => false,
              ))
              ->add('website', null, array(
                  'required'  => false,
              ))
          )
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Pengaturan berhasil disimpan.');
        }
        return $this->appService->load('master/pusat/badan_hukum_index.html.twig', array(
        	'form'		=> $form->createView()
        ));
    }

    /**
     * @Route("/master/perguruan_tinggi", name="perguruan_tinggi_index")
     */
    public function perguruanTinggiIndexAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository(Setting::class)
          ->findOneByName('data_perguruan_tinggi');
        if( ! $data ) {
          $data = new Setting();
          $data->setName('data_perguruan_tinggi');
        }
        $now = new\DateTime();
        $data->setModifiedAt($now);
        $builder = $this->createFormBuilder($data);
        $form = $builder
          ->add(
              $builder->create('value', FormType::class, array('by_reference' => true))
              ->add('kode_badan_hukum', null, array(
                  'required'  => false,
                  'label'   => 'Kode Badan Hukum',
              ))
              ->add('kode_perguruan_tinggi', null, array(
                  'required'  => false,
                  'label'   => 'Kode Perguruan Tinggi',
              ))
              ->add('nama_perguruan_tinggi', null, array(
                  'required'  => false,
                  'label'   => 'Nama Perguruan Tinggi',
              ))
              ->add('singkatan_nama_perguruan_tinggi', null, array(
                  'label'   => 'Singkatan Nama',
                  'required'  => false,
              ))
              ->add('tanggal_berdiri', DateType::class, array(
                  'required'  => false,
                  'label'   => 'Tanggal Berdiri',
                  'widget'=> 'single_text',
                  // prevents rendering it as type="date", to avoid HTML5 date pickers
                  'html5' => false,
                  'format' => 'dd-mm-yyyy',
                  // adds a class that can be selected in JavaScript
                  'attr' => ['class' => 'js-datepicker'],
              ))
              ->add('nomor_akta', null, array(
                  'label'   => 'Nomor',
              ))
              ->add('tanggal_akta', DateType::class, array(
                  'required'  => false,
                  'label'   => 'Tanggal',
                  'widget'=> 'single_text',
                  // prevents rendering it as type="date", to avoid HTML5 date pickers
                  'html5' => false,
                  'format' => 'dd-mm-yyyy',
                  // adds a class that can be selected in JavaScript
                  'attr' => ['class' => 'js-datepicker'],
              ))
              ->add('nomor_pengesahan', null, array(
                  'label'   => 'Nomor'
              ))
              ->add('tanggal_pengesahan', DateType::class, array(
                  'required'  => false,
                  'label'   => 'Tanggal',
                  'widget'=> 'single_text',
                  // prevents rendering it as type="date", to avoid HTML5 date pickers
                  'html5' => false,
                  'format' => 'dd-mm-yyyy',
                  // adds a class that can be selected in JavaScript
                  'attr' => ['class' => 'js-datepicker'],
              ))
              ->add('akreditasi', EntityType::class, array(
                  'required'  => false,
                  'class' => Master::class,
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('m')
                          ->where('m.type = :type')
                          ->setParameter('type', 'akreditasi');
                  },
                  'choice_value' => function (Master $entity = null) {
                      return $entity ? $entity->getId() : '';
                  },
                  'choice_label'  => 'nama',
                  'placeholder'   => '-- PILIH --'
              ))
              ->add('alamat_utama', TextareaType::class, array(
                  'required'  => false,
                  'label'     => 'Alamat Utama',
              ))
              ->add('alamat_lain', TextareaType::class, array(
                  'required'  => false,
                  'label'     => 'Alamat Lain',
              ))
              ->add('kota', EntityType::class, array(
                  'required'  => false,
                  'class' => 'AppBundle:Wilayah',
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('w')
                          ->where('w.level = :level')
                          ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 2));
                  },
                  'attr'          => array( 'class' => 'form-control select2' ),
                  'choice_label'  => 'nama',
                  'choice_value' => function (Wilayah $entity = null) {
                      return $entity ? $entity->getId() : '';
                  },
                  'placeholder'   => '-- PILIH --'
              ))
              ->add('provinsi', EntityType::class, array(
                  'required'  => false,
                  'class' => 'AppBundle:Wilayah',
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('w')
                          ->where('w.level = :level')
                          ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 1));
                  },
                  'attr'          => array( 'class' => 'form-control select2' ),
                  'choice_label'  => 'nama',
                  'choice_value' => function (Wilayah $entity = null) {
                      return $entity ? $entity->getId() : '';
                  },
                  'placeholder'   => '-- PILIH --'
              ))
              ->add('kode_pos', null, array(
                  'required'  => false,
                  'label'   => 'Kode Pos',
              ))
              ->add('telp', null, array(
                  'required'  => false,
                  'label'   => 'Telepon',
              ))
              ->add('fax', null, array(
                  'required'  => false,
                  'label'   => 'Faximili',
              ))
              ->add('email', EmailType::class, array(
                  'required'  => false,
                  'label'   => 'Email',
              ))
              ->add('website', null, array(
                  'required'  => false,
                  'label'   => 'Website',
              ))
          )
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Pengaturan berhasil disimpan.');
        }
        return $this->appService->load('master/pusat/perguruan_tinggi_index.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/master/pimpinan", name="pimpinan_index")
     */
    public function pimpinanIndexAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository(Setting::class)
          ->findOneByName('data_pimpinan');
        if( ! $data ) {
          $data = new Setting();
          $data->setName('data_pimpinan');
        }
        $now = new\DateTime();
        $data->setModifiedAt($now);
        $builder = $this->createFormBuilder($data);
        $form = $builder
          ->add(
              $builder->create('value', FormType::class, array('by_reference' => true))
                ->add('tahun', EntityType::class, array(
                    'label' => 'Tahun Akademik',
                    'class' => TahunAkademik::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('t');
                    },
                    'choice_value' => function (TahunAkademik $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'choice_label'  => 'tahun',
                    'placeholder'   => '-- PILIH --'
                ))
                ->add('ketua', null, array(
                    'required'    => false,
                ))
                ->add('sekretaris', null, array(
                    'required'    => false,
                ))
                ->add('bendahara', null, array(
                    'required'    => false,
                ))
                ->add('nomorSk', null, array(
                    'required'    => false,
                    'label'       => 'Nomor SK',
                ))
                ->add('tanggalSk', DateType::class, array(
                    'required'    => false,
                    'label'   => 'Tanggal SK',
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'format'    => 'dd-mm-yyyy',
                    'attr'      => array('class' => 'js-datepicker'),
                ))
                ->add('rektor', EntityType::class, array(
                    'required'    => false,
                    'label' => 'Rektor/Ketua/Direktur',
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                            ->where('d.hakAkses = :hakAkses')
                            ->setParameter('hakAkses', $this->appService->getMasterTermObject('hak_akses', 3));
                    },
                    'choice_value' => function (User $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr'          => array( 'class' => 'form-control select2' ),
                    'choice_label'  => 'nama',
                    'placeholder'   => '-- Pilih Dosen --'
                ))
                ->add('wakilBidang1', EntityType::class, array(
                    'required'    => false,
                    'label' => 'Pembantu/Wakil Bidang I',
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                            ->where('d.hakAkses = :hakAkses')
                            ->setParameter('hakAkses', $this->appService->getMasterTermObject('hak_akses', 3));
                    },
                    'choice_value' => function (User $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr'          => array( 'class' => 'form-control select2' ),
                    'choice_label'  => 'nama',
                    'placeholder'   => '-- Pilih Dosen --'
                ))
                ->add('wakilBidang2', EntityType::class, array(
                    'required'    => false,
                    'label' => 'Pembantu/Wakil Bidang II',
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                            ->where('d.hakAkses = :hakAkses')
                            ->setParameter('hakAkses', $this->appService->getMasterTermObject('hak_akses', 3));
                    },
                    'choice_value' => function (User $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr'          => array( 'class' => 'form-control select2' ),
                    'choice_label'  => 'nama',
                    'placeholder'   => '-- Pilih Dosen --'
                ))
                ->add('wakilBidang3', EntityType::class, array(
                    'required'    => false,
                    'label' => 'Pembantu/Wakil Bidang III',
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                            ->where('d.hakAkses = :hakAkses')
                            ->setParameter('hakAkses', $this->appService->getMasterTermObject('hak_akses', 3));
                    },
                    'choice_value' => function (User $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr'          => array( 'class' => 'form-control select2' ),
                    'choice_label'  => 'nama',
                    'placeholder'   => '-- Pilih Dosen --'
                ))
                ->add('wakilBidang4', EntityType::class, array(
                    'required'    => false,
                    'label' => 'Pembantu/Wakil Bidang IV',
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                            ->where('d.hakAkses = :hakAkses')
                            ->setParameter('hakAkses', $this->appService->getMasterTermObject('hak_akses', 3));
                    },
                    'choice_value' => function (User $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr'          => array( 'class' => 'form-control select2' ),
                    'choice_label'  => 'nama',
                    'placeholder'   => '-- Pilih Dosen --'
                ))
                ->add('wakilBidang5', EntityType::class, array(
                    'required'    => false,
                    'label' => 'Pembantu/Wakil Bidang V',
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                            ->where('d.hakAkses = :hakAkses')
                            ->setParameter('hakAkses', $this->appService->getMasterTermObject('hak_akses', 3));
                    },
                    'choice_value' => function (User $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'attr'          => array( 'class' => 'form-control select2' ),
                    'choice_label'  => 'nama',
                    'placeholder'   => '-- Pilih Dosen --'
                ))
                ->add('nomorSkRektor', null, array(
                    'required'    => false,
                    'label'   => 'Nomor SK Rektor',
                ))
                ->add('tanggalSkRektor', DateType::class, array(
                    'required'    => false,
                    'label'   => 'Tanggal SK Rektor',
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'format'    => 'dd-mm-yyyy',
                    'attr'      => ['class' => 'js-datepicker'],
                ))
          )
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Pengaturan berhasil disimpan.');
        }
        return $this->appService->load('master/pusat/pimpinan_index.html.twig', array(
            'form'      => $form->createView()
        ));
    }

    /**
     * @Route("/master/reset_password", name="reset_password_index")
     */
    public function resetPasswordIndexAction(Request $request, UserPasswordEncoderInterface $encoder)
    {

        if ( $request->get('username') != '' ) {
          $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('username'));
          if ($data) {
            return $this->redirectToRoute('reset_password_edit', array('nim' => $data->getUserName()));
          }
        }

        return $this->appService->load('master/pusat/reset_password_index.html.twig', array(

        ));
    }


    /**
     * @Route("/master/reset_password/edit/{nim}", name="reset_password_edit")
     */
    public function resetPasswordCariAction(Request $request, UserPasswordEncoderInterface $encoder, $nim)
    {


        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findOneByUsername($nim);

        $form = $this->createFormBuilder($data)
          ->add('nama', null, array(
              'label' => 'Nama Lengkap',
              'attr'  => array(
                  'readonly'  => 'readonly'
              ),
          ))
          ->add('username', null, array(
              'label'   => 'NIM/NIDN',
              'attr'  => array(
                  'readonly'  => 'readonly'
              ),
          ))
          ->add('password', RepeatedType::class, array(
              'type' => PasswordType::class,
              'invalid_message' => 'The password fields must match.',
              'options' => array('attr' => array('class' => 'password-field')),
              'required' => true,
              'first_options'  => array('label' => 'Kata Sandi Baru'),
              'second_options' => array('label' => 'Ulangi Kata Sandi'),
          ))
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $plainPassword = $data->getPassword();
            $encoded = $encoder->encodePassword($data, $plainPassword);

            $data->setPassword($encoded);
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Pengaturan berhasil disimpan.');
        }

        return $this->appService->load('master/pusat/reset_password_edit.html.twig', array(
            'form'      => $form->createView()
        ));
    }
}
