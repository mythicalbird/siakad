<?php

namespace AppBundle\Controller\Master\Pusat;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Master;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class ProdiController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/master/prodi", name="prodi_index")
     */
    public function indexAction(Request $request)
    {
        $prodiList = $this->getDoctrine()->getRepository(ProgramStudi::class)->findAll();
        return $this->appService->load('master/pusat/prodi_index.html.twig', array(
        	'prodiList' => $prodiList,
        ));
    }

    /**
     * @Route("/master/prodi/edit", name="prodi_edit")
     */
    public function editAction(Request $request)
    {
        if (!empty($request->get('aksi')) && $request->get('aksi') == "tambah") {
            $data = new \AppBundle\Entity\ProgramStudi();
        } elseif (!empty($request->get('aksi')) && $request->get('aksi') == "hapus") {
        } elseif (!empty($request->get('id'))) {
            $data = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')->find($request->get('id'));
        } else {
            exit;
        }
        $roleDosen = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findOneBy(['type' => 'hak_akses', 'nama' => 'DOSEN']);
        $builder = $this->createFormBuilder($data);
        $builder
            ->add('kode', null, array(
                'label' => 'Kode Prodi',
            ))
            ->add('kodeProdi', null, array(
                'label' => 'Kode DIKTI',
            ))
            ->add('namaProdi', null, array(
                'label' => 'Nama Prodi',
            ))
            ->add('jenjang', ChoiceType::class, array(
                'label' => 'Jenjang',
                'choices'   => array(
                    'D3'    => 'D3',
                    'S1'    => 'S1',
                    'S2'    => 'S2',
                    'S3'    => 'S3',
                ),
                'placeholder' => '-- PILIH --'
            ))
            ->add('gelarAkademik')
            ->add('sksLulus', null, array(
                'label' => 'SKS Lulus',
            ))
            ->add('status', ChoiceType::class, array(
                'label' => 'Status Prodi',
                'choices'   => array(
                    'AKTIF'         => 'AKTIF',
                    'ALIF BENTUK'   => 'ALIH BENTUK',
                    'HAPUS'         => 'HAPUS',
                    'ALIH KELOLA'   => 'ALIH KELOLA',
                    'MERGER'        => 'MERGER',
                    'NON AKTIF'     => 'NON AKTIF',
                ),
                'placeholder' => '-- PILIH --'
            ))
            ->add('tglBerdiri', DateType::class, array(
                'label'     => 'Tanggal Berdiri',
                'required'  => false,
                'widget'    => 'single_text',
                'html5'     => false,
                'format'    => 'dd-mm-yyyy',
                'attr'      => ['class' => 'js-datepicker'],
            ))
            // ->add('mulaiSemester', EntityType::class, array(
            //     'label' => 'Mulai Semester',
            //     'class' => 'AppBundle:Semester',
            //     'query_builder' => function (EntityRepository $er) {
            //         return $er->createQueryBuilder('s');
            //     },
            //     'choice_label' => 'nama',
            //     'placeholder' => '-- PILIH --',
            // ))
            // ->add('semesterAwal', EntityType::class, array(
            //     'label' => 'Semester Awal',
            //     'class' => 'AppBundle:Semester',
            //     'query_builder' => function (EntityRepository $er) {
            //         return $er->createQueryBuilder('s');
            //     },
            //     'choice_label' => 'nama',
            //     'placeholder' => '-- PILIH --',
            // ))
            ->add('frekuensiKurikulum', ChoiceType::class, array(
                'required'    => false,
                'label'       => 'Frekuensi Kurikulum',
                'choices'     => array(
                    'SETIAP 1 TAHUN'        => 'SETIAP 1 TAHUN',
                    'SETIAP 2 TAHUN'        => 'SETIAP 2 TAHUN',
                    'SETIAP 3 TAHUN'        => 'SETIAP 3 TAHUN',
                    'SETIAP 4 TAHUN'        => 'SETIAP 4 TAHUN',
                    'SESUAI PERATURAN PEMERINTAH' => 'SESUAI PERATURAN PEMERINTAH',
                    'SESUAI KEBUTUHAN'      => 'SESUAI KEBUTUHAN',
                ),
                'placeholder' => '-- PILIH --',
            ))
            ->add('pelaksanaanKurikulum', ChoiceType::class, array(
                'required'  => false,
                'label'     => 'Pelaksanaan Kurikulum',
                'choices'   => array(
                    'OLEH P.S. SENDIRI'           => 'OLEH P.S. SENDIRI',
                    'BERSAMA TIM DLM PERG. TINGGI'=> 'BERSAMA TIM DLM PERG. TINGGI',
                    'ORIENTASI PERG. TINGGI LAIN' => 'ORIENTASI PERG. TINGGI LAIN',
                    'ORIENTASI KEBUTUHAN PASAR'   => 'ORIENTASI KEBUTUHAN PASAR',
                    'BERSAMA STAKEHOLDER'         => 'BERSAMA STAKEHOLDER',
                ),
                'placeholder'=> '-- PILIH --',
            ))
            ->add('namaSesi', ChoiceType::class, array(
                'label' => 'Nama Sesi',
                'choices'   => array(
                    'semester'         => 'semester',
                    'cawu'   => 'cawu',
                ),
                'placeholder' => '-- PILIH --'
            ))
            ->add('jumlahSesi', null, array(
                'label' => 'Jumlah Sesi/Tahun',
            ))
            ->add('batasSesi', null, array(
                'label' => 'Batas Sesi',
            ))
            // ->add('ketua', ChoiceType::class, array(
            //     'label' => 'Ketua Prodi',
            //     'choices' => $this->appService->getDosenChoices(),
            //     'placeholder' => '-- Pilih Dosen --',
            // ))
            ->add('hpKetua', null, array(
                'label' => 'HP Ketua',
            ))
            // ->add('operator', ChoiceType::class, array(
            //     'label' => 'Operator',
            //     'choices' => $this->appService->getDosenChoices(),
            //     'placeholder' => '-- Pilih Dosen --',
            // )) 
            ->add('hpOperator', null, array(
                'label' => 'HP Operator',
            ))
            ->add('nomorSkDikti', null, array(
                'required'  => false,
                'label' => 'Nomor SK Dikti',
            ))
            ->add('tglSkDikti')
            ->add('tglBerakhirSkDikti')
            ->add('nomorSkBan', null, array(
                'required'  => false,
                'label' => 'Nomor SK BAN',
            ))
            ->add('tglSkBan')
            ->add('tglBerakhirSkBan')
            ->add('akreditasi')
            ->add('alamat')
            ->add('pos')
            ->add('telp')
            ->add('fax')
            ->add('email')
            ->add('website')
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ));
        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('prodi_index');
        }
        return $this->appService->load('master/pusat/prodi_edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
