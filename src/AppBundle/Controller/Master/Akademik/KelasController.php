<?php

namespace AppBundle\Controller\Master\Akademik;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class KelasController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/master/kelas", name="kelas_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        // $dataKelas = $em->getRepository('AppBundle:Kelas')
        //     ->findBy(array(
        //         'prodi' => $this->getUser()->getProdi(),
        //     ));
        // foreach ($dataKelas as $kelas) {
        //     if ( is_null($kelas->getTa()) ) {
        //         $kelas->setTa( $this->appService->getTahunAkademik() );
        //         $em->persist($kelas);
        //         $em->flush();
        //     }
        // }
        $dataKelas = $em->getRepository('AppBundle:Kelas')
            ->findBy(array(
                // 'ta'    => $this->appService->getTahunAkademik(),
                'prodi' => $this->getUser()->getProdi(),
            ));
        return $this->appService->load('master/akademik/kelas_index.html.twig', array(
        	'data' => $dataKelas,
        ));
    }

    /**
     * @Route("/master/kelas/edit", name="kelas_edit")
     */
    public function editAction(Request $request)
    {
        if (!empty($request->get('id'))) {
            $data = $this->getDoctrine()->getRepository('AppBundle:Kelas')->find($request->get('id'));
        } elseif (!empty($request->get('aksi')) && $request->get('aksi') == "tambah") {
            $data = new \AppBundle\Entity\Kelas();
        } else {
            exit;
        }
        if ( null !== $this->getUser()->getProdi() ) {
            $data->setProdi($this->getUser()->getProdi());
        }
        $form = $this->createFormBuilder($data)
            ->add('prodi', EntityType::class, array(
                'required'  => false,
                'class' => 'AppBundle:ProgramStudi',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p');
                },
                'choice_label' => 'namaProdi',
                'placeholder'   => '-- Pilih --',
            ))
            ->add('nama', null, array(
                'label' => 'Nama Kelas'
            ))
            ->add('ruangan', null, array(
                'label' => 'Ruangan'
            ))
            ->add('kapasitas', null, array(
                'label' => 'Kapasitas'
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('kelas_index');
        }
        return $this->appService->load('master/akademik/kelas_edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
