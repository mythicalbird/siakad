<?php

namespace AppBundle\Controller\Master\Akademik;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use AppBundle\Entity\BobotNilai;
use AppBundle\Entity\BatasSks;
use AppBundle\Entity\Predikat;
use AppBundle\Entity\ProdiKurikulum;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class AkademikController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/master/akademik/predikat/{aksi}", name="predikat")
     */
    public function predikatIndexAction(Request $request, $aksi = 'index')
    {
        if($aksi == 'edit') {
          if (!empty($request->get('id'))) {
              $data = $this->getDoctrine()->getRepository('AppBundle:Predikat')
                ->find($request->get('id'));
          } else {
              $data = new Predikat();
          }
          if (!$data) {
              throw $this->createNotFoundException();
          }
          $form = $this->createFormBuilder($data)
              ->add('predikat')
              ->add('bobotMin')
              ->add('bobotMax')
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class'   => 'btn btn-primary'
                  ),
              ))
              ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('predikat');
          }
        } else {
           $data = $this->getDoctrine()->getRepository('AppBundle:Predikat')
              ->findAll();
        }
        return $this->appService->load('master/akademik/predikat_'.$aksi.'.html.twig', [
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : null
        ]);
    }

    /**
     * @Route("/master/akademik/batas_sks/{aksi}", name="batas_sks")
     */
    public function batasSksIndexAction(Request $request, $aksi = 'index')
    {
        if($aksi == 'edit') {
          if (!empty($request->get('id'))) {
              $data = $this->getDoctrine()->getRepository('AppBundle:BatasSks')
                ->find($request->get('id'));
          } else {
              $data = new BatasSks();
          }
          if (!$data) {
              throw $this->createNotFoundException();
          }
          if ( null !== $this->getUser()->getProdi() ) {
            $data->setProdi($this->getUser()->getProdi());
          }
          $form = $this->createFormBuilder($data)
              ->add('prodi', EntityType::class, array(
                'label'     => 'Program Studi',
                'class'     => 'AppBundle:ProgramStudi',
                'query_builder' => function(EntityRepository $er) {
                          return $er->createQueryBuilder('p');
                },
                'choice_label' => 'namaProdi',
                'placeholder' => '-- Pilih --',
              ))
              ->add('ipkMin', null, array(
                  'label' => 'IPK Minimal'
              ))
              ->add('ipkMax', null, array(
                  'label' => 'IPK Maximal'
              ))
              ->add('jumlahSks', null, array(
                  'label' => 'Batas SKS'
              ))
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class' => 'btn btn-primary'
                  )
              ))
              ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('batas_sks');
          }
        } else {
           $data = $this->getDoctrine()->getRepository('AppBundle:BatasSks')
              ->findByProdi($this->getUser()->getProdi());
        }
        return $this->appService->load('master/akademik/batas_sks_'.$aksi.'.html.twig', [
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : null
        ]);
    }

}
