<?php

namespace AppBundle\Controller\Master\Akademik;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use AppBundle\Entity\Kelas;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\BobotNilai;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class FasilitasController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/master/fasilitas", name="fasilitas_index")
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository(BobotNilai::class)
            ->findAll();
        return $this->appService->load('MasterBundle:Default:bobot_nilai_index.html.twig', [
            'data' => $data
        ]);
    }

    /**
     * @Route("/master/{type}/hapus", name="master_delete")
     */
    public function deleteAction(Request $request, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $master = new Master();
        $master->setType($type);
        $form = $this->createFormBuilder($master)
            ->add('nama', TextType::class, array(
                'label' => 'Tambah ' . ucwords($type),
                'attr'  => array(
                    'placeholder' => 'Nama ' . ucwords($type),
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan'
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $slug = str_replace(' ', '_', strtolower($data->getNama()));
            $master->setSlug(preg_replace('/[^A-Za-z0-9\-]/', '', $slug));
            $em->persist($master);
            $em->flush();
            $this->addFlash('success', ucwords($type) . ' berhasil ditambahkan.');
            return $this->redirectToRoute('master_index', array('type' => $type));
        }
        $masterList = $em->getRepository(Master::class)
            ->findByType($type);
        return $this->appService->load('default/master_index.html.twig', [
            'masterList' => $masterList,
            'form'  => $form->createView(),
            'data'  => $data,
        ]);
    }
  
    public function listDosenPengampu($string) {
      $string = explode("|", $string);
      $result = array();
      foreach( $string as $arr ) {
        $dp = explode("::", $arr);
        $result[$dp[0]] = array(
          'dosen_id'  => ( isset($dp[1]) ) ? $dp[1] : 0,
          'dosen_nama'=> ( isset($dp[2]) ) ? $dp[2] : '',
        );
      }
      return $result;
    }
}
