<?php

namespace AppBundle\Controller\Master\Akademik;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Makul;
use AppBundle\Entity\Master;
use AppBundle\Entity\Kelas;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Kurikulum;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Form\Type\DosenPengampuType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Port\Excel\ExcelReader;
use Port\Steps\StepAggregator;

class MataKuliahController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/master/mata_kuliah/{aksi}", name="makul_index")
     */
    public function indexAction(Request $request, $aksi = 'index')
    {
        $em = $this->getDoctrine()->getManager();
			
        if ( $aksi == "edit" ) {

            if ( !empty($request->get('id')) ) {
                $data = $this->getDoctrine()->getRepository('AppBundle:Makul')
                    ->find($request->get('id'));
            } else {
                $data = new \AppBundle\Entity\Makul();
            }

            $semesterChoices = array();
            for ($i=1; $i <= 14; $i++) { 
                $semesterChoices['Semester ' . $i] = $i;
            }
            $builder = $this->createFormBuilder($data);
            $form = $builder
                ->add('prodi', EntityType::class, array(
                    'label' => 'Program Studi',
                    'class' => 'AppBundle:ProgramStudi',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('p');
                    },
                    'choice_label' => 'namaProdi',
                    'placeholder' => '-- PILIH --',
                ))
                ->add('kode', null, array(
                    'label'     => 'Kode Mata Kuliah',
					'attr'	=> array(
						'readonly'	=> ( $aksi == 'edit' && !empty($request->get('id')) ) ? true : false
					),
                ))
                ->add('nama', null, array(
                    'label'     => 'Nama Mata Kuliah (ind)',
                ))
                ->add('namaEng', null, array(
                    'label'     => 'Nama Mata Kuliah (eng)',
                ))
                ->add('sksTeori', null, array(
                    'label'     => 'SKS Teori',
                ))
                ->add('sksPraktek', null, array(
                    'label'     => 'SKS Praktek',
                ))
                ->add('sksLapangan', null, array(
                    'label'     => 'SKS Lapangan',
                ))
                ->add('kelompok', EntityType::class, array(
                    'label' => 'Kelompok Mata Kuliah',
                    'class' => Master::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('m')
                            ->where('m.type = :type')
                            ->setParameter('type', 'kelompok_makul');
                    },
                    'choice_label' => 'nama',
                    'placeholder' => '-- PILIH --',
                ))
                ->add('jenis', EntityType::class, array(
                    'label' => 'Jenis Mata Kuliah',
                    'class' => Master::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('m')
                            ->where('m.type = :type')
                            ->setParameter('type', 'jenis_makul');
                    },
                    'choice_label' => 'nama',
                    'placeholder' => '-- PILIH --',
                ))
                ->add('silabus', null, array(
                    'label'     => 'Ada Silabus',
                ))
                ->add('satuanAcara', null, array(
                    'label'     => 'Satuan Acara Perkuliahan',
                ))
                ->add('bahanAjar', null, array(
                    'label'     => 'Ada Bahan Ajar',
                ))
                ->add('diktat', null, array(
                    'label'     => 'Diktat',
                ))
                ->add('nilaiMinimal', null, array(
                    'label'     => 'Nilai Minimal Lulus',
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Simpan',
                    'attr'  => array(
                        'class' => 'btn btn-primary'
                    )
                ))
            ;
            $form = $builder->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em->persist($data);
                $em->flush();
                $this->addFlash('success', 'Data berhasil disimpan.');
                return $this->redirectToRoute('makul_index');
            }
 
        } else {
            if ( !empty($request->get('kurikulum')) ) {
                $kurikulum = $em->getRepository('AppBundle:Kurikulum')
                    ->find($request->get('kurikulum'));

                $dataMakul = $em->getRepository('AppBundle:KurikulumMakul')
                    ->findByKurikulum($kurikulum);
                foreach ($dataMakul as $makul) {
                    $this->response['result'][] = array(
                        'id'        => $makul->getId()
                    );
                }

            } else {
                $this->response['error'] = 'Kurikulum tidak boleh kosong!';
            }
            $data = $this->getDoctrine()->getRepository('AppBundle:Makul')
                ->findAll();
            foreach ($data as $mk) {
                $this->response['result'][] = array(
                    'id'            => $mk->getId(),
                    'kode_mk'       => $mk->getKode(),
                    'nama_mk'       => $mk->getNama(),
                    'sks_t'         => $mk->getSksTeori(),
                    'sks_p'         => $mk->getSksPraktek(),
                    'sks_l'         => $mk->getSksLapangan(),
                );
            }
            $jenisMakul = $this->getDoctrine()->getRepository('AppBundle:Master')
                ->findByType('jenis_makul');

            $upload = new \AppBundle\Entity\Upload();
            $builder = $this->createFormBuilder($upload);
            $form = 
                $builder
                ->add('imageFile', VichFileType::class, [
                      'label' => false
                  ]
                )
                ->add('submit', SubmitType::class, array(
                    'label' => 'Import',
                    'attr'  => array(
                        'class' => 'btn btn-primary'
                    )
                ))
            ;
            $form = $builder->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($upload);
                $em->flush();

                // libxml_use_internal_errors(true);
                $path = $this->get('kernel')->getProjectDir() . '/web/uploads/';
                $path .= $upload->getImageName();
                $file = new \SplFileObject($path);
                $reader = new ExcelReader($file, 8);
                $response = new JsonResponse();
                $response->setData($reader);
                return $response;
            }
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('master/akademik/makul_'.$aksi.'.html.twig', [
                'form'          => ( null !== $form ) ? $form->createView() : '',
                'data'          => $this->response,
                'jenisMakul'    => ( isset($jenisMakul) ) ? $jenisMakul : ''
            ]);
        }
    }

    /**
     * @Route("/master/kurikulum/mata_kuliah/{aksi}", name="kurikulum_makul_index")
     */
    public function kurikulumAction(Request $request, $aksi = 'index')
    {
        $em = $this->getDoctrine()->getManager();
        $dataKelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
          ->findAll();
        $dataDosen = $this->getDoctrine()->getRepository('AppBundle:Dosen')
          ->findAll();
        $dataKurikulum = $this->getDoctrine()->getRepository('AppBundle:Kurikulum')
            ->findBy(array(
                'prodi' => $this->getUser()->getProdi()
            )); 
        $params = array(
            'dataKelas'         => $dataKelas,
            'dataDosen'     => $dataDosen,
            // 'dosenPengampu' => $this->listDosenPengampu($data->getDosenPengampu()),
            'jenisMakul'    => ( isset($jenisMakul) ) ? $jenisMakul : '',
            'dataKurikulum' => $dataKurikulum
        );

        if ( $aksi == 'edit' && !empty($request->get('id')) ) {
            $makul = $em->getRepository('AppBundle:KurikulumMakul')
                ->find($request->get('id'));
            if ( !$makul ) {
                $makul = new \AppBundle\Entity\KurikulumMakul();
            }
            $makul->setTa( $makul->getKurikulum()->getTa() );

            if ( $makul->getKurikulum()->getTa()->getKodeSemester() == 2 ) {
                $semesterChoices = array(
                    'Semester 2 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 2,
                    'Semester 4 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 4,
                    'Semester 6 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 6,
                    'Semester 8 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 8,
                    'Semester 10 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 10,
                    'Semester 12 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 12,
                    'Semester 14 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 14,
                );
            } else {
                $semesterChoices = array(
                    'Semester 1 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 1,
                    'Semester 3 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 3,
                    'Semester 5 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 5,
                    'Semester 7 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 7,
                    'Semester 9 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 9,
                    'Semester 11 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 11,
                    'Semester 13 (' . $makul->getKurikulum()->getTa()->getNama() . ')' => 13,                );
            }

            $form = $this->createFormBuilder($makul)
                ->add('kurikulum', EntityType::class, array(
                    'label' => 'Kurikulum',
                    'class' => 'AppBundle:Kurikulum',
                    'query_builder' => function (EntityRepository $er) use ($makul) {
                        return $er->createQueryBuilder('k')
                            ->where('k.id=:id')
                            ->setParameters(array(
                                'id' => $makul->getKurikulum()->getId()
                            ));
                    },
                    'mapped'    => false,
                    'attr'  => array(
                        'readonly'      => true
                    ),
                    'choice_label' => 'nama',
                ))
                ->add('makul', EntityType::class, array(
                    'label' => 'Mata Kuliah',
                    'class' => 'AppBundle:Makul',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('k');
                    },
                    'choice_label' => 'nama',
                    'placeholder' => '-- PILIH --',
                    'attr'         => array(
                        'class'     => 'select2 form-control',
                        'style'     => 'width:100%'
                    ),
                ))
                ->add('semester', ChoiceType::class, array(
                    'choices' => $semesterChoices,
                ))
                ->add('kelas', EntityType::class, array(
                    'label' => 'Kelas',
                    'class' => 'AppBundle:Kelas',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('k');
                    },
                    'choice_label' => function(Kelas $kelas = null) {
                        $label = $kelas->getNama();
                        if ( !empty($kelas->getRuangan()) ) {
                            $label .= " (" . $kelas->getRuangan() . ")";
                        }
                        return $label;
                    },
                    'placeholder' => '-- PILIH --',
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Simpan Perubahan',
                    'attr'  => array(
                        'class' => 'btn btn-primary'
                    )
                ))
                ->getForm();
            $form->handleRequest($request);
            if ( $form->isSubmitted() && $form->isValid() ) {
                $em->persist($makul);
                $em->flush();
                $this->addFlash('success', 'Mata kuliah berhasil disimpan.');
                return $this->redirectToRoute('kurikulum_makul_index', array(
                    'kurikulum' => $makul->getKurikulum()->getId()
                ));
            } 
            $params['makul'] = $makul;
            $params['form'] = $form->createView();
        } else {
            if ( !empty($request->get('kurikulum')) ) {

                $kurikulum = $em->getRepository('AppBundle:Kurikulum')
                    ->find($request->get('kurikulum'));

                $dataMakul = $em->getRepository('AppBundle:KurikulumMakul')
                    ->findByKurikulum($kurikulum);
                foreach ($dataMakul as $makul) {
                    if ( null !== $makul->getMakul() ) {
                        $result = array(
                            'id'        => $makul->getId(),
                            'kode_mk'   => $makul->getMakul()->getKode(),
                            'nama_mk'   => $makul->getMakul()->getNama(),
                            'semester'  => $makul->getSemester(),
                            'sks_t'     => $makul->getMakul()->getSksTeori(),
                            'sks_p'     => $makul->getMakul()->getSksPraktek(),
                            'sks_l'     => $makul->getMakul()->getSksLapangan(),
                            'kelas'     => ( null !== $makul->getKelas() ) ? $makul->getKelas()->getNama() : '',
                            'dosen'     => array(),
                            'jumlah_krs'=> count($makul->getKrs())
                        );
                        if ( null !== $makul->getDosenPengampu() ) {
                            foreach ($makul->getDosenPengampu() as $dp) {
                                if ( null !== $dp->getDosen() ) {
                                    $dosen = $dp->getDosen();
                                    if ( null !== $dosen->getUser() ) {
                                        $result['dosen'][] = $dosen->getUser()->getNama();
                                    }
                                }
                            }
                        }
                        $this->response['result'][] = $result;
                    }
                }

                $makul = new \AppBundle\Entity\KurikulumMakul();
                $makul->setKurikulum($kurikulum);

                if ( $kurikulum->getTa()->getKodeSemester() == 2 ) {
                    $semesterChoices = array(
                        'Semester 2 (' . $kurikulum->getTa()->getNama() . ')' => 2,
                        'Semester 4 (' . $kurikulum->getTa()->getNama() . ')' => 4,
                        'Semester 6 (' . $kurikulum->getTa()->getNama() . ')' => 6,
                        'Semester 8 (' . $kurikulum->getTa()->getNama() . ')' => 8,
                        'Semester 10 (' . $kurikulum->getTa()->getNama() . ')' => 10,
                        'Semester 12 (' . $kurikulum->getTa()->getNama() . ')' => 12,
                        'Semester 14 (' . $kurikulum->getTa()->getNama() . ')' => 14,
                    );
                } else {
                    $semesterChoices = array(
                        'Semester 1 (' . $kurikulum->getTa()->getNama() . ')' => 1,
                        'Semester 3 (' . $kurikulum->getTa()->getNama() . ')' => 3,
                        'Semester 5 (' . $kurikulum->getTa()->getNama() . ')' => 5,
                        'Semester 7 (' . $kurikulum->getTa()->getNama() . ')' => 7,
                        'Semester 9 (' . $kurikulum->getTa()->getNama() . ')' => 9,
                        'Semester 11 (' . $kurikulum->getTa()->getNama() . ')' => 11,
                        'Semester 13 (' . $kurikulum->getTa()->getNama() . ')' => 13,                );
                }
                
                $form = $this->createFormBuilder($makul)
                    // ->add('kurikulum', EntityType::class, array(
                    //     'label' => 'Kurikulum',
                    //     'class' => 'AppBundle:Kurikulum',
                    //     'query_builder' => function (EntityRepository $er) {
                    //         return $er->createQueryBuilder('k')
                    //             ->where('k.prodi=:prodi')
                    //             ->setParameter('prodi', $this->getUser()->getProdi());
                    //     },
                    //     'choice_label' => 'nama',
                    //     'placeholder' => '-- PILIH --',
                    // ))
                    ->add('kurikulum', TextType::class, array(
                        'label' => 'Kurikulum',
                        'attr' => array(
                            'readonly'  => true,
                        ),
                        'data' => $makul->getKurikulum()->getNama(),
                        'mapped' => false,
                    ))
                    ->add('makul', EntityType::class, array(
                        'label' => 'Mata Kuliah',
                        'class' => 'AppBundle:Makul',
                        'query_builder' => function (EntityRepository $er) {
                            return $er->createQueryBuilder('k');
                        },
                        'choice_label' => 'nama',
                        'placeholder' => '-- PILIH --',
                        'attr'         => array(
                            'class'     => 'select2 form-control',
                            'style'     => 'width:100%'
                        ),
                    ))
                    ->add('semester', ChoiceType::class, array(
                        'choices' => $semesterChoices,
                    ))
                    ->add('kelas', EntityType::class, array(
                        'label' => 'Kelas',
                        'class' => 'AppBundle:Kelas',
                        'query_builder' => function (EntityRepository $er) {
                            return $er->createQueryBuilder('k');
                        },
                        'choice_label' => 'nama',
                        'placeholder' => '-- PILIH --',
                        'choice_label' => function(Kelas $kelas = null) {
                            $label = $kelas->getNama();
                            if ( !empty($kelas->getRuangan()) ) {
                                $label .= " (" . $kelas->getRuangan() . ")";
                            }
                            return $label;
                        },
                    ))
                    ->getForm();
                $form->handleRequest($request);
                if ( $form->isSubmitted() && $form->isValid() ) {
                    $em->persist($makul);
                    $em->flush();
                    $this->addFlash('success', 'Mata kuliah berhasil disimpan.');
                    return $this->redirectToRoute('kurikulum_makul_index', array(
                        'kurikulum' => ( !empty($request->get('kurikulum')) ) ? $request->get('kurikulum') : ''
                    ));
                }
                $params['form'] = $form->createView();

            } else {
                $this->response['error'] = 'Kurikulum tidak boleh kosong!';
            }
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            $params['dataDosen'] = $em->getRepository('AppBundle:Dosen')
                ->findAll();
            $params['data'] = $this->response;
            return $this->appService->load('master/akademik/kurikulum_makul_'.$aksi.'.html.twig', $params);
        }
    }

    /**
     * @Route("/master/mata_kuliah/kurikulum/simpan_dp", name="simpan_dp_action")
     * @Method({"POST"})
     */
     public function simpanDpAction(Request $request) 
     {
        if ( !empty($request->get('id_dosen')) ) {
            $em = $this->getDoctrine()->getManager();
            $dosen = $em->getRepository('AppBundle:Dosen')
                ->find($request->get('id_dosen'));
            if ( $dosen ) {
                $makul = $em->getRepository('AppBundle:KurikulumMakul')->find($request->get('id_kr_mk'));
                if ( $makul ) {
                    $dp = new \AppBundle\Entity\DosenPengampu();
                    $dp->setMakul($makul);
                    $dp->setDosen($dosen);
                    $dp->setStatus('publish');
                    $em->persist($dp);
                    $em->flush();

                    $this->addFlash('success', 'Dosen pengajar berhasil disimpan.');
                    return $this->redirectToRoute('kurikulum_makul_index', array(
                        'aksi'  => 'edit',
                        'id'    => $makul->getId()
                    ));
                }
            }
        }
        return new Response();
     }

    /**
     * @Route("/master/makuls/import", name="makul_import")
     * @Method({"POST"})
     */
     public function importAction(Request $request) 
     {
        $response = new JsonResponse();

        return $response;
     }
}
