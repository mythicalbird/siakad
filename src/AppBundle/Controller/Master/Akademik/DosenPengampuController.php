<?php

namespace AppBundle\Controller\Master\Akademik;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Makul;
use AppBundle\Entity\Master;
use AppBundle\Entity\Kelas;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Kurikulum;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Form\Type\DosenPengampuType;

class DosenPengampuController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/master/dosen_pengampu", name="dosen_pengampu")
     */
    public function dosenPengampuIndexAction(Request $request)
    {
        $dataKurikulum = $this->getDoctrine()->getRepository('AppBundle:Kurikulum')
            ->findByProdi($this->getUser()->getProdi());
        $dataKelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
            ->findByProdi($this->getUser()->getProdi());

        $args = array(
            'kurikulum' => ( !empty($request->get('kurikulum')) ) ? $request->get('kurikulum') : '',
            'semester' => ( !empty($request->get('semester')) ) ? $request->get('semester') : '',
        );

        if ( empty($args['kurikulum']) ) {
            $this->response['error'] = "Kurikulum tidak boleh kosong!";
        } elseif ( empty($args['semester']) ) {
            $this->response['error'] = "Semester tidak boleh kosong!";
        } else {
            $params = array(
                'prodi'     => $this->getUser()->getProdi(),
                'semester'  => $args['semester']
            );
            $kurikulum = $this->getDoctrine()->getRepository('AppBundle:Kurikulum')
                ->find($args['kurikulum']);
            if ( $kurikulum ) {
                $params['kurikulum'] = $kurikulum;
            }
            $dataMakul = $this->getDoctrine()->getRepository('AppBundle:KurikulumMakul')
                ->findBy($params);
            foreach ($dataMakul as $kr_makul) {
                if ( null !== $kr_makul->getMakul() ) {
                    $makul = $kr_makul->getMakul();
                    $res = array(
                        'id'        => $kr_makul->getId(),
                        'id_mk'     => $makul->getId(),
                        'kode_mk'   => $makul->getKode(),
                        'nm_mk'     => $makul->getNama(),
                        'semester'  => $kr_makul->getSemester(),
                        'kelas'     => ( null !== $kr_makul->getKelas() ) ? $kr_makul->getKelas()->getNama() : '',
                        'dosen'     => array()
                    );
                    if ( count($kr_makul->getDosenPengampu()) > 0 ) {
                        foreach ($kr_makul->getDosenPengampu() as $dp) {
                            $res['dosen'][] = array(
                                'id'        => $dp->getId()
                            );
                        }
                    }
                    $this->response['result'][] = $res;
                }
            }
        }

        if ( !empty($request->get('kurikulum')) && !empty($request->get('semester')) ) {

        }
        $dataDosen = $this->getDoctrine()->getRepository('AppBundle:Dosen')
          ->findAll();
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('MasterBundle:Default:dosen_pengampu.html.twig', [
                'data'          => $this->response,
                'dataKelas'     => $dataKelas,
                'dataMakul'     => ( isset($dataMakul) ) ? $dataMakul : null,
                'dataDosen'     => $dataDosen,
                'dataKurikulum' => $dataKurikulum
            ]);
        }
    }

    /**
     * @Route("/_ajax/edit/dosen_pengampu", name="dosen_pengampu_update")
     * @Method({"POST"})
     */
    public function dosenPengampuEditAction(Request $request)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $pk = explode(":", $request->get('pk'));
        $makul = $em->getRepository('AppBundle:Makul')
            ->find($pk[0]);
        $data = $em->getRepository('AppBundle:DosenPengampu')
        ->findOneBy(array(
            'makul' => $makul,
            'kelas' => $pk[1]
        ));
        if ( $data ) {
            $dosen = $em->getRepository('AppBundle:Dosen')
                ->find($request->get('value'));
            if ( $dosen ) {
                $data->setDosen($dosen);
                $em->persist($data);
                $em->flush();
            }
        }
    
        $response->setData(array(
            'id' => $request->get('pk'),
            'success' => 1
        ));
        return $response;
    }
}
