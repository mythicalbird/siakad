<?php

namespace AppBundle\Controller\Master\Akademik;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Kurikulum;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\ProgramStudi;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class KurikulumController extends Controller
{
    protected $appService;
    protected $em;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/master/kurikulum", name="kurikulum_index")
     */
    public function indexAction(Request $request)
    {
        // $data = $this->getDoctrine()->getRepository('AppBundle:Kurikulum')
        //     ->findAll();
        $data = $this->getDoctrine()->getRepository('AppBundle:Kurikulum')
            ->findBy(array(
                // 'ta'    => $this->appService->getTahunAkademik(),
                // 'prodi' => $this->getUser()->getProdi()
            ));
        $em = $this->getDoctrine()->getManager();
        $dataKurikulum = $em->createQueryBuilder()
            ->select('k')
            ->from('AppBundle:Kurikulum', 'k')
            ->where('k.status=:status')
            ->setParameters(array(
                'status'    => 'publish'
            ))
            ->getQuery()
            ->getResult();
        foreach ($dataKurikulum as $kurikulum) {
            $this->response['result'][] = array(
                'id'            => $kurikulum->getId(),
                'kode'          => $kurikulum->getKode(),
                'nama'          => $kurikulum->getNama(),
                'sks_wajib'     => $kurikulum->getSksWajib(),
                'sks_pilihan'   => $kurikulum->getSksPilihan(),
                'jumlah_sks'    => $kurikulum->getSksWajib()+$kurikulum->getSksPilihan()
            );
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('master/akademik/kurikulum_index.html.twig', array(
                'data' => $this->response
            ));
        }
    }

    /**
     * @Route("/master/kurikulum/edit", name="kurikulum_edit")
     */
    public function editAction(Request $request)
    {
        if (!empty($request->get('id'))) {
            $data = $this->getDoctrine()->getRepository('AppBundle:Kurikulum')->find($request->get('id'));
        } elseif (!empty($request->get('aksi')) && $request->get('aksi') == "tambah") {
            $data = new \AppBundle\Entity\Kurikulum();
            $currentProdi = $this->appService->getCurrentProdi($this->getUser()->getId());
            if ($currentProdi) {
                $data->setProdi($currentProdi);
            }
        } else {
            exit;
        }
        $form = $this->createFormBuilder($data)
            ->add('prodi', EntityType::class, array(
                'label' => 'Program Studi',
                'class' => ProgramStudi::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p');
                },
                'choice_label' => 'namaProdi',
            ))
            ->add('ta', EntityType::class, array(
                'label' => 'Periode/Tahun Akademik',
                'class' => 'AppBundle:TahunAkademik',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s');
                },
                'choice_label' => function(TahunAkademik $entity = null) {
									$label = '';
									$label .= $entity->getTahun();
									$label .= "/" . ($entity->getTahun()+1);
									if ( $entity->getKodeSemester() == 1 ) {
										$label .= " Ganjil";
									} elseif ( $entity->getKodeSemester() == 2 ) {
										$label .= " Genap";
									} elseif ( $entity->getKodeSemester() == 3 ) {
										$label .= " Pendek";
									}
									return $label;
								},
            ))
            // ->add('tahun', null, array(
            //     'label' => 'Tahun berjalan'
            // ))
            ->add('nama', null, array(
                'label' => 'Nama Kurikulum'
            ))
            ->add('sksWajib', null, array(
                'label' => 'SKS Wajib'
            ))
            ->add('sksPilihan', null, array(
                'label' => 'SKS Pilihan'
            ))
            // ->add('jumlahSksWajib', null, array(
            //     'label' => 'Jumlah SKS Wajib'
            // ))
            // ->add('jumlahSksPilihan', null, array(
            //     'label' => 'Jumlah SKS Pilihan'
            // ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('kurikulum_index');
        }
        return $this->appService->load('master/akademik/kurikulum_edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
