<?php

namespace AppBundle\Controller\Perpustakaan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Service\AppService;

class IdentitasController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/perpustakaan/identitas", name="perpustakaan_index")
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Setting')
          ->findOneByName('data_identitas_perpustakaan');
        if( ! $data ) {
          $data = new \AppBundle\Entity\Setting();
          $data->setName('data_identitas_perpustakaan');
        }
        $now = new\DateTime();
        $data->setModifiedAt($now);
        $builder = $this->createFormBuilder($data);
        $form = $builder
          ->add(
              $builder->create('value', FormType::class, array('by_reference' => true))
              ->add('nipn', null, array(
                  'label'   => 'NIPN',
              ))
              ->add('nama', null, array(
                  'label'   => 'Nama Perpustakaan',
              ))
              ->add('singkatan', null, array(
                  'label'   => 'Nama Singkatan',
              ))
              ->add('alamat', TextareaType::class)
              ->add('telp', null, array(
                  'label'   => 'Telepon',
              ))
              ->add('email', EmailType::class)
              ->add('pustakawan')
          )
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Pengaturan berhasil disimpan.');
        }
        return $this->appService->load('perpustakaan/index.html.twig', array(
            'form'  => $form->createView()
        ));
    }
}
