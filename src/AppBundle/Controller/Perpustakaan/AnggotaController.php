<?php

namespace AppBundle\Controller\Perpustakaan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Service\AppService;

class AnggotaController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/perpustakaan/anggota/{role}", name="anggota_perpustakaan_index")
     */
    public function anggotaIndexAction(Request $request, $role)
    {
        if($role == 'dosen') {
          $hakAkses = $this->getDoctrine()->getRepository('AppBundle:Master')
            ->findOneBy(['type' => 'hak_akses', 'nama' => 'DOSEN']);
        } elseif($role == 'pegawai') {
          $hakAkses = $this->getDoctrine()->getRepository('AppBundle:Master')
            ->findOneBy(['type' => 'hak_akses', 'nama' => 'PEGAWAI']);
        } else {
          $hakAkses = $this->getDoctrine()->getRepository('AppBundle:Master')
            ->findOneBy(['type' => 'hak_akses', 'nama' => 'MAHASISWA']);
        }
        $dataUser = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findBy(array(
            'prodi'     => $this->getUser()->getProdi(),
            'hakAkses'  => $hakAkses
          ));
        foreach ($dataUser as $user) {
          $tgl_lahir = ( null !== $user->getTglLahir() ) ? $user->getTglLahir() : null;
          if ( $role == 'mahasiswa' ) {
            if ( null !== $user->getDataMahasiswa() ) {
              $mhs = $user->getDataMahasiswa();
              if ( !empty($request->get('angkatan')) ) {
                if ( $request->get('angkatan') == $mhs->getAngkatan() ) {
                  // $status = $this->appService->getMasterTermObject('status_mahasiswa');
                  $this->response['result'][] = array(
                    'id'          => $user->getId(),
                    'username'    => $user->getUsername(),
                    'nama'        => $user->getNama(),
                    'jk'          => $user->getJk(),
                    'tpt_lahir'   => $user->getTptLahir(),
                    // 'tgl_lahir'   => ( null !== $tgl_lahir ) ? $tgl_lahir->format('d F Y') : '',
                    'tgl_lahir'   => $tgl_lahir,
                    'semester'    => $mhs->getSemester(),
                    'status'      => ( null !== $mhs->getStatus() ) ? $mhs->getStatus()->getNama() : ''
                  );
                }
              } else {
                // $status = $this->appService->getMasterTermObject('status_mahasiswa');
                $this->response['result'][] = array(
                  'id'          => $user->getId(),
                  'username'    => $user->getUsername(),
                  'nama'        => $user->getNama(),
                  'jk'          => $user->getJk(),
                  'tpt_lahir'   => $user->getTptLahir(),
                  // 'tgl_lahir'   => ( null !== $tgl_lahir ) ? $tgl_lahir->format('d F Y') : '',
                  'tgl_lahir'   => $tgl_lahir,
                  'semester'    => $mhs->getSemester(),
                  'status'      => ( null !== $mhs->getStatus() ) ? $mhs->getStatus()->getNama() : ''
                );
              }
            }
          }
          elseif ( $role == 'dosen' ) {
            if ( null !== $user->getDataDosen() ) {
              $dosen = $user->getDataDosen();
              $this->response['result'][] = array(
                'id'          => $user->getId(),
                'username'    => $user->getUsername(),
                'nama'        => $user->getNama(),
                'jk'          => $user->getJk(),
                'tpt_lahir'   => $user->getTptLahir(),
                // 'tgl_lahir'   => ( null !== $tgl_lahir ) ? $tgl_lahir->format('d F Y') : '',
                'tgl_lahir'   => $tgl_lahir
              );
            }
          }
          else {
            $this->response['result'][] = array(
              'id'          => $user->getId(),
              'username'    => $user->getUsername(),
              'nama'        => $user->getNama(),
              'jk'          => $user->getJk(),
              'tpt_lahir'   => $user->getTptLahir(),
              // 'tgl_lahir'   => ( null !== $tgl_lahir ) ? $tgl_lahir->format('d F Y') : '',
              'tgl_lahir'   => $tgl_lahir 
            );
          }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
          return $this->appService->load('perpustakaan/anggota/'.$role.'_index.html.twig', array(
              'data'  => $this->response
          ));
        }
    }

    /**
     * @Route("/perpustakaan/cetak/anggota/{id}", name="anggota_perpustakaan_cetak")
     */
    public function cetakAction(Request $request, $id)
    {
        $perpustakaan = $this->appService->getSetting('data_identitas_perpustakaan');
        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->find($id);
        return $this->appService->load('cetak/perpus_kta.html.twig', array(
            'data'  => $data,
            'perpustakaan' => $perpustakaan
        ));
    }
}
