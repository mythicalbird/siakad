<?php

namespace AppBundle\Controller\Perpustakaan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Pmb;
use AppBundle\Entity\PmbPeriode;
use AppBundle\Entity\Master;
use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\Semester;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use AppBundle\Form\Type\PmbFormType;
use AppBundle\Service\AppService;

class LaporanController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/perpustakaan/laporan/histori_peminjaman", name="histori_peminjaman")
     */
    public function indexAction(Request $request)
    {
        $dataDosen = $this->getDoctrine()->getRepository('AppBundle:Dosen')
          ->findAll();
        $dataPgw = $this->getDoctrine()->getRepository('AppBundle:Pegawai')
          ->findAll();
        return $this->appService->load('perpustakaan/laporan/history_index.html.twig', array(
          'dataDosen' => $dataDosen,
          'dataPgw' => $dataPgw
        ));
    }

    /**
     * @Route("/perpustakaan/laporan/pustaka_belum_kembali", name="pustaka_belum_kembali")
     */
    public function pustakaBelumKembaliAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:PerpusPeminjaman')
          ->findAll();
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:PerpusPeminjaman')
          ->findBySudahKembali(0);
        return $this->appService->load('perpustakaan/laporan/belum_kembali_index.html.twig', array(
            'data'  => $data
        ));
    }
 
    /**
     * @Route("/perpustakaan/laporan/pustaka_belum_kembali/cetak/{id}", name="pustaka_belum_kembali_cetak")
     */
    public function pustakaBelumKembaliCetakAction(Request $request, $id)
    {
        $pustaka = $this->getDoctrine()->getRepository('AppBundle:PerpusPustaka')
          ->find($id);

        $data = $this->getDoctrine()->getRepository('AppBundle:PerpusPeminjaman')
          ->findByPustaka($pustaka);
        $perpustakaan = $this->appService->getSetting('data_identitas_perpustakaan');
        return $this->appService->load('cetak/perpus_pustaka_belum_kembali.html.twig', array(
            'data'  => $data,
            'pustaka' => $pustaka,
            'perpustakaan'  => $perpustakaan
        ));
    }
}
