<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;
use AppBundle\Service\AppService;

class OptimizeController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/optimize/optimize/{table}", name="optimize_index")
     */
    public function indexAction(Request $request, $table = 'master')
    {
        $message = '';
        $em = $this->getDoctrine()->getManager();
        if ( $table == 'dosen' ) {
            $data = $em->getRepository('AppBundle:Dosen')
                ->findAll();
            foreach ($data as $row) {
                if ( null === $row->getUser() ) {
                    $em->remove($row);
                }
            }
            $message = 'Table dosen berhasil dioptimize';
        } elseif ( $table == 'mahasiswa' ) {
            $data = $em->getRepository('AppBundle:Mahasiswa')
                ->findAll();
            foreach ($data as $row) {
                if ( null === $row->getUser() ) {
                    $em->remove($row);
                }
            }
            $message = 'Table mahasiswa berhasil dioptimize';
        } elseif ( $table == 'kurikulum' ) {
            $data = $em->getRepository('AppBundle:Kurikulum')
                ->findAll();
            foreach ($data as $kurikulum) {
                $dataMakul = $em->getRepository('AppBundle:Makul')
                    ->findByKurikulum($kurikulum);
                foreach ($dataMakul as $makul) {
                    $dataKrs = $em->getRepository('AppBundle:Krs')
                        ->findByMakul($makul);
                    foreach ($dataKrs as $krs) {
                        $dataNp = $em->getRepository('AppBundle:NilaiPerkuliahan')
                            ->findByKrs($krs);
                        foreach ($dataNp as $np) {
                            $em->remove($np);
                        }
                        $em->remove($krs);
                    }
                    $dataDp = $em->getRepository('AppBundle:DosenPengampu')
                        ->findByMakul($makul);
                    foreach ($dataDp as $dp) {
                        $em->remove($dp);
                    }
                    $em->remove($makul);
                }
                $em->remove($kurikulum);
            }
            $message = 'Table kurikulum berhasil dioptimize';
        } elseif ( $table == 'jadwal' ) {
            $data = $em->getRepository('AppBundle:Jadwal')
                ->findAll();
            foreach ($data as $row) {
                if ( null === $row->getMakul() ) {
                    $em->remove($row);
                }
            }
            $message = 'Table jadwal berhasil dioptimize';
        } 
        $em->flush();
        return new Response($message);
    }

    /**
     * @Route("/optimize/force_delete/{table}", name="force_delete_index")
     */
    public function deleteAction(Request $request, $table = 'master')
    {
        $message = '';
        $em = $this->getDoctrine()->getManager();
        if ( $table == 'user' ) {
            $data = $em->getRepository('AppBundle:User')
                ->findAll();
            foreach ($data as $row) {
                $dataPerpusPeminjaman = $em->getRepository('AppBundle:PerpusPeminjaman')
                    ->findByAnggota($row);
                foreach ($dataPerpusPeminjaman as $data1) {
                    $em->remove($data1);
                }
                if ( null !== $row->getDataMahasiswa() ) {
                    $mhs = $row->getDataMahasiswa();
                    $dataMhsCuti = $em->getRepository('AppBundle:MahasiswaCuti')
                        ->findByMahasiswa($mhs);
                    foreach ($dataMhsCuti as $cuti) {
                        $em->remove($cuti);
                    }
                    $dataMhsPendidikan = $em->getRepository('AppBundle:MahasiswaPendidikan')
                        ->findByMahasiswa($mhs);
                    foreach ($dataMhsPendidikan as $pd) {
                        $em->remove($pd);
                    }
                    $dataMhsAktifitas = $em->getRepository('AppBundle:MahasiswaAktifitas')
                        ->findByMahasiswa($mhs);
                    foreach ($dataMhsAktifitas as $aktifitas) {
                        $em->remove($aktifitas);
                    }
                    $dataBerkas = $em->getRepository('AppBundle:Berkas')
                        ->findByMahasiswa($mhs);
                    foreach ($dataBerkas as $berkas) {
                        $em->remove($berkas);
                    }
                    $dataMhsNilai = $em->getRepository('AppBundle:Nilai')
                        ->findByMahasiswa($mhs);
                    foreach ($dataMhsNilai as $nilai) {
                        $em->remove($nilai);
                    }
                    $dataKrs = $em->getRepository('AppBundle:Krs')
                        ->findByMahasiswa($mhs);
                    foreach ($dataKrs as $krs) {
                        $dataMhsNilaiPerkuliahan = $em->getRepository('AppBundle:NilaiPerkuliahan')
                            ->findByKrs($krs);
                        foreach ($dataMhsNilaiPerkuliahan as $np) {
                            $em->remove($np);
                        }
                        $em->remove($krs);
                    }
                    $em->remove($mhs);
                }
                if ( null !== $row->getDataDosen() ) {
                    $dosen = $row->getDataDosen();
                    $dataDosenPengampu = $em->getRepository('AppBundle:DosenPengampu')
                        ->findByDosen($dosen);
                    foreach ($dataDosenPengampu as $dp) {
                        $em->remove($dp);
                    }
                    $em->remove($dosen);
                }
                $role_super_admin = $this->appService->getMasterTermObject('hak_akses', 1);
                $role_admin = $this->appService->getMasterTermObject('hak_akses', 2);
                if ( $row->getHakAkses() != $role_super_admin || $row->getHakAkses() != $role_admin ) {
                    $em->remove($row);
                }
            }
            $message = 'Table user berhasil dioptimize';
        } elseif ( $table == 'dosen' ) {
            $data = $em->getRepository('AppBundle:Dosen')
                ->findAll();
            foreach ($data as $row) {
                $dataMahasiswa = $em->getRepository('AppBundle:Mahasiswa')
                    ->findByPa($row);
                foreach ($dataMahasiswa as $mhs) {
                    $mhs->setPa(null);
                    $em->persist($mhs);
                    $em->flush();
                }
                if ( null !== $row->getUser() ) {
                    $em->remove($row->getUser());
                }
                $em->remove($row);
            }
            $message = 'Table dosen berhasil dioptimize';
        } elseif ( $table == 'mahasiswa' ) {
            $data = $em->getRepository('AppBundle:Mahasiswa')
                ->findAll();
            foreach ($data as $row) {
                if ( null !== $row->getUser() ) {
                    $user = $row->getUser();
                    // $user->se
                    $em->remove($user);
                }
                $em->remove($row);
            }
            $message = 'Table mahasiswa berhasil dioptimize';
        } elseif ( $table == 'kurikulum' ) {
            $data = $em->getRepository('AppBundle:Kurikulum')
                ->findAll();
            foreach ($data as $kurikulum) {
                $dataMakul = $em->getRepository('AppBundle:KurikulumMakul')
                    ->findByKurikulum($kurikulum);
                foreach ($dataMakul as $makul) {
                    $dataKrs = $em->getRepository('AppBundle:Krs')
                        ->findByMakul($makul);
                    foreach ($dataKrs as $krs) {
                        $dataNp = $em->getRepository('AppBundle:NilaiPerkuliahan')
                            ->findByKrs($krs);
                        foreach ($dataNp as $np) {
                            $em->remove($np);
                        }
                        $em->remove($krs);
                    }
                    $dataJadwal = $em->getRepository('AppBundle:Jadwal')
                        ->findByMakul($makul);
                    foreach ($dataJadwal as $jadwal) {
                        $em->remove($jadwal);
                    }
                    $dataDp = $em->getRepository('AppBundle:DosenPengampu')
                        ->findByMakul($makul);
                    foreach ($dataDp as $dp) {
                        $em->remove($dp);
                    }
                    $em->remove($makul);
                }
                $em->remove($kurikulum);
            }
            $message = 'Table kurikulum berhasil dioptimize';
        } elseif ( $table == 'makul' ) {
            $data = $em->getRepository('AppBundle:Makul')
                ->findAll();
            foreach ($data as $row) {
                $em->remove($row);
            }
            $message = 'Table makul berhasil dikosongkan...';
        } elseif ( $table == 'kelas' ) {
            $data = $em->getRepository('AppBundle:Kelas')
                ->findAll();
            foreach ($data as $row) {
                $dataMahasiswa = $em->getRepository('AppBundle:Mahasiswa')
                    ->findByKelas($row);
                foreach ($dataMahasiswa as $mhs) {
                    $mhs->setKelas(null);
                    $em->persist($mhs);
                    $em->flush();
                }
                $em->remove($row);
            }
            $message = 'Table kelas berhasil dikosongkan...';
        } elseif ( $table == 'cuti' ) {
            $data = $em->getRepository('AppBundle:MahasiswaCuti')
                ->findAll();
            foreach ($data as $row) {
                $em->remove($row);
            }
            $message = 'Table mahasiswa_cuti berhasil dikosongkan...';
        } 
        $em->flush();
        return new Response($message);
    }

}
