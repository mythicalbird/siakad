<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Service\AppService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class KrsController extends Controller
{
		
		protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

		public function __construct(AppService $appService) {
			$this->appService = $appService;
		}

    /**
     * @Route("/api/v1/krs/{nim}")
     * @Method("GET")
     * @param $nim
     * @return JsonResponse
     */
    public function indexAction($nim = null)
    {
		    $response = new JsonResponse();
        if ( null !== $nim ) {
          $user = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findOneByUsername($nim);
          if ( !$user ) {
            $this->response['error'] = "Mahasiswa dengan NPM " . $nim . " tidak ditemukan!";
          } else {
            if ( null !== $user->getDataMahasiswa() ) {
              $mhs = $user->getDataMahasiswa();
              $result = array(
                'mahasiswa' => array(
                  'id'          => $mhs->getId(),
                  'id_user'     => $user->getId(),
                  'nama'        => $user->getNama(),
                  'npm'         => $user->getUsername(),
                  'prodi'       => ( null !== $user->getProdi() ) ? $user->getProdi()->getNamaProdi() : null,
                  'angkatan'    => (int)$mhs->getAngkatan(),
                  'semester'    => $mhs->getSemester(),
                ),
                'krs'       => null
              );
              foreach ($mhs->getKrs() as $krs) {
                if ( null !== $krs->getMakul() ) {
                  $result['krs'][] = array(
                    'id'          => $krs->getId(),
                    'kode_mk'     => $krs->getMakul()->getKode(),
                    'nama_mk'     => $krs->getMakul()->getNama(),
                    'sks_teori'   => $krs->getMakul()->getSksTeori(),
                    'sks_praktek' => $krs->getMakul()->getSksPraktek(),
                    'sks_lapangan'=> $krs->getMakul()->getSksLapangan(),
                    'jml_sks'     => $krs->getMakul()->getSksTeori()+$krs->getMakul()->getSksPraktek()+$krs->getMakul()->getSksLapangan(),
                  );
                }
              }
              $this->response['result'] = $result;
            }
          }
        } else {
          $dataKrs = $this->getDoctrine()->getRepository('AppBundle:Krs')
            ->findAll();
          foreach ($dataKrs as $krs) {
            if ( null !== $krs->getMahasiswa() ) {
              $mhs = $krs->getMahasiswa();
              if ( null !== $mhs->getUser() ) {
                $result = array(
                  'id'              => $krs->getId(),
                  'id_mahasiswa'    => $mhs->getId(),
                  'id_user'         => $mhs->getUser()->getId(),
                  'npm'             => $mhs->getUser()->getUsername(),
                  'nama_mahasiswa'  => $mhs->getUser()->getNama(),
                  'smt_mahasiswa'   => $mhs->getSemester() 
                );
                if ( null !== $krs->getMakul() ) {
                  $result['id_makul'] = $krs->getMakul()->getId();
                  $result['nama_makul'] = $krs->getMakul()->getNama();
                  $result['smt_makul'] = $krs->getMakul()->getSemester();
                }
                $result['status'] = $krs->getStatus();
                $this->response['result'] = $result;
              }
            }
          }
          // $this->response['error'] = "NPM harus diisi!";
        }
      //   $response->setData($this->response);
		    // return $response;
        return new Response( json_encode($this->response) );
    }

}
