<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Service\AppService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NilaiController extends Controller
{
		
		protected $appService;
    private $response = array(
      'error'   => null,
      'result'  => array()
    );

		public function __construct(AppService $appService) {
			$this->appService = $appService;
		}

    /**
     * @Route("/api/v1/nilai/rekap", name="api_nilai_rekap")
     */
    public function rekapAction(Request $request)
    {
		    $response = new JsonResponse();
        if ( empty($request->get('angkatan')) ) {
          $this->response['error'] = 'Tahun angkatan harus diisi';
        } 
        elseif ( empty($request->get('semester')) ) {
          $this->response['error'] = 'Semester harus diisi';
        }
        else {
          $dataMahasiswa = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
            ->findBy(array(
              'angkatan'  => $request->get('angkatan'),
              'semester'  => $request->get('semester')
            ));
          foreach ($dataMahasiswa as $mhs) {
            if ( null !== $mhs->getUser() ) {
              $result = array(
                'id'            => $mhs->getId(),
                'npm'           => $mhs->getUser()->getUsername(),
                'nama'          => $mhs->getUser()->getNama(),
                'jk'            => $mhs->getUser()->getJk(),
                'prodi'         => ( null !== $mhs->getUser()->getProdi() ) ? $mhs->getUser()->getProdi()->getNamaProdi() : null,
                'ipk'           => $this->appService->getIpkMahasiswa($mhs)
              );
              $this->response['result'][] = $result;
            }
          }
        }
        $response->setData($this->response);
		    return $response;
    }

    /**
     * @Route("/api/v1/nilai/ipk/{npm}", name="api_nilai_rekap")
     * @param $npm, $semester
     */
    public function ipkAction(Request $request, $npm = null)
    {
        $response = new JsonResponse();
        if ( null !== $npm ) {
          
          $user = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findOneByUsername($npm);

          if ( null !== $user->getDataMahasiswa() ) {
            $mhs = $user->getDataMahasiswa();

            $result = array(
              'id'    => $mhs->getId(),
              'npm'   => $user->getUsername(),
              'nama'  => $user->getNama(),
              'jk'    => $user->getJk(),
              'ipk'   => 0
            );

            if ( empty($request->get('semester')) ) {
              
              $ipk = $this->appService->getIpkMahasiswa($mhs, $request->get('semester'));
              $result['ipk'] = $ipk;


            } else {



            }

            $this->response['result'] = $result;

          }

        } else {
          $this->response['error'] = "NPM harus diisi!";
        }
        $response->setData($this->response);
        return $response;
    }

}
