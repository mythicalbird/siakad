<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Service\AppService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserController extends Controller
{
		
		protected $appService;
    private $response = array(
      'error'   => null,
      'result'  => array()
    );

		public function __construct(AppService $appService) {
			$this->appService = $appService;
		}

    /**
     * @Route("/api/v1/user/current", name="api_user_current")
     */
    public function indexAction(Request $request)
    {
		    $response = new JsonResponse();
        $result = array(
          'id'          => $this->getUser()->getId(),
          'username'    => $this->getUser()->getUsername(),
          'email'       => $this->getUser()->getEmail(),
          'name'        => $this->getUser()->getNama(),
          'roles'       => $this->getUser()->getRoles(),
        );
        $this->response['result'] = $result;
        $response->setData($this->response);
		    return $response;
    }

}
