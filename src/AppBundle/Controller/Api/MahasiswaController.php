<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Service\AppService;
use AppBundle\Service\DosenPengampu;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MahasiswaController extends Controller
{
		
		protected $appService;
    protected $response = array();

		public function __construct(AppService $appService) {
			$this->appService = $appService;
      $this->response['error'] = null;
		}

    /**
     * @Route("/api/v1/mahasiswa/list", name="api_mahasiswa_lis")
     */
    public function indexAction(Request $request)
    {
		    $response = new JsonResponse();
        
        $data = $this->getDoctrine()->getRepository('AppBundle:Dosen')
          ->findAll();
        foreach ($data as $mhs) {
          $this->result[] = array(
            'nama'  => $mhs->getUser()->getNama()
          );
        }
        $response->setData($this->result);
		    return $response;
    }

}
