<?php

namespace AppBundle\Controller\Akademika\StatusMahasiswa;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Kelas;
use AppBundle\Entity\Mahasiswa;
use AppBundle\Entity\Semester;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class LulusDropoutController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/status_mahasiswa/lulus_dropout", name="mahasiswa_dropout_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dataMahasiswa = $em->createQueryBuilder()
            ->select('m')
            ->from('AppBundle:Mahasiswa', 'm')
            ->where('m.status=:status_lulus OR m.status=:status_dropout')
            ->setParameters(array(
                'status_lulus'  => $this->appService->getMasterTermObject('status_mahasiswa', 'L'),
                'status_dropout'  => $this->appService->getMasterTermObject('status_mahasiswa', 'D'),
            ))
            ->getQuery()
            ->getResult();
        foreach ($dataMahasiswa as $mhs) {
            if ( null !== $mhs->getUser() ) {
                $jml_sks = 0;
                $dataKrs = $em->getRepository('AppBundle:Krs')
                    ->findByMahasiswa($mhs);
                foreach ($dataKrs as $krs) {
                    if ( null !== $krs->getMakul() && null !== $krs->getMakul()->getMakul() ) {
                        $makul = $krs->getMakul()->getMakul();
                        $makul_sks = $makul->getSksTeori()+$makul->getSksPraktek()+$makul->getSksLapangan();
                        $jml_sks += $makul_sks;
                    }
                }

                $this->response['result'][] = array(
                    'id'            => $mhs->getId(),
                    'id_user'       => $mhs->getUser()->getId(),
                    'nim'           => $mhs->getUser()->getUsername(),
                    'nama'          => $mhs->getUser()->getNama(),
                    'angkatan'      => $mhs->getAngkatan(),
                    'status'        => $mhs->getStatus()->getNama(),
                    'sks'           => $jml_sks
                );
            }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/status_mahasiswa/lulus_dropout_index.html.twig', array(
              'data'          => $this->response
            ));
        }
    }
}
