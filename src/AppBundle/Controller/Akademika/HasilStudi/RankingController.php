<?php

namespace AppBundle\Controller\Akademika\HasilStudi;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class RankingController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/akademika/hasil_studi/rangking", name="hs_rangking_index")
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
            ->findAll();
        return $this->appService->load('AkademikaBundle:Default:hs_rangking_index.html.twig', array(
          'data'  => $data
        ));
    }
  
    /**
     * @Route("/akademika/hasil_studi/ijazah", name="hs_ijazah_index")
     */
    public function nilaiTugasAkhirIndexAction(Request $request)
    {
    	  $data = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
            ->findAll();
        return $this->appService->load('AkademikaBundle:Default:hs_ijazah_index.html.twig', array(
        	'data'	=> $data
        ));
    }

}
