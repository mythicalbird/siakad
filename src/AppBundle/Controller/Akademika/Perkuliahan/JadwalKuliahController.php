<?php

namespace AppBundle\Controller\Akademika\Perkuliahan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class JadwalKuliahController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }
  
    /**
     * @Route("/akademika/perkuliahan/jadwal", name="jadwal_kuliah_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $prodi = $this->getUser()->getProdi();
        $params = array(
          'dataHari'      => $em->getRepository('AppBundle:Master')
              ->findByType('hari'),
          'dataKelas'     => $em->createQueryBuilder()->select('k')
              ->from('AppBundle:Kelas', 'k')
              // ->where('k.prodi = :prodi')
              // ->setParameter('prodi', $this->getUser()->getProdi())
              ->getQuery()
              ->getResult(),
          'dataJamKuliah' => $em->createQueryBuilder()->select('j')
              ->from('AppBundle:JamKuliah', 'j')
              ->where('j.prodi=:prodi')
              ->setParameter('prodi', $prodi)
              ->orderBy('j.jam', 'ASC')
              ->getQuery()
              ->getResult(),
          'dataMakul'     => array()
        );

        $args = array(
          'semester'  => $request->get('semester'),
          'kelas'  => $request->get('kelas'),
        );

        if ( !empty( $args['semester'] ) ) {

          $params['ta'] = $this->appService->getTa( $request->get('semester'), true );

          // if ( !empty( $args['kelas'] ) ) {

            if ( !empty($request->get('kelas')) ) {
              $kelas = $em->getRepository('AppBundle:Kelas')
                ->find($request->get('kelas'));
              if ( $kelas ) {
                $args['kelas'] = $kelas->getNama();
              }
            }
            foreach ($params['dataHari'] as $hari) {
              $this->response['result'][$hari->getKode()] = array(
                'hari'            => $hari->getNama(),
                'kelas_kuliah'    => array()
              );
              foreach ($params['dataJamKuliah'] as $jam) {
                $args['jam'] = $jam->getJam();
                $dataKelasPerkuliahan = $em->createQueryBuilder()
                  ->select('j')
                  ->from('AppBundle:Jadwal', 'j')
                  ->where('j.ta=:ta AND j.hari=:hari AND j.jam=:jam')
                  ->setParameters(array(
                    'ta'        => $params['ta'],
                    'hari'      => $hari,
                    'jam'       => $jam
                  ))
                  ->getQuery()
                  ->getResult();
                foreach ($dataKelasPerkuliahan as $jadwal) {
                  $key = $hari->getKode() . "_j" . $jam->getId() . "_s" . $jadwal->getSemester();
                  $this->response['result'][$hari->getKode()]['kelas_kuliah'][$key] = array(
                    'id'          => $jadwal->getId(),
                    'semester'    => $jadwal->getSemester(),
                    'kelas'       => $jadwal->getKelas(),
                    'makul'       => $jadwal->getMakul(),
                    'jam'         => $jam->getDari()->format('H:i') . " - " . $jam->getSampai()->format('H:i'),
                    'hari'        => ( null !== $jadwal->getHari() ) ? $jadwal->getHari()->getNama() : ''
                  );
                }
              }
            }

            if ( $params['ta']->getKodeSemester() == 1 ) {
              $params['dataSemester'] = [1,3,5,7];
            } else {
              $params['dataSemester'] = [2,4,6,8];
            }

            $kurikulum = $em->getRepository('AppBundle:Kurikulum')
              ->findOneBy(array(
                'prodi'   => $this->getUser()->getProdi(),
                'ta'      => $params['ta'],
                'status'  => 'publish'
              ));
            foreach ($params['dataSemester'] as $smt) {
              if ( ! $kurikulum ) {
                $params['dataMakul'][$smt] = array();
              } else {
                $params['dataMakul'][$smt] = $em->createQueryBuilder()
                  ->select('m')
                  ->from('AppBundle:KurikulumMakul', 'm')
                  ->where('m.kurikulum=:kurikulum and m.semester=:semester')
                  ->setParameters(array(
                    // 'prodi'     => $this->getUser()->getProdi(),
                    'kurikulum' => $kurikulum,
                    'semester'  => $smt
                  ))
                  ->getQuery()
                  ->getResult();
              }
            }

          // } else {
            
          //   $this->response['error'] = 'Kelas harus dipilih!';

          // }

        } else {
          
          $this->response['error'] = 'Periode/Semester harus dipilih!';

        }

        if ( $this->get('security.authorization_checker')->isGranted('ROLE_DOSEN') ) {
          $template = 'jadwal_kuliah_dosen_index';
        } elseif ( $this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA') ) {
          $template = 'jadwal_kuliah_mahasiswa_index';
        } else {
          $template = 'jadwal_kuliah_index';
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            // return $this->appService->load('akademika/perkuliahan/'.$template.'.html.twig', array(
            //   'ta'          => $ta,
            //   'data'        => $this->response,
            //   // 'hari'        => $hari,
            //   'dataJamKuliah'         => $dataJamKuliah,
            //   // 'semester'    => $semester,
            //   'dataKelas'       => $dataKelas,
            //   'dataMakul'       => $dataMakul,
            //   'dataSemester'  => $dataSemester
            // ));

            $params['data'] = $this->response;
            return $this->appService->load('akademika/perkuliahan/'.$template.'.html.twig', $params);
        }
    }

    /**
     * @Route("/_ajax/jadwal_kuliah/update", name="jadwal_kuliah_update_action")
     * @Method({"POST"})
     */
    public function jadwalKuliahUpdateAction(Request $request)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $jadwal = $em->getRepository('AppBundle:Jadwal')
          ->find( $request->get('pk') );
        if ( $jadwal ) {

          $makul = $em->getRepository('AppBundle:KurikulumMakul')
            ->find( $request->get('value') );

          if ( $makul ) {

            if ( null !== $makul->getKelas() ) {
              $jadwal->setKelas( $makul->getKelas() );
            }
            $jadwal->setMakul($makul);
            $em->persist($jadwal);
            $em->flush();
          }

          $response->setData(array(
            'id' => $jadwal->getId(),
            'success' => 1
          ));
        }
        return $response;
    }
  
}
