<?php

namespace AppBundle\Controller\Akademika\Perkuliahan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\JamKuliah;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Presensi;
use AppBundle\Service\AppService;

class PresensiMahasiswaController extends Controller
{  
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }
    /**
     * @Route("/akademika/perkuliahan/presensi/mahasiswa", name="presensi_mahasiswa_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ta = $this->appService->getTahunAkademik();

        $dataKelas = $em->getRepository('AppBundle:Kelas')
          ->findAll();
        $dataMahasiswa = array();
        $params = array(
          'dataKelas' => $dataKelas
        );

        if ( !empty( $request->get('semester') ) ) {

          $params['ta'] = $ta;

          if ( !empty( $request->get('kelas') ) ) {

            $params['kelas'] = $em->getRepository('AppBundle:Kelas')
              ->find( $request->get('kelas') );
            $params['dataMakul'] = $em->getRepository('AppBundle:KurikulumMakul')
              ->findBy(array(
                'kelas'     => $params['kelas'],
                'semester'  => $request->get('semester')
              ));

            if ( !empty( $request->get('makul') ) ) {

              $params['makul'] = $em->getRepository('AppBundle:KurikulumMakul')
                ->find( $request->get('makul') );

              if ($this->get('security.authorization_checker')->isGranted('ROLE_DOSEN')) {
                  if ( null !== $this->getUser()->getDataDosen() ) {
                    $kelas = null;
                    if ( !empty($request->get('kelas')) ) {
                      $tmp_kelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
                          ->find($request->get('kelas'));
                      if ( $tmp_kelas ) {
                        $kelas = $tmp_kelas->getNama();
                      }
                    }
                    // $dataMakul = $this->appService->getDosenMakul( $this->getUser()->getDataDosen(), $kelas );
                  }
              } else {

                $dataMahasiswaUserList = $em->getRepository('AppBundle:User')
                  ->findBy(array(
                    'prodi'     => $this->getUser()->getProdi(),
                    'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4)
                  ));

                $dataMahasiswa = $em->getRepository('AppBundle:Mahasiswa')
                  ->findBy(array(
                    'kelas' => $params['kelas']
                  ));
                foreach ($dataMahasiswa as $mhs) {
                  if ( null !== $mhs->getUser() ) {
                    $user = $mhs->getUser();
                    $this->response['result'][] = array(
                      'id'        => $mhs->getId(),
                      'id_user'   => $user->getId(),
                      'nim'       => $user->getUsername(),
                      'nama'      => $user->getNama(),
                      'angkatan'  => $mhs->getAngkatan(),
                      'presensi'  => array(
                        'hadir' => $this->appService->getCountPresensiMahasiswa($mhs, $params['makul'], 'hadir'),
                        'sakit' => $this->appService->getCountPresensiMahasiswa($mhs, $params['makul'], 'sakit'),
                        'ijin'  => $this->appService->getCountPresensiMahasiswa($mhs, $params['makul'], 'ijin'),
                        'alpa'  => $this->appService->getCountPresensiMahasiswa($mhs, $params['makul'], 'alpa'),
                      )
                    );
                  }
                }

              }

              // if ( $data ) {

                $form = $this->createFormBuilder()
                  ->add('tanggal', DateType::class, array(
                      'label'     => 'Tanggal',
                      'widget'    => 'single_text',
                      'html5'     => false,
                      'format'    => 'dd-mm-yyyy',
                      'attr'      => ['class' => 'js-datepicker'],
                  ))
                  ->add('jam', EntityType::class, array(
                      'class'     => 'AppBundle:JamKuliah',
                      'query_builder' => function (EntityRepository $er) {
                          return $er->createQueryBuilder('jk');
                      },
                      'attr'          => array( 'class' => 'form-control' ),
                      'choice_label'  => function(JamKuliah $jam = null) {
                        return $jam->getJam() . ": " . $jam->getDari()->format('H:i') . "-" . $jam->getSampai()->format('H:i');
                      },
                  ))
                  ->add('mhs', CollectionType::class, array(
                      'label' => false,
                      'entry_type' => HiddenType::class,
                  ))
                  ->add('presensi', CollectionType::class, array(
                      'label' => false,
                      'entry_type' => HiddenType::class,
                  ))
                  ->add('id_makul', HiddenType::class)
                  ->add('ret', HiddenType::class)
                  ->getForm();
                $form->handleRequest($request);
                if ( $form->isSubmitted() ) {
                  $post = $_POST['form'];
                  // echo "<pre>";
                  // var_dump($post);
                  // echo "</pre>";exit;
                  $tanggal = new\DateTime($post['tanggal']);
                  $jam = $em->getRepository('AppBundle:JamKuliah')->find($post['jam']);
                  for($i = 0; $i < count($post['mhs']); $i++) {
                    $mhs = $em->getRepository('AppBundle:Mahasiswa')
                      ->find($post['mhs'][$i]);
                    $ket = $post['presensi'][$i];
                    $presensi = $em->getRepository('AppBundle:PresensiMahasiswa')
                      ->findOneBy(array(
                        'makul'         => $params['makul'],
                        'mahasiswa'     => $mhs,
                        'tanggal'       => $tanggal, 
                        'jam'           => $jam
                      ));
                    if ( !$presensi ) {
                      $presensi = new \AppBundle\Entity\PresensiMahasiswa();
                      $presensi->setMakul($params['makul']);
                      $presensi->setMahasiswa($mhs);
                      $presensi->setTanggal($tanggal);
                      $presensi->setJam($jam);
                    }
                    $presensi->setKet(strtolower($ket));
                    $em->persist($presensi);
                    $em->flush();
                  }
                  $this->addFlash('success', 'Data berhasil disimpan');
                  return $this->redirect($post['ret'], 302);
                }

                $params['form'] = $form->createView(); 

            } else {

              $this->response['error'] = 'Mata Kuliah tidak boleh kosong!';

            }


          } else {

            $this->response['error'] = 'Kelas tidak boleh kosong!';

          }
          

        } else {
        
          $this->response['error'] = 'Semester tidak boleh kosong!';

        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            $params['data'] = $this->response;
            return $this->appService->load('akademika/perkuliahan/presensi_mahasiswa_index.html.twig', $params);
        }
    }

    /**
     * @Route("/akademika/perkuliahan/presensi_mahasiswa/edit", name="presensi_mahasiswa_edit")
     * @Method({"POST"})
     */
    public function presensiMahasiswaEditAction(Request $request)
    {
        // NON AJAX
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('username'));

        if ( $user ) {

            $dataPresensi = $request->get('presensi');
            foreach ($dataPresensi as $ps) {

              $presensi = $em->getRepository('AppBundle:PresensiMahasiswa')
                ->find($ps['id']);

              if ( $presensi ) {

                if ( isset($ps['jam_id']) ) {
                  $jam = $em->getRepository('AppBundle:JamKuliah')
                    ->find($ps['jam_id']);
                  if ( $jam ) {
                    $presensi->setJam($jam);
                  }
                }
                
                if ( isset($ps['ket']) ) {
                  $presensi->setKet($ps['ket']);
                }

                $presensi->setTahunAkademik($this->appService->getTahunAkademik());

                $em->persist($presensi);
                $em->flush();

              }

            }
        
        }

        $this->addFlash('success', 'Data berhasil diperbarui...');
        return $this->redirectToRoute('presensi_mahasiswa_index');
    }

    /**
     * @Route("/_ajax/akademika/perkuliahan/presensi_mahasiswa/edit", name="presensi_mahasiswa_edit_ajax")
     * @Method({"POST"})
     */
    public function presensiDosenEditAjaxAction(Request $request)
    {
        // AJAX
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('username'));

        $html = '';

        if ( $user ) {
          if ( null !== $user->getDataMahasiswa() ) {
            $mhs = $user->getDataMahasiswa();
              $dataPresensi = $em->getRepository('AppBundle:PresensiMahasiswa')
                ->findBy(array(
                  // 'makul'         => $params['makul'],
                  'mahasiswa'     => $mhs,
                  // 'tanggal'       => $tanggal, 
                  // 'jam'           => $jam
                ));
              if ( $dataPresensi ) {
                $no = 1;
                $count = 0;
                foreach ($dataPresensi as $ps) {
                  $html .= '<tr>';
                  $html .= '<td>'.$no.'</td>';
                  $html .= '<td>'.$this->appService->hari($ps->getTanggal()).', '.$ps->getTanggal()->format('d-m-Y').'</td>';
                  $html .= '<td>';

                  $html .= '<select class="form-control" name="presensi['.$count.'][jam_id]">';
                  $html .= '<option value="">-- Pilih Jam --</option>';
                  $jamKuliah = $em->getRepository('AppBundle:JamKuliah')
                    ->findByProdi($this->getUser()->getProdi());
                  if ( $jamKuliah ) {
                    foreach ($jamKuliah as $jam) {
                      $html .= '<option value="';
                      $html .= $jam->getId();
                      $html .= '"';
                      $html .= ( null !== $ps->getJam() && $jam == $ps->getJam() ) ? ' selected' : '';
                      $html .= '>';
                      $html .= $jam->getJam();
                      $html .= ( null !== $jam->getDari() ) ? ' >> ' . $jam->getDari()->format('H:i') : '';
                      $html .= ( null !== $jam->getSampai() ) ? ' - '.$jam->getSampai()->format('H:i') : '';
                      $html .= '</option>';
                    }
                  }
                  $html .= '</select>';
                  $html .= '<input type="hidden" name="presensi['.$count.'][id]" value="'.$ps->getId().'">';
                  $html .= '</td>';
                  $html .= '<td align="center"><input type="radio" name="presensi['.$count.'][ket]" value="hadir"';
                  $html .= ($ps->getKet() == 'hadir') ? ' checked' : '';
                  $html .= ' required></td>';
                  $html .= '<td align="center"><input type="radio" name="presensi['.$count.'][ket]" value="sakit"';
                  $html .= ($ps->getKet() == 'sakit') ? ' checked' : '';
                  $html .= ' required></td>';
                  $html .= '<td align="center"><input type="radio" name="presensi['.$count.'][ket]" value="ijin"';
                  $html .= ($ps->getKet() == 'ijin') ? ' checked' : '';
                  $html .= ' required></td>';
                  $html .= '<td align="center"><input type="radio" name="presensi['.$count.'][ket]" value="alpa"';
                  $html .= ($ps->getKet() == 'alpa') ? ' checked' : '';
                  $html .= ' required></td>';
                  $no++;
                  $count++;
                }
              }
          }
        }

        $response->setData($html);

        return $response;
    }

}
