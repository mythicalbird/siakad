<?php

namespace AppBundle\Controller\Akademika;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\WisudaPeriode;
use AppBundle\Entity\TahunAkademik;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class WisudaController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/wisudawan", name="wisudawan")
     */
    public function wisudawanAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Wisudawan')
          ->findAll();
        return $this->appService->load('akademika/wisuda/wisudawan.html.twig', array(
        	'data'	=> $data,
        ));
    }

    /**
     * @Route("/akademika/wisuda", name="wisuda_pengajuan_index")
     */
    public function indexAction(Request $request)
    {

        $params = array();

        // cek periode
        $ta = $this->appService->getTahunAkademik();

        $tahun = $this->getDoctrine()->getRepository('AppBundle:TahunAkademik')
          ->findOneBy(array(
            'tahun' => $ta->getTahun(),
            'kodeSemester'  => 2,
          ));

        if ( $tahun ) {
          $periode = $this->getDoctrine()->getRepository('AppBundle:WisudaPeriode')
            ->findOneByTahun($tahun);
          if( $periode ) {
            $params['periode'] = $periode;
          }
        }


        if ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) {
          
          $sudahAjukan = 1;
          $data = $this->getDoctrine()->getRepository('AppBundle:Wisudawan')
              ->findOneByMahasiswa($this->getUser()->getDataMahasiswa());
          if ( ! $data ) {
            $data = new \AppBundle\Entity\Wisudawan();
            $sudahAjukan = 0;
          }
          
          $data->setMahasiswa( $this->getUser()->getDataMahasiswa() );

        } else {

          if ( !empty($request->get('nim')) ) {

            $user = $this->getDoctrine()->getRepository('AppBundle:User')
              ->findOneByUsername( $request->get('nim') );
            if ( $user ) {

              $mhs = $user->getDataMahasiswa();
              $params['mhs'] = $mhs;

              $data = $this->getDoctrine()->getRepository('AppBundle:Wisudawan')
                ->findOneByMahasiswa( $mhs );

              if ( ! $data ) {
                $data = new \AppBundle\Entity\Wisudawan();
              }

              if ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) { 
                $data->setMahasiswa($this->getUser()->getDataMahasiswa());
              } else {
                $data->setMahasiswa($mhs);
              }

            }

          } else {

            // if ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) {
              $data = $this->getDoctrine()->getRepository('AppBundle:Wisudawan')
                  ->findOneByMahasiswa($this->getUser()->getDataMahasiswa());
              $sudahAjukan = ( $data ) ? 1 : 0;
            // }

          }

        }

        $form = $this->createFormBuilder($data)
          ->add('periode', EntityType::class, array(
              'label' => 'Periode',
              'class' => 'AppBundle:WisudaPeriode',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('p');
              },
              'choice_label' => function (WisudaPeriode $entity = null) {
                  return $entity ? $entity->getTahun()->getNama() . ' ( ' . $entity->getNama() . ' )' : '';
              },
              'placeholder'   => '-- Pilih periode --' 
          ))
          ->add('toga')
          ->add('ukuranToga')
          ->add('topi')
          ->add('ukuranTopi')
          ->add('kalung')
          ->add('undangan')
          ->add('photo')
          ->add('ijazah')
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class' => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);

        $params['form'] = $form->createView();

        if($form->isSubmitted() && $form->isValid()) {
          $post = $form->getData();

          $em = $this->getDoctrine()->getManager();

          $em->persist($data);
          $em->flush();
          $this->addFlash('success', 'Data berhasil disimpan.');
          return $this->redirectToRoute('wisudawan');
        }

        $params['sudahAjukan'] = ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) ? $sudahAjukan : 0;

        return $this->appService->load('akademika/wisuda/index.html.twig', $params);
    }


    /**
     * @Route("/akademika/wisuda/periode/{aksi}", name="wisuda_periode_index")
     */
    public function periodeAction(Request $request, $aksi = "index")
    {
        if( $aksi == "edit" )  {
          if( !empty($request->get('id')) ) {
            $data = $this->getDoctrine()->getRepository('AppBundle:WisudaPeriode')
                ->find($request->get('id')); 
          } else {
            $data = new WisudaPeriode();
          }
          $tahunChoices = array();
          for ($i=date('Y')+3; $i >= 2000; $i--) { 
            $tahunChoices[$i] = $i;
          }
          $form = $this->createFormBuilder($data)
              ->add('tahun', ChoiceType::class, array(
                  'label'     => 'Tahun',
                  'choices'   => $tahunChoices,
              ))
              ->add('nama', null, array(
                  'label' => 'Nama Periode',
                  'attr'  => array(
                    'placeholder' => 'contoh: Periode ' . date('Y') . '/' . (date('Y')+1)
                  )
              ))
              ->add('tglMulai', DateType::class, array(
                  'label'     => 'Tanggal Mulai',
                  'widget'    => 'single_text',
                  'html5'     => false,
                  'format'    => 'dd-MM-yyyy',
                  'attr'      => ['class' => 'js-datepicker'],
              ))
              ->add('tglSelesai', DateType::class, array(
                  'label'     => 'Tanggal Selesai',
                  'widget'    => 'single_text',
                  'html5'     => false,
                  'format'    => 'dd-MM-yyyy',
                  'attr'      => ['class' => 'js-datepicker'],
              ))
              ->add('tglWisuda', DateType::class, array(
                  'label'     => 'Tanggal Wisuda',
                  'widget'    => 'single_text',
                  'html5'     => false,
                  'format'    => 'dd-MM-yyyy',
                  'attr'      => ['class' => 'js-datepicker'],
              ))
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class' => 'btn btn-primary'
                  )
              ))
              ->getForm();
              $form->handleRequest($request);
              if($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();
                $this->addFlash('success', 'Data berhasil disimpan.');
                return $this->redirectToRoute('wisuda_periode_index');
              }
        } else {
          $data = $this->getDoctrine()->getRepository('AppBundle:WisudaPeriode')
              ->findAll(); 
        }
        return $this->appService->load('akademika/wisuda/periode_'.$aksi.'.html.twig', array(
          'data'  => $data,
          'form'  => (isset($form)) ? $form->createView() : '',
        ));
    }
  
    /**
     * @Route("/akademika/wisuda/alumni", name="alumni_index")
     */
    public function alumniAction(Request $request)
    {
        $params = array(
          'status'  => $this->appService->getMasterTermObject('status_mahasiswa', 'L')
        );
        $dataAlumni = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
          ->findBy($params);
        foreach ($dataAlumni as $alumni) {
          if ( null !== $alumni->getUser() && $alumni->getUser()->getStatus() != 'trash' ) {
            $this->response['result'][] = array(
              'id'              => $alumni->getId(),
              'id_user'         => $alumni->getUser()->getId(),
              'nim'             => $alumni->getUser()->getUsername(),
              'nama'            => $alumni->getUser()->getNama(),
              'angkatan'        => $alumni->getAngkatan(),
              'email'           => $alumni->getUser()->getEmail(),
              'telp'            => $alumni->getUser()->getTelp(),
              'hp'              => $alumni->getUser()->getHp(),
              'ipk'             => '',
            ); 
          }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/wisuda/alumni_index.html.twig', array(
              'data'  => $this->response
            ));
        }
    }


    /**
     * @Route("/akademika/wisudawan/cari/mahasiswa", name="wisudawan_cari_mahasiswa")
     * @Method({"POST"})
     */
    public function cariMahasiswaAction(Request $request)
    {
        $response = new JsonResponse();
        $mahasiswa = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('nim'));
        if ($mahasiswa) {
          if( null !== $mahasiswa->getProdi() ) {
            $prodi = $mahasiswa->getProdi()->getNamaProdi();
          } else {
            $prodi = 'Tidak ditemukan';
          }
          $html = '<div class="row"><div class="col-md-12"><table class="table table-bordered table-responsive"><tr><td>Nama Mahasiswa</td>';
          $html .= '<td><strong>'.$mahasiswa->getNama().'</strong></td>';
          $html .= '</tr><tr><td>NIM</td>';
          $html .= '<td><em>'.$mahasiswa->getUsername().'</em></td>';
          $html .= '</tr><tr><td>Tahun Angkatan</td>';
          $html .= '<td>'.$mahasiswa->getNama().'</td>';
          $html .= '</tr><tr><td>Kode Prodi</td>';
          $html .= '<td>'.$prodi.'</td>';
          $html .= '</tr><tr><td>Jurusan</td>';


            $html .= '<td></td>';
            $html .= '</tr>';


            $html .= '</table></div></div>';
            $html .= '<div class="clearfix"></div>';
          $response->setData($html);
        } else {
          $response->setData('<div class="alert alert-error">Mahasiswa tidak ditemukan!</div>');
        }
        return $response;
    }
}
