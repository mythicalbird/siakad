<?php

namespace AppBundle\Controller\Akademika;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Kelas;
use AppBundle\Entity\Mahasiswa;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class KrsController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/krs/konfirmasi", name="krs_index")
     */
    public function indexAction(Request $request, $aksi = 'index')
    {
        $em = $this->getDoctrine()->getManager();
        $params = array(
          'dataDosen' => $em->getRepository('AppBundle:Dosen')
              ->findAll(),
          'dataMakul' => $em->getRepository('AppBundle:Makul')
              ->findAll(),
          'dataKelas' => []
        );
        $id_dosen = ( !empty($request->get('dosen')) ) ? $request->get('dosen') : 0;
        if ( empty($id_dosen) ) {
          
          $this->response['error'] = 'Dosen harus dipilih!';

        } else {

          if ( empty($request->get('tahun')) ) {

            $this->response['error'] = 'Angkatan mahasiswa harus dipilih!';

          } else {

            if ( $this->get('security.authorization_checker')->isGranted('ROLE_DOSEN') ) {
                
              $dosen = $this->getUser()->getDataDosen();

            } else {

              $dosen = $em->getRepository('AppBundle:Dosen')
                ->find($request->get('dosen'));

            }

            $params['dataMakul'] = array();
            $dosenMakulDiampu = $dosen->getMakulDiampu();
            $unset = [];
            foreach ($dosenMakulDiampu as $dp) {
              if ( null !== $dp->getMakul() && null !== $dp->getMakul()->getMakul() ) {
                if ( !in_array($dp->getMakul()->getMakul()->getId(), $unset) ) {
                  $params['dataMakul'][] = array(
                    'id'  => $dp->getMakul()->getMakul()->getId(),
                    'nama'  => $dp->getMakul()->getMakul()->getNama(),
                  );
                  array_push($unset, $dp->getMakul()->getMakul()->getId());
                }
              }
            }

            if ( empty($request->get('makul')) ) {

              $this->response['error'] = 'Mata kuliah harus dipilih!';

            } else {

              $makul = $em->getRepository('AppBundle:Makul')
                ->find($request->get('makul'));

              $params['dataKelas'] = $this->appService->getListKelasByMakul($makul, $this->getUser()->getProdi());  

              if ( empty($request->get('kelas')) ) {

                $this->response['error'] = 'Kelas mata kuliah harus dipilih!';

              } else {

                $kelas = $em->getRepository('AppBundle:Kelas')
                  ->find($request->get('kelas'));

                $dataMakulKurikulum = $em->createQueryBuilder()
                  ->select('k')
                  ->from('AppBundle:KurikulumMakul', 'k')
                  ->where('k.kelas!=:kelas and k.makul=:makul')
                  ->setParameters(array(
                    'kelas' => $kelas,
                    'makul' => $makul
                  ))
                  ->getQuery()
                  ->getResult(); 

                $krsParams = [ 
                  'status_trash'  => 'trash',
                  'status_aktif'  => 'aktif',
                  'makul'   => $dataMakulKurikulum
                ];
                $dataKrs = $em->createQueryBuilder()
                  ->select('k')
                  ->from('AppBundle:Krs', 'k')
                  ->where('k.status!=:status_trash or k.status!=:status_aktif and k.makul in (:makul)')
                  ->setParameters($krsParams)
                  ->getQuery()
                  ->getResult();
                foreach ($dataKrs as $krs) {
                  if ( null !== $krs->getMakul()->getMakul() ) {
                    $makul = $krs->getMakul()->getMakul();
                    $mhs = $krs->getMahasiswa();
                    if ( !empty($request->get('tahun')) && $mhs->getAngkatan() == $request->get('tahun') ) {
                      $this->response['result'][] = array(
                        'id'                  => $krs->getId(),
                        'id_makul'            => $makul->getId(),
                        'id_kurikulum_makul'  => $krs->getMakul()->getId(),
                        'kode_mk'             => $makul->getKode(),
                        'nama_mk'             => $makul->getNama(),
                        'kelas'               => ( null !== $krs->getMakul()->getKelas() ) ? $krs->getMakul()->getKelas()->getNama() : '',
                        'nim_mhs'             => $mhs->getUser()->getUsername(),
                        'nama_mhs'            => $mhs->getUser()->getNama(),
                        'angkatan'            => $mhs->getAngkatan(),
                        'semester'            => $krs->getMakul()->getSemester(),
                        'status'              => $krs->getStatus()
                      );
                    }
                  }
                }

              }

            }


          }

        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            $params['data'] = $this->response;
                // 'dataMakul' => $dataMakul,
                // 'dataDosen' => $dataDosen,
                // 'dataKelas' => ( isset($dataKelas) ) ? $dataKelas : null
            return $this->appService->load('akademika/krs/krs_konfirmasi_index.html.twig', $params);
        }
    }

    /**
     * @Route("/akademika/cetak_krs/{nim}", name="krs_cetak")
     */
    public function cetakAction(Request $request, $nim)
    {
        $em = $this->getDoctrine()->getManager();
        if ( null !== $nim ) {
          $user = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findOneByUsername($nim);
          if ( !$user ) {
            $this->response['error'] = "Mahasiswa dengan NPM " . $nim . " tidak ditemukan!";
          } else {
            if ( null !== $user->getDataMahasiswa() ) {
              $mhs = $user->getDataMahasiswa();
              $semester = ( !empty($request->get('semester')) ) ? $request->get('semester') : $mhs->getSemester();
              $result = array(
                'semester'  => $semester,
                'mahasiswa' => array(
                  'id'          => $mhs->getId(),
                  'id_user'     => $user->getId(),
                  'nama'        => $user->getNama(),
                  'nim'         => $user->getUsername(),
                  'prodi'       => ( null !== $user->getProdi() ) ? $user->getProdi()->getNamaProdi() : null,
                  'angkatan'    => (int)$mhs->getAngkatan(),
                  'semester'    => $mhs->getSemester(),
                  'nama_pa'         => '',
                  'nip_pa'          => ''
                ),
                'krs'       => null
              );
              $dataKrs = $em->createQueryBuilder()
                ->select('k')
                ->from('AppBundle:Krs', 'k')
                ->where('k.mahasiswa=:mhs and k.status!=:status_trash')
                ->setParameters(array(
                  'mhs' => $mhs,
                  'status_trash'  => 'trash'
                ))
                ->getQuery()
                ->getResult();
              foreach ($dataKrs as $krs) {
                if ( $krs->getSemester() == $semester && null !== $krs->getMakul() ) {
                  $result['krs'][] = array(
                    'id'          => $krs->getId(),
                    'kode_mk'     => $krs->getMakul()->getMakul()->getKode(),
                    'nama_mk'     => $krs->getMakul()->getMakul()->getNama(),
                    'nama_mk_eng' => $krs->getMakul()->getMakul()->getNamaEng(),
                    'sks_teori'   => $krs->getMakul()->getMakul()->getSksTeori(),
                    'sks_praktek' => $krs->getMakul()->getMakul()->getSksPraktek(),
                    'sks_lapangan'=> $krs->getMakul()->getMakul()->getSksLapangan(),
                    'jml_sks'     => $krs->getMakul()->getMakul()->getSksTeori()+$krs->getMakul()->getMakul()->getSksPraktek()+$krs->getMakul()->getMakul()->getSksLapangan(),
                    'status'      => $krs->getStatus() ,
                    'jadwal'      => $this->appService->getKrsJadwalKuliah($krs)
                  );
                }
              }
              $this->response['result'] = $result;
            }
          }
        } else {
          $dataKrs = $this->getDoctrine()->getRepository('AppBundle:Krs')
            ->findAll();
          foreach ($dataKrs as $krs) {
            if ( null !== $krs->getMahasiswa() ) {
              $mhs = $krs->getMahasiswa();
              if ( null !== $mhs->getUser() ) {
                $result = array(
                  'id'              => $krs->getId(),
                  'id_mahasiswa'    => $mhs->getId(),
                  'id_user'         => $mhs->getUser()->getId(),
                  'nim'             => $mhs->getUser()->getUsername(),
                  'nama_mahasiswa'  => $mhs->getUser()->getNama(),
                  'smt_mahasiswa'   => $mhs->getSemester(),
                  'nama_pa'         => '',
                  'nip_pa'          => ''
                );
                if ( null !== $krs->getMakul() ) {
                  $result['id_mk'] = $krs->getMakul()->getId();
                  $result['nama_mk'] = $krs->getMakul()->getNama();
                  $result['nama_mk_eng'] = $krs->getMakul()->getNamaEng();
                  $result['smt_mk'] = $krs->getMakul()->getSemester();
                  $result['status'] = $krs->getStatus();
                }
                $this->response['result'] = $result;
              }
            }
          }
          // $this->response['error'] = "NPM harus diisi!";
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('cetak/mahasiswa_krs.html.twig', array(
                'data'      => $this->response
            ));
        }
    }

    /**
     * @Route("/akademika/krs/ubah/{aksi}", name="krs_ubah")
     */
    public function ubahAction(Request $request, $aksi = 'index')
    {
        $em = $this->getDoctrine()->getManager();
        $dataMakul = $em->getRepository('AppBundle:Makul')
          ->findAll();
        $dataAspekNilai = $em->getRepository('AppBundle:AspekNilai')
              ->findByTahun($this->appService->getTahunAkademik());
        $dataBobotNilai = $em->getRepository('AppBundle:BobotNilai')
              ->findAll();
        if ( !empty($request->get('nim')) ) {
          $mhsUser = $em->getRepository('AppBundle:User')
            ->find($request->get('nim'));
          if ( $mhsUser && null !== $mhsUser->getDataMahasiswa() ) {
            $mhs = $mhsUser->getDataMahasiswa();
          } else {

          }
        } else {
          $mhs = null;
        }
        $dataKrs = $em->createQueryBuilder()
          ->select('k')
          ->from('AppBundle:Krs', 'k')
          ->where('k.status!=:status and k.mahasiswa=:mahasiswa')
          ->setParameters(array(
            'mahasiswa' => $mhs,
            'status'    => 'trash'
          ))
          ->getQuery()
          ->getResult();
        $no = 1;
        foreach ($dataKrs as $krs) {
          if ( null !== $krs->getMakul()->getMakul() ) {
            $makul = $krs->getMakul()->getMakul();
            $mhs = $krs->getMahasiswa();

            $result = array(
              'id'                  => $krs->getId(),
              'id_mhs'              => $mhs->getId(),
              'id_user'             => $mhs->getUser()->getId(),
              'id_makul'            => $makul->getId(),
              'id_kurikulum_makul'  => $krs->getMakul()->getId(),
              'kode_mk'             => $makul->getKode(),
              'nama_mk'             => $makul->getNama(),
              'semester'            => $krs->getSemester(),
              'kelas'               => ( null !== $krs->getMakul()->getKelas() ) ? $krs->getMakul()->getKelas()->getNama() : '',
              'nim'                 => $mhs->getUser()->getUsername(),
              'nama'                => $mhs->getUser()->getNama(),
              'jk'                  => $mhs->getUser()->getJk(),
              'nilai_aspek'         => array(),
              'nilai_huruf'         => $krs->getNilaiHuruf(), 
              'nilai_angka'         => ( $krs->getNilaiAngka() > 0 ) ? $krs->getNilaiAngka() : 0.00,
              'lulus'               => null,
              'status'              => $krs->getStatus()
            );

            foreach ($dataAspekNilai as $aspek) {
              $nilai = $this->appService->getKrsNilaiAspek($krs, $aspek);
              $nilai_akhir = $this->appService->getKrsNilaiAspek($krs, $aspek, true);
              $result['nilai_aspek'][] = array(
                'id'              => $aspek->getId(),
                'id_np'           => $this->appService->getKrsNilaiAspek($krs, $aspek, false, true),
                'nama_aspek'      => $aspek->getNama(),
                'persentase'      => $aspek->getPersen(),
                'nilai'           => $nilai,
                'nilai_akhir'     => $nilai_akhir, // nilai setelah dipersentase
              );
            }

            if ( !empty($request->get('dt')) && $request->get('dt') == "true" ) {
              $button_html = '';
              $dt_result = array(
                $no,
                // $result['nim'],
                // $result['nama'],
                $result['nama_mk'],
                $result['semester'],
                $result['kelas']
              );

              foreach ($result['nilai_aspek'] as $aspek) {
                $nilai_aspek_html = '';
                $nilai_aspek_html .= '<span id="aspek-'.$aspek['id'].'-'.$result['id'].'">';
                if ( $aspek['nama_aspek'] != 'Absensi' ) {
                  $nilai_aspek_html .= '<a href="#" class="nilai-aspek-'.$aspek['id'].'" data-type="number" data-pk="'.$aspek['id_np'].'">';
                }
                $nilai_aspek_html .= $aspek['nilai'];
                if ( $aspek['nama_aspek'] != 'Absensi' ) {
                  $nilai_aspek_html .= '</a>';
                }
                // $nilai_aspek_html .= '<span class="aspek-persen">('.$aspek['nilai_akhir'].')</span>';
                $nilai_aspek_html .= '</span>';
                $dt_result[] = $nilai_aspek_html;
              }

              $nilai_huruf_html = '<a href="#" class="nilai_huruf" data-type="select" data-pk="'.$result['id'].'" data-url="';
              $nilai_huruf_html .= $this->generateUrl('nilai_semester_krs_update_nilai', array('type' => 'huruf'));
              $nilai_huruf_html .= '">'.$result['nilai_huruf'].'</a>';
              $dt_result[] = $nilai_huruf_html;

              $dt_result[] = '<span class="krs-'.$result['id'].' nilai_angka">'.$result['nilai_angka'].'</span>';

              // $dt_result[] = $result['status'];
              $this->response['result'][] = $dt_result;
              $no++;
            } else {
              $this->response['result'][] = $result;
            }
          }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            if ( !empty($request->get('dt')) && $request->get('dt') == "true" ) {
              $response->setData(array('data'=> $this->response['result']));
            } else {
              $response->setData($this->response);
            }
            return $response;
        } else {
            return $this->appService->load('akademika/krs/krs_ubah_index.html.twig', array(
                'data'      => $this->response,
                'dataMakul'       => $dataMakul,
                'dataAspekNilai'  => $dataAspekNilai,
                'dataBobotNilai'  => $dataBobotNilai
            ));
        }
    }


    /**
     * @Route("/akademika/ambil_krs", name="krs_ambil")
     */
    public function ambilAction(Request $request, $aksi = 'index', $param = array())
    {
        // form actions
        // param: id_makul = array
        if ( !empty( $request->get('id_makul') ) && !empty( $request->get('semester') ) ) {
            $this->ambilKrsAction($request);
        }

        $em = $this->getDoctrine()->getManager();
        $ta = $this->appService->getTahunAkademik();
        $param['ta'] = $ta;


        // $this->response['result']['ta'] = $ta->getNama();
        $this->response['result']['paket_krs'] = array();
        $this->response['result']['krs_sudah_diambil'] = array();

        if ( $this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA') ) {

          $mhs = $this->getUser()->getDataMahasiswa();

          $this->response['result']['mahasiswa'] = array(
            'id'            => $mhs->getId(),
            'id_user'       => $this->getUser()->getId(),
            'nama'          => $this->getUser()->getNama()
          );
          
          if ( !empty($request->get('semester')) ) {

              // $kurikulum = $

              // $dataMakul = $em->getRepository('AppBundle:KurikulumMakul')
              //     ->findBy(array(
              //       // 'prodi'     => $this->getUser()->getProdi(),
              //       'semester'  => $request->get('semester'),
              //       // 'ta'        => $param['ta']
              //       'kelas'     => $mhs->getKelas()
              //       // 'kurikulum' => $kurikulum
              //     ));
              $dataMakul = $em->createQueryBuilder()
                ->select('mk')
                ->from('AppBundle:KurikulumMakul', 'mk')
                ->where('mk.semester=:semester and mk.kelas=:kelas and mk.status!=:status_trash')
                ->setParameters(array(
                  'semester'      => $request->get('semester'),
                  'kelas'         => $mhs->getKelas(),
                  'status_trash'  => 'trash'
                ))
                ->getQuery()
                ->getResult();
              if ( $dataMakul ) {
                  foreach ($dataMakul as $kr_makul) {
                    $dosen_pengampu = array();
                    if ( null !== $kr_makul->getDosenPengampu() && count($kr_makul->getDosenPengampu()) > 0 ) {
                      foreach ($kr_makul->getDosenPengampu() as $dp) {
                        $dosen_pengampu[] = $dp->getDosen()->getUser()->getNama();
                      } 
                      $makul = $kr_makul->getMakul();
                      if ( null !== $kr_makul->getKurikulum() && $kr_makul->getKurikulum()->getProdi() == $mhs->getUser()->getProdi() ) {
                        $this->response['result']['paket_krs'][] = array(
                          'id'            => $kr_makul->getId(),
                          'id_mk'         => $makul->getId(),
                          'kode_mk'       => $makul->getKode(),
                          'nama_mk'       => $makul->getNama(),
                          'sks_teori'     => $makul->getSksTeori(),
                          'sks_praktek'   => $makul->getSksPraktek(),
                          'sks_lapangan'  => $makul->getSksLapangan(),
                          'jml_sks'       => $makul->getSksTeori()+$makul->getSksPraktek()+$makul->getSksLapangan(),
                          'kelas'         => ( null !== $kr_makul->getKelas() ) ? $kr_makul->getKelas()->getNama() : '',
                          'dosen_pengampu'=> $dosen_pengampu
                        );
                      }
                    }

                  }
              }
          }


          $dataKrs = $em->getRepository('AppBundle:Krs')
            ->findBy(array(
              'mahasiswa' => $mhs
            ));
          if ( $dataKrs ) {
            foreach ($dataKrs as $krs) {
              $this->response['result']['krs_sudah_diambil'][] = array(
                'id'            => $krs->getId(),
                'semester'      => $krs->getSemester(),
                'kelas'         => ( null !== $krs->getMakul()->getKelas() ) ? $krs->getMakul()->getKelas()->getNama() : '',
                'kode_mk'       => $krs->getMakul()->getMakul()->getKode(),
                'nama_mk'       => $krs->getMakul()->getMakul()->getNama(),
                'sks_teori'     => $krs->getMakul()->getMakul()->getSksTeori(),
                'sks_praktek'   => $krs->getMakul()->getMakul()->getSksPraktek(),
                'sks_lapangan'  => $krs->getMakul()->getMakul()->getSksLapangan(),
                'jml_sks'       => $krs->getMakul()->getMakul()->getSksTeori()+$krs->getMakul()->getMakul()->getSksPraktek()+$krs->getMakul()->getMakul()->getSksLapangan(),
                'status'        => $krs->getStatus()
              );
            }
          }

        } else {

          if ( empty($request->get('nim')) ) {

            $this->response['error'] = "NPM harus diisi!";

          } else {

            if ( !empty($request->get('nim')) ) {
                
              $user = $em->getRepository('AppBundle:User')
                  ->findOneByUsername($request->get('nim'));
              
              if ( !$user ) {
                $this->response['error'] = 'Mahasiswa tidak ditemukan!';
              } else {

                if ( null === $user->getDataMahasiswa() ) {
                  $this->response['error'] = 'Mahasiswa tidak ditemukan!';
                } else {

                  /*------------*/
                  $mhs = $user->getDataMahasiswa();
                  $this->response['result']['mahasiswa'] = array(
                    'id'            => $mhs->getId(),
                    'id_user'       => $user->getId(),
                    'nama'          => $user->getNama(),
                    'nim'           => $user->getUsername(),
                    'prodi'         => $user->getProdi()->getJenjang() . ' ' . $user->getProdi()->getNamaProdi()
                  );
                  /*------------*/

                  if ( !empty($request->get('semester')) ) {


                        $dataKurikulum = $em->createQueryBuilder()
                          ->select('k')
                          ->from('AppBundle:Kurikulum', 'k')
                          ->where('k.prodi=:prodi and k.status!=:status_trash')
                          ->setParameters(array(
                            'prodi'         => $mhs->getUser()->getProdi(),
                            'status_trash'  => 'trash'
                          ))
                          ->getQuery()
                          ->getResult();

                        $dataMakul = $em->createQueryBuilder()
                          ->select('mk')
                          ->from('AppBundle:KurikulumMakul', 'mk')
                          ->where('mk.semester=:semester and mk.status!=:status_trash and mk.kurikulum in (:kurikulum)')
                          ->setParameters(array(
                            'semester'      => $request->get('semester'),
                            // 'kelas'     => $mhs->getKelas(),
                            'status_trash'  => 'trash',
                            'kurikulum'     => $dataKurikulum
                          ))
                          ->getQuery()
                          ->getResult();
                        if ( $dataMakul ) {
                            foreach ($dataMakul as $kr_makul) {
                              $dosen_pengampu = array();
                              if ( null !== $kr_makul->getDosenPengampu() && count($kr_makul->getDosenPengampu()) > 0 ) {
                                foreach ($kr_makul->getDosenPengampu() as $dp) {
                                  $dosen_pengampu[] = $dp->getDosen()->getUser()->getNama();
                                }
                                $makul = $kr_makul->getMakul();
                                if ( null !== $kr_makul->getKurikulum() && $kr_makul->getKurikulum()->getProdi() == $mhs->getUser()->getProdi() ) {
                                  $this->response['result']['paket_krs'][] = array(
                                    'id'            => $kr_makul->getId(),
                                    'id_mk'         => $makul->getId(),
                                    'kode_mk'       => $makul->getKode(),
                                    'nama_mk'       => $makul->getNama(),
                                    'sks_teori'     => $makul->getSksTeori(),
                                    'sks_praktek'   => $makul->getSksPraktek(),
                                    'sks_lapangan'  => $makul->getSksLapangan(),
                                    'jml_sks'       => $makul->getSksTeori()+$makul->getSksPraktek()+$makul->getSksLapangan(),
                                    'kelas'         => ( null !== $kr_makul->getKelas() ) ? $kr_makul->getKelas()->getNama() : '',
                                    'dosen_pengampu'=> $dosen_pengampu
                                  );
                                }
                              }

                            }
                        }
                      // }
                  }

                  $dataKrs = $em->getRepository('AppBundle:Krs')
                    ->findBy(array(
                      'mahasiswa' => $mhs,
                      'semester'  => $request->get('semester')
                    ));
                  if ( $dataKrs ) {
                    foreach ($dataKrs as $krs) {
                      $this->response['result']['krs_sudah_diambil'][] = array(
                        'id'            => $krs->getId(),
                        'semester'      => $krs->getSemester(),
                        'kode_mk'       => $krs->getMakul()->getMakul()->getKode(),
                        'nama_mk'       => $krs->getMakul()->getMakul()->getNama(),
                        'sks_teori'     => $krs->getMakul()->getMakul()->getSksTeori(),
                        'sks_praktek'   => $krs->getMakul()->getMakul()->getSksPraktek(),
                        'sks_lapangan'  => $krs->getMakul()->getMakul()->getSksLapangan(),
                        'jml_sks'       => $krs->getMakul()->getMakul()->getSksTeori()+$krs->getMakul()->getMakul()->getSksPraktek()+$krs->getMakul()->getMakul()->getSksLapangan(),
                        'status'        => $krs->getStatus()
                      );
                    }
                  }

                }

              }

            }
          }


        }
        /*------------*/
        if ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) {
            $template = 'krs_ambil_index_mahasiswa.html.twig';
        } else {
            $template = 'krs_ambil_index.html.twig';
        }
        /*------------*/
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            $param['data'] = $this->response;
            return $this->appService->load('akademika/krs/'.$template, $param);
        }
    }

    
    private function ambilKrsAction($request) 
    {
        $ids_makul = $request->get('id_makul');
        $em = $this->getDoctrine()->getManager();
        $ta = $this->appService->getTahunAkademik();

        if ( !empty( $request->get('nim') ) ) {
            $user = $em->getRepository('AppBundle:User')
                ->findOneByUsername( $request->get('nim') );
        } else {
            $user = $this->getUser();
        }

        if ( null !== $user->getDataMahasiswa() ) {

            $mhs = $user->getDataMahasiswa();

            foreach ($ids_makul as $id_makul) {
                
                $makul = $em->getRepository('AppBundle:KurikulumMakul')
                    ->find($id_makul);

                if ( $makul ) {
                    
                    $krs = $em->getRepository('AppBundle:Krs')
                        ->findOneBy(array(
                            'mahasiswa' => $mhs,
                            'semester'  => $makul->getSemester(),
                            'makul'     => $makul
                        ));

                    if ( !$krs ) {
                        $krs = new \AppBundle\Entity\Krs();
                        $krs->setMahasiswa($mhs);
                        $krs->setSemester($makul->getSemester());
                        $krs->setMakul($makul);
                    }
                    $krs->setStatus('proses');
                    $em->persist($krs);
                    $em->flush();

                }

            }

        }

        $this->addFlash('success', 'KRS berhasil diambil.');
        $redirectParam = array( 'semester' => $makul->getSemester() );
        if ( !empty( $request->get('nim') ) ) {
            $redirectParam['nim'] = $request->get('nim');
        }
        return $this->redirectToRoute('krs_ambil', $redirectParam);

    }

}
