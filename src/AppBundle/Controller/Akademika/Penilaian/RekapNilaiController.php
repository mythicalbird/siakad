<?php

namespace AppBundle\Controller\Akademika\Penilaian;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\AspekNilai;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class RekapNilaiController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }
  
    /**
     * @Route("/akademika/penilaian/rekap_nilai", name="rekap_nilai_index")
     */
    public function indexAction(Request $request)
    {
        // sama dengan aktifitas kuliah mahasiswa di forlap
        $params = array();
        if ( !empty( $request->get('semester') ) ) {
          if ( !empty( $request->get('angkatan') ) ) {
            $params['angkatan'] = $request->get('angkatan');
          }
          $dataMahasiswa = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
              ->findBy($params);
          foreach ($dataMahasiswa as $mhs) {
            if ( null !== $mhs->getUser() ) {
              $user = $mhs->getUser();
              if ( $user->getProdi() == $this->getUser()->getProdi() ) {
                $this->response['result'][] = array(
                  'id'          => $mhs->getId(),
                  'id_user'     => $user->getId(),
                  'nim'         => $user->getUsername(),
                  'nama'        => $user->getNama(),
                  'jk'          => $user->getJk(),
                  'ipk'         => $this->appService->getIpkMahasiswa($mhs, $request->get('semester'))
                );
              }
            }
          }          

        } else {

          $this->response['error'] = "Semester tidak boleh kosong";

        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/penilaian/rekap_nilai_index.html.twig', array(
              'data'  => $this->response
            ));
        }
    }
}
