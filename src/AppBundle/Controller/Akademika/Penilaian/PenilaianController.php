<?php

namespace AppBundle\Controller\Akademika\Penilaian;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\AspekNilai;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class PenilaianController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/penilaian/kartu_ujian", name="kartu_ujian_index")
     */
    public function indexAction(Request $request, $params = array())
    {
        if ( !empty($request->get('semester')) ) {
            $params['semester'] = $request->get('semester');
        }
        if ( !empty($request->get('kelas')) ) {
          $kelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
            ->find($request->get('kelas'));
          if ($kelas) {
            $params['kelas'] = $kelas;
          }
        }

        $em = $this->getDoctrine()->getManager();
        $dataMahasiswa = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
          ->findBy($params);

        $options = $this->appService->getGlobalOption();
        $dataKelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
            ->findByProdi($this->getUser()->getProdi());
        return $this->appService->load('akademika/penilaian/kartu_ujian_index.html.twig', array(
        	'data'	=> $dataMahasiswa,
          'dataKelas' => $dataKelas,
        ));
    }

    /**
     * @Route("/akademika/penilaian/kartu_ujian/cetak", name="kartu_ujian_cetak")
     * @Method({"POST"})
     */
    public function cetakAction(Request $request)
    {
        $data = ( !empty($request->get('nim')) ) ? $request->get('nim') : array();
        return $this->appService->load('akademika/penilaian/kartu_ujian_cetak.html.twig', array(
          'data'  => $data
        ));
    }
    
    /**
     * @Route("/akademika/penilaian/nilai/tugas_akhir", name="nilai_tugas_akhir_index")
     */
    public function nilaiTugasAkhirIndexAction(Request $request)
    {
    	  $data = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
            ->findAll();
        return $this->appService->load('akademika/penilaian/tugas_akhir_nilai.html.twig', array(
        	'data'	=> $data
        ));
    }

}
