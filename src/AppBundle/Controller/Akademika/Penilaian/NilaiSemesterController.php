<?php

namespace AppBundle\Controller\Akademika\Penilaian;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\AspekNilai;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class NilaiSemesterController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }
  
    /**
     * @Route("/akademika/penilaian/nilai_semester", name="nilai_semester_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* Data Select */
        $params = array(
          'dataKurikulum'   => $em->getRepository('AppBundle:Kurikulum')
              ->findByProdi($this->getUser()->getProdi()),
          'dataAspekNilai'  => $this->getDoctrine()->getRepository('AppBundle:AspekNilai')
              ->findByTahun($this->appService->getTahunAkademik()),
          'dataBobotNilai'  => $this->getDoctrine()->getRepository('AppBundle:BobotNilai')
              ->findAll()
        );        
        /* Data Select */

        $args = array(
          'semester'    => ( !empty($request->get('semester')) ) ? $request->get('semester') : '',
          'kurikulum'   => ( !empty($request->get('kurikulum')) ) ? $request->get('kurikulum') : '',
          'makul'       => ( !empty($request->get('makul')) ) ? $request->get('makul') : '',
          'kelas'       => ( !empty($request->get('kelas')) ) ? $request->get('kelas') : '' 
        );

        // $options = $this->appService->getGlobalOption();
        $ta = $this->appService->getTahunAkademik();

        if ( !empty( $args['semester'] ) && !empty( $args['kurikulum'] ) ) {

          $kurikulum = $em->getRepository('AppBundle:Kurikulum')
            ->find( $args['kurikulum'] );
					
          if ( !$kurikulum ) {
            
            $this->response['error'] = "Kurikulum tidak ditemukan!";

          } else {
						
						$params['kurikulum'] = $kurikulum;
            $paramListMakul = array();

            if ( !empty($request->get('makul')) ) {
              $kurikulum_makul = $em->getRepository('AppBundle:KurikulumMakul')
                ->find( $request->get('makul') );
              $makul = $kurikulum_makul->getMakul();
              $params['makul'] = $makul;

              $paramListMakul = array(
                'kurikulum'   => $kurikulum
              );

            }

            $dataMakulChoices = array();
            // $dataMakul = $em->getRepository('AppBundle:KurikulumMakul')
            //   ->findBy($paramListMakul);
            $dataMakul = $kurikulum->getMakuls();
            $unset = array();
            foreach ($dataMakul as $choice_makul) {
              if ( !in_array($choice_makul->getMakul()->getKode(), $unset) ) {
                $dataMakulChoices[] = $choice_makul;
                // array_push($unset, $choice_makul->getMakul()->getKode());
              }
            }
            $params['dataMakul'] = $dataMakulChoices;

            if ( !empty($request->get('makul')) ) {
              // if ( !empty($request->get('kelas')) ) {
              //   $kelas = $em->getRepository('AppBundle:Kelas')
              //     ->find($request->get('kelas'));
              //   if ( $kelas ) {
              //     $paramListMakul['kelas'] = $kelas;
              //   }
              // }
              // $params['dataKelas'] = $em->getRepository('AppBundle:Kelas')
              //   ->findAll();
              $dataKelas = array();
              $unset = array();
              foreach ($dataMakul as $mk_kls) {
                if ( null !== $mk_kls->getKelas() ) {
                  if ( !in_array($mk_kls->getKelas()->getId(), $unset) ) {
                    $dataKelas[] = array(
                      'id'    => $mk_kls->getKelas()->getId(),
                      'nama'  => $mk_kls->getKelas()->getNama()
                    );
                    $unset[] = $mk_kls->getKelas()->getId();
                  }
                }
              }
              $params['dataKelas'] = $dataKelas;

              // $dataNilaiKrs = $em->createQueryBuilder()
              //   ->select('k')
              //   ->from('AppBundle:Krs', 'k')
              //   ->where('k.makul=:makul and k.semester=:semester and k.status!=:trash')
              //   ->setParameters(array(
              //     'makul'     => $kurikulum_makul,
              //     'semester'  => $args['semester'],
              //     'trash'     => 'trash'
              //   ))
              //   ->getQuery()
              //   ->getResult();
              $dataNilaiKrs = $kurikulum_makul->getKrs();
              foreach ($dataNilaiKrs as $krs) {
                if ( null !== $krs->getMahasiswa() ) {
                  $mhs = $krs->getMahasiswa();
                  if ( null !== $mhs->getUser() ) {
                    $result = array(
                      'id'          => $krs->getId(),
                      'id_mhs'      => $mhs->getId(),
                      'id_user'     => $mhs->getUser()->getId(),
                      'nim'         => $mhs->getUser()->getUsername(),
                      'nama'        => $mhs->getUser()->getNama(),
                      'jk'          => $mhs->getUser()->getJk(),
                      'nilai_aspek' => array(),
                      'nilai_huruf' => $krs->getNilaiHuruf(), 
                      'nilai_angka' => ( $krs->getNilaiAngka() > 0 ) ? $krs->getNilaiAngka() : 0.00,
                      'lulus'       => null
                    );

                    foreach ($params['dataAspekNilai'] as $aspek) {
                      $nilai = $this->appService->getKrsNilaiAspek($krs, $aspek);
                      $nilai_akhir = $this->appService->getKrsNilaiAspek($krs, $aspek, true);
                      $result['nilai_aspek'][] = array(
                        'id'              => $aspek->getId(),
                        'id_np'           => $this->appService->getKrsNilaiAspek($krs, $aspek, false, true),
                        'nama_aspek'      => $aspek->getNama(),
                        'persentase'      => $aspek->getPersen(),
                        'nilai'           => $nilai,
                        'nilai_akhir'     => $nilai_akhir, // nilai setelah dipersentase
                      );
                    }

                    // $result['nilai_huruf'] = $this->appService->getBobotNilai($nilai_final, true);
                    // $result['nilai_angka'] = $this->appService->getBobotNilai($nilai_final);
                    // $result['lulus'] = $this->appService->getBobotNilai($nilai_final, false, true);

                    $this->response['result'][] = $result;
                  }
                }
              }

              // $params['dataMakul'] = $em->getRepository('AppBundle:KurikulumMakul')
              //   ->findByKurikulum($kurikulum);

              // if ( !empty($args['makul']) ) {

              //   $kurikulum_makul = $em->getRepository('AppBundle:KurikulumMakul')
              //     ->find( $request->get('makul') );
              //   $makul = $kurikulum_makul->getMakul();
              //   $params['makul'] = $makul;

              //   // data kelas dari dosen pengampu
              //   $params['dataKelas'] = $em->getRepository('AppBundle:DosenPengampu')
              //     ->findByMakul($makul);

              //   if ( !empty($args['kelas']) ) {

              //     $kelas = $em->getRepository('AppBundle:Kelas')
              //       ->find( $request->get('kelas') );

              //     $dataMahasiswa = $em->getRepository('AppBundle:User')
              //       ->findBy(array(
              //         'prodi'     => $this->getUser()->getProdi(),
              //         'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4)
              //       ));

              //     foreach ($dataMahasiswa as $user) {

              //       if ( null !== $user->getDataMahasiswa() ) {

              //         $mhs = $user->getDataMahasiswa();

              //         //ambil kelas -> dosen pengampu -> makul
              //         if ( null !== $mhs->getUser() ) {

              //           // if ( $mhs->getKelas() == $kelas ) {

              //             $nilai_final = 0;
              //             $result = array(
              //               'id'          => $mhs->getId(),
              //               'id_user'     => $user->getId(),
              //               'nim'         => $user->getUsername(),
              //               'nama'        => $user->getNama(),
              //               'jk'          => $user->getJk(),
              //               'nilai_aspek' => array(),
              //               'nilai_huruf' => '', 
              //               'nilai_angka' => 0,
              //               'lulus'       => null
              //             );

              //             $aspekNilai = $this->getDoctrine()->getRepository('AppBundle:AspekNilai')->findAll();
              //             foreach ($aspekNilai as $aspek) {
              //               $np = $this->appService->getNilaiPerkuliahan($mhs, $makul, $aspek);
              //               $result['nilai_aspek'][] = array(
              //                 'id'              => $aspek->getId(),
              //                 'nama_aspek'      => $aspek->getNama(),
              //                 'persentase'      => $aspek->getPersen(),
              //                 'nilai'           => $np[0],
              //                 'nilai_akhir'     => $np[1], // nilai setelah dipersentase
              //               );
              //               $nilai_final += $np[1];
              //             }

              //             $result['nilai_huruf'] = $this->appService->getBobotNilai($nilai_final, true);
              //             $result['nilai_angka'] = $this->appService->getBobotNilai($nilai_final);
              //             $result['lulus'] = $this->appService->getBobotNilai($nilai_final, false, true);

              //             $this->response['result'][] = $result;
              //           // }
              //         }

              //         // if ( null !== $user->getDataMahasiswa() ) {
              //         //   $mhs = $user->getDataMahasiswa();

              //         //   $nilai = $em->getRepository('AppBundle:Nilai')
              //         //     ->findOneBy(array(
              //         //       'mahasiswa' => $mhs,
              //         //       'makul'     => $kurikulum_makul,
              //         //       'semester'  => $request->get('semester'),
              //         //       'kelas'     => $kelas
              //         //     ));
              //         //   if ( !$nilai ) {
              //         //     $nilai = new \AppBundle\Entity\Nilai();
              //         //     $nilai->setMahasiswa($mhs);
              //         //     $nilai->setMakul($kurikulum_makul);
              //         //     $nilai->setSemester($request->get('semester'));
              //         //     $nilai->setKelas($kelas);
              //         //     $em->persist($nilai);
              //         //     $em->flush();
              //         //   }

              //         //   $aspekNilai = $this->getDoctrine()->getRepository('AppBundle:AspekNilai')->findAll();
              //         //   $nilai_aspek = array();
              //         //   $nilai_final = 0;
              //         //   foreach ($aspekNilai as $aspek) {
              //         //     $np = $this->appService->getNilaiPerkuliahan($mhs, $makul, $aspek);
              //         //     $nilai_aspek[] = array(
              //         //       'id'              => $aspek->getId(),
              //         //       'nama_aspek'      => $aspek->getNama(),
              //         //       'persentase'      => $aspek->getPersen(),
              //         //       'nilai'           => $np[0],
              //         //       'nilai_akhir'     => $np[1], // nilai setelah dipersentase
              //         //     );
              //         //     $nilai_final += $np[1];
              //         //   }

              //         //   $result = array(
              //         //     'id'          => $mhs->getId(),
              //         //     'id_user'     => $user->getId(),
              //         //     'id_nilai'    => $nilai->getId(),
              //         //     'nim'         => $user->getUsername(),
              //         //     'nama'        => $user->getNama(),
              //         //     'jk'          => $user->getJk(),
              //         //     'nilai_aspek' => $nilai_aspek,
              //         //     'nilai_huruf' => $nilai->getNilaiHuruf(), 
              //         //     'nilai_angka' => $nilai->getNilaiAngka(),
              //         //     'lulus'       => null
              //         //   );

              //         //   $this->response['result'][] = $result;

              //         // }

              //       }

              //     } 
              //   } 

              // } else {

              //   $this->response['error'] = "Harap pilih mata kuliah!";

              // }
            }
            //
          }

        } else {

          $this->response['error'] = "Semester/Kurikulum tidak boleh kosong!";

        }


        /*----------------*/
        if ($this->get('security.authorization_checker')->isGranted('ROLE_DOSEN')) {
          $template = 'nilai_semester_dosen_index';
        } else {
          $template = 'nilai_semester_index';
        }
        /*----------------*/

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            $params['data'] = $this->response;
            return $this->appService->load( 'akademika/penilaian/'.$template.'.html.twig', $params );
        }
    }

    /**
     * @Route("/akademika/penilaian/nilai/semester/action", name="nilai_tugas_akhir_input_action")
     */
    public function inputNilaiTugasAkhirAction(Request $request)
    {
        if ( !empty($request->get('nim')) ) {
          $nim = $request->get('nim');
          $nilai = $request->get('nilai');
          $em = $this->getDoctrine()->getManager();
          if ( is_array($nim) && count($nim) > 0 ) {
            for ($i=0; $i < count($nim); $i++) { 
              $user = $this->getDoctrine()->getRepository('AppBundle:User')
                  ->findOneByUsername($nim[$i]);
              $makul = $this->getDoctrine()->getRepository('AppBundle:Makul')
                  ->find($request->get('makul'));
              if ( $user && $makul ) {
                $mhs = $user->getDataMahasiswa();
                $krs = $this->getDoctrine()->getRepository('AppBundle:Krs')
                  ->findOneBy(array(
                      'mahasiswa' => $mhs,
                      'makul'     => $makul
                  ));
                if ( $krs ) {
                  $krs->setNilaiAkhir($nilai[$i]);
                  $em->persist($krs);
                  $em->flush();
                }
              }
            }
          }
          $this->addFlash('success', 'Data nilai akhir berhasil disimpan.');
          return $this->redirectToRoute('nilai_semester_index', array(
              'semester'  => $request->get('semester'),
              'kurikulum'  => $request->get('kurikulum'),
              'makul'  => $request->get('makul'),
              'kelas'  => $request->get('kelas'),
          ));
        }
    }

    /**
     * @Route("/_ajax/edit/nilai_aspek", name="_edit_nilai_aspek")
     * @Method({"POST"})
     */
    public function ajaxUpdateNilaiAction(Request $request)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $np = $em->getRepository('AppBundle:NilaiPerkuliahan')
          ->find($request->get('pk'));
        if ( $np ) {
          $np->setNilai($request->get('value'));
          $em->persist($np);
          $em->flush();          
          $response->setData(array(
            'id'          => $np->getId(),
            'mhs'         => $np->getKrs()->getMahasiswa()->getId(),
            'nilai'       => $np->getNilai(),
            'nilai_final' => '('.(($np->getNilai()*$np->getAspekNilai()->getPersen())/100).')',
            'persentase'  => $np->getAspekNilai()->getPersen(),
            'success'     => 1
          ));
        }
        return $response;
    }

    /**
     * @Route("/akademika/penilaian/nilai_semester/_ajax/krs_update_nilai/{type}", name="nilai_semester_krs_update_nilai")
     * @Method({"POST"})
     */
    public function ajaxKrsUpdateNilaiAction(Request $request, $type = 'angka')
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $krs = $em->getRepository('AppBundle:Krs')
          ->find($request->get('pk'));
        if ( $krs ) {
          if ( $type == 'huruf' ) {
            $krs->setNilaiHuruf( $request->get('value') );
          } elseif ( $type == 'angka' ) {
            $krs->setNilaiAngka( $request->get('value') );
          }

          // auto angka 
          $angka = 0.00;
          if ( in_array( $request->get('value'), ['A','B','C','D','E','T'] ) ) {
            $bobot = $em->getRepository('AppBundle:BobotNilai')
              ->findOneByPredikat($request->get('value'));
            if ( $bobot ) {
              $angka = $bobot->getBobot();
              $krs->setNilaiAngka($angka);
            }
          }

          $em->persist($krs);
          $em->flush();

          $response->setData(array(
            'success'     => 1,
            'krs_id'      => $krs->getId(),
            'angka'       => $angka,
          ));
        }
        return $response;
    }

}
