<?php

namespace AppBundle\Controller\Grafik;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\AppService;

class PmbController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/grafik/pmb/prodi", name="grafik_pmb_prodi")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Program Studi Mahasiswa Baru',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );
        $prodiData = $em->getRepository('AppBundle:ProgramStudi')
          ->findAll();
        for ( $i = 0; $i < count($prodiData); $i++ ) {
            $count = 0;
            $dataMahasiswaUser = $em->getRepository('AppBundle:User')
                ->findBy(array(
                  'prodi'     => $prodiData[$i],
                  'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4)
                ));
            foreach ($dataMahasiswaUser as $user) {
                if ( null !== $user->getDataMahasiswa() && $user->getDataMahasiswa()->getMaba() === 1 ) {
                    $count++;
                }
            }

            $this->response['result']['rows'][] = array( ucwords($prodiData[$i]->getNamaProdi()), $count );
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/pmb/prodi.html.twig', array(
                'data'  => $this->response
            ));
        }
    }

    /**
     * @Route("/grafik/pmb/angkatan", name="grafik_pmb_angkatan")
     */
    public function angkatanAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Data Angkatan',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );

        for ( $i = 2000; $i < date('Y')+3; $i++ ) {
            $tahun = $i;
            $count = 0;
            $dataMahasiswa = $em->getRepository('AppBundle:Mahasiswa')
                ->findBy(array(
                  'angkatan'    => $tahun,
                  'status'      => $this->appService->getMasterTermObject('status_mahasiswa', 'A')
                ));
            if ( count($dataMahasiswa) > 0 ) {
                foreach ($dataMahasiswa as $mhs) {
                    $count++;
                }
                $this->response['result']['rows'][] = array( (string)$tahun, $count );
            }
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/pmb/angkatan.html.twig', array(
                'data'  => $this->response
            ));
        }
    }

    /**
     * @Route("/grafik/pmb/agama", name="grafik_pmb_agama")
     */
    public function agamaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Data Agama',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );

        $agama = $em->createQueryBuilder()
            ->select('m')
            ->from('AppBundle:Master', 'm')
            ->where('m.type = :type AND m.kode != :kode')
            ->setParameters(array(
                'type'  => 'agama',
                'kode'  => '98'
            ))
            ->getQuery()
            ->getResult();
        for ( $i = 0; $i < count($agama); $i++ ) {
            $count = 0;
            $dataMahasiswaUser = $em->getRepository('AppBundle:User')
                ->findBy(array(
                  // 'prodi'     => $prodiData[$i],
                  'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4),
                  'agama'  => $this->appService->getMasterTermObject('agama', $agama[$i]->getKode())
                ));
            foreach ($dataMahasiswaUser as $user) {
                if ( null !== $user->getDataMahasiswa() && $user->getDataMahasiswa()->getStatus() == $this->appService->getMasterTermObject('status_mahasiswa', 'A') ) {
                    $count++;
                }
            }

            $this->response['result']['rows'][] = array( $agama[$i]->getNama(), $count );
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/pmb/agama.html.twig', array(
                'data'  => $this->response
            ));
        }
    }

    /**
     * @Route("/grafik/pmb/jk", name="grafik_pmb_jk")
     */
    public function jkAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Data Jenis Kelamin',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );

        $data = array('L', 'P');
        $params = array(
            'maba'        => 0,
            'status'      => $this->appService->getMasterTermObject('status_mahasiswa', 'A'),
            // 'angkatan'    => $result['query']['tahun']
        );
        for ( $i = 0; $i < count($data); $i++ ) {
            $count = 0;
            $mhsData = $em->getRepository('AppBundle:Mahasiswa')->findBy($params);
            if ( $mhsData ) {
                foreach ($mhsData as $mhs) {
                    if ( null !== $mhs->getUser() && $mhs->getUser()->getJk() == $data[$i] ) {
                        $count++;
                    }
                }
            }
            $label = ( $data[$i] == 'L' ) ? 'Laki-laki' : 'Perempuan';
            $this->response['result']['rows'][] = array( $label, $count );
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/pmb/jk.html.twig', array(
                'data'  => $this->response
            ));
        }
    }

}
