<?php

namespace AppBundle\Controller\Grafik;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Service\AppService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MahasiswaController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/grafik/mahasiswa/prodi", name="grafik_mahasiswa_prodi")
     */
    public function prodiAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Program Studi Mahasiswa',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );
        $prodiData = $em->getRepository('AppBundle:ProgramStudi')
          ->findAll();
        for ( $i = 0; $i < count($prodiData); $i++ ) {
            $count = 0;
            // $dataMahasiswaUser = $em->createQueryBuilder()
            //     ->select('u')
            //     ->from('AppBundle:User', 'u')
            //     ->where('u.prodi=:prodi and u.hakAkses')
            $dataMahasiswaUser = $em->getRepository('AppBundle:User')
                ->findBy(array(
                  'prodi'     => $prodiData[$i],
                  'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4)
                ));
            foreach ($dataMahasiswaUser as $user) {
                if ( null !== $user->getDataMahasiswa() && $user->getDataMahasiswa()->getStatus() == $this->appService->getMasterTermObject('status_mahasiswa', 'A') ) {
                    $count++;
                }
            }

            $this->response['result']['rows'][] = array( ucwords($prodiData[$i]->getNamaProdi()), $count );
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/mahasiswa/prodi.html.twig', array(
                'data'  => $this->response
            ));
        }
    }

    /**
     * @Route("/grafik/mahasiswa/angkatan", name="grafik_mahasiswa_angkatan")
     */
    public function angkatanAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Data Angkatan',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );

        for ( $i = 2000; $i < date('Y')+3; $i++ ) {
            $tahun = $i;
            $count = 0;
            $dataMahasiswa = $em->getRepository('AppBundle:Mahasiswa')
                ->findBy(array(
                  'angkatan'    => $tahun,
                  'status'      => $this->appService->getMasterTermObject('status_mahasiswa', 'A')
                ));
            if ( count($dataMahasiswa) > 0 ) {
                foreach ($dataMahasiswa as $mhs) {
                    $count++;
                }
                $this->response['result']['rows'][] = array( (string)$tahun, $count );
            }
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/mahasiswa/angkatan.html.twig', array(
                'data'  => $this->response
            ));
        }
    }

    /**
     * @Route("/grafik/mahasiswa/agama", name="grafik_mahasiswa_agama")
     */
    public function agamaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Data Agama',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );

        $agama = $em->createQueryBuilder()
            ->select('m')
            ->from('AppBundle:Master', 'm')
            ->where('m.type = :type AND m.kode != :kode')
            ->setParameters(array(
                'type'  => 'agama',
                'kode'  => '98'
            ))
            ->getQuery()
            ->getResult();
        for ( $i = 0; $i < count($agama); $i++ ) {
            $count = 0;
            $dataMahasiswaUser = $em->getRepository('AppBundle:User')
                ->findBy(array(
                  // 'prodi'     => $prodiData[$i],
                  'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4),
                  'agama'  => $this->appService->getMasterTermObject('agama', $agama[$i]->getKode())
                ));
            foreach ($dataMahasiswaUser as $user) {
                if ( null !== $user->getDataMahasiswa() && $user->getDataMahasiswa()->getStatus() == $this->appService->getMasterTermObject('status_mahasiswa', 'A') ) {
                    $count++;
                }
            }

            $this->response['result']['rows'][] = array( $agama[$i]->getNama(), $count );
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/mahasiswa/agama.html.twig', array(
                'data'  => $this->response
            ));
        }
    }

    /**
     * @Route("/grafik/mahasiswa/jk", name="grafik_mahasiswa_jk")
     */
    public function jkAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Data Jenis Kelamin',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );

        $data = array('L', 'P');
        $params = array(
            'maba'        => 0,
            'status'      => $this->appService->getMasterTermObject('status_mahasiswa', 'A'),
            // 'angkatan'    => $result['query']['tahun']
        );
        for ( $i = 0; $i < count($data); $i++ ) {
            $count = 0;
            $mhsData = $em->getRepository('AppBundle:Mahasiswa')->findBy($params);
            if ( $mhsData ) {
                foreach ($mhsData as $mhs) {
                    if ( null !== $mhs->getUser() && $mhs->getUser()->getJk() == $data[$i] ) {
                        $count++;
                    }
                }
            }
            $label = ( $data[$i] == 'L' ) ? 'Laki-laki' : 'Perempuan';
            $this->response['result']['rows'][] = array( $label, $count );
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/mahasiswa/jk.html.twig', array(
                'data'  => $this->response
            ));
        }
    }

    /**
     * @Route("/grafik/mahasiswa/asal_daerah", name="grafik_mahasiswa_asal_daerah")
     */
    public function asalDaerahIndexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Data Jenis Kelamin',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );

        $dataMahasiswa = $em->getRepository('AppBundle:Mahasiswa')
            ->findBy(array(
                'status'  => $this->appService->getMasterTermObject('status_mahasiswa', 'A')
            ));
        $mhs_wilayah = array();
        foreach ($dataMahasiswa as $mhs) {
            if ( null !== $mhs->getUser() && null !== $mhs->getUser()->getWilayah() ) {
                $count = 0;
                $wilayah = $mhs->getUser()->getWilayah();
                if ( !in_array($wilayah->getId(), $mhs_wilayah) ) {

                    
                    if ( null !== $wilayah->getParent() ) {
                        
                        $label = ( null !== $wilayah->getParent()->getParent() ) ? $wilayah->getParent()->getParent()->getNama() : $wilayah->getParent()->getNama();

                    } else {

                        $label = $wilayah->getNama();

                    }

                    $count++;

                    $this->response['result']['rows'][] = array( $label, $count );

                    array_push($mhs_wilayah, $wilayah->getId());

                }
            }
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/mahasiswa/jk.html.twig', array(
                'data'  => $this->response
            ));
        }
    }
}
