<?php

namespace AppBundle\Twig;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class AppExtension extends \Twig_Extension
{

    protected $em;
    protected $generator;
    protected $router;
    protected $uploader;

    public function __construct(EntityManager $entityManager, UrlGeneratorInterface $generator, Router $router, UploaderHelper $uploader)
    {
        $this->em = $entityManager;
        $this->generator = $generator;
        $this->router = $router;
        $this->uploader = $uploader;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('json_decode', array($this, 'jsonDecode')),
            new \Twig_SimpleFilter('tanggal', array($this, 'formatTanggal')),
            new \Twig_SimpleFilter('bulan', array($this, 'formatBulan')),
        );
    }

    public function formatBulan($nama_bulan)
    {
        switch ($nama_bulan) {
            case 'January':
                $bulan = 'Januari';
                break;
            case 'February':
                $bulan = 'February';
                break;
            case 'March':
                $bulan = 'Maret';
                break;
            case 'May':
                $bulan = 'Mei';
                break;
            case 'July':
                $bulan = 'Juli';
                break;
            case 'August':
                $bulan = 'Agustus';
                break;
        }
        return $nama_bulan;
    }

    public function formatTanggal($tglObj, $format = 'Y-m-d')
    {
        if ( $tglObj == "now" ) {
            $tglObj = new\DateTime("now");
        }
        if (is_a($tglObj, 'DateTime')) {
            $tgl = $tglObj->format('d');
            $bulan = $tglObj->format('F');
            $tahun = $tglObj->format('Y');
            switch ($bulan) {
                case 'January':
                    $bulan = 'Januari';
                    break;
                case 'February':
                    $bulan = 'February';
                    break;
                case 'March':
                    $bulan = 'Maret';
                    break;
                case 'May':
                    $bulan = 'Mei';
                    break;
                case 'July':
                    $bulan = 'Juli';
                    break;
                case 'August':
                    $bulan = 'Agustus';
                    break;
            }
            return $tgl . ' ' . $bulan . ' ' . $tahun;
        } else {
            return '';
        }
    }
  
    public function jsonDecode($str) {
        return json_decode($str, true);
    }

    public function getGlobals()
    {
        $prodiList = $this->em->getRepository('AppBundle:ProgramStudi')
          ->findAll();
        $modules = $this->em->getRepository('AppBundle:Module')
          ->findAll();
        return array(
            'prodiList'     => $prodiList,
            'modules'       => $modules,
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'gravatar', function($email, $atts = array(), $img = true, $s = 80, $d = 'mm', $r = 'g') {
              $url = 'https://www.gravatar.com/avatar/';
              $url .= md5( strtolower( trim( $email ) ) );
              $url .= "?s=$s&d=$d&r=$r";
              if ( $img ) {
                  $url = '<img src="' . $url . '"';
                  foreach ( $atts as $key => $val )
                      $url .= ' ' . $key . '="' . $val . '"';
                  $url .= ' />';
              }
              return $url;
            }),
            new \Twig_SimpleFunction( 'avatar', function($user = null, $atts = array(), $img = true, $s = 80, $d = 'mm', $r = 'g') {
                $url = '';
                if ( null !== $user ) {

                    if ( null !== $user->getImageName() ) {

                        $url = $this->uploader->asset($user, 'imageFile');
                        $url = '<img src="' . $url . '"';
                        foreach ( $atts as $key => $val )
                            $url .= ' ' . $key . '="' . $val . '"';
                            $url .= ' />';

                    } else {

                        $url = 'https://www.gravatar.com/avatar/';
                        $url .= md5( strtolower( trim( $user->getEmail() ) ) );
                        $url .= "?s=$s&d=$d&r=$r";
                        if ( $img ) {
                            $url = '<img src="' . $url . '"';
                            foreach ( $atts as $key => $val )
                                $url .= ' ' . $key . '="' . $val . '"';
                                $url .= ' />';
                        }

                    }

                }
                return $url;
            }),
            new \Twig_SimpleFunction( 'price', function($amount) {
                return 'Rp ' . number_format($amount, 0, ',', '.');
            }),
            new \Twig_SimpleFunction( 'setting', function($name) {
                $setting = $this->em->getRepository('AppBundle:Setting')
                    ->findOneByName($name);
                return $setting;
            }),
            new \Twig_SimpleFunction( 'hitungPresensiDosen', function( $user, $ket, $ta = null ){
                $param = array(
                    'user'  => $user,
                    'ket'   => strtolower($ket)
                );
                if ( null !== $ta ) {
                    $param['tahunAkademik'] = $ta;
                } else {
                    $ta = $this->em->getRepository('AppBundle:TahunAkademik')
                        ->findOneByAktif(1);
                    if ( $ta ) {
                        $param['tahunAkademik'] = $ta;
                    }
                }
                $presensi = $this->em->getRepository('AppBundle:PresensiDosen')
                    ->findBy($param);
                $jumlah = count($presensi);
                return ($jumlah >  0) ? $jumlah : '-';
            }),
            new \Twig_SimpleFunction( 'hitungPresensiMahasiswa', function( $mhs, $makul, $ket ){
                $params = array(
                    'makul'         => $makul,
                    'mahasiswa'     => $mhs,
                    'ket'           => strtolower($ket)
                );
                $presensi = $this->em->getRepository('AppBundle:PresensiMahasiswa')
                    ->findBy($params);
                $jumlah = count($presensi);
                return ($jumlah >  0) ? $jumlah : '-';
            }),
            new \Twig_SimpleFunction( 'getBobotNilai', function($nilai, $prodi) {
                return $this->getBobotNilai($nilai, $prodi);
            }),
            new \Twig_SimpleFunction( 'print_sidebar_menus', function($_route) {
                return $this->printMenus($_route);
            }),

            new \Twig_SimpleFunction( 'printIps', function($mahasiswa = null, $semester = null) {
                return $this->getIps($mahasiswa, $semester);
            }),
            new \Twig_SimpleFunction( 'printIpk', function($mahasiswa) {
                $ipk = 0;
                $totalNilaiIps = 0;
                $jmlKhs = 0;
                if ( null !== $mahasiswa ) {

                    $dataKhs = $this->em->getRepository('AppBundle:Khs')
                        ->findByMahasiswa($mahasiswa);
                    if ( $dataKhs && null !== $mahasiswa->getSemester() ) {
                        foreach ($dataKhs as $khs) {

                            $nilaiIps = $this->getIps($mahasiswa, $khs->getSemester());
                            if ( $nilaiIps > 0 ) {
                                $totalNilaiIps += $nilaiIps;
                            }

                        }
                        $ipk = $totalNilaiIps/$mahasiswa->getSemester()->getSemester();
                    }
                }
                return $ipk;
            }),
            new \Twig_SimpleFunction( 'printBatasStudi', function($id_mhs){
                $mhs = $this->em->getRepository('AppBundle:Mahasiswa')
                    ->find($id_mhs);
                if ( null !== $mhs->getUser() && null !== $mhs->getUser() && null !== $mhs->getUser()->getProdi() ) {
                    $prodi = $mhs->getUser()->getProdi();
                    if ( null !== $prodi->getBatasSesi() ) {
                        $batasSesi = (int) $prodi->getBatasSesi();
                        $batasSesi = (int) $batasSesi/$prodi->getJumlahSesi();
                        if ( !empty($mhs->getAngkatan() && $batasSesi > 0 ) ) {
                            return $mhs->getAngkatan()+$batasSesi;
                        }
                    }
                }
                return '';
            }),
            new \Twig_SimpleFunction( 'printBiayaKuliahDibayar', function($biayaKuliah) {
                $transaksi = $this->em->getRepository('AppBundle:PembayaranTransaksi')
                    ->findByBiayaKuliah($biayaKuliah);
                $total = 0;
                if ( $transaksi ) {
                    foreach ($transaksi as $trx) {
                        $total += $trx->getJumlah();
                    }
                }
                return $total;
            }),
            new \Twig_SimpleFunction( 'dateRange', function($daterange, $key = null){
                $result = array(
                    'mulai' => '',
                    'akhir' => ''
                );
                if ( $daterange != '' ) {
                    $array = explode(" s/d ", $daterange);
                    if ( count($array) > 0 ) {
                        if (isset($array[0])) {
                            $dateMulai = explode('-', $array[0]);
                            $dateMulai = $dateMulai[2] . '-' . $dateMulai[1] . '-' . $dateMulai[0];
                            $result['mulai'] = new\DateTime($dateMulai);
                        }
                        if (isset($array[1])) {
                            $dateAkhir = explode('-', $array[1]);
                            $dateAkhir = $dateAkhir[2] . '-' . $dateAkhir[1] . '-' . $dateAkhir[0];
                            $result['akhir'] = new\DateTime($dateAkhir);
                        }
                    }
                }
                if ( $key === null ) {
                    return $result;
                }
                return ( isset($result[$key]) ) ? $result[$key] : '';
            }),
            new \Twig_SimpleFunction( 'dateFormat', function($date, $format = 'd-m-Y'){
                return date($format, strtotime($date));
            }),
            new \Twig_SimpleFunction( 'gender', function($jk){
                if ( strtoupper($jk)  == "L" ) {
                    $gender = 'Laki-laki';
                } else {
                    $gender = 'Perempuan';
                }
                return $gender;
            }),
            new \Twig_SimpleFunction( 'getBatasAngsuran', function($semester, $angsuran){
                $btsAng = $this->em->getRepository('AppBundle:BatasAngsuran')
                    ->findOneBy(array(
                        'semester'  => $semester,
                        'angsuran'  => $angsuran
                    ));
                return ( $btsAng ) ? $btsAng->getTanggal() : '-';
            }),
            new \Twig_SimpleFunction( 'getUser', function($username){
                $u = $this->em->createQueryBuilder()
                    ->select('u')
                    ->from('AppBundle:User', 'u')
                    ->where('u.username= :username')
                    ->setParameter('username', $username)
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult();
                return $u;
            }),
            new \Twig_SimpleFunction( 'getMahasiswa', function($nim, $field){
                $mhs = $this->em->createQueryBuilder()
                    ->select('u')
                    ->from('AppBundle:User', 'u')
                    ->where('u.username= :nim')
                    ->setParameter('nim', $nim)
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                if ($mhs && isset($mhs[$field]) ) {
                    $result = $mhs[$field];
                } else {
                    $result = '';
                }
                return $result;
            }),
            new \Twig_SimpleFunction( 'getDosenPengampu', function($makul, $kelas = ''){
                // makul = objct
                // kelas = "string"
                $data = $this->em->createQueryBuilder()
                    ->select('q')
                    ->from('AppBundle:DosenPengampu', 'q')
                    ->where('q.makul=:makul AND q.kelas=:kelas')
                    ->setParameter('makul', $makul)
                    ->setParameter('kelas', $kelas)
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult();
                if ( ! $data ) {
                    $data = new \AppBundle\Entity\DosenPengampu();
                    $data->setMakul($makul);
                    $data->setKelas($kelas);
                    $data->setDosen(null);
                    $this->em->persist($data);
                    $this->em->flush();
                }
                return ( $data && null !== $data->getDosen() && null !== $data->getDosen()->getUser() ) ? $data->getDosen()->getUser()->getNama() : '';
            }),    
            new \Twig_SimpleFunction( 'getJadwalKuliah', function($ta, $semester, $hari, $jam, $return = ''){
                // retrun makul
                $jadwal = $this->em->getRepository('AppBundle:Jadwal')
                    ->findOneBy(array( 
                        'ta'        => $ta,
                        'semester'  => $semester,
                        'hari'      => $hari,
                        'jam'       => $jam
                    ));
                if ( !$jadwal ) {
                    $jadwal = new \AppBundle\Entity\Jadwal();
                    $jadwal->setTa($ta);
                    $jadwal->setSemester($semester);
                    $jadwal->setHari($hari);
                    $jadwal->setJam($jam);
                    $this->em->persist($jadwal);
                    $this->em->flush();
                }
                if ( $return == 'id' ) {
                    return $jadwal->getId();
                } elseif ( $return == 'kelas' ) {
                    return ( null !== $jadwal->getKelas() ) ? $jadwal->getKelas()->getNama() : '';
                } else {
                    return ( null !== $jadwal->getMakul() && null !== $jadwal->getMakul()->getMakul() ) ? $jadwal->getMakul()->getMakul()->getNama() : '';
                }
            }),           
            new \Twig_SimpleFunction( 'formCariMahasiswa', function($action_path = 'cari_mahasiswa', $form_id = 'cariForm', $nim = '', $method = 'post'){ 
                $html = '';
                $html .= '<form action="'.$action_path.'" method="'.$method.'" class="form-horizontal" id="'.$form_id.'">';
                $html .= '<div class="form-group">';
                $html .= '<label class="col-sm-3 control-label required" for="form_nim">NIM</label>';
                $html .= '<div class="col-sm-6"><input name="nim" required="required" class="form-control" type="text" value="'.$nim.'"></div>';
                $html .= '<div class="clear"></div>';
                $html .= '</div><div class="form-group"><div class="col-sm-3"></div><div class="col-sm-6"><button type="submit" class="btn btn-primary btn">Cari</button></div></div></form>';
                $html .= '<div id="'.$form_id.'Msg"></div>';
                return $html;
            }),
            new \Twig_SimpleFunction( 'selectList', function($type, $path, $label = true, $wrap_class = 'form-inline'){ 
                $html = '';
                $html .= '<div class="'.$wrap_class.'">';
                if ( $type == 'angkatan' ) {
                    if ( $label ) {
                        $html .= '<label>Angkatan</label>';
                    }
                    $html .= '<select class="form-control" onchange="this.options[this.selectedIndex].value && (window.location = \''.$this->generator->generate($path, [], UrlGeneratorInterface::ABSOLUTE_URL).'?angkatan=\'+this.options[this.selectedIndex].value);">';
                    $html .= '<option value="">-- PILIH --</option>';
                    for ($i=date('Y'); $i >= 1980; $i--) { 
                        $html .= '<option value="'.$i.'" ';
                        $html .= (!empty($_GET['angkatan']) && $_GET['angkatan'] == $i) ? 'selected' : '';
                        $html .= '>'.$i.'</option>';
                    }
                    $html .= '</select>';
                } elseif ( $type == 'semester' ) {
                    if ( $label ) {
                        $html .= '<label>Semester</label>';
                    }
                    $html .= '<select class="form-control" onchange="this.options[this.selectedIndex].value && (window.location = \''.$this->generator->generate($path, [], UrlGeneratorInterface::ABSOLUTE_URL).'?semester=\'+this.options[this.selectedIndex].value);">';
                    $html .= '<option value="">-- PILIH --</option>';
                    for ($i=1; $i <= 14; $i++) { 
                        $html .= '<option value="'.$i.'" ';
                        $html .= (!empty($_GET['semester']) && $_GET['semester'] == $i) ? 'selected' : '';
                        $html .= '>Semester '.$i.'</option>';
                    }
                    $html .= '</select>';
                } elseif ( $type == 'kelas' ) {
                    if ( $label ) {
                        $html .= '<label>Kelas</label>';
                    }
                    $html .= '<select class="form-control" onchange="this.options[this.selectedIndex].value && (window.location = \''.$this->generator->generate($path, [], UrlGeneratorInterface::ABSOLUTE_URL).'?semester=\'+this.options[this.selectedIndex].value);">';
                    $html .= '<option value="">-- PILIH --</option>';
                    $kelas = $this->em->getRepository('AppBundle:Kelas')
                        ->findAll();
                    if ($kelas) {
                        foreach ($kelas as $kls) { 
                            $html .= '<option value="'.$kls->getId().'" ';
                            $html .= (!empty($_GET['semester']) && $_GET['semester'] == $kls->getId()) ? 'selected' : '';
                            $html .= '>'.$kls->getNama().'</option>';
                        }
                    }
                    $html .= '</select>';
                } elseif ( $type == 'makul_dosen' ) {
                    if ( $label ) {
                        $html .= '<label>Kelas</label>';
                    }
                    $html .= '<select class="form-control" onchange="this.options[this.selectedIndex].value && (window.location = \''.$this->generator->generate($path, [], UrlGeneratorInterface::ABSOLUTE_URL).'?semester=\'+this.options[this.selectedIndex].value);">';
                    $html .= '<option value="">-- PILIH --</option>';
                    $kelas = $this->em->getRepository('AppBundle:Kelas')
                        ->findAll();
                    if ($kelas) {
                        foreach ($kelas as $kls) { 
                            $html .= '<option value="'.$kls->getId().'" ';
                            $html .= (!empty($_GET['semester']) && $_GET['semester'] == $kls->getId()) ? 'selected' : '';
                            $html .= '>'.$kls->getNama().'</option>';
                        }
                    }
                    $html .= '</select>';
                }
                
                $html .= '</div>';
                return $html;
            }),
            new \Twig_SimpleFunction( 'printTerm', function($object, $return = 'nama'){ 
                return $this->printTerm($object, $return);
            }),
            new \Twig_SimpleFunction( 'printTotalPerpusPeminjaman', function($user){ 
                $count = 0;
                $data = $this->em->getRepository('AppBundle:PerpusPeminjaman')
                    ->findBy(array(
                        'anggota'       => $user,
                        'sudahKembali'  => 0,
                    ));
                if ($data) {
                    $count += count($data);
                }
                return $count;
            }),
            new \Twig_SimpleFunction( 'printTotalPerpusPengembalian', function($user){ 
                $count = 0;
                $data = $this->em->getRepository('AppBundle:PerpusPeminjaman')
                    ->findBy(array(
                        'anggota'       => $user,
                        'sudahKembali'  => 1,
                    ));
                if ($data) {
                    $count += count($data);
                }
                return $count;
            }),
            new \Twig_SimpleFunction( 'printIdKrs', function($khs = null, $makul){ 
                // base on makul
                $result = '';
                if ( null !== $khs ) {
                    $data = $this->em->getRepository('AppBundle:Krs')
                        ->findOneBy(array(
                            'khs'       => $khs,
                            'makul'     => $makul,
                        ));
                    if ($data) {
                        $result = $data->getId();
                    }
                }
                return $result;
            }),
            new \Twig_SimpleFunction( 'getBobotNilai', function($nilai, $huruf = false, $lulus = false){ 

                $bobot = $this->em->createQueryBuilder()
                    ->select('b')
                    ->from('AppBundle:BobotNilai', 'b')
                    ->where('b.global = :global AND b.min <= :nilai AND b.max >= :nilai')
                    ->setParameter('global', 1)
                    ->setParameter('nilai', $nilai)
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult();
                if ( !$bobot ) {
                    return '-';
                }
                if ( $lulus ) {
                    return ( $bobot->getLulus() ) ? 'Ya' : 'Tidak';
                }
                if ( $huruf ) {
                    return $bobot->getPredikat();
                }
                return $bobot->getBobot();
            }),
            new \Twig_SimpleFunction( 'getNilaiPerkuliahan', function($mhs, $makul, $aspekNilai, $aspekNilaiDosen = null, $ta = null){ 

                $krs = $this->em->getRepository('AppBundle:Krs')
                    ->findOneBy(array(
                        'mahasiswa' => $mhs,
                        'makul'     => $makul
                    ));
                if ( !$krs ) {
                    return 0;
                }

                if ( $aspekNilai->getNama() == 'Absensi' ) {

                    $presensi = $this->em->getRepository('AppBundle:PresensiMahasiswa')
                        ->findBy(array(
                            // 'tahunAkademik' => $ta,
                            'semester'  => $mhs->getSemester(),
                            'kelas'     => $mhs->getKelas(),
                            'makul'     => $makul,
                            'user'      => $mhs->getUser(),
                            'ket'       => 'hadir'
                        ));
                    $presensiCount = count($presensi);
                    $jmlPertemuan = 14;

                    $final_np = (( ( $presensiCount/$jmlPertemuan )*$aspekNilai->getPersen() )/100)*100;

                    $np = array(
                        $presensiCount,
                        round($final_np, 2), // setelah di persen
                        0
                    );

                } else {

                    $nilai = $this->em->getRepository('AppBundle:NilaiPerkuliahan')
                        ->findOneBy(array(
                            'krs'           => $krs,
                            'aspekNilai'    => $aspekNilai
                        ));

                    if ( !$nilai ) {
                        $nilai = new \AppBundle\Entity\NilaiPerkuliahan();
                        $nilai->setKrs($krs);
                        if ( null !== $aspekNilaiDosen ) {
                            $nilai->setAspekNilaiDosen($aspekNilaiDosen);
                        } else {
                            $nilai->setAspekNilai($aspekNilai);
                        }
                        $nilai->setNilai(0);
                        $this->em->persist($nilai);
                        $this->em->flush();
                    }
                    $final_np = ($nilai->getNilai()*$aspekNilai->getPersen())/100;
                    $np = array(
                        $nilai->getNilai(),
                        round($final_np, 2), // setelah di persen
                        $nilai->getId()
                    );

                }

                return $np; // id, nilai real, nilai persenstasi
            }),
            new \Twig_SimpleFunction( 'getNilaiPerKrs', function($krs = null){ 
                // pemanggilan di KHS
                $result = array();
                $nilai_final = 0;
                if ( null !== $krs ) {
                    $mhs = $krs->getMahasiswa();
                    $makul = $krs->getMakul();
                    $aspekNilaiMakul = $this->em->getRepository('AppBundle:AspekNilai')->findAll();

                    foreach ($aspekNilaiMakul as $aspekNilai) {

                        if ( $aspekNilai->getNama() == 'Absensi' ) {

                            $presensi = $this->em->getRepository('AppBundle:PresensiMahasiswa')
                                ->findBy(array(
                                    // 'tahunAkademik' => $ta,
                                    'semester'  => $mhs->getSemester(),
                                    'kelas'     => $mhs->getKelas(),
                                    'makul'     => $makul,
                                    'user'      => $mhs->getUser(),
                                    'ket'       => 'hadir'
                                ));
                            $presensiCount = count($presensi);
                            $jmlPertemuan = 14;

                            $final_np = (( ( $presensiCount/$jmlPertemuan )*$aspekNilai->getPersen() )/100)*100;

                            $np = array(
                                'aspek' => $aspekNilai,
                                'nilai' => array(
                                    $presensiCount,
                                    round($final_np, 2), // setelah di persen
                                    0
                                )
                            );

                        } else {

                            $nilai = $this->em->getRepository('AppBundle:NilaiPerkuliahan')
                                ->findOneBy(array(
                                    'krs'           => $krs,
                                    'aspekNilai'    => $aspekNilai
                                ));

                            if ( !$nilai ) {
                                $nilai = new \AppBundle\Entity\NilaiPerkuliahan();
                                $nilai->setKrs($krs);
                                if ( null !== $aspekNilaiDosen ) {
                                    $nilai->setAspekNilaiDosen($aspekNilaiDosen);
                                } else {
                                    $nilai->setAspekNilai($aspekNilai);
                                }
                                $nilai->setNilai(0);
                                $this->em->persist($nilai);
                                $this->em->flush();
                            }
                            $final_np = ($nilai->getNilai()*$aspekNilai->getPersen())/100;
                            $np = array(
                                'aspek' => $aspekNilai,
                                'nilai' => array(
                                    $nilai->getNilai(),
                                    round($final_np, 2), // setelah di persen
                                    $nilai->getId()
                                )
                            );

                        }

                        if ( isset($np) ) {
                            $result[] = $np;
                        }

                    }
                }

                // return $np; // id, nilai real, nilai persenstasi
                return $result;
            }),
            new \Twig_SimpleFunction( 'printNilaiAkhir', function($idKrs, $makul){ 

                $nilai = '-';

                if ( null !== $makul ) {
                    $krs = $this->em->getRepository('AppBundle:Krs')
                        ->find($idKrs);
                    if ( $krs && $krs->getMakul() == $makul ) {
                        $nilai = $krs->getNilaiAkhir();
                    }
                }
                return $nilai;
            }),
            new \Twig_SimpleFunction( 'printOrangtua', function($mhsObject, $orangtua, $field){ 
                if ( null !== $mhsObject->getOrangtua() ) {
                    $ot = $mhsObject->getOrangtua()[$orangtua];
                    if ( isset($ot[$field]) ) {
                        if ($field == 'agama' || $field == 'pekerjaan' || $field == 'penghasilan' || $field == 'kota' || $field == 'provinsi') {
                            return $this->printTerm($ot[$field]);
                        } else {
                            return $ot[$field];
                        }
                    }
                }
                return '';
            }),
            new \Twig_SimpleFunction( 'printJurusan', function($object = null){ 
                if ( null !== $object ) {
                    return $object->getNama();
                }
                return '';
            }),
            new \Twig_SimpleFunction( 'getKelas', function($id){ 
                $kelas = $this->em->getRepository('AppBundle:Kelas')
                    ->find($id);
                if ($kelas) {
                    return $kelas;
                }
                return '';
            }),
        );
    }

    private function getIps($mahasiswa, $semester = null) {
        $ips = 0;
        $jmlNilai = 0;
        $jmlSks = 0;
        $makuls = array();
        if ( null !== $mahasiswa ) {

            if ( null != $mahasiswa->getSemester() ) {

                $semester = ( null !== $semester ) ? $semester : $mahasiswa->getSemester()->getSemester();

                $khs = $this->em->getRepository('AppBundle:Khs')
                    ->findOneBy(array(
                        'mahasiswa' => $mahasiswa,
                        'semester'  => $semester,
                    ));
                if ( $khs ) {
                    if ( count($khs->getKrs()) > 0 ) {
                        foreach ($khs->getKrs() as $krs) {
                            if ( null !== $krs->getMakul() ) {
                                $makul = $krs->getMakul();
                                // $makuls[] = $makul;
                                if ( $makul->getSemester() == $semester && null !== $krs->getNilaiAkhir() ) {

                                    if ( $makul->getSksTeori() > 0 ) {
                                        $jmlSks += $makul->getSksTeori();
                                    }
                                    if ( $makul->getSksPraktek() > 0 ) {
                                        $jmlSks += $makul->getSksPraktek();
                                    }
                                    if ( $makul->getSksLapangan() > 0 ) {
                                        $jmlSks += $makul->getSksLapangan();
                                    }

                                    if ( null !== $krs->getNilaiAkhir() ) {
                                                                            
                                        $nilaiAkhir = ($krs->getNilaiAkhir());
                                        if ( $nilaiAkhir > 0 ) {
                                            $jmlNilai = $nilaiAkhir;
                                            $bobot = $this->getBobotNilai($nilaiAkhir, $mahasiswa->getUser()->getProdi());
                                            if ( $bobot['bobot'] > 0 ) {
                                                $jmlNilai += $jmlSks*$bobot['bobot'];
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            } 

        }
        if ( $jmlNilai > 0 && $jmlSks > 0 ) {
            $ips = ($jmlNilai/$jmlSks);
        }
        return $ips;
    }

    private function printTerm($object = null, $return = 'nama') {
        if ( null !== $object ) {
            if ( $return == 'nama' ) {
                return $object->getNama();
            } else {
                return $object->getKode();
            }
        }
        return '';
    }
  
    private function getBobotNilai($nilai, $prodi) {
        $result = array(
            'predikat' => '',
            'bobot' => 0,
            'lulus' => 0,
        );
        if ( $nilai > 0 ) {

            $bobotNilai = $this->em->getRepository('AppBundle:BobotNilai')
                ->findByProdi($prodi);
            foreach ($bobotNilai as $bobot) {
                if ( $nilai >= $bobot->getMin() && $nilai <= $bobot->getMax() ) {
                    $result = array(
                        'predikat'  => $bobot->getPredikat(),
                        'bobot'     => $bobot->getBobot(),
                        'lulus'     => $bobot->getLulus(),
                    );
                    break;
                }
            }

        }
        return $result;
    }

}