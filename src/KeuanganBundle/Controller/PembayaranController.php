<?php

namespace KeuanganBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Inventaris;
use AppBundle\Entity\Master;
use AppBundle\Entity\Lokasi;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PembayaranController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/keuangan/transaksi_pembayaran", name="transaksi_pembayaran")
     */
    public function indexAction(Request $request, $aksi = 'index')
    {
        if($aksi == 'edit') {
          if (!empty($request->get('id'))) {
              $data = $this->getDoctrine()->getRepository('AppBundle:Master')
                ->find($request->get('id'));
          } else {
              $data = new Master();
              $data->setType('dispensasi_pembayaran');
          }
          if (!$data) {
              throw $this->createNotFoundException();
          }
	        $form = $this->createFormBuilder($data)
	            ->add('nama', null, array(
	            	'label'	=> 'Nama Rekomendasi'
	            ))
	            ->add('custom1', null, array(
	            	'label'	=> 'Sampai Semester'
	            ))
	            ->add('custom2', IntegerType::class, array(
	            	'label'	=> 'Diskon'
	            ))
	            ->add('submit', SubmitType::class, array(
	                'label' => 'Simpan',
	                'attr'  => array(
	                    'class' => 'btn btn-primary'
	                )
	            ))
	            ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('dispensasi_pembayaran');
          }
        } else {
           $data = $this->getDoctrine()->getRepository('AppBundle:PembayaranBiayaKuliah')
              ->findAll();
        }
        $params = array(
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : null
        );
        if ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) {
            $dataTransaksiPembayaran = $this->getDoctrine()->getRepository('AppBundle:PembayaranTransaksi')
                ->findByMahasiswa($this->getUser()->getDataMahasiswa());   
            if ( $dataTransaksiPembayaran ) {
              $params['dataTransaksiPembayaran'] = $dataTransaksiPembayaran;
            }
        }
        return $this->appService->load('KeuanganBundle:Default:transaksi_pembayaran_'.$aksi.'.html.twig', $params);
    }


    /**
     * @Route("/keuangan/konfirmasi", name="konfirmasi_pembayaran")
     */
    public function konfirmasiIndexAction(Request $request, $aksi = 'index')
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) {

          $upload = new \AppBundle\Entity\PembayaranTransaksi();
          $form = $this->createFormBuilder($upload)
              ->add('imageFile', VichFileType::class, [
                  'label'   => 'File Bukti',
                  'required' => false,
              ])
              ->add('biayaKuliah', EntityType::class, array(
                  'class' => 'AppBundle:PembayaranBiayaKuliah',
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('p')
                          ->where('p.semester=:semester')
                          ->setParameter('semester', $this->getUser()->getDataMahasiswa()->getSemester() );
                  },
                  'choice_label' => 'nama',
                  'placeholder'   => '-- Pilih --',
              ))
              ->add('jumlah')
              ->add('submit', SubmitType::class, array(
                  'label' => 'Upload',
                  'attr'  => array(
                      'class' => 'btn btn-primary'
                  )
              ))
              ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $now = new\DateTime();
              $upload->setTanggal($now);
              $upload->setMahasiswa($this->getUser()->getDataMahasiswa());
              $em->persist($upload);
              $em->flush();
              $this->addFlash('success', 'File berhasil diupload.');
              return $this->redirectToRoute('konfirmasi_pembayaran');
          }
        } else {

        }
        $data = $this->getDoctrine()->getRepository('AppBundle:PembayaranTransaksi')
          ->findAll();
        return $this->appService->load('KeuanganBundle:Default:konfirmasi_pembayaran_index.html.twig', [
            'data'  => $data,
            'form'  => isset($form) ? $form->createView() : null
        ]);
    }


    /**
     * @Route("/keuangan/laporan", name="laporan_pembayaran")
     */
    public function laporanIndexAction(Request $request, $aksi = 'index')
    {
        if($aksi == 'edit') {
          if (!empty($request->get('id'))) {
              $data = $this->getDoctrine()->getRepository('AppBundle:Master')
                ->find($request->get('id'));
          } else {
              $data = new Master();
              $data->setType('dispensasi_pembayaran');
          }
          if (!$data) {
              throw $this->createNotFoundException();
          }
          $form = $this->createFormBuilder($data)
              ->add('nama', null, array(
                'label' => 'Nama Rekomendasi'
              ))
              ->add('custom1', null, array(
                'label' => 'Sampai Semester'
              ))
              ->add('custom2', IntegerType::class, array(
                'label' => 'Diskon'
              ))
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class' => 'btn btn-primary'
                  )
              ))
              ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('dispensasi_pembayaran');
          }
        } else {
           $data = $this->getDoctrine()->getRepository(Master::class)->findByType('dispensasi_pembayaran');
        }
        return $this->appService->load('KeuanganBundle:Default:laporan_pembayaran_'.$aksi.'.html.twig', [
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : null
        ]);
    }
}
