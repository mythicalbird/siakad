<?php

namespace PmbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\PmbPeriode;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Wilayah;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\Type\MabaFormType;
use AppBundle\Service\AppService;
use Port\Excel\ExcelWriter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MabaController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/pmb/maba/calon", name="calon_maba_index")
     */
    public function indexAction(Request $request)
    {
        $periodeList = $this->getDoctrine()->getRepository('AppBundle:PmbPeriode')
          ->findByProdi($this->getUser()->getProdi());
        if ( !empty($request->get('periode')) ) {
          $periode = $this->getDoctrine()->getRepository('AppBundle:PmbPeriode')
            ->find($request->get('periode'));
        } 
        return $this->appService->load('PmbBundle:Default:maba_index.html.twig', array(
            'periode'       => ( isset($periode) && false !== $periode ) ? $periode : null,
            'periodeList'   => $periodeList
        ));
    }

    /**
     * @Route("/pmb/maba/tambah", name="calon_maba_tambah")
     */
    public function tambahAction(Request $request)
    {
        $periode = $this->getDoctrine()
            ->getRepository('AppBundle:PmbPeriode')
            ->findOneBy(array(
                'status'  => 1,
                'tahun'   => date('Y')
            ));

        if (!$periode) {
            throw $this->createNotFoundException(
                'Tidak ada periode PMB yang terbuka saat ini!'
            );
        }

        $asalNamaPtChoices = array();
        $asalJenjangChoices = array();
        $asalProdiChoices = array();
        $asalNamaPtObjects = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findByType('asal_perguruan_tinggi');
        if ( $asalNamaPtObjects ) {
          foreach ($asalNamaPtObjects as $asalNamaPt) {
            $asalNamaPtChoices[$asalNamaPt->getNama()] = $asalNamaPt->getNama();
          }
        }
        $asalJenjangObjects = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findByType('jenjang_pendidikan');
        if ( $asalJenjangObjects ) {
          foreach ($asalJenjangObjects as $asalJenjang) {
            $asalJenjangChoices[$asalJenjang->getNama()] = $asalJenjang->getNama();
          }
        }
        $asalProdiObjects = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findByType('asal_prodi');
        if ( $asalProdiObjects ) {
          foreach ($asalProdiObjects as $asalProdi) {
            $asalProdiChoices[$asalProdi->getNama()] = $asalProdi->getNama();
          }
        }
        $option = $this->appService->getGlobalOption();
        $ta = $option['ta'];

        $data = array();
        $builder = $this->createFormBuilder($data);
        $builder
          ->add('periode', EntityType::class, array(
              'class' => 'AppBundle:PmbPeriode',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('p');
              },
              'choice_label' => function(PmbPeriode $entity = null) {
                  return 'Periode ' . $entity->getTahun() . ' - ' . $entity->getProdi()->getNamaProdi();
              },
              'placeholder' => '-- Pilih --',
          ))
          ->add('jalur', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'jalur_pmb');
              },
              'choice_label' => 'nama',
              'placeholder' => '-- Pilih --',
          ))
          ->add('tptLahir', null, array(
              'label' => 'Tempat Lahir'
          ))
          ->add('tglLahir', DateType::class, array(
              'label' => 'Tanggal Lahir',
              'widget'    => 'single_text',
              'html5'     => false, 
              'format'    => 'dd-MM-yyyy',
              'attr'      => ['class' => 'js-datepicker'],
          ))
          ->add('nama', null, array(
              'label' => 'Nama Mahasiswa'
          ))
          ->add('ktp', null, array(
              'label' => 'No KTP'
          ))
          ->add('noKk', null, array(
              'label' => 'No KK'
          ))
          ->add('npwp', null, array(
              'label' => 'No NPWP'
          ))
          ->add('jk', ChoiceType::class, array(
              'label' => 'Jenis Kelamin',
              'choices'   => array(
                  'Laki-laki' => 'L',
                  'Perempuan' => 'P'
              ),
              'placeholder'   => '-- Pilih --' 
          ))
          ->add('agama', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'agama');
              },
              'choice_label' => 'nama',
              'placeholder' => '-- Pilih --',
          ))
          ->add('wargaNegara', ChoiceType::class, array(
            'label'   => 'Warga Negara',
            'choices' => array(
              'WNI' => 'ID',
              'WNA' => 'WNA',
            ),
          ))
          ->add('statusSipil', ChoiceType::class, array(
            'label'         => 'Status Sipil',
            'choices'       => array(
              'Bujangan'  => 'bujangan',
              'Duda'      => 'duda',
              'Janda'     => 'janda',
              'Menikah'   => 'menikah',
            ),
            'placeholder'   => '-- Pilih --'
          ))
          ->add('telp', null, array(
            'required'  => true,
            'label'   => 'No. Telp/HP',
            'attr'  => array(),
          ))
          ->add('email', null, array(
            'required'  => true,
          ))
          ->add('statusMasuk', ChoiceType::class, array(
            'required'      => false,
            'label'        => 'Status Masuk',
            'choices' => array(
              'BARU'  => 'BARU',
              'PINDAHAN'  => 'PINDAHAN',
            ),
            'placeholder'   => '-- Pilih --'
          ))
          ->add('kodeSekolah', null, array(
              'required'      => false,
              'label'         => 'Kode Sekolah',
          ))
          ->add('namaSekolah', null, array(
              'required'        => false,
              'label'          => 'Nama Sekolah',
          ))
          ->add('nis', null, array(
              'required'      => false,
              'label' => 'NIS',
          ))
          ->add('nilaiUn', null, array(
              'required'      => false,
              'label' => 'Nilai UN',
          ))
          ->add('asalKodePt', null, array(
              'required'      => false,
              'label' => 'Kode Perguruan Tinggi',
          ))
          ->add('asalNamaPt', ChoiceType::class, array(
              'required'      => false,
              'label'         => 'Nama Perguruan Tinggi',
              'choices'       => $asalNamaPtChoices,
              'placeholder' => '-- Pilih --',
          ))
          ->add('asalJenjang', ChoiceType::class, array(
              'required'      => false,
              'label' => 'Jenjang',
              'choices'       => $asalJenjangChoices,
              'placeholder' => '-- Pilih --',
          ))
          ->add('asalProdi', ChoiceType::class, array(
              'required'      => false,
              'label'         => 'Program Studi',
              'choices'       => $asalProdiChoices,
              'placeholder'   => '-- Pilih --',
          ))
          ->add('asalNim', null, array(
              'required'      => false,
              'label' => 'NIM Asal',
          ))
          ->add('asalSksDiakui', null, array(
              'required'      => false,
              'label' => 'SKS Diakui',
          )) 
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
        ;

        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
          $data = $form->getData();
          $data['alamat'] = '';
          $data['rt'] = '';
          $data['rw'] = '';
          $data['dusun'] = '';
          $data['desa'] = '';
          $data['pos'] = '';
          $data['maba'] = 1;
          $data['riwayatPendidikan'] = null;
          // $data['alamat'] = '';
          // $data['alamat'] = '';
          // $data['alamat'] = '';
          // $data['alamat'] = '';
          $ta = $this->appService->getTahunAkademik();
          $maba = $this->appService->newMahasiswa($data, $ta);
          // $mhs = $this->appService->tambahMahasiswa($data, $pmb);

          $this->addFlash('success', 'Calon Maba berhasil ditambah.');
          return $this->redirectToRoute('calon_maba_edit', array('id' => $maba->getId()));
        }
        return $this->appService->load('PmbBundle:Default:maba_tambah.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/pmb/maba/edit/{id}", name="calon_maba_edit")
     */
    public function editAction(Request $request, $id)
    {

        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->find($id);
        $builder = $this->createFormBuilder($data);
        $builder
          ->add('ktp', null, array(
              'label' => 'No KTP'
          ))
          ->add('noKk', null, array(
              'label' => 'No KK'
          ))
          ->add('npwp', null, array(
              'label' => 'NPWP'
          ))
          ->add('tptLahir', null, array(
              'label' => 'Tempat Lahir'
          ))
          ->add('tglLahir', DateType::class, array(
              'label' => 'Tanggal Lahir',
              'widget'    => 'single_text',
              'html5'     => false,
              'format'    => 'dd-MM-yyyy',
              'attr'      => ['class' => 'js-datepicker'],
          ))
          ->add('nama', null, array(
              'label' => 'Nama Mahasiswa'
          ))
          ->add('jk', ChoiceType::class, array(
              'label' => 'Jenis Kelamin',
              'choices'   => array(
                  'Laki-laki' => 'L',
                  'Perempuan' => 'P'
              ),
              'placeholder'   => '-- Pilih --' 
          ))
          ->add('agama', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'agama');
              },
              'choice_label' => 'nama',
              'placeholder' => '-- Pilih --',
          ))
          ->add('wargaNegara', ChoiceType::class, array(
            'label'   => 'Warga Negara',
            'choices' => array(
              'WNI' => 'ID',
              'WNA' => 'WNA',
            ),
            'placeholder'   => '-- Pilih --'
          ))

          ->add('telp', null, array(
            'required'  => true,
            'label'   => 'No. Telp/HP',
            'attr'  => array(),
          ))
          ->add('email', null, array(
            'required'  => true,
          ))
          ->add('alamat', null, array(
              'required'  => true,
          ))
          ->add('rt', null, array(
              'label' => 'RT',
          ))
          ->add('rw', null, array(
              'label' => 'RW',
          ))
          ->add('dusun')
          ->add('desa')
          ->add('wilayah', EntityType::class, array(
              'label' => 'Kec/Kab/Pro',
              'class' => 'AppBundle:Wilayah',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('w')
                      ->where('w.level= :level')
                      ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 3));
              },
              'choice_label' => function( Wilayah $entity = null ){
                $choice_label = $entity->getNama();
                if ( null !== $entity->getParent() ) {
                  $parent = $entity->getParent();
                  $choice_label .= ', ' . $parent->getNama();
                  if ( null !== $parent->getParent() ) {
                    $choice_label .= ', ' . $parent->getParent()->getNama();
                  }
                }
                return $choice_label;
              },
              'attr'  => array(
                'class' => 'select2',
                'style' => 'width:100%'
              ),
          ))         
          ->add('pos', null, array(
              'label' => 'Kodepos',
          ))
          ->add('statusSipil', ChoiceType::class, array(
            'label'         => 'Status Sipil',
            'choices'       => array(
              'Bujangan'  => 'bujangan',
              'Duda'  => 'duda',
              'Janda' => 'janda',
              'Menikah' => 'menikah',
            ),
            'placeholder'   => '-- Pilih --'
          ))
          ->add('prodi', EntityType::class, array(
              'class' => 'AppBundle:ProgramStudi',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('p');
              },
              'choice_label' => 'namaProdi',
              'placeholder' => '-- Pilih --',
          ))
          ->add('dataMahasiswa', MabaFormType::class)

        ;

        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!empty($request->get('aksi')) && $request->get('aksi') == "tambah") {
              $data->setPassword($data->getUsername());
            }
            $mahasiswa= $data->getDataMahasiswa();
            if ( null !== $mahasiswa->getBerkas() ) {
              foreach ($mahasiswa->getBerkas() as $berkas) {
                $em->persist($berkas);
              }
            }
            $em->persist($mahasiswa);
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('calon_maba_index', array(
              'periode' => $mahasiswa->getData()['pmb']['periode']->getId()
            ));
        }
        return $this->appService->load('PmbBundle:Default:maba_edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/pmb/maba_cetak", name="calon_maba_cetak")
     */
    public function mabaCetakAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
          ->findAll();
        return $this->appService->load('cetak/maba_data.html.twig', array(
          'data' => $data
        ));
    }

    /**
     * @Route("/pmb/maba_export", name="calon_maba_export")
     * @Method({"POST"})
     */
    public function mabaExportAction(Request $request)
    {

        $response = new JsonResponse();

        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findBy(array(
              'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4)
          ));

        $namaFile = 'data_maba.xlsx';
        $filePath = 'uploads/' . $namaFile;
        $path = $this->get('kernel')->getProjectDir() . '/web/' . $filePath;
        // var_dump($path); exit;
        $file = new \SplFileObject( $path, 'w');
        $writer = new ExcelWriter($file, date('Y-m-d') . 'Data Maba');

        $writer->prepare();
        $writer->writeItem(['No', 'ID MABA', 'Nama Lengkap', 'Tempat, Tanggal Lahir', 'Jenis Kelamin', 'Agama', 'Asal Daerah', 'Nama Orang Tua', 'Alamat Orang Tua', 'Pekerjaan Orang Tua', 'Asal Sekolah', 'NIS Asal', 'Nilai UN', 'Asal PT', 'Asal Prodi', 'NIM Asal', 'SKS Diakui']);
        
        $no = 1;
        foreach ($data as $user) {
          if ( null !== $user->getDataMahasiswa() ) {
            $mhs = $user->getDataMahasiswa();
            if ( $mhs->getMaba() == 1 ) {

              $ttl = $user->getTptLahir();
              $tglLahir = ( null !== $user->getTglLahir() ) ? $user->getTglLahir() : '';
              $ttl .= ( $tglLahir != '' ) ? ', ' . $tglLahir->format('d/m/Y') : '';
              $writer->writeItem([
                'No'                        => $no, 
                'ID MABA'                   => $user->getId(),
                'Nama Lengkap'              => $user->getNama(),
                'Tempat, Tanggal Lahir'     => $ttl,
                'Jenis Kelamin'             => $user->getJk(),
                'Agama'                     => ( null !== $user->getAgama() ) ? $user->getAgama()->getNama() : '',
                'Asal Daerah'               => $user->getTptLahir(),
                'Nama Orang Tua'            => '',
                'Alamat Orang Tua'          => '',
                'Pekerjaan Orang Tua'       => '',
                'Asal Sekolah'              => $mhs->getNamaSekolah(),
                'NIS Asal'                  => $mhs->getNis(),
                'Nilai UN'                  => $mhs->getNilaiUn(),
                'Asal PT'                   => $mhs->getAsalNamaPt(),
                'Asal Prodi'                => $mhs->getAsalProdi(),
                'NIM Asal'                  => $mhs->getAsalNim(),
                'SKS Diakui'                => $mhs->getAsalSksDiakui(),
              ]);
              $no++;

            }
          }
        }

        $writer->finish();

        $manager = $this->get('assets.packages');
        $url = $manager->getUrl($filePath);
        $response->setData($url);
        return $response;
    }
  
    /**
     * @Route("/pmb/nilai/usm", name="nilai_usm_index")
     */
    public function nilaiUsmIndexAction(Request $request)
    {
        $periodeList = $this->getDoctrine()->getRepository('AppBundle:PmbPeriode')
          ->findAll();
        if ( !empty($request->get('periode')) ) {
          $periode = $this->getDoctrine()->getRepository('AppBundle:PmbPeriode')
            ->find($request->get('periode'));
        } 
        return $this->appService->load('PmbBundle:Default:maba_nilai_usm_index.html.twig', array(
            'periode'       => ( isset($periode) && false !== $periode ) ? $periode : null,
            'periodeList'   => $periodeList,
        ));
    }
  
    /**
     * @Route("/pmb/maba/her", name="her_maba_index")
     */
    public function herIndexAction(Request $request)
    {
        $periodeList = $this->getDoctrine()->getRepository('AppBundle:PmbPeriode')
          ->findAll();
        if ( !empty($request->get('periode')) ) {
          $periode = $this->getDoctrine()->getRepository('AppBundle:PmbPeriode')
            ->find($request->get('periode'));
        } 
        $role = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findOneBy(['type' => 'hak_akses', 'nama' => 'MAHASISWA']);
        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findByHakAkses($role);
        $form = $this->createFormBuilder()
            ->add('ids', CollectionType::class, array(
                'label'         => false,
                // each entry in the array will be an "email" field
                'entry_type'    => CheckboxType::class,
                // these options are passed to each "email" type
                'entry_options' => array(
                    'attr' => array('class' => 'email-box'),
                ),
                'allow_add'     => true,
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'PROSES PENERIMAAN',
                'attr'  => array(
                    'class'   => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
          $post = $form->getData();
          $em = $this->getDoctrine()->getManager();
          foreach(array_keys($post['ids']) as $id) {
            $mhs = $em->getRepository('AppBundle:Mahasiswa')
              ->find($id);
            if( $mhs ) {
              $user = $mhs->getUser();
              $user->setTglMasuk( new\DateTime() );
              $em->persist($user);
              $mhs->setStatus( $this->appService->getMasterTermObject('status_mahasiswa', 'A') );
              $mhs->setMaba(0);
              $mhs->setSemester(1);
              // $semesterMasuk = $this->getDoctrine()->getRepository('AppBundle:Semester')
              //   ->findOneByKode( $mhs->getPeriode()->getTahun().'1' );
              // $mhs->setSemesterMasuk($semesterMasuk);
              $em->persist($mhs);
              $em->flush();
            }
          }
          $this->addFlash('success', 'Data berhasil disimpan.');
          return $this->redirectToRoute('her_maba_index');
        }
        return $this->appService->load('PmbBundle:Default:maba_her_index.html.twig', array(
            'data'	          => $data,
            'form'            => $form->createView(),
            'periodeList'   => $periodeList,
            'periode'       => ( isset($periode) && false !== $periode ) ? $periode : null,
        ));
    }

    /**
     * @Route("/pmb/maba/cetak", name="cetak_maba")
     */
    public function cetakAction(Request $request)
    {
        $role = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findOneBy(['type' => 'hak_akses', 'nama' => 'MAHASISWA']);
        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findByHakAkses($role);
        return $this->appService->load('cetak/maba_data.html.twig', array(
            'data'            => $data,
        ));
    }

}
