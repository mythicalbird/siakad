<?php

namespace PmbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class DefaultController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/pmb/periode/{aksi}", name="periode_pmb_index")
     */
    public function indexAction(Request $request, $aksi = 'index')
    {
        if ($aksi == 'edit') {

          if ( !empty($request->get('id')) ) {
              $data = $this->getDoctrine()->getRepository('AppBundle:PmbPeriode')
                ->find($request->get('id'));
          } else {
              $data = new \AppBundle\Entity\PmbPeriode();
              $data->setStatus('inactive');
          }
          $form = $this->createFormBuilder($data)
              ->add('prodi', EntityType::class, array(
                'label'     => 'Program Studi',
                'class'     => 'AppBundle:ProgramStudi',
                'query_builder' => function(EntityRepository $er) {
                          return $er->createQueryBuilder('p');
                },
                'choice_label' => 'namaProdi',
                'placeholder' => '-- Pilih --',
              ))
              ->add('tahun', ChoiceType::class, array(
                'label'     => 'Tahun Akademik',
                // 'class'     => 'AppBundle:TahunAkademik',
                // 'query_builder' => function(EntityRepository $er) {
                //           return $er->createQueryBuilder('t')
                //               ->where('t.status!=:status')
                //               ->setParameter('status', 'trash');
                // },
                // 'choice_label' => 'tahun',
                'choices' => $this->appService->getTahunChoices(),
                'placeholder' => '-- Pilih --',
              ))
              ->add('kapasitas')
              ->add('biayaPendaftaran')
              ->add('nilaiMinimal', null, array(
                  'label'   => 'Nilai Minimal Lulus USM',
              ))
              ->add('tglPendaftaran', null, array(
                   'label'  => 'Pendaftaran',
                   'attr' => array(
                      'class'   => 'drpick'
                   )
              ))
              ->add('tglUsm', null, array(
                   'label'  => 'Ujian Saringan Masuk',
                   'attr' => array(
                      'class'   => 'drpick'
                   )
              ))
              ->add('tglHer', null, array(
                   'label'  => 'Pembayaran & Pendaftaran Ulang',
                   'attr' => array(
                      'class'   => 'drpick'
                   )
              ))
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                   'attr' => array(
                      'class'   => 'btn btn-primary'
                   )
              ))
              ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('periode_pmb_index');
          }

        } else {

            $data = $this->getDoctrine()->getRepository('AppBundle:PmbPeriode')
                  ->findAll();

        }
        return $this->appService->load('PmbBundle:Default:periode_pmb_'.$aksi.'.html.twig', array(
        	'data'	=> $data,
          'form'  => ( isset($form) ) ? $form->createView() : ''
        ));
    }

    /**
     * @Route("/_ajax/edit/status_pmb", name="status_pmb_update")
     * @Method({"POST"})
     */
    public function statusPmbEditAction(Request $request)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:PmbPeriode')
          ->find($request->get('id'));
        if ( $data ) {
            $status = ( $data->getStatus() == 'inactive' ) ? 'active' : 'inactive';
            $data->setStatus($status);
            $em->persist($data);
            $em->flush();
            $response->setData(array(
              'status' => $status
            ));
        }
        return $response;
    }
}
