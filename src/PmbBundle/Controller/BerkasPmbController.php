<?php

namespace PmbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\PmbPeriode;
use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class BerkasPmbController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/pmb/berkas_upload/{aksi}", name="master_berkas_upload")
     */
    public function indexAction(Request $request, $aksi = 'index')
    {
        if ($aksi == "edit") {
          if (!empty($request->get('id'))) {
              $data = $this->getDoctrine()->getRepository('AppBundle:Master')
                ->find($request->get('id'));
          } else {
              $data = new Master(); 
              $data->setType('berkas_pmb');
          }
          $form = $this->createFormBuilder($data)
              ->add('nama', null, array(
                  'label'   => 'Nama Berkas PMB',
              ))
              ->add('ket', TextareaType::class, array(
                  'required'  => false,
                  'label'   => 'Keterangan',
              ))
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class'   => 'btn btn-primary'
                  )
              ))
              ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              if ( empty($request->get('id')) ) {
                $kode = $this->appService->slugify($data->getNama());
                $data->setKode( $kode );
              }
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('master_berkas_upload');
          }
        } else {
          $data = $this->getDoctrine()->getRepository(Master::class)
            ->findByType('berkas_pmb');
        }
        return $this->appService->load('PmbBundle:Default:berkas_pmb_'.$aksi.'.html.twig', array(
          'data'  => $data,
          'form'  => ( isset($form) ) ? $form->createView() : ''
        ));
    }
}
